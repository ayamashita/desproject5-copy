<%@taglib uri='/WEB-INF/cewolf.tld' prefix='cewolf' %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
		
<%@ include file="head.jsp" %>



<table border="0" width="100%" bgcolor="#F5F5F5">
<tr>
	<td><img class="blockOnPrint" src="" width="10" height="0"></td>
	<td class="path"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listStudies.do">Studies</a> > Aggregated study information</td>
</tr>
<tr>
	<td></td>
	<td><h1>Aggregated study information</h1></td>
</tr>
<tr>
<td></td>
<td align="center" valign="top">
<jsp:useBean id="pageViews" class="no.simula.des.beans.AggregatedStudiesBean"/>
<cewolf:chart 
    id="line" 
    title="" 
    type="verticalbar3d" 
    xaxislabel="Year" 
    yaxislabel="Studies">
    <cewolf:colorpaint color="#F5F5F5"/>
    <cewolf:data>
        <cewolf:producer id="pageViews"/>
    </cewolf:data>
</cewolf:chart>
<p>
<cewolf:img chartid="line" renderer="cewolf" width="600" height="250"/>
<P>
</td>
</tr>
<tr>
	<td></td>
	<td class="bodytext">
		Figure: Number of Studies by Type of Studies Per Year	
	</td>
</tr>

<tr>
	<td></td>
	<td>&nbsp;
	</td>
</tr>

</table>
 <%@ include file="footer.jsp" %>