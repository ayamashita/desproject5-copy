<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Delete study material</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
			<td><img src="" width="0" height="30"></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Confirm</font></td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td><img src="" width="0" height="135"></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td class="h2"><b>You are about to delete</b></td>
			   			</tr>
			   			<tr>
								<td class="bodytext-bold">Study material:</td>
			   			</tr>
						<tr bgcolor="white">
								<td class="bodytext"><bean:write name="studyForm" property='<%="studyMaterial["+request.getParameter("studyMaterialId")+"].description"%>'/></td>
			   			</tr>
						<tr>
								<td class="bodytext-bold">From study:</td>
			   		</tr>
			   		<tr bgcolor="white">
								<td class="bodytext"><bean:write name="studyForm" property="name"/></td>
						</tr>
						
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
			   				<td><img src="" width="0" height="30"></td>
								<td>&nbsp;&nbsp;</td>
						   	<td align="center" class="bodytext-bold">
						   		<%-- <a href="deleteStudyMaterial.do?studyMaterialId=<bean:write name="studyForm" property='<%="studyMaterial["+request.getParameter("studyMaterialId")+"].study_material_id"%>'/>&studyMaterialIndex=<%=request.getParameter("studyMaterialId")%>&submit=delete" >Submit</a> --%>
						   		<a href="deleteStudyMaterial.do?studyMaterialId=<%=request.getParameter("studyMaterialId")%>&submit=delete" >Submit</a>
						   		&nbsp;&nbsp;
						   		<a href="Javascript:window.self.close()">Cancel</a></td>
								<td>&nbsp;&nbsp;</td>
   		</tr>
   	</table>
   </body>
</html>
