<html>
<head>
<title>[ simula . research laboratory ]</title>
<link rel="stylesheet" href="http://www.simula.no/simula.css" media="screen">
<link rel="stylesheet" href="http://www.simula.no/simula.css" media="print">
<link rel="stylesheet" href="<%=request.getContextPath()%>/des.css" media="screen">
<link rel="stylesheet" href="<%=request.getContextPath()%>/des_print.css" media="print">


<script language='javascript' type='text/javascript' src='<%=request.getContextPath()%>/desScript.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0"> 
<style>


</style>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" class="bodyBackground" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c">
<div id="top" <logic:present name="user">style="background: #ef1607;"</logic:present>>
	<table cellspacing="0" cellpadding="0" border="0" class="blockOnPrint">
		<tr class="blockOnPrint">
			<td valign="bottom" class="h1"><logic:present name="user"><font color="#ADAEAD" size="6">ADMIN TOOL</font><br></logic:present><img src="http://www.simula.no/grafikk/logo-part1.png" alt=""/><img src="http://www.simula.no/grafikk/logo-part2.png" alt=""/></td>
			<td style="width: 100%;">
				&nbsp;
			</td>
			<td class="search" align="right">
				<form name="simple_search" method="post" action="http://www.simula.no/search.php">
					<input type="text" class="searchField" name="search_text" />
					<input type="hidden" name="search_advance" value="n">
					<img src="images/topright-search-normal.png"  onClick="performSimulaSearch()"/>
				</form>
				<a href="http://www.simula.no/search.php" class="advanced"></a>
			</td>
		<tr>
	</table>
</div>

<div id="navBar">
	<div id="menu">
		<a href="http://www.simula.no"class="firsta">home</a>
		<a href="http://www.simula.no/news.php" class="2a">news</a>
		<a href="http://www.simula.no/publication.php" class="3a">publications</a>
		<a href="http://www.simula.no/project.php" class="4a">projects</a>
		<a href="/des/listStudies.do" class="5a">studies</a>
		<a href="http://www.simula.no/department.php" class="6a">research</a>
		<a href="http://www.simula.no/innovation.php" class="6a">innovation</a>
		<a href="http://www.simula.no/people.php" class="7a">people</a>
		<a href="http://www.simula.no/opportunity.php" class="8a">opportunities</a>
	</div>
	<div id="intranet">
		<a href="https://www.simula.no/intranet/">intranet</a>
	</div>
	<div id="login">
		<logic:notPresent name="user">
			<a href="Javascript:openPopup('logonPopup.do','logonpopup');">login</a> 
		</logic:notPresent>
		<logic:present name="user">
			<a href="logout.do">logout</a>
		</logic:present>
	</div>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr bgcolor="#F5F5F5" class="blockOnPrint">
  	<td class="blockOnPrint">&nbsp;
  	</td>
  	<td align="right" class="bodytext-bold">
  		<logic:present name="user">
  			<bean:write name="user" property="first_name"/>&nbsp;<bean:write name="user" property="family_name"/>&nbsp;
  		|&nbsp;<a href="https://www.simula.no/user/change_information.php">Change personal info</a>&nbsp;
  		|&nbsp;<a href="https://www.simula.no/user/change_password.php">Change password</a>&nbsp;
  			<logic:greaterThan name="user" property="privilege" value="1">
  				|&nbsp;<a href="Javascript:openPopupGeneral('grantPrivileges.do','GrantPrivileges',410,800,'yes')">grant admin privilges</a>
  			</logic:greaterThan>
  		</logic:present>
  		<img src="" width="10", height="0" border="0">
  	</td>
  </tr>
</table>
