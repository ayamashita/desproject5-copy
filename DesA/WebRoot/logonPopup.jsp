<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Logon</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Expires" content="0"> 
  </head>
	
  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<html:form action="authenticatePopup" method="post">
   	<html:hidden property="login" value="login"/>
   	<table bgcolor="#F5F5F5" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
				<td><img src="" width="0" height="30"></td>
				<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Log in</font></td>
				<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   			<td colspan="2"></td>
   			<td colspan="2" class="bodytext-bold">
				    		<span style="color:red;">
				    		<html:errors/>
				</span>&nbsp;
				</td>
   		</tr>
   		<tr>
   			<td><img src="" width="0" height="100"></td>
				<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<table width="100%" cellspacing="2" cellpadding="0" border="0" class="bodytext">
			   			<tr>
			   				<td class="bodytext-bold">Username</td>
			   				<td><html:text property="username" size="30"/></td>
			   			</tr>
			   			<tr>
								<td class="bodytext-bold">Password</td>
								<td><html:password property="password" size="30"/></td>
			   			</tr>
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr valign="top">
			   				<td><img src="" width="0" height="35" border="0"></td>
								<td>&nbsp;&nbsp;</td>
						   	<td align="center" class="bodytext-bold">
						   		<a href="Javascript:document.logonForm.submit()">Login</a> 
						   		&nbsp;&nbsp;
						   		<a href="Javascript:window.self.close()">Cancel</a></td>
								<td>&nbsp;&nbsp;</td>
   		</tr>
   	</table>
   	</html:form> 
   </body>
</html>
