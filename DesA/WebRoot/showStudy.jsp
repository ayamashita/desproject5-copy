<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
		
		<%@ include file="head.jsp" %>
	
    <table bgcolor="#F5F5F5" border="1" width="100%" cellspacing="0" cellpadding="0">
    	<tr>
    		<td><img class="blockOnPrint" src="" width="10" height="0"><img class="blockOnPrint" src="" width="0" height="20">
    		</td>
    		<td colspan="5" class="path" valign="top"><div id="navActions"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present>
    			<a href="listStudies.do">Studies</a> > <bean:write name="studyForm" property="name" /></div>
    		</td>
    		<td>
    			<img class="blockOnPrint" src="" width="10" height="0">
    		</td>
    		
    	</tr>
    	<tr>
			 	<td></td>
			    <td colspan="5">
			    	<h1><bean:write name="studyForm" property="name" /></h1>
			    	<logic:present name="msgKey">
																			                	<span style="color:green;"><b>
																			                  <bean:message key='<%=(String)request.getAttribute("msgKey")%>' />
																			                  </b>
																			                  </span>
                </logic:present>
			    </td>
			    <td>
			    </td>
    	</tr>
    	<tr>
    		<td rowspan="1">
    		</td>	
    		<td rowspan="1" style="width: 300px;word-wrap : break-word;" valign="top"  width="50%"><span class="bodytext">
    			<bean:write name="studyForm" property="descriptionHtmlCoded" filter="false"/></span>
    		</td>
    		<td rowspan="1"><img class="blockOnPrint" src="" width="10" height="0"></td>
    		<td valign="top">
    		<table>
    		<td class="bodytext-bold" width="10%">
    			Start of study:
    		</td>
    		<td class="bodytext">
    				<bean:write name="studyForm" property="startDateAsString" />
				  	<%--<bean:write name="studyForm" property="startDay" />/<bean:write name="studyForm" property="startMonth" />-<bean:write name="studyForm" property="startYear" />--%>
    		</td>
    	<tr>
    	<tr>
    		<td class="bodytext-bold">
				    			End of study:
				    		</td>
				    		<td class="bodytext">
				    		<bean:write name="studyForm" property="endDateAsString" />
								  	<%-- <bean:write name="studyForm" property="endDay" />/<bean:write name="studyForm" property="endMonth" />-<bean:write name="studyForm" property="endYear" /> --%>
    		</td>
    	</tr>
    	<tr>
			    		<td class="bodytext-bold">
							    			Duration:
							    		</td>
							    		<td class="bodytext">
											  	<bean:write name="studyForm" property="duration" /> &nbsp;<bean:write name="studyForm" property="durationUnitFull" />
			    		</td>
    	</tr>
    	<tr>
			    		<td class="bodytext-bold">
							    			Study type:
							    		</td>
							    		<td class="bodytext">
											  	<bean:write name="studyForm" property="type" />
			    		</td>
    	</tr>
    	<tr>
			    		<td class="bodytext-bold">
							    			Keywords:
							    		</td>
							    		<td class="bodytext">
											  	<bean:write name="studyForm" property="keywords" />
			    		</td>
    	</tr>
    	<tr>
			    		<td class="bodytext-bold">
							    			No of <br>participants:
							    		</td>
							    		<td class="bodytext">
											  	<bean:write name="studyForm" property="students" /> student(s)<br>
											  	<bean:write name="studyForm" property="professionals" /> professional(s)
			    		</td>
    	</tr>
    	<tr>					<%boolean color= true;%>
						    		<td class="bodytext-bold" valign="top">
										    			Responsibles:
										    		</td>
										    		<td>
										    		<table width="100%" cellspacing="0" cellpadding="0">
										    			<logic:iterate id="responsible" name="studyForm" property="responsibles">
										    			<tr class="bodytext" <%=(color==true?"bgcolor=\"white\"":"") %> >
										    				<td width="20%" nowrap><a href='http://www.simula.no/people_one.php?people_id=<bean:write name="responsible" property="id" filter="true"/>'><bean:write name="responsible" property="first_name" filter="true"/> <bean:write name="responsible" property="family_name" filter="true"/></a>&nbsp;</td>
										    				<td><bean:write name="responsible" property="position" filter="true"/></td>
										    			</tr>
										    			<%color=(color==true?false:true); %>
										    			</logic:iterate>
										    		</table>
										    		
						    		</td>
    	</tr>
    	</table>
    	</td>
    	<tr>
			    		<td>
			    		</td>
			    		<td colspan="5" class="bodytext">
								&nbsp;	
						    </td>
			    		<td>
			    		</td>
			    	</tr>
    	<tr>
    	<tr>
    		<td>
    		</td>
    		<td colspan="5" class="bodytext-bold">
					Study notes	
			    </td>
    		<td>
    		</td>
    	</tr>
    	<tr>
			    		<td>
			    		</td>
			    		<td colspan="5" class="bodytext">
								<bean:write name="studyForm" property="notesHtmlCoded" filter="false"/>
						    </td>
			    		<td>
			    		</td>
			    	</tr>
    	
    	<tr>
						    		<td>
						    		</td>
						    		<td colspan="5" class="bodytext">
											&nbsp;	
									    </td>
						    		<td>
						    		</td>
						    	</tr>
						    	
			    	<tr>
			    	<tr>
			    		<td>
			    		</td>
			    		<td colspan="5" class="bodytext-bold">
								Study material	
						    </td>
			    		<td>
			    		</td>
			    	</tr>
			    	<%color= true;%>
			    	<logic:iterate id="material" name="studyForm" property="studyMaterial">
			    	<tr >
						    		<td>
						    		</td>
						    		<td colspan="5" class="bodytext" <%=(color==true?"bgcolor=\"white\"":"") %> >
											<logic:equal name="material" property="isUrl" value="true">
												<a href="<bean:write name="material" property="url" filter="true"/>" target="_new"><bean:write name="material" property="description" filter="true"/></a>
											</logic:equal>
											<logic:equal name="material" property="isUrl" value="false">
												<a href='downloadStudyMaterial.do?studyMaterialId=<bean:write name="material" property="study_material_id" filter="true"/>&studyId=<bean:write name="material" property="study_id" filter="true"/>'><bean:write name="material" property="description" filter="true"/></a>
                   		</logic:equal>
									    </td>
						    		<td>
						    		</td>
						   </tr>
						   <%color=(color==true?false:true); %>
						   </logic:iterate>
						    	
							
							<tr>
						    		<td>
						    		</td>
						    		<td colspan="5" class="bodytext">
											&nbsp;	
									    </td>
						    		<td>
						    		</td>
						    	</tr>
						    	
			    	<tr>
			    	<tr>
			    		<td>
			    		</td>
			    		<td colspan="5" class="bodytext-bold">
								Publications	
						    </td>
			    		<td>
			    		</td>
			    	</tr>
			    	<%color= true;%>
			    	<logic:iterate id="publication" name="studyForm" property="publications">
			    	<tr>
						    		<td>
						    		</td>
						    		<td colspan="5" <%=(color==true?"bgcolor=\"white\"":"") %> class="bodytext">
											 <b><bean:write name="publication" property="editor" filter="true"/></b> -
											  <bean:write name="publication" property="year" filter="true"/>: "
                  			<a href='http://www.simula.no/publication_one.php?publication_id=<bean:write name="publication" property="id" filter="true"/>'><bean:write name="publication" property="title" filter="true"/></a>", <br><img class="blockOnPrint" src="" width="20" height="0">
                  				<bean:write name="publication" property="shortAbstact" filter="true"/> 
									    </td>
						    		<td>
						    		</td>
						    	</tr>
						    	<%color=(color==true?false:true); %>
    	</logic:iterate>
    	<tr>
						    		<td>
						    		</td>
						    		<td colspan="5" class="bodytext-bold">
											&nbsp;	
									    </td>
						    		<td>
						    		</td>
			    	</tr>
    	
    </table>
    <%@ include file="footer.jsp" %>