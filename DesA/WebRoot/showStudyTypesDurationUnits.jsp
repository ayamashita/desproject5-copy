<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



 
<%@ include file="head.jsp" %>

<table border="0" width="100%" bgcolor="#F5F5F5" cellpadding="0" cellspacing="0">
  <tr>
			<td><img src="" width="10" height=""></td>
			<td colspan="6" class="path"><logic:present name="user"><a href="frontPage.do">Front page</a> </logic:present> > 
			<a href="listStudies.do">Studies</a> > Study types and Duration Units</td>
			<td><img src="" width="10" height=""></td>
	</tr>
  <tr>
  	<td>&nbsp;</td>
  	<td colspan="6"><h1>Study types and duration units</h1>
  	</td>
  	<td>&nbsp;</td>
  </tr>
  <tr>
	  	<td>&nbsp;</td>
	  	<td colspan="6" class="bodytext">Create new study type or duration unit by clicking "add new" below. Delete existing by clicking "delete".
	  	</td>
	  	<td>&nbsp;</td>
  </tr>
  <tr>
  <td>&nbsp;<td>
  <td width="50%"><h2>Study types <a href="JavaScript:openPopup('editStudyTypeDuration.do?action=add&type=studytype')">(Add new)</a></h2> </td>
  <td><img src="" width="10" height=""></td>
  <td width="2"></td>
  <td><img src="" width="10" height=""></td>
  <td width="50%"><h2>Duration units <a href="JavaScript:openPopup('editStudyTypeDuration.do?action=add&type=duration')">(Add new)</a></h2> <h2></td>
  <td></td>
  </tr>
  <td><td>
	  <td width="50%" valign="top">
	  <table width="100">
	  		<logic:iterate id="studytype" name="StudyTypes">
	  			<tr>
	  			<td class="bodytext-bold" width="20%">
	  				<bean:write name="studytype" property="name"/>
	  			</td>
	  			<!--<td class="bodytext">
	  				<a href="JavaScript:openPopup('editStudyTypeDuration.do?action=rename&type=studytype&id=<bean:write name="studytype" property="value"/>&name=<bean:write name="studytype" property="name"/>')">Rename</a>
	  			</td>-->
	  			<td class="bodytext">
						<img src="" width="10" height="0"><a href="JavaScript:openPopup('editStudyTypeDuration.do?action=delete&type=studytype&id=<bean:write name="studytype" property="value"/>&name=<bean:write name="studytype" property="name"/>')">Delete</a>
	  			</td>
	  			</tr>
	  		</logic:iterate>
	  </table>
	  </td>
	  <td><img src="" width="10" height=""></td>
	  <td width="2"></td>
	  <td><img src="" width="10" height=""></td>
	  <td width="50%" valign="top">
	  <table width="100" border="0" >
	  	<logic:iterate id="durationunit" name="DurationUnits">
				  			<tr>
				  			<td class="bodytext-bold">
				  				<bean:write name="durationunit" property="name"/>
				  			</td>
				  			<td class="bodytext" align="left">
									<img src="" width="10" height="0"><a href="JavaScript:openPopup('editStudyTypeDuration.do?action=delete&type=duration&id=<bean:write name="durationunit" property="value"/>&name=<bean:write name="durationunit" property="name"/>')">Delete</a>
				  			</td>
				  			</tr>
				  		</logic:iterate>
	  	</table>
	  </td>
 		 <td></td>
 	<tr>
	  	<td colspan="8">&nbsp;</td>
	</tr>
</table>
<%@ include file="footer.jsp" %>
  
  
  
  
  