<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
		
		<%@ include file="head.jsp" %>	
		
		<%
			String studyType = new String("");
			String startYear = new String("");
			String endYear = new String("");
			if(request.getSession().getAttribute("studySearchForm") != null ){
					studyType = ((no.simula.des.struts.forms.StudySearchForm)request.getSession().getAttribute("studySearchForm")).getStudyType();		
					startYear = ((no.simula.des.struts.forms.StudySearchForm)request.getSession().getAttribute("studySearchForm")).getStartYear();		
					endYear = ((no.simula.des.struts.forms.StudySearchForm)request.getSession().getAttribute("studySearchForm")).getEndYear();
					studyType = (studyType == null? "": studyType);
					startYear = (startYear == null? "": startYear);
					endYear = (endYear == null? "": endYear);
					//System.out.println("types: " + studyType + "start: " + startYear + " end: "+ endYear );  
			}
		%>
    <table bgcolor="#F5F5F5" border="0" width="100%">  
    	<tr>
						<td></td>
						<td class="path"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listStudies.do">Studies</a> > study search</td>
			</tr>
    	<tr>
    		<td><img src="" width="10" height="0" border="0">
    		</td>
    		<td>
    		<h1>Study search</h1>	
    		</td>
    	</tr>
    	<tr>
    	<tr>
    		<td>
    			<img src="" width="10" height="0" border="0">
				  <img src="" width="0" height="600" border="0">
    		</td>
    		<td class="bodytext" width="100%" valign="top">
    			<table border="0" class="bodytext">
    				<tr>
    					<html:form action="performSearchStudies.do">
    					<td colspan ="3">
    					Search in registered studies:	
    					</td>
    				</tr>
    				<tr>
						 	<td colspan ="3">
						   <html:text property="freetext" size="50"/>
						  </td>
    				</tr>
    				<tr>
    					<td>
    						Study type
    					</td>
    					<td colspan="2">
    						End date between:
    					</td>
    				</tr>
    				<tr>
    					<td><html:select property="studyType" size="1">
    						<option value="">All study types</option>
    						<logic:iterate id="type" name="studyTypes">
    								<option value="<bean:write name="type" property="name"/>" 
    									<logic:equal name="type" property="name" value='<%=studyType%>' >selected</logic:equal> >
    									<bean:write name="type" property="name"/></option>
    						</logic:iterate>
    						
    						</html:select>
    					</td>
    					<td><html:select property="startYear" size="1">
    						<option value="">All years</option>
    						<logic:iterate id="s_year" name="endYears">
    							<option value="<bean:write name="s_year"/>"
    							<logic:equal name="s_year" value='<%=startYear%>' >selected</logic:equal> >
    							<bean:write name="s_year"/></option>
    						</logic:iterate>
    						
    					<%--	<option value="01" <logic:equal name="studySearchForm" property="endMonth" value="01">selected</logic:equal>>January</option>
    						<option value="02" <logic:equal name="studySearchForm" property="endMonth" value="02">selected</logic:equal>>February</option>
    						<option value="03" <logic:equal name="studySearchForm" property="endMonth" value="03">selected</logic:equal>>March</option>
    						<option value="04" <logic:equal name="studySearchForm" property="endMonth" value="04">selected</logic:equal>>April</option>
    						<option value="05" <logic:equal name="studySearchForm" property="endMonth" value="05">selected</logic:equal> >May</option>
    						<option value="06" <logic:equal name="studySearchForm" property="endMonth" value="06">selected</logic:equal>>June</option>
    						<option value="07" <logic:equal name="studySearchForm" property="endMonth" value="07">selected</logic:equal>>July</option>
    						<option value="08" <logic:equal name="studySearchForm" property="endMonth" value="08">selected</logic:equal>>August</option>
    						<option value="09" <logic:equal name="studySearchForm" property="endMonth" value="09">selected</logic:equal>>September</option>
    						<option value="10" <logic:equal name="studySearchForm" property="endMonth" value="10">selected</logic:equal>>October</option>
    						<option value="11" <logic:equal name="studySearchForm" property="endMonth" value="11">selected</logic:equal>>November</option>
    						<option value="12" <logic:equal name="studySearchForm" property="endMonth" value="12">selected</logic:equal>>December</option>
    						
    					--%>	
    						</html:select>
    						
    					</td>
    					<td align="right">
    					<html:select property="endYear" size="1">
    						<option value="">All years</option>
    						<logic:iterate id="e_year" name="endYears">
								 	<option value="<bean:write name="e_year"/>"
								 	<logic:equal name="e_year" value='<%=endYear%>' >selected</logic:equal> >
								 	<bean:write name="e_year"/></option>
    						</logic:iterate>
    					</html:select>
    					</td>
    				</tr>
    				<tr>
    					<td colspan ="3">
							  <b>Study responsible<b>
    					</td>
    				</tr>
    				<tr>
    					<td colspan="3" valign="top">
    						<table cellpadding="0" cellspacing="0">
    							<tr>
											<td class="bodytext">First name:</td><td><img src="" width="15" height="0"></td>
											<td class="bodytext">Last name:</td>
									</tr>						    				
									 <tr>
									 <td><html:text property="responsible_firstname" size="20"/></td>
									 <td></td>
									 <td><html:text property="responsible_familyname" size="23"/></td>
    						</table>
    					</td>
							</tr>
    				 
						
						<tr>
							<td colspan ="3" align="right" class="bodytext-bold">
								<html:hidden property="isSearch" value="true"/>
								<a href="javascript: document.studySearchForm.submit();">Search</a>
								<!--<html:submit/>-->
    						</html:form>
							</td>
						</tr>
    			</table>
    		</td>
    	</tr>
    
    </table>
    
    
    
    
 <%@ include file="footer.jsp" %>
