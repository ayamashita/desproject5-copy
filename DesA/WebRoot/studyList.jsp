<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>



  <%@ include file="head.jsp" %>	


<table border="0" width="100%" bgcolor="#F5F5F5" cellpadding="0" cellspacing="0">
  <tr>
		<td></td>
		<td colspan="7" class="path"><div id="navActions"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listStudies.do">Studies</a></div></td>
	</tr>
<tr>
  <tr>
  	<td><img class="blockOnPrint" src="" border="0" width="10" height="0"></td>
  	<td colspan="6"><h1>Studies</h1>
  	</td>
  	<td><img class="blockOnPrint" src="" border="0" width="10" height="0"></td>
  </tr>
  <tr>
  		<td></td>
  		<td colspan="6">
  		<table width="100%">
  		<tr>
	  	<td class="bodytext">
	  	There are <span class="bodytext-bold"><bean:write name="studySearchForm" property="studies.totalStudies"/> registrated</span> studies in the study base 	
	  	</td>
	  	<td align="right" class="bodytext-bold"><div id="navActions">
	  		<a href="startSearchStudies.do">Study search</a>&nbsp;&nbsp;&nbsp;
	  		<logic:present name="user">
    			<logic:greaterThan name="user" property="privilege" value="0">
	  				<a href="editStudy.do">Create new study</a>&nbsp;&nbsp;&nbsp;&nbsp;
	  			</logic:greaterThan>
	  		</logic:present>
	  				<logic:present name="user">
						  <logic:greaterThan name="user" property="privilege" value="0">
							 <a href="showStudyTypesDurationUnits.do">Study types and Duration units</a>&nbsp;&nbsp;&nbsp;&nbsp;
						</logic:greaterThan>
	  		</logic:present>
	  				<html:link page="/aggregatedStudies.do">Aggregated study report</html:link>&nbsp;&nbsp;&nbsp;
	  		<logic:present name="user">
	  			<logic:greaterThan name="user" property="privilege" value="0">	
	  				<a href="downloadCSV.do">CSV-format</a>&nbsp;&nbsp;&nbsp;
	  			</logic:greaterThan>
	  		</logic:present>
	  		<logic:equal name="studySearchForm" property="isViewAll" value="false">
	  			<html:link page="/listStudies.do?action=viewAll&start=1"  paramId="count" paramName="studySearchForm" paramProperty="studies.totalStudies">View all</html:link>&nbsp;
	  		</logic:equal>
	  		<logic:equal name="studySearchForm" property="isViewAll" value="true">
	  			<html:link page="/listStudies.do?action=viewFirstPage">View first page </html:link>
				</logic:equal>	  		
	  		<logic:equal name="studySearchForm" property="hasPreviousPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/listStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="previousPageIndex">&#60;&#60;Previous page</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasPreviousPage" value="false">
    		</logic:equal>
    		<logic:equal name="studySearchForm" property="hasNextPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/listStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="nextPageIndex">Next page&#62;&#62;</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasNextPage" value="false">
				      &nbsp;
    </logic:equal>
    		</div>
	  	</td>
	  	</tr>
	  	</table>
	  	</td>
	  	<td></td>
  </tr>
  <tr>
  	<td></td>
  	<td colspan="6">
  	<hr style="height: 1px; color: black; width: 100%;">
  	</td>
  	<td></td>
  </tr>
 
   
  <tr class="bodytext-bold">
  	<td></td>
    <th align="left" ><html:link page="/listStudies.do?sort=study_name&start=1">
    	Study name <bean:write name="studySearchForm" property="studySortBean.start"/>-<bean:write name="studySearchForm" property="studies.endIndex"/>
    	&nbsp;(total <bean:write name="studySearchForm" property="studies.totalStudies"/>)&nbsp;<img class="blockOnPrint" src="images/Icon_sort_arrow.gif">
    	</html:link></th>
    <th align="left"><html:link page="/listStudies.do?sort=study_type&start=1">Study type&nbsp;<img class="blockOnPrint" src="images/Icon_sort_arrow.gif"></html:link></th>
    <th align="center" nowrap><html:link page="/listStudies.do?sort=study_end_date&&start=1">End date&nbsp;<img class="blockOnPrint" src="images/Icon_sort_arrow.gif"></html:link></th>
    <th colspan="3" align="left">Study description</th>
  	<td></td>
  </tr>

<tr>
  	<td></td>
  	<td colspan="6">
  	<hr style="height: 1px; color: black; width: 100%;">
  	</td>
  	<td></td>
  <tr>
<%boolean color= true;%>
<logic:iterate id="study" name="studySearchForm" property="studies.studies">
    <tr class="bodytext" <%=(color==true?"bgcolor=\"#cccccc\"":"") %>  valign="top">
    <td  bgcolor="#F5F5F5"></td>
    <td align="left">
    	<html:link page="/showStudy.do"  paramId="study" paramName="study" paramProperty="id">
      	<bean:write name="study" property="name" filter="true"/>
      </html:link>&nbsp;
    </td>
    <td align="left">
      <bean:write name="study" property="type" filter="true"/>&nbsp;
    </td>
    <td align="center" nowrap>
      <bean:write name="study" property="endDateAsString" filter="true"/>&nbsp;
    </td>
    <td align="left">
      <bean:write name="study" property="shortDescription" filter="true"/>
    </td>
    <td align="right">
    	<logic:present name="user">
    		<logic:greaterThan name="user" property="privilege" value="0"><div id="navActions">
      		<html:link page="/editStudy.do"  paramId="study" paramName="study" paramProperty="id">Edit</html:link>&nbsp;
      		</div>
      	</logic:greaterThan>
      </logic:present>
    </td>
    <td align="right">
    	<logic:present name="user">
    		<logic:greaterThan name="user" property="privilege" value="0"><div id="navActions">
      		<a href="Javascript:openPopup('deleteStudy.do?study=<bean:write name="study" property="id"/>','DeleteStudy' );" >Delete</a>
      		</div>
      	</logic:greaterThan>
      </logic:present>
      
    </td>
    <td bgcolor="#F5F5F5"></td>
  </tr>
  <%color=(color==true?false:true); %>
</logic:iterate>
 <tr>
   	<td></td>
   	<td colspan="6">
   	<hr style="height: 1px; color: black; width: 100%;">
   	</td>
   	<td></td>
  </tr>
 <tr>
  		<td></td>
  		<td colspan="6">
  		<table width="100%">
  		<tr>
	  	<td class="bodytext">
	  	&nbsp;
	  	</td>
	  	<td align="right" class="bodytext-bold"><div id="navActions">
	  		<a href="startSearchStudies.do">Study search</a>&nbsp;&nbsp;&nbsp;
	  		<logic:present name="user">
    			<logic:greaterThan name="user" property="privilege" value="0">
	  				<a href="editStudy.do">Create new study</a>&nbsp;&nbsp;&nbsp;&nbsp;
	  			</logic:greaterThan>
	  		</logic:present>
	  		<logic:present name="user">
										  <logic:greaterThan name="user" property="privilege" value="0">
											 <a href="showStudyTypesDurationUnits.do">Study types and Duration units</a>&nbsp;&nbsp;&nbsp;&nbsp;
										</logic:greaterThan>
	  		</logic:present>
	  			<html:link page="/aggregatedStudies.do">Aggregated study report</html:link>&nbsp;&nbsp;&nbsp;
	  		<logic:present name="user">	
	  			<logic:greaterThan name="user" property="privilege" value="0">
	  				<a href="downloadCSV.do">CSV-format</a>&nbsp;&nbsp;&nbsp;
	  			</logic:greaterThan>
	  		</logic:present>
	  		<logic:equal name="studySearchForm" property="isViewAll" value="false">
					  			<html:link page="/listStudies.do?action=viewAll&start=1"  paramId="count" paramName="studySearchForm" paramProperty="studies.totalStudies">View all</html:link>&nbsp;
					  		</logic:equal>
					  		<logic:equal name="studySearchForm" property="isViewAll" value="true">
					  			<html:link page="/listStudies.do?action=viewFirstPage">View first page </html:link>
				</logic:equal>	  	
	  		<logic:equal name="studySearchForm" property="hasPreviousPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/listStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="previousPageIndex">&#60;&#60;Previous  page</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasPreviousPage" value="false">
    		</logic:equal>
    		<logic:equal name="studySearchForm" property="hasNextPage" value="true">
				    	&nbsp;&nbsp;<html:link page="/listStudies.do"  paramId="start" paramName="studySearchForm" paramProperty="nextPageIndex">Next page&#62;&#62;</html:link>
				     </logic:equal>
				     <logic:equal name="studySearchForm" property="hasNextPage" value="false">
				      &nbsp;
    </logic:equal>
    			</div>
	  	</td>
	  	</tr>
	  	</table>
	  	</td>
	  	<td></td>
  </tr>
  
</table>

 <%@ include file="footer.jsp" %>