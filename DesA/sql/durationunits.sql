-- MySQL dump 8.22
--
-- Host: localhost    Database: des
---------------------------------------------------------
-- Server version	3.23.57-nt

--
-- Table structure for table 'durationunits'
--

CREATE TABLE durationunits (
  duration_id int(3) NOT NULL auto_increment,
  duration_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (duration_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'durationunits'
--


INSERT INTO durationunits VALUES (1,'Hours');
INSERT INTO durationunits VALUES (2,'Days');
INSERT INTO durationunits VALUES (3,'Weeks');
INSERT INTO durationunits VALUES (4,'Months');

