-- MySQL dump 8.22
--
-- Host: localhost    Database: des
---------------------------------------------------------
-- Server version	3.23.57-nt

--
-- Table structure for table 'publicationsstudies'
--

CREATE TABLE publicationsstudies (
  publication_id int(10) NOT NULL default '0',
  study_id int(10) NOT NULL default '0',
  PRIMARY KEY  (publication_id,study_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'publicationsstudies'
--


INSERT INTO publicationsstudies VALUES (1,1);
INSERT INTO publicationsstudies VALUES (2,1);

