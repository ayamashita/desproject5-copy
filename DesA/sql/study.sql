-- MySQL dump 8.22
--
-- Host: localhost    Database: des
---------------------------------------------------------
-- Server version	3.23.57-nt

--
-- Table structure for table 'study'
--

CREATE TABLE study (
  study_id int(10) NOT NULL auto_increment,
  study_name varchar(200) NOT NULL default '',
  study_type int(3) NOT NULL default '0',
  study_description text NOT NULL,
  study_keywords varchar(100) default NULL,
  study_notes text,
  study_students int(3) default NULL,
  study_professionals int(3) default NULL,
  study_duration int(4) default NULL,
  study_duration_unit int(1) default NULL,
  study_start_date date default NULL,
  study_end_date date NOT NULL default '0000-00-00',
  study_created_by int(4) default NULL,
  study_created_date date default NULL,
  study_edited_by int(4) default NULL,
  study_edited_date date default NULL,
  PRIMARY KEY  (study_id),
  UNIQUE KEY study_id (study_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'study'
--


INSERT INTO study VALUES (1,'A study',4,'A simple test study','study, simple','note',2,4,2,4,'2003-01-01','2004-12-31',17,'2003-01-01',17,'2004-01-01');
INSERT INTO study VALUES (2,'The study',1,'Simple test study','study, simple','more notes',3,2,23,3,'2001-01-12','2003-01-11',10,'2001-10-12',11,'2002-01-12');
INSERT INTO study VALUES (3,'Some study',2,'Description of a study','study','notes',8,6,3,4,'1999-08-01','2002-01-07',45,'1999-08-01',45,'1999-08-01');

