--
-- Table structure for table 'studymateriel'
--

CREATE TABLE studymaterial(
  study_material_id int(10) NOT NULL auto_increment,		
  study_id int(10) NOT NULL,
  study_material_description VARCHAR(255) NOT NULL default '',
  study_material_isUrl char NOT NULL default '0',
  study_material_file LONGBLOB,	
  study_material_filename varchar(255),
  study_material_filetype varchar(255),		
  study_material_url varchar(255),  	
  PRIMARY KEY (study_material_id,study_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'study'
--


INSERT INTO studymaterial (study_id, study_material_description, study_material_isUrl, study_material_url) VALUES (1, "Dette er en url", '1', 'http://www.vg.no' );

