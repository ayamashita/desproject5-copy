package no.simula.des.beans;

import no.simula.des.data.DesContentDatabase;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;


/**
 * This bean is responsible for fetching the front page message from the database.
 * The class supports having messages on more than one page. Once the messages for
 * a page has been loaded, they are cached.
 *
 */
public class ContentMessageBean extends ActionForm {
    private static Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    private static Hashtable messages = new Hashtable();
    private int id;
    private int page_id;
    private String text;

    /**
     * Hidden default constructor
     *
     */
    private ContentMessageBean() {
    }

    /**
     * Returns the message(s) related to a page
     *
     * @param pageId Id of the page to fetch messages for
     * @return a Collection containing the messages related to the page
     */
    public static Collection getInstance(int pageId) {
        try {
            if (messages.containsKey(new Integer(pageId))) {
                return (Collection) messages.get(new Integer(pageId));
            } else {
                DesContentDatabase contentDb = new DesContentDatabase();
                Collection col = contentDb.retriveContent(pageId);
                messages.put(new Integer(pageId), col);

                return col;
            }
        } catch (Exception e) {
            log.error("Error fetching content for page= " + pageId, e);

            return new ArrayList();
        }
    }

    public int getId() {
        return id;
    }

    public int getPage_id() {
        return page_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPage_id(int page_id) {
        this.page_id = page_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Cleans out the message cache
     *
     */
    public static void forceUpdate() {
        messages = new Hashtable();
    }

    /**
     * This method is used for testing only
     */
    public static void main(String[] args) {
        DesContentDatabase.IS_TEST = true;

        ArrayList content = (ArrayList) ContentMessageBean.getInstance(1);
        content = (ArrayList) ContentMessageBean.getInstance(1);
        log.debug("done");
    }
}
