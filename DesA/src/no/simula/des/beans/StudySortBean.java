/*
 * Created on 01.okt.2003
 *
 */
package no.simula.des.beans;

import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;


/**
 * This bean contains parameters necessary to determine how to sort study lists
 */
public class StudySortBean {
    //Request parameters
    public final static String START = "start";
    public final static String COUNT = "count";
    public final static String ORDER_BY = "sort";
    public final static String ORDER_DIRECTION = "dir";

    //Attributes and their default values
    private int start = 1;
    private int count = 20; //count = 0 -> show all
    private String orderBy = "study_end_date";
    private String orderDirection = "DESC";

    //Attributes for search
    private String searchString;
    private String studyType;
    private String startYear;
    private String endYear;
    private String responsible_firstname;
    private String responsible_familyname;

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Initialize bean based on request parameters
     *
     * @param params HashMap containing the request parameters
     * @throws NumberFormatException
     */
    public void setSortParams(HashMap params) throws NumberFormatException {
        if (params.containsKey(START)) {
            start = Integer.parseInt(((String[]) params.get(START))[0]);
        }

        if (params.containsKey(COUNT)) {
            count = Integer.parseInt(((String[]) params.get(COUNT))[0]);
        }

        if (params.containsKey(ORDER_BY)) {
            String tmp = ((String[]) params.get("sort"))[0];

            //Order direction = ASC unless specified
            if (tmp.equals(orderBy)) {
                orderDirection = (orderDirection.equals("ASC") ? "DESC" : "ASC");
            } else {
                orderDirection = "DESC";
            }

            orderBy = tmp;
        }

        if (params.containsKey(ORDER_DIRECTION)) {
            orderDirection = ((String[]) params.get(ORDER_DIRECTION))[0];
        }
    }

    public int getCount() {
        return count;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public int getStart() {
        return start;
    }

    public void setCount(int i) {
        count = i;
    }

    public void setOrderBy(String string) {
        orderBy = string;
    }

    public void setOrderDirection(String string) {
        orderDirection = string;
    }

    public void setStart(int i) {
        start = i;
    }


    public String getEndYear() {
        return endYear;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }


    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }


  public String getStartYear() {
    return startYear;
  }
  public void setStartYear(String startYear) {
    this.startYear = startYear;
  }
  public String getResponsible_familyname() {
    return responsible_familyname;
  }
  public String getResponsible_firstname() {
    return responsible_firstname;
  }
  public void setResponsible_familyname(String responsible_familyname) {
    this.responsible_familyname = responsible_familyname;
  }
  public void setResponsible_firstname(String responsible_firstname) {
    this.responsible_firstname = responsible_firstname;
  }
}
