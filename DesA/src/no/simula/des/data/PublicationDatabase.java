/*
 * Created on 13.okt.2003
 *
*/
package no.simula.des.data;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;



/**
 * This class manages database access for publications related queries. As publications
 * are used in DES only as "links to related publications", the class never actually
 * deletes publications from the database - only relationships to studies are removed.
 */
public class PublicationDatabase extends DataObject {
    private final static String SINGLE_PUBLICATION_QUERY =
        "select publication_id, "+
        "publication_year, "+
        "publication_title, "+
        "publication_abstract, "+
        "publication_internal, "+
        "if(publication_internal='y',p.people_first_name,'') firstname, "+
        "if(publication_internal='y',p.people_family_name,'External author' ) lastname "+
        "from "+Constants.SIMULAWEB_DB+".publication LEFT JOIN "+Constants.SIMULAWEB_DB+".people p ON publication_owner_id=p.people_id and publication_internal='y' "+
        "where publication_id = ? " +
        "order by lastname ASC;";

    private final static String RELATED_PUBLICATIONS_QUERY =
        "select pub.publication_id, "+
        "publication_year, "+
        "publication_title, " +
        "publication_abstract, "+
        "publication_internal, "+
        "if(publication_internal='y',p.people_first_name,'') firstname, "+
        "if(publication_internal='y',p.people_family_name,'External author' ) lastname "+
        "from "+Constants.SIMULAWEB_DB+".publication pub " +
        "LEFT JOIN "+Constants.SIMULAWEB_DB+".people p  ON publication_owner_id=p.people_id and publication_internal='y', "+
        "publicationsstudies pubstud " +
        "where study_id = ? and pub.publication_id = pubstud.publication_id "+
        "order by lastname ASC;";

        /*
        "SELECT publication.publication_id, " + "       publication_editor, " +
        "       publication_title, " + "       publication_year, " +
        "       publication_abstract " + "FROM   " + Constants.SIMULAWEB_DB +
        ".publication, publicationsstudies " + "WHERE  (study_id=?) AND " +
        "       (publication.publication_id=publicationsstudies.publication_id);";
        */
    private final static String DELETE_PUBLICATION =
        "DELETE FROM publicationsstudies " + "WHERE  (publication_id=?) AND " +
        "       (study_id=?);";

    private final static String ADD_PUBLICATION =
        "INSERT IGNORE INTO publicationsstudies " + "VALUES (?, ?);";

    private final static String ALL_PUBLICATIONS_QUEREY =
    "select publication_id, "+
    "publication_year, "+
    "publication_title, "+
    "publication_abstract, "+
    "publication_internal, "+
    "if(publication_internal='y',p.people_first_name,'') firstname, "+
    "if(publication_internal='y',p.people_family_name,'External author' ) lastname "+
    "from "+Constants.SIMULAWEB_DB+".publication LEFT JOIN "+Constants.SIMULAWEB_DB+".people p ON publication_owner_id=p.people_id and publication_internal='y' "+
    "order by lastname ASC;";

    //"SELECT * from " +
    //    Constants.SIMULAWEB_DB + ".publication;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrieves a single publication from the publication table
     *
     * @param id the publication to retrieve
     * @return the PublicationBean representing the given id
     */
    public PublicationBean getPublication(int id) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(SINGLE_PUBLICATION_QUERY);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            rs.first();

            PublicationBean publicationBean = populatePublicationBean(rs);

            return publicationBean;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrievs all publications related to one study
     *
     * @param studyId identifies the study
     * @return the publications
     */
    public Collection getPublicationList(int studyId) throws Exception {
        //Find all related publications using publications / publicationsstudies
        ResultSet rs = null;
        ArrayList publications = new ArrayList();

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(RELATED_PUBLICATIONS_QUERY);
            pstmt.setInt(1, studyId);

            rs = pstmt.executeQuery();

            //Populate publication beans and add to list
            for (int i = 0; rs.next(); i++) {
                PublicationBean bean = populatePublicationBean(rs);
                publications.add(bean);
            }

            return publications;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrieves all publications
     *
    * @return the publications
     * @throws Exception
     */
    public Collection getPublicationList() throws Exception {
        ResultSet rs = null;
        ArrayList publications = new ArrayList();

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ALL_PUBLICATIONS_QUEREY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                PublicationBean bean = populatePublicationBean(rs);
                publications.add(bean);
            }

            return publications;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new Exception(ex.toString());
        } finally {
            this.closeConnection(rs);
        }
    }

    /**
     * The method adds and removes relationships between publications and studies at the
     * database level.
     *
     * @param studyId identifies which study we are changing the relationships for.
     * @param publications all publications related to the study, including any added or deleted
     *                     ones.
     * @throws Exception
     */
    public void updatePublicationStudyRelationship(int studyId,
        Collection publications) throws Exception {
        if( publications == null) return;
        Iterator iterator = publications.iterator();

        //Discover any changes
        while (iterator.hasNext()) {
            PublicationBean bean = (PublicationBean) iterator.next();

            if (bean.getDeleted()) {
                //Delete publication from study
                deletePublicationFromStudy(studyId, bean.getId());
            } else if (bean.getAdded()) {
                //Add publication to study
                addPublicationToStudy(studyId, bean.getId());
            }
        }
    }

    /**
     * This method relates a publication to a study
     *
     * @param studyId
     * @param publicationId
     */
    void addPublicationToStudy(int studyId, int publicationId)
        throws Exception {
        //TODO Can we assume that relation is not already existing?
        //If the addPublication action handles this, no additional check (SELECT) is needed here
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ADD_PUBLICATION);
            pstmt.setInt(1, publicationId);
            pstmt.setInt(2, studyId);

            int addCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(addCount + " publications added to study " + studyId);
            }
            /*
            if (addCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not added from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * This method removes the relationship between a study and a publication in the DES database
     *
     * @param studyId
     * @param publicationId
     */
    void deletePublicationFromStudy(int studyId, int publicationId)
        throws Exception {
        //Delete all matching entries in publicationsstudies...
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DELETE_PUBLICATION);
            pstmt.setInt(1, publicationId);
            pstmt.setInt(2, studyId);

            int delCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(delCount + " publications deleted from study " +
                    studyId);
            }
            /*
            if (delCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }
    

    /**
     * Collects data from the result set and populates a publication bean
     *
     * @param rs the SQL result set
     * @return the populated PublicationBean
     */
    private PublicationBean populatePublicationBean(ResultSet rs)
        throws SQLException {
        PublicationBean bean = new PublicationBean();

        bean.setId(rs.getInt(1));
        bean.setYear(rs.getInt(2));
        bean.setTitle(rs.getString(3));
        bean.setAbstact(rs.getString(4));
        boolean b = (rs.getString(5).equals("n")? true:false);
        bean.setExternal(b);
        String firstname = rs.getString(6);
        String lastname = rs.getString(7);
        if(b || (firstname == null && lastname == null ) ){
          firstname = (firstname == null? "": firstname );
          lastname = (lastname == null? "n/a": lastname);

          bean.setEditor(lastname);
        }
        else{
          bean.setEditor(lastname+","+firstname);
        }
        return bean;
    }
}
