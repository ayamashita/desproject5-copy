/*
 * Created on 13.okt.2003
 *
 */
package no.simula.des.data.beans;


/**
 * The class represents an entry in the publication table
 */
public class PublicationBean {
    private int id;
    private String editor;
    private int year;
    private String title;
    private String pubAbstact;
    private boolean isExternal = false;

    //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;

    public String toString() {
        StringBuffer out = new StringBuffer();
        out.append("id=" + id);
        out.append(", editor=" + editor);
        out.append(", year=" + year);
        out.append(", title=" + title);
        out.append(", isExtenal=" + isExternal);
        if (pubAbstact != null) {
            out.append(", abstract!=null");
        }

        return out.toString();
    }

    /**
     * @return
     */
    public String getEditor() {
        return editor;
    }

    /**
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * @return
     */
    public String getAbstact() {
        return pubAbstact;
    }

    public String getShortAbstact() {
        int length = pubAbstact.length();

        if (length > 100) {
            return pubAbstact.substring(0, 100) + "...";
        }

        return pubAbstact;
    }

    /**
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     * @param string
     */
    public void setEditor(String string) {
        editor = string;
    }

    /**
     * @param i
     */
    public void setId(int i) {
        id = i;
    }

    /**
     * @param string
     */
    public void setAbstact(String string) {
        pubAbstact = string;
    }

    /**
     * @param string
     */
    public void setTitle(String string) {
        title = string;
    }

    /**
     * @param i
     */
    public void setYear(int i) {
        year = i;
    }

    /**
     * @return
     */
    public boolean getAdded() {
        return isAdded;
    }

    /**
     * @return
     */
    public boolean getDeleted() {
        return isDeleted;
    }

    /**
     * @param b
     */
    public void setAdded(boolean b) {
        isAdded = b;
    }

    /**
     * @param b
     */
    public void setDeleted(boolean b) {
        isDeleted = b;
    }

    public void setExternal(boolean b){
      this.isExternal = b;
    }

    public boolean getExternal( ){
      return isExternal;
    }
}
