package no.simula.des.data.beans;

/**
 * Represents a file related to study material element
 */

public class StudyMaterialFileBean {

  private String filename;
  private byte[] file;
  private String contentType;

  public StudyMaterialFileBean() {
  }

  public byte[] getFile() {
    return file;
  }
  public String getContentType() {
    return contentType;
  }
  public String getFilename() {
    return filename;
  }
  public void setFilename(String filename) {
    this.filename = filename;
  }
  public void setFile(byte[] file) {
    this.file = file;
  }
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }
}