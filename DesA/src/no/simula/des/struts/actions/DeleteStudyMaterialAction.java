/*
 * Created on 30.sep.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package no.simula.des.struts.actions;

import no.simula.des.data.StudyMaterialDatabase;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Deletes a study material element from a study
 */
public class DeleteStudyMaterialAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("DeleteStudyMaterialAction called");

        StudyForm studyForm = (StudyForm) form;

        String studyMaterialId = request.getParameter("studyMaterialId");

        if ((studyMaterialId == null) || studyMaterialId.equals("")) {
            ActionErrors errors = new ActionErrors();
            errors.add("deleteMaterial",
                new ActionError(
                    "error.des.studyMaterial.delete.studyMaterialId.missing"));
            this.saveErrors(request, errors);

            return mapping.findForward("failure");
        } else {
            String submit = request.getParameter("submit");

            if ((submit != null) && submit.equals("delete")) {
                log.debug("Returning success");

                ArrayList material = (ArrayList) studyForm.getStudyMaterial();
                StudyMaterialBean bean = (StudyMaterialBean) material.get(Integer.parseInt(
                            studyMaterialId));
                log.debug("Deleteing studyMaterial:" + bean.getDescription()  );
                bean.setDeleted( true );

                /*
                StudyMaterialDatabase stmdao = new StudyMaterialDatabase();
                stmdao.deleteStudyMaterial(Integer.parseInt(
                        request.getParameter("studyMaterialId")));
                */
                return mapping.findForward("success");
            }

            return mapping.findForward("confirm");
        }

        //return null;
    }
}
