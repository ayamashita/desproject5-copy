/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.beans.ContentMessageBean;
import no.simula.des.data.DesContentDatabase;
import no.simula.des.data.beans.ContentBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.FrontPageForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Opens the "Edit front page" form with the current front page content loaded
 */
public class EditFrontPageAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 2;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        HashMap params = (HashMap) request.getParameterMap();
        FrontPageForm frontPageForm = (FrontPageForm) form;

        if ((params != null) && (params.size() != 0)) {
            log.debug("Params:" + params);

            int id = Integer.parseInt(request.getParameter("id"));
            int pageId = Integer.parseInt(request.getParameter("page_id"));
            String text = request.getParameter("text");
            log.debug("The text:" + text);

            DesContentDatabase contentDb = new DesContentDatabase();
            boolean isOk = contentDb.updateContent(id, pageId, text);

            if (isOk) {
                ContentMessageBean.forceUpdate();

                return (mapping.findForward("saved"));
            } else {
                return (mapping.findForward("failure"));
            }
        } else {
            log.debug("Showing edit page");

            ArrayList messages = (ArrayList) ContentMessageBean.getInstance(Constants.FRONTPAGE_ID);
            ContentBean msg = (ContentBean) messages.get(0);
            frontPageForm.setId(msg.getId());
            frontPageForm.setPage_id(msg.getPage_id());
            frontPageForm.setText(msg.getText());

            return (mapping.findForward("success"));
        }
    }
}
