package no.simula.des.struts.actions;

import no.simula.des.data.DurationUnitsDatabase;
import no.simula.des.data.StudyTypesDatabase;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Title:EditStudyTypesDurationsAction
 * Description: This action class is responsible for showing the studytypes and duration units
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author: Anders Aas Hanssen
 * @version 1.0
 */
public class EditStudyTypesDurationsAction extends DesAction {
    private static final String ACTION_ADD = "add";
    private static final String ACTION_RENAME = "rename";
    private static final String ACTION_DELETE = "delete";
    private static final String TYPE_STUDY_TYPE = "studytype";
    private static final String TYPE_DURATION = "duration";
    private static final String ID = "id";
    private static final String NAME = "name";

    public int getAccessLevel() {
        return 2;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        String action = request.getParameter("action");
        String type = request.getParameter("type");
        String execute = request.getParameter("execute");

        if (execute == null) {
            if (ACTION_ADD.equals(action)) {
                request.setAttribute("action", ACTION_ADD);
                request.setAttribute("type", type);

                return (mapping.findForward("add"));
            }
            else if (ACTION_DELETE.equals(action)) {
                request.setAttribute("action", ACTION_DELETE);
                request.setAttribute("type", type);
                request.setAttribute(ID, request.getParameter("id"));
                request.setAttribute(NAME, request.getParameter("name"));

                return (mapping.findForward("delete"));
            }

            if (ACTION_RENAME.equals(action)) {
                request.setAttribute("action", ACTION_RENAME);
                request.setAttribute("type", type);
                request.setAttribute(ID, request.getParameter(ID));
                request.setAttribute(NAME, request.getParameter(NAME));

                return (mapping.findForward("add"));
            }
        } else if (execute.equals("ok")) {
            if (ACTION_ADD.equals(action)) {
                if (TYPE_DURATION.equals(type)) {
                    DurationUnitsDatabase db = new DurationUnitsDatabase();
                    String name = request.getParameter("name");
                    db.insertDurationUnit(name);
                } else if (TYPE_STUDY_TYPE.equals(type)) {
                    StudyTypesDatabase db = new StudyTypesDatabase();
                    String name = request.getParameter("name");
                    db.insertStudyType(name);
                }

                return (mapping.findForward("success"));
            }
            else if (ACTION_DELETE.equals(action)) {
                boolean isOk = false;
                String descType = new String();

                if (TYPE_DURATION.equals(type)) {
                    descType = "duration unit";

                    DurationUnitsDatabase db = new DurationUnitsDatabase();
                    String id = request.getParameter(ID);
                    isOk = db.deleteDurationUnit(Integer.parseInt(id));

                    if (isOk) {
                        return (mapping.findForward("success"));
                    }
                } else if (TYPE_STUDY_TYPE.equals(type)) {
                    descType = "study type";

                    StudyTypesDatabase db = new StudyTypesDatabase();
                    String id = request.getParameter(ID);
                    isOk = db.deleteStudyType(Integer.parseInt(id));

                    if (isOk) {
                        return (mapping.findForward("success"));
                    }
                }

                if (!isOk) {
                    request.setAttribute("type", type);
                    request.setAttribute(NAME, request.getParameter(NAME));
                    request.setAttribute(ID, request.getParameter(ID));

                    ActionErrors errors = new ActionErrors();
                    errors.add("deleteFailed",
                        new ActionError("error.des.studytype.duration.delete",
                            new String[] { descType }));
                    this.saveErrors(request, errors);

                    return new ActionForward("/deleteStudyTypeDuration.jsp");
                }
            }
        }

        return (mapping.findForward("success"));
    }
}
