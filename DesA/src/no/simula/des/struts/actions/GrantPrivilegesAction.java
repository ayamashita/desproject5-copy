/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.AdminPrivilegesDatabase;
import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.GrantPrivilegesForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This action stores changes made to admin privileges
 */
public class GrantPrivilegesAction extends DesAction {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 2;

        return min_access_level;
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        HashMap parameters = (HashMap) request.getParameterMap();

        GrantPrivilegesForm gpForm = (GrantPrivilegesForm) form;

        try {
            if (parameters.containsKey("update")) {
                log.debug("Updateing admin privileges database");

                HashMap tmp = new HashMap();
                Iterator iter = parameters.keySet().iterator();

                while (iter.hasNext()) {
                    String key = (String) iter.next();

                    if (!key.equals("update")) {
                        tmp.put(key, request.getParameter(key));
                    }
                }

                HashMap entries = this.getUpdateAdminPrivileges(gpForm, tmp);
                AdminPrivilegesDatabase gpDao = new AdminPrivilegesDatabase();
                int updated = gpDao.updateAdminPrivileges(entries);
                log.debug(entries);

                ActionMessages messages = new ActionMessages();
                messages.add("grantprivilges updated",
                    new ActionMessage("no.simula.des.grantprivilges.updated"));
                this.saveMessages(request, messages);
            }

            PeopleDatabase peopleDb = new PeopleDatabase();
            PeoplesBean peoples = peopleDb.getPeoples();
            gpForm.setPeoples(peoples);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug(e.getMessage());

            return mapping.findForward("failure");
        }

        log.debug("Done fetching peoples and privileges");

        return (mapping.findForward("success"));
    }

    public HashMap getUpdateAdminPrivileges(GrantPrivilegesForm form,
        HashMap parameters) {
        HashMap changeEntries = new HashMap();
        PeoplesBean peoples = form.getPeoples();
        Iterator iter = peoples.getPeoples().iterator();

        while (iter.hasNext()) {
            PeopleBean people = (PeopleBean) iter.next();
            String id = Integer.toString(people.getId());
            String privilege = Integer.toString(people.getPrivilege());

            if (parameters.containsKey(id) &&
                    !parameters.get(id).equals(privilege)) {
                changeEntries.put(new Integer(id),
                    new Integer((String) parameters.get(id)));
                log.debug("OK");
            }
        }

        return changeEntries;
    }
}
