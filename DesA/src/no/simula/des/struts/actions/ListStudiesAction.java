/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.struts.forms.StudySearchForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This action lists studies in various ways
 */
public class ListStudiesAction extends Action {
    //Attribute names
    private final static String STUDY_LIST_ATTRIBUTE = "studyList";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        HashMap parameters = (HashMap) request.getParameterMap();

        StudySearchForm stsForm = (StudySearchForm) form;
        String action = request.getParameter("action");
        if( action == null )action="";
        if( action.equals("viewAll")){
          stsForm.setIsViewAll( true );
          log.debug("Setting view all to true");
        }
        else if(action.equals("viewFirstPage")) {
          stsForm.setIsViewAll( false );
          stsForm.getStudySortBean().setCount(20);
          stsForm.getStudySortBean().setStart(1);
          log.debug("Setting view all to false");
        }
        try {
            StudiesBean studies = performSearch(stsForm, parameters);
            stsForm.setStudyList(studies);
        } catch (NumberFormatException nfe) {
            log.error("Malformed request parameters", nfe);

            return mapping.findForward("failure");
        }
         catch (Exception e) {
            log.error(e.getMessage());

            return mapping.findForward("failure");
        }

        log.debug("Done fetching studies");

        return (mapping.findForward("success"));

        //failure: login screen / frontpage?
    }

    public StudiesBean performSearch(StudySearchForm form, HashMap parameters)
        throws NumberFormatException, Exception {
        StudyDatabase list = new StudyDatabase();

        StudySortBean sortBean = form.getStudySortBean();

        if (form.getIsSearch()) {
            sortBean = new StudySortBean();
            form.setEndYear("");
            form.setStartYear("");
            form.setFreetext("");
            form.setResponsible_firstname("");
            form.setResponsible_familyname("");
            form.setIsSearch(false);
            form.setStudyType("");
        }

        sortBean.setSortParams(parameters);

        StudiesBean studies = list.getStudies(sortBean);

        //Collection studies = list.getStudies( sortBean );
        return studies;
    }
}
