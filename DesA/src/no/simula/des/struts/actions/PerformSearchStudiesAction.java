/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.struts.forms.StudySearchForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This action performs searches for studies.
 * Search criteria is defined in the request parameters.
 */
public class PerformSearchStudiesAction extends Action {
    //Attribute names
    private final static String STUDY_LIST_ATTRIBUTE = "studyList";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("Performing search");

        HashMap parameters = (HashMap) request.getParameterMap();

        StudySearchForm stsForm = (StudySearchForm) form;

        try {
            log.debug("stsForm.isSearch: " + stsForm.getIsSearch());

            StudiesBean studies = performSearch(stsForm, parameters);
            stsForm.setStudyList(studies);
        } catch (Exception e) {
            log.error(e.getMessage());

            return mapping.findForward("failure");
        }

        log.debug("Done fetching studies");

        //request.getSession().setAttribute("studySearchForm", stsForm);
        return (mapping.findForward("success"));


    }

    public StudiesBean performSearch(StudySearchForm form, HashMap parameters)
        throws NumberFormatException, Exception {
        StudyDatabase list = new StudyDatabase();

        StudySortBean sortBean = form.getStudySortBean();
        sortBean.setSortParams(parameters);
        sortBean.setStartYear(form.getStartYear());
        sortBean.setEndYear(form.getEndYear());
        sortBean.setStudyType(form.getStudyType());
        sortBean.setResponsible_firstname( form.getResponsible_firstname() );
        sortBean.setResponsible_familyname( form.getResponsible_familyname() );
        //sortBean.setResponsible(form.getStudyResponsible());
        sortBean.setSearchString(form.getFreetext());
        log.debug(" Freetext" + form.getFreetext());
        log.debug(" StartYear" + form.getStartYear());
        log.debug(" EndYear" + form.getEndYear());


        StudiesBean studies = list.advancedSearchStudies(sortBean);

        return studies;
    }


}
