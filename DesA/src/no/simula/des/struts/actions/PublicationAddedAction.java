/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.PublicationDatabase;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Updates the list of publications that are related to a study as a result of
 * changes made in the "Select publications" popup.
 */
public class PublicationAddedAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm stdForm = (StudyForm) form;

        HashMap params = (HashMap) request.getParameterMap();

        ArrayList containsPublications = new ArrayList();
        Iterator iter = stdForm.getPublications().iterator();

        while (iter.hasNext()) {
            PublicationBean item = (PublicationBean) iter.next();
             if(!item.getDeleted()){
              containsPublications.add(String.valueOf(item.getId()));
             }
        }

        Collection publications = (Collection) request.getSession()
                                                      .getAttribute("publications");

        ArrayList newPublications = new ArrayList();
        iter = publications.iterator();

        while (iter.hasNext()) {
            PublicationBean item = (PublicationBean) iter.next();
            String id = String.valueOf(item.getId());

            if (params.containsKey(id) && !containsPublications.contains(id)) {
                log.debug("Is added:" + item.getId());
                item.setAdded(true);
                newPublications.add(item);
            }

            //log.debug( "Is added:" + item.getAdded() );
        }

        log.debug("Removing old publications");
        iter = stdForm.getPublications().iterator();

        while (iter.hasNext()) {
            PublicationBean item = (PublicationBean) iter.next();
            String id = String.valueOf(item.getId());

            if (!params.containsKey(id)) {
                log.debug("Is deleted:" + item.getId());
                item.setDeleted(true);
            }

            newPublications.add(item);
        }

        /*
        PublicationDatabase pubDao = new PublicationDatabase();
        pubDao.updatePublicationStudyRelationship(Integer.parseInt(
                stdForm.getId()), (Collection) newPublications);

        */

        stdForm.setPublications(newPublications);
        request.getSession().removeAttribute("publications");

        return (mapping.findForward("success"));
    }
}
