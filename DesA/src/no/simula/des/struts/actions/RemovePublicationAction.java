/*
 * Created on 14.okt.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This Action removes a publication from a study. The relation between the two is NOT
 * removed at the database level until SaveStudyAction is called
 *
 */
public class RemovePublicationAction extends DesAction {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    /* (non-Javadoc)
     * @see no.simula.des.struts.actions.DesAction#executeAuthenticated(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm studyForm = (StudyForm) form;
        int pubToRemove = -1;

        try {
            pubToRemove = Integer.parseInt(request.getParameter("publication"));
        } catch (Exception e) {
            log.error("Unable to parse \"publication\" parameter", e);

            return mapping.findForward("failure");
        }

        //Get Collection from form.
        Iterator publications = studyForm.getPublications().iterator();

        //Find publication. Limited number of elements -> use linear search
        while (publications.hasNext()) {
            PublicationBean pub = (PublicationBean) publications.next();

            if (pub.getId() == pubToRemove) {
                //Mark bean for deletion
                pub.setDeleted(true);

                break;
            }
        }

        return mapping.findForward("success");
    }
}
