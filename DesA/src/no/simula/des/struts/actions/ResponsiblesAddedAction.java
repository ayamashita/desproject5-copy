/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.PeopleDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Updates the list of responsibles that are related to a study as a result of
 * changes made in the "Select responsibles" popup.
 */
public class ResponsiblesAddedAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm stdForm = (StudyForm) form;

        HashMap params = (HashMap) request.getParameterMap();

        ArrayList containsResponsibles = new ArrayList();
        Iterator iter = stdForm.getResponsibles().iterator();

        while (iter.hasNext()) {
            PeopleBean item = (PeopleBean) iter.next();
            if(!item.getDeleted()){
              containsResponsibles.add(String.valueOf(item.getId()));
            }
        }

        PeoplesBean peoples = (PeoplesBean) request.getSession().getAttribute("peoples");

        ArrayList newResponsibles = new ArrayList();
        iter = peoples.getPeoples().iterator();

        while (iter.hasNext()) {
            PeopleBean item = (PeopleBean) iter.next();
            String id = String.valueOf(item.getId());

            if (params.containsKey(id) && !containsResponsibles.contains(id) ) {
                log.debug("Is added:" + item.getId());
                item.setAdded(true);
                newResponsibles.add(item);
            }

            //log.debug( "Is added:" + item.getAdded() );
        }

        log.debug("Removing old responsibles");
        iter = stdForm.getResponsibles().iterator();

        while (iter.hasNext()) {
            PeopleBean item = (PeopleBean) iter.next();
            String id = String.valueOf(item.getId());

            if (!params.containsKey(id)) {
                log.debug("Is deleted:" + item.getId());
                item.setDeleted(true);
            }

            newResponsibles.add(item);
        }

        log.debug("The following responsibles should be added " +
            newResponsibles);
        /*
        PeopleDatabase peopleDao = new PeopleDatabase();
        peopleDao.updateResponsibleStudyRelationship(Integer.parseInt(
                stdForm.getId()), (Collection) newResponsibles);
        */
        stdForm.setResponsibles(newResponsibles);
        request.getSession().removeAttribute("peoples");

        return (mapping.findForward("success"));
    }
}
