/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.struts.actions.DesAction;
import no.simula.des.struts.forms.StudyMaterialForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Opens the form where users can add study material to a study
 */
public class StartStudyMaterialAction extends DesAction {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        log.debug("StartStudyMaterial called");

        /*
        String study_id = (String) request.getParameter("study_id");


        if ((study_id == null) || study_id.equals("")) {
            log.debug("StartStudyMaterial, study_id: " + study_id +
                ", Returning error");

            ActionErrors errors = new ActionErrors();
            errors.add("studymaterial",
                new org.apache.struts.action.ActionError(
                    "error.des.studyMaterial.studyIdRequired",
                    "StudyId required "));
            saveErrors(request, errors);

            return (mapping.findForward("success"));

            //return (mapping.getInputForward());
        }
        else {
            log.debug("StartStudyMaterial, study_id: " + study_id);

            StudyMaterialForm stdmForm = (StudyMaterialForm) form;
            stdmForm.setStudy_id(Integer.parseInt(study_id));
            return (mapping.findForward("success"));
        }
       */
      return (mapping.findForward("success"));
    }
}
