/*
 * Created on 14.okt.2003
 *
 */
package no.simula.des.struts.actions;

import no.simula.des.data.StudyDatabase;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This Action deletes a study.
 *
 */
public class StudyDeletedAction extends DesAction {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        String strStudyId = request.getParameter("study");
        String studyName = request.getParameter("studyName");

        if (strStudyId == null) {
            return mapping.findForward("failure");
        }

        try {
            Integer.parseInt(strStudyId);
        } catch (Exception e) {
            log.error("study id is not parsable", e);

            return mapping.findForward("failure");
        }

        try {
            StudyDatabase studyDb = new StudyDatabase();
            studyDb.deleteStudy(Integer.parseInt(strStudyId));
        } catch (Exception e) {
            log.error("Error deleting study=" + strStudyId, e);

            return mapping.findForward("failure");
        }

        request.setAttribute("studyId", strStudyId);
        request.setAttribute("studyName", studyName);

        return mapping.findForward("success");
    }
}
