                    </td>
                    <td width="150" nowrap="true">
                      <h1>&nbsp;</h1>

                      <% if(request.isUserInRole("db-admin") || request.isUserInRole("study-admin")) { %>

                        <ul class="menu">
                          <li><halogen:menuitem action="/adm/welcome"/></li>
                        </ul>
                        <ul class="menu">
                          <li><halogen:menuitem action="/adm/studies/manage"/></li>
                          <li><halogen:menuitem action="/adm/studies/edit" new="true"/></li>
                        </ul>
                        <ul class="menu">
                          <li><halogen:menuitem action="/adm/studymaterial/manage"/></li>
                        </ul>
                        <ul class="menu">
                          <li><halogen:menuitem action="/adm/studies/search"/></li>
                          <li><halogen:menuitem action="/adm/aggregatedstudies/view"/></li>
                          <li><halogen:menuitem action="/adm/studies/export"/></li>
                        </ul>
                        
                        <ul class="menu">
                          <li><halogen:menuitem action="/adm/report/manage"/></li>
                          <li><halogen:menuitem action="/adm/report/edit" new="true"/></li>
                        </ul>

                        <% if(request.isUserInRole("db-admin")) { %>
                          <ul class="menu">
                            <li><halogen:menuitem action="/adm/openingtext/edit"/></li>
                            <li><halogen:menuitem action="/adm/privileges/manage"/></li>
                            <li><halogen:menuitem action="/adm/studytypes/manage"/></li>
                            <li><halogen:menuitem action="/adm/durationunits/manage"/></li>
                          </ul>
                        <% } %>
                        
                        <ul class="menu">
                          <li>
                            <html:link 
                              onclick="javascript:window.open('','help','width=640,height=600,scrollbars=yes,resizable=yes')" 
                              page="/secure/help-admin.html" 
                              target="help"
                            >
                              help
                            </html:link>
                          </li>
                        </ul>
                        <% if(request.isUserInRole("db-admin") || request.isUserInRole("study-admin")) { %>
         				<ul class="menu">
                      
                            <li><halogen:menuitem action="/logout"/></li>
                         
                        </ul>
                        <%} %>

                      <% }
                      else { %>
                        <ul class="menu">
                          <li><halogen:menuitem action="/studies/search"/></li>
                          <li><halogen:menuitem action="/aggregatedstudies/view"/></li>
                        </ul>

                        <ul class="menu">
                          <li>
                            <html:link 
                              onclick="javascript:window.open('','help','width=640,height=600,scrollbars=yes,resizable=yes')" 
                              page="/help.html" 
                              target="help"
                            >
                              help
                            </html:link>
                          </li>
                        </ul>
                        <ul class="menu">
                          <li><a href="<%= loginURL %>">login</a></li>
                        </ul>
                      <% } %>  
                    </td>
                  </tr>
                </table>
              </td>
              <td width="10%"></td>
            </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr vAlign="top" bgColor="#fe4314" height="128">
        <td bgColor="#fe7519" height="128"></td>
        <td vAlign="top" bgColor="#ef1607" height="128">
          <table height="135" cellSpacing="0" cellPadding="0" width="100%" border="0">
            <tbody>
            <tr>
              <td vAlign="top" width="20%" bgColor="#fe7c19">&nbsp;</td>
              <td vAlign="top" width="20%" bgColor="#fe6318">&nbsp;</td>
              <td vAlign="top" width="20%" bgColor="#fe4314">&nbsp;</td>
              <td vAlign="top" width="20%" bgColor="#fe0f0c">&nbsp;</td>
              <td vAlign="top" width="20%" bgColor="#ef1607">&nbsp;</td>
            </tr>
            </tbody>
          </table>
        </td>
      </tr>
      </tbody>
    </table>
  </body>
</html>