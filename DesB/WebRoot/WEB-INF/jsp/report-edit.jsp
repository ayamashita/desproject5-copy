<%@include file="include/include-top.jsp"%>

<%if (request.getAttribute("noaccess")!= null) { %>
<h1> You do not have access to this report! </h1>
<%return; } %>

<h1>
	<logic:present name="<%=Constants.FORM_REPORT%>" property="id">Edit study</logic:present>
	<logic:notPresent name="<%=Constants.FORM_REPORT%>" property="id">Register new study</logic:notPresent>
</h1>
<html:form action="/adm/report/edit" method="post">
	<logic:messagesPresent>
		<table class="error">
			<tr>
				<td>
					<ul>
						<html:messages id="error">
							<li>
								<bean:write name="error" />
							</li>
						</html:messages>
					</ul>
				</td>
			</tr>
		</table>
	</logic:messagesPresent>

	<%-- The main table that divides the page into two columns --%>
	<table class="layout">
		<tr>

			<%-- Column One --%>
			<td>
				Report information
				<table class="container">
					<tr>
						<td>
							name*
						</td>
						<td>
							<html:text property="name" name="<%=Constants.FORM_REPORT%>"
								style="width: 100%" />
						</td>
					</tr>

					<tr>
						<td>
							Time sort order*
						</td>
						<td>
							<html:select property="sortOrderId"
								name="<%=Constants.FORM_REPORT%>">
								<html:optionsCollection
									name="<%= Constants.BEAN_SORT_OPTIONS %>" value="id"
									label="sortOrderDesc" />
							</html:select>
						</td>
					</tr>

					<tr>
						<td>
							columns*
						</td>
						<td>
							<table class="container">
								<tr>
									<th>
										available columns
									</th>
									<th />
									<th>
										selected columns
									</th>
								</tr>
								<tr>
									<td>
										<logic:empty name="<%= Constants.FORM_REPORT %>"
											property="allColumns">
											<html:select property="addColumn" multiple="true" size="5"
												disabled="true">
												<html:option value="-1" key="lists.option.empty" />
											</html:select>
										</logic:empty>
										<logic:notEmpty name="<%= Constants.FORM_REPORT %>"
											property="allColumns">
											<html:select property="addColumn" multiple="true" size="5">
												<html:optionsCollection property="allColumns" value="id"
													label="displayName" />
											</html:select>
										</logic:notEmpty>
									</td>
									<td style="text-align: center">
										<html:submit property="addCol" value=">" />
										<br />
										<html:submit property="removeCol"
											value="<"/>
				                  </td>
				                  <td>
				                    <logic:empty name="<%= Constants.FORM_REPORT %>" property="columns">
				                      <html:select property="removeColumn" multiple="true" size="5" disabled="true">
				                        <html:option value="-1" key="lists.option.empty"/>
				                      </html:select>
				                    </logic:empty>
				                    <logic:notEmpty name="<%= Constants.FORM_REPORT %>" property="columns">
				                      <html:select property="removeColumn" multiple="true" size="5">
				                        <html:optionsCollection property="columns" value="id" label="displayName"/>
				                      </html:select>
				                    </logic:notEmpty>
				                  </td>
				                </tr>
				              </table>
				            </td>
				          </tr>
				          
				    <tr>
						<td>
							criterium*
						</td>
						<td>
							<html:select property="criteriumId" name="<%= Constants.FORM_REPORT %>">
								<html:optionsCollection name="<%= Constants.BEAN_REPORT_CRITERIA %>"
									value="id" label="criteriaName" />
							</html:select>
						</td>
					</tr>
					
					<tr>
						<td>
							responsible*
						</td>
						<td>
							<table class="container">
								<tr>
									<th>
										available persons
									</th>
									<th />
									<th>
										responsibles
									</th>
								</tr>
								<tr>
									<td>
										<logic:empty name="<%= Constants.FORM_REPORT %>"
											property="persons">
											<html:select property="addResponsible" multiple="true"
												size="5" disabled="true">
												<html:option value="-1" key="lists.option.empty" />
											</html:select>
										</logic:empty>
										<logic:notEmpty name="<%= Constants.FORM_REPORT %>"
											property="persons">
											<html:select property="addResponsible" multiple="true"
												size="5">
												<html:optionsCollection property="persons" value="id"
													label="fullName" />
											</html:select>
										</logic:notEmpty>
									</td>
									<td style="text-align: center">
										<html:submit property="addResp" value=">" />
										<br />
										<html:submit property="removeResp"
											value="<"/>
                  </td>
                  <td>
                    <logic:empty name="<%= Constants.FORM_REPORT %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_REPORT %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5">
                        <html:optionsCollection property="responsibles" value="id" label="fullName"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          
        </table>
      </td>
      <%-- End of Column One --%>
      
    </tr>
  </table>
  <%-- End of Main Table --%>

  <html:submit property="save" value="save changes"/>
  <html:cancel value="cancel"/>

 </html:form>


<%@include file="include/include-bottom.jsp"%>