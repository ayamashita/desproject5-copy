<%@include file="include/include-top.jsp"%>

  <h1>Report query result</h1>

  <logic:present name="<%= Constants.TABLE_QUERY_REPORT %>">
    <logic:notEmpty name="<%= Constants.TABLE_QUERY_REPORT %>" property="source.content">
      <html:form action="/adm/report/query">
      <halogen:table name="<%= Constants.TABLE_QUERY_REPORT %>"/>
      </html:form>
      <html:link target="_blank" page="/pdf/report.pdf">printer friendly version</html:link>
    </logic:notEmpty>
    <logic:empty name="<%= Constants.TABLE_QUERY_REPORT %>" property="source.content">
      No matching studies found.
    </logic:empty>
  </logic:present>
  <logic:notPresent name="<%= Constants.TABLE_QUERY_REPORT %>">
    No matching studies found.
  </logic:notPresent>

<%@include file="include/include-bottom.jsp"%>