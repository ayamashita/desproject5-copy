<%@page import="no.simula.des.presentation.web.forms.StudyMaterialForm"%>
<%@page import="java.util.List"%>
<%@include file="include/include-top.jsp"%>
<%
  StudyMaterialForm form = (StudyMaterialForm)request.getSession().getAttribute(Constants.FORM_STUDYMATERIAL);
  List nav = (List)request.getAttribute(no.halogen.presentation.web.Constants.NAVIGATION_PATH);
  String action = (String)nav.get(nav.size() - 1);
%>

  <script>
    <!--
    function changeType(type) {
      if(type == "file") {
        document.forms(1).elements["attachRemoteFile"].disabled = false;
        document.forms(1).elements["url"].disabled = true;
        document.all["file"].color = "#000000";
        document.all["url"].color  = "#AEAEAE";
      }
      else {
        document.forms(1).elements["attachRemoteFile"].disabled = true;
        document.forms(1).elements["url"].disabled = false;
        document.all["file"].color = "#AEAEAE";
        document.all["url"].color  = "#000000";
      }
    }
    // -->
  </script>

  <h1>
    <logic:present name="<%= Constants.FORM_STUDYMATERIAL %>" property="id">Edit study material</logic:present>
    <logic:notPresent name="<%= Constants.FORM_STUDYMATERIAL %>" property="id">Register new study material</logic:notPresent>
  </h1>

  <html:form action="<%= action %>" enctype="multipart/form-data" method="post">

    <logic:messagesPresent>
      <table class="error">
        <tr>
          <td>
            <ul>
              <html:messages id="error">
                <li><bean:write name="error"/></li>
              </html:messages>
            </ul>
          </td>
        </tr>
      </table>
    </logic:messagesPresent>

    <table class="container">
      <tr>
        <td>description</td>
        <td><html:text property="description" size="36"/></td>
      </tr>
      <tr>
        <td>type</td>
        <td>
          <logic:iterate name="<%= Constants.FORM_STUDYMATERIAL %>" property="types" id="type" type="no.simula.des.StudyMaterial">
            <html:radio property="type" idName="type" value="type" onclick="<%= ( "changeType('" + type.getType() + "')" ) %>">
              <bean:message key="<%= ("studymaterial.type." + type.getType()) %>"/>
            </html:radio>
          </logic:iterate>
        </td>
      </tr>
      <tr>
        <td id="file" colspan="2">
          file
          <table class="container">
            <% if((form.getRemoteFile() != null && form.getRemoteFile().getFileSize() > 0) || form.getName() != null) { %>
              <tr>
                <td colspan="2">
                  <% if(form.getRemoteFile() != null && form.getRemoteFile().getFileSize() > 0) { %>
                    <b><bean:write name="<%= Constants.FORM_STUDYMATERIAL %>" property="remoteFile.fileName"/></b>
                  <% } else {  %>
                    <html:link page="<%= "/file/" + form.getId() + "/" + form.getName() %>">
                      <b><bean:write name="<%= Constants.FORM_STUDYMATERIAL %>" property="name"/></b>
                    </html:link>
                  <% } %>
                </td>
              </tr>
              <tr>
                <td colspan="2" class="button">
                  <%--html:submit property="attach" value="change file"/--%>
                  <html:submit property="remove" value="remove file"/>
                </td>
              </tr>

            <% } else { %>
              <tr>
                <td colspan="2">
                  <i>--- empty ---</i>
                </td>
              </tr>
              <tr>
                <td colspan="2" class="button">
                  <html:file property="remoteFile" size="36"/>
                  <%--html:submit property="attach" value="attach file"/--%>
                </td>
              </tr>
            <% } %>
            <tr>
              <td>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td id="url" colspan="2">
          external resource
          <table class="container">
            <tr>
              <td>url</td>
              <td>
                <html:text property="url" size="42"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <html:submit property="save" value="save changes"/>
    <html:cancel value="cancel"/>
 </html:form>

<%@include file="include/include-bottom.jsp"%>