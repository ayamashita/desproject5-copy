insert into report_sort_order (report_sort_order_id, report_sort_order_name, report_sort_order_desc) values
(1,'-', 'unsorted'), (2,'ASC', 'ascending'), (3,'DESC', 'descending');

insert into report_criteria (report_criteria_id, report_criteria_name) values (1,'OR'), (2,'AND');

# these are columns that can be read from study database
insert into rep_column (column_name, column_display_name, column_prop_name)
values 
	('stu_name', 'Study Name', 'name'),
	('stu_description', 'Study description', 'description'),
	('study_start', 'Start of Study', 'start'),
	('study_end', 'End of Study', 'end'),
	('stu_numb_stud', 'No of Student participants', 'noOfStudents'),
	('stu_numb_prof', 'No of Professional participants', 'noOfProfessionals'),
	('stu_notes', 'Study Notes', 'notes');
	 
# these are special columns, that have to be handled individually	 
insert into rep_column (column_name, column_display_name, column_type, column_prop_name)
values  
	('study_type', 'Study type', 1, 'type'),
	('study_responsible', 'Study_responsible', 1, 'responsibles'),
	('study_duration', 'Duration of Study', 1, 'duration'),
	('study_publications', 'Publication',1, 'publications'),
	('study_files', 'Study material',1, 'studyMaterial'),
	('study_owner', 'Study Owner',1, 'ownedBy'),
	('last_edited_by', 'Last edited by',1, 'lastEditedBy'),
	('keywords', 'Keywords', 1, 'keywords');