/*
 * Created on 23.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.persistence;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class DeletePersistentObjectException extends Exception {

	public DeletePersistentObjectException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DeletePersistentObjectException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DeletePersistentObjectException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public DeletePersistentObjectException() {
		super();
		// TODO Auto-generated constructor stub
	}

}
