package no.halogen.persistence;

import java.util.Iterator;
import java.util.List;

import no.simula.Person;
import no.simula.wsclient.StringIdObject;

/**
 * Contains utility methods to use with the persistence framework.
 * 
 * @author Stian Eide
 */
public class ListUtils {

	/** Creates a new instance of ListUtils */
	public ListUtils() {
	}

	/**
	 * Returns the persistable object with an id of <code>id</code>, or
	 * <code>null</code> if no match is found.
	 * 
	 * @param list
	 *            the list of peristable objects
	 * @param id
	 *            the id of the object to be found
	 * @return persistable object, or <code>null</code> if not found
	 * @throws IllegalCastException
	 *             if list elementes not of type <code>Persistable</code>
	 */
	public static Persistable get(List list, Integer id) {
		for (Iterator i = list.iterator(); i.hasNext();) {
			Persistable o = (Persistable) i.next();
			if (o.getId().equals(id)) {
				return (o);
			}
		}
		return (null);
	}

	public static StringIdObject get(List list, String id) {
		for (Iterator i = list.iterator(); i.hasNext();) {
			StringIdObject o = (StringIdObject) i.next();
			if (o.getId().equals(id)) {
				return (o);
			}
		}
		return (null);
	}
}