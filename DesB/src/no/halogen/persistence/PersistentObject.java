/*
 * Created on 22.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.persistence;

import java.io.Serializable;
import java.util.List;

/**
 * @author Frode Langseth
 *
 * Interface containing the common methods for persistent objects
 */
public interface PersistentObject extends Serializable {
	/**
	 * Method create
   * Saves a new instance of an <code>object</code>, to make the object persistent
	 * @param instance Object the <code>object</code> to save
	 * @return Integer <code>Identificator</code> of the saved object
	 * @throws CreatePersistentObjectException if the <code>object</code> couldn't be saved
	 */
	public abstract Integer create(final Object instance) throws CreatePersistentObjectException;
	/**
	 * Method delete
   * Deletes an <code>PersistentObject</code> from the persistence storage
	 * @param id Integer <code>Identificator</code> of the object to delete
	 * @return boolean success - true or false?
	 * @throws DeletePersistentObjectException if the object identified by <code>id</code> couldn't be deleted
	 */
	public abstract boolean delete(final Integer id) throws DeletePersistentObjectException;
	/**
	 * Method findByKey
	 * Finds an <code>object</code> by the object's <code>Zidentificator</code>
	 * @param id Integer the <code>identificator</code> of the <code>object</code> to find
	 * @return Object - The <code>PersistentObject</code> with the given <code>identificator</code>
	 */
	public abstract Object findByKey(final Integer id);
	/**
	 * Method findByKeys
	 * Finds a list of <code>PersistentObject</code>s, by a list of <code>identificators</code>
	 * @param entityIds List the list of <code>identificators</code>
	 * @return List <code>objects</code> related to any of the <code>identificators</code> in <code>entityIds</code>
	 */
	public abstract List findByKeys(final List entityIds);
	/**
	 * Method find
	 * Finds all <code>PersistentObject</code>s for a particular class that implements <code>PersistentObject</code> 
	 * @return List all persistent <code>objects</code> of the <code>PersistentObject</code> class
	 */
	public abstract List find();
	/**
	 * Method update
	 * Updates the values and relations for a <code>PersistentObject</code> 
	 * @param instance Object the object containing the new values to be saved in the persistence store. The <code>object</code> must contain an <code>identifier</code> to update the correct data in the persistent store
	 * @return boolean success - true or false?
	 * @throws UpdatePersistentObjectException if the <code>object</code> and it's values and relations couldn't be updated 
	 */
	public abstract boolean update(final Object instance) throws UpdatePersistentObjectException;
	/**
	 * Method update
	 * Updates the values and relations for a <code>PersistentObject</code>
	 * @param id Integer <code>identification</code> of the object to update 
	 * @param instance Object Object the object containing the new values to be saved in the persistence store.
	 * @return boolean success - true or false?
	 * @throws UpdatePersistentObjectException if the <code>object</code> and it's values and relations couldn't be updated
	 */
	public abstract boolean update(final Integer id, final Object instance) throws UpdatePersistentObjectException;
}