/*
 * Created on 23.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.persistence;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class UpdatePersistentObjectException extends Exception {

	public UpdatePersistentObjectException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UpdatePersistentObjectException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public UpdatePersistentObjectException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UpdatePersistentObjectException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}
}
