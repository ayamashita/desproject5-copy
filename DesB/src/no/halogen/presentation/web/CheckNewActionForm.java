package no.halogen.presentation.web;

import org.apache.struts.action.ActionForm;

/**
 *
 * This is a base Action Form for all forms that have an id field and is
 * mapped to an action that may handle both new or a edit-requests.
 *
 * @author  Stian Eide
 */
public class CheckNewActionForm extends ActionForm {
  
  /** Creates a new instance of BaseActionForm */
  public CheckNewActionForm() {
  }

  /** Holds value of property id. */
  private Integer id;
  
  /** Getter for property id.
   * @return Value of property id.
   *
   */
  public Integer getId() {
    return this.id;
  }
  
  /** Setter for property id.
   * @param id New value of property id.
   *
   */
  public void setId(Integer id) {
    this.id = id;
  }    
}
