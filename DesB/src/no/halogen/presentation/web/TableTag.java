package no.halogen.presentation.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import no.halogen.utils.table.ActionTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableRenderer;
import no.simula.des.presentation.web.actions.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.RequestUtils;

/**
 *  This tag renders a html-table from a <code>Table</code>-object. It uses
 *  the {@link no.halogen.utils.table.TableRenderer} to render the table
 *  as html.
 */
public class TableTag extends TagSupport {

  private static Log log = LogFactory.getLog(TableTag.class);
  
  /** property declaration for tag attribute: name.
   */
  private String name;
  
  /** property declaration for tag attribute: selectableRows.
   */
  private boolean selectableRows;
  
  /** property declaration for tag attribute: id.
   */
  private String id;
  
  /** This variable contains an IDRef for the referenced tag name */
  private Table table = null;
  
  /** Creates a new instance of <CODE>TableTag</CODE>. */  
  public TableTag() {
    super();
  }
  
  /** Fill in this method to perform other operations from doStartTag().
   * @throws JspException if no table could be generated
   */
  public void otherDoStartTagOperations() throws JspException {
        
    try {
      TableRenderer renderer = (TableRenderer)pageContext.getServletContext().getAttribute(Constants.TABLE_RENDERER);
      JspWriter out = pageContext.getOut();
      out.print(renderer.getHTML(pageContext, table, id, selectableRows));      
    }

    catch(Exception e) {
      log.error("Failed to generate tabletag.", e);
      throw new JspException("Failed to generate tabletag");
    }
  }
  
  
  /** Fill in this method to determine if the tag body should be evaluated
   * Called from doStartTag().
   * @return <CODE>true</CODE>
   */
  public boolean theBodyShouldBeEvaluated() {
    return(true);
  }
  
  /**
   * Fill in this method to perform other operations from doEndTag().
   */
  public void otherDoEndTagOperations() {}
  
  /** Fill in this method to determine if the rest of the JSP page
   * should be generated after this tag is finished.
   * Called from doEndTag().
   * @return <CODE>true</CODE>
   */
  public boolean shouldEvaluateRestOfPageAfterEndTag()  {
    return(true);
  }
  
  /** This method is called when the JSP engine encounters the start tag,
   * after the attributes are processed.
   * Scripting variables (if any) have their values set here.
   * @return EVAL_BODY_INCLUDE if the JSP engine should evaluate the tag body, otherwise return SKIP_BODY.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if an <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doStartTag() throws JspException {
    {
      // Find the referenced tag <name>, from the session.
      String refTagName = getName();
      table = (no.halogen.utils.table.Table)pageContext.getAttribute(refTagName, PageContext.SESSION_SCOPE);
    }
    
    otherDoStartTagOperations();
    
    if(theBodyShouldBeEvaluated()) {
      return(EVAL_BODY_INCLUDE);
    }
    else {
      return(SKIP_BODY);
    }
  }
  
  /** This method is called after the JSP engine finished processing the tag.
   * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if a <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doEndTag() throws JspException {
    otherDoEndTagOperations();
    
    if(shouldEvaluateRestOfPageAfterEndTag()) {
      return(EVAL_PAGE);
    }
    else {
      return(SKIP_PAGE);
    }
  }
  
  /** Returns the name in session scope where the table to be rendered is stored.
   * @return the key in session scope
   */  
  public String getName() {
    return(name);
  }
  
  /** Set the name in session scope where the table is stored
   * @param value the name of this table in session scope
   */  
  public void setName(String value) {
    name = value;
  }
  
  /** Returns whether this table has selectable rows. This will generate checkboxes in
   * the rightmost column of the table.
   * @return <CODE>true</CODE> - if the table has selectable columns
   */  
  public boolean hasSelectableRows() {
    return(selectableRows);
  }
  
  /** Set whether this table has selectable rows or not. This will generate
   * checkboxes in the rightmost column of the table.
   * @param value <CODE>true</CODE> if the table has selectable rows
   */  
  public void setSelectableRows(boolean value) {
    selectableRows = value;
  }
  
  /** This is the name that the checkboxes are given if this table has selectable
   * rows.
   * @return the name of the checkboxes
   */  
  public String getId() {
    return id;
  }
  
  /** This is the name that the checkboxes are given if this table has selectable
   * rows.
   * @param value the name of the checkboxes
   */  
  public void setId(String value) {
    id = value;
  } 
}