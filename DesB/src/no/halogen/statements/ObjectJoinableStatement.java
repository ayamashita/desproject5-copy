/**
 * @(#) ObjectJoinableStatement.java
 */

package no.halogen.statements;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * If more than one table must be joined in a statement to get the wanted result, the statement must implement this interface.
 * A joined statement consists of two or more <code>ObjectStatements</code>, the conditions that joins the statements together, and a method to get results from the statement
 * 
 * @see no.halogen.statement.ObjectStatement
 * @author Frode Langseth 
 */
public interface ObjectJoinableStatement {
  /**
   * Method executeSelect
   * Executes a select statement that joins two or more database tables.
   * The tables in the statement must be located inside the same physical database
   * 
   * When two tables that are added sequentially, and have key columns that can be used as join condition, it is not necessary to code a join condition, it will be generated  
   * 
   * The result is returned in a list. Inside the list, there's a  new list, one for each row in the result set. 
   * In each row list, there's databean objects containing the field values from the database tables. 
   * There's one databean for each <code>ObjectStatement</code> in the row list. 
   * The order of the databeans in the row list is syncrenous with the order the <code>ObjectStatement</code>s is added to the <code>joinable statement</code>
   * 
   * @return List the results from the statement
   * @throws StatementException
   */
  List executeSelect() throws StatementException;

  /**
   * Method fetchListResults
   * Gets the results from the <code>ResultSet</code>. 
   * This metod is just a wrapper method that will call the same method in the <code>ObjectStatement</code> that composes 
   * the <code>joinable statement</code>, and then buiklds the result structure (with row lists, etc.)
   * 
   * @param resultSet ResultSet the <code>ResultSet</code> that contains the results from a query. Will only be called after select statements 
   * @return List the results from the query
   * @throws SQLException if something goes wrong while fetching the result
   */
  List fetchListResults(ResultSet resultSet) throws SQLException;

  /**
   * Method getWhereClause
   * Get the where clause for the statement. This will often contains thejoin conditions between tables, and might as well contain other conditions
   * 
   * @return String the where clause
   */
  String getWhereClause();

  /**
   * Method setWhereClause
   * Sets the where clause for the statement. If the join condition betwee ntwo tables is anything other than the table keys, 
   * a join condition for the tables must be provided in the where clause
   * Other conditions might very well also be added to the where clause
   * 
   * @param whereClause String the where clause
   */
  void setWhereClause(String whereClause);

  /**
   * Method setQueryCollection
   * Sets list that contains the <code>ObejctStatement</code>s that forms the joinable statement.
   * The order the <code>ObjectStatement</code>s is deciding for the order of the databeans in the result row list, and possibly automaticly generating the join conditions
   *  
   * @param queryCollection List the <code>ObjectStatement</code>s representing the tables that will be joind in the statement
   */
  public void setQueryCollection(List queryCollection);

  /**
   * Method getQueryCollection
   * Gets the list of <code>ObjectStatement</code>s that forms the joinable statement
   * 
   * @return List the <code>ObjectStatement</code>s representing the tables that will be joind in the statement
   */
  public List getQueryCollection();

  /**
   * Method setConditions
   * In a <code>PreparedStatement</code>, values are represented by '?' characters. 
   * This methods receives a list with the actual values, so that the <code>PreparedStatement</code> can be populated with 
   * the values before it is executed
   * 
   * @param conditions List of condition values
   */
  void setConditions(List conditions);
  /**
   * Method getConditions
   * In a <code>PreparedStatement</code>, values are represented by '?' characters. 
   * This methods gets a list with the actual values
   * 
   * @return List of condition values
   */
  List getConditions();
  
  void setOrderBy(String orderByClause);
  String getOrderBy();
}
