/**
 * @(#) ObjectStatementImpl.java
 */

package no.halogen.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.naming.NamingException;

import no.halogen.persistence.Persistable;
import no.halogen.utils.sql.LoggableStatement;
import no.halogen.utils.sql.SqlHelper;

/**
 * Implementation of the ObjectStatement interface
 * 
 * @author Frode Langseth
 */
public abstract class ObjectStatementImpl implements Entity, ObjectStatement {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(ObjectStatementImpl.class);

  /**
   * Field dataBean
   */
  private Object dataBean;
  /**
   * Field whereClause
   */
  private String whereClause;
  /**
   * Field conditions
   */
  private ArrayList conditions = new ArrayList(); // all Conitions must be set here, in the same order that the '?' characters appears in the prepared statement 

  /**
   * Field conn
   */
  private Connection conn = null;

  /**
   * Method getDataBean
   * @return Object
   */
  public Object getDataBean() {
    return dataBean;
  }

  /**
   * Method getWhereClause
   * @return String
   * @see no.halogen.statements.Entity#getWhereClause()
   */
  public String getWhereClause() {
    return whereClause;
  }

  /**
   * Method setDataBean
   * @param dataBean Object
   */
  public void setDataBean(final Object dataBean) {
    this.dataBean = dataBean;
  }

  /**
   * Method setWhereClause
   * @param whereClause String
   */
  public void setWhereClause(String whereClause) {
    this.whereClause = whereClause;
  }

  /* (non-Javadoc)
   * @see halogen.statements.Entity#getInsertColumnNames()
   */
  /**
   * Method getInsertColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public abstract String getInsertColumnNames();

  /**
   * Method getInsertValues
   * @return String
   */
  public abstract String getInsertValues();

  /* (non-Javadoc)
   * @see halogen.statements.Entity#getKey()
   */
  /**
   * Method getKey
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public abstract String getKey();

  /* (non-Javadoc)
   * @see halogen.statements.Entity#getSelectColumnNames()
   */
  /**
   * Method getSelectColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public abstract String getSelectColumnNames();

  /* (non-Javadoc)
   * @see halogen.statements.Entity#getTableName()
   */
  /**
   * Method getTableName
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public abstract String getTableName();

  /* (non-Javadoc)
   * @see halogen.statements.Entity#getUpdateValuesString()
   */
  /**
   * Method getUpdateValuesString
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public abstract String getUpdateValuesString();

  /**
   * Performs the deletion of data from a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @return boolean
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeDelete()
   */
  public boolean executeDelete() throws StatementException {
    Persistable entity = (Persistable) getDataBean();
    //The data bean that is going to be deleted. Needs to instantiate it to get the id, eventually, use the executeDelete(Integer entityId) instead

    if (entity == null) {
      log.error("executeDelete() - No dataBean set. The DataBean must be set before this method is called by using ObjectStatement.setDataBean method.");
      throw new StatementException("Error! Could not delete record, missing databean");
    }

    return executeDelete(entity.getId());
  }

  /**
   * Performs the deletion of data from a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @param entityId Integer
   * @return boolean
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeDelete(Integer)
   */
  public boolean executeDelete(Integer entityId) throws StatementException {
    Connection conn = null;
    LoggableStatement pstmt = null;

    try {
      int result; // The return from the execution of the statement
      String whereClause = null; //If the where clause is specified. If whereClause is null, the key are used to delete the correct row

      // Gets connection to the database
      conn = SqlHelper.getConnection();

      //Creates a StringBuffer to contain the generated delete statement, and the generates the statement
      StringBuffer statement = new StringBuffer("DELETE FROM ");

      statement.append(getTableName());

      whereClause = getWhereClause();

      if (whereClause == null) {
        statement.append(" WHERE " + getKey() + " = ?");
        conditions.add(entityId);
      } else {
        statement.append(whereClause);
      }

      pstmt = new LoggableStatement(conn, statement.toString());

      generateValues(pstmt);

      // execute the delete
      result = pstmt.executeUpdate();

      //FIXME sometimes it must be zero...
      if (result < 0) {
        log.error("executeDelete() - Could not delete record." + getTableName() + ", " + entityId);
        return (false);
      }
    } catch (NamingException e) {
      log.error("executeDelete() - NamingException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not delete record.");
    } catch (SQLException e) {
      log.error("executeDelete() - SQLException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not delete record.");
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return (true);
  }

  /**
   * Performs the deletion of data from a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @param entityId Integer
   * @return boolean
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeDelete(Integer)
   */
  public boolean executeDelete(String entityId) throws StatementException {
    Connection conn = null;
    LoggableStatement pstmt = null;

    try {
      int result; // The return from the execution of the statement
      String whereClause = null; //If the where clause is specified. If whereClause is null, the key are used to delete the correct row

      // Gets connection to the database
      conn = SqlHelper.getConnection();

      //Creates a StringBuffer to contain the generated delete statement, and the generates the statement
      StringBuffer statement = new StringBuffer("DELETE FROM ");

      statement.append(getTableName());

      whereClause = getWhereClause();

      if (whereClause == null) {
        statement.append(" WHERE " + getKey() + " = ?");
        conditions.add(entityId);
      } else {
        statement.append(whereClause);
      }

      pstmt = new LoggableStatement(conn, statement.toString());

      generateValues(pstmt);

      // execute the delete
      result = pstmt.executeUpdate();

      if (result <= 0) {
        log.error("executeDelete() - Could not delete record." + getTableName() + ", " + entityId);
        return (false);
      }
    } catch (NamingException e) {
      log.error("executeDelete() - NamingException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not delete record.");
    } catch (SQLException e) {
      log.error("executeDelete() - SQLException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not delete record.");
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return (true);
  }

  /**
   * Performs the insertion of data into a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @return Integer
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeInsert()
   */
  public Integer executeInsert() throws StatementException {
    Connection conn = null;
    LoggableStatement pstmt = null;

    Integer entityKey = null;

    try {
      int result; // The return from the execution of the statement

      // Gets connection to the database
      conn = SqlHelper.getConnection();

      //Creates a StringBuffer to contain the generated insert statement, and the generates the statement
      StringBuffer statement = new StringBuffer("INSERT INTO ");

      statement.append(getTableName() + "(" + getInsertColumnNames() + ") ");
      statement.append("VALUES " + getInsertValues());

      pstmt = new LoggableStatement(conn, statement.toString());

      generateValues(pstmt);

      // execute the insert
      result = pstmt.executeUpdate();

      if (result <= 0) {
        log.error("executeInsert() - Could not insert record.");
        return new Integer(0);
      }

      /* Fetches the generated keys for the entity inserted
       * There should only be one row with one key
       */
      ResultSet rs = pstmt.getGeneratedKeys();

      if (rs != null && rs.next()) {
        // if there's more keys, they're ignored, as there should be only one
        entityKey = new Integer(rs.getInt(1));
      }

      rs.close();
    } catch (NamingException e) {
      log.error("executeInsert() - NamingException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not insert record.");
    } catch (SQLException e) {
      log.error("executeInsert() - SQLException looking up context..." + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not insert record.");
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return entityKey;
  }

  /**
  	 * Performs fetching of requested data
  	 * Uses the properties of the specialized classes that inherits this class to generate the correct statement.
  	 * The key condition is provided as a parameter
  	 * 
  	 * @author Frode Langseth
  	 * @version 0.1
  	 * @param entityId - The id on the entity to fetch data for
   * @return A populated entity
  	 * 
  	 * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeSelect(Integer)
   */
  public Object executeSelect(Integer entityId) throws StatementException {
    conditions.clear();
    StringBuffer whereClause = new StringBuffer();

    /*
     * set the conditions for the query
     */
    whereClause.append(" WHERE ");
    whereClause.append(getKey());
    whereClause.append(" = ?");
    setWhereClause(whereClause.toString());
    conditions.add(entityId);

    List list = executeSelectStatement(); //execute the query

    Object entity = new Object();
    ;

    if (!list.isEmpty()) {
      entity = list.get(0);
    }

    return (entity); //returns the first entity (should never be one than more, since the entity is fetched by querying one singel key
  }

  /**
  	 * Performs fetching of requested data
  	 * Uses the properties of the specialized classes that inherits this class to generate the correct statement.
  	 * 
  	 * This methods requires that the dataBean is provided, and populated with an ID
  	 * 
  	 * @author Frode Langseth
  	 * @version 0.1
  	 * @return A populated entity
  	 * 
  	 * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeSelect()
   */
  public List executeSelect() throws StatementException {
    Object entity = getDataBean();

    if (entity == null && getWhereClause() == null) {
      throw new StatementException("Error! No dataBean set or whereClause is empty!");
    } else if (getWhereClause() == null && entity instanceof Persistable) { //if whereClause, and no conditions have been set, use the entity's id to find the entity
      conditions.clear();
      Persistable idEntity = (Persistable) entity;
      /*
       * set the conditions for the query
       */
      if (idEntity.getId() != null && idEntity.getId().intValue() > 0) {
        setWhereClause(" WHERE " + getKey() + " = ?");
        conditions.add(idEntity.getId());
      }
    }

    List list = executeSelectStatement(); //execute the query

    return (list); //execute the query and returns the first entity (should never be one than more, since the entity is fetched by querying one singel key
  }

  /**
  	 * Performs fetching of requested data
  	 * Uses the properties of the specialized classes that inherits this class to generate the correct statement.
  	 * 
  	 * This methods requires that the dataBean is provided, and populated with an ID
  	 * 
  	 * @author Frode Langseth
  	 * @version 0.1
  	 * @param entityIds - List of keys (Integer) that identifies the entities to fetch
  	 * @return List of populated entities
  	 * 
  	 * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeSelect(List)
   */
  public List executeSelect(List entityIds) throws StatementException {
    StringBuffer valueString = new StringBuffer();

    /*
     * If there's content in the List, build a prepared statement string with '?' characters, and add the parameters to the conditions List
     * If there's nothing in the list, no where statement will be generated, and all rows in the table will be returned
     */
    if (!entityIds.isEmpty()) {
      conditions.clear();
      /*
       * set the conditions for the query
       */
      valueString.append(" WHERE " + getKey() + " IN (");

      Iterator i = entityIds.iterator();
      boolean firstIteration = true;

      while (i.hasNext()) {
        if (!firstIteration) {
          valueString.append(", ");
        }

        valueString.append("?");
        conditions.add(i.next());

        firstIteration = false;
      }

      valueString.append(")");

      setWhereClause(valueString.toString());
    }

    return (executeSelectStatement());
  }

  /**
   * Performs fetching of requested data
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * and get the key for the entity to update
   * It is required that the conitions is set before execution of this statement using setWhereClause
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @return List
   * @throws StatementException
   */
  private List executeSelectStatement() throws StatementException {
    LoggableStatement pstmt = null;

    //Persistable entity = (Persistable) getDataBean(); //The data bean that is going to be fetched. Needs to instantiate it to be able to populate the result from the query
    ArrayList results = new ArrayList();

    /*
        if (entity == null) {
          log.error("executeSelectStatement() - No dataBean set. The DataBean must be set before this method is called by using ObjectStatement.setDataBean method.");
          throw new StatementException("Error! Could not select any record, missing databean");
        }
    */

    try {
      int result; // The return from the execution of the statement

      // Gets connection to the database
      if (conn == null) {
        conn = SqlHelper.getConnection();
      }

      //Creates a StringBuffer to contain the generated insert statement, and the generates the statement
      StringBuffer statement = new StringBuffer("SELECT ");

      statement.append(getSelectColumnNames());
      statement.append(" FROM " + getTableName());

      String whereClause = getWhereClause();

      //if there's no Where statement, all rows in the table will be fetched
      if (whereClause != null && whereClause.length() > 0) {
        statement.append(getWhereClause());
      }

      pstmt = new LoggableStatement(conn, statement.toString());

      /*
       * Populate the prepared statement with values for "?" characters 
       * 
       * Exactly the same test as above, but the prepared statement must be created "in between" ....
       */
      if (whereClause != null && whereClause.length() > 0) {
        generateValues(pstmt);
      }

      /*
       * The following lines were commented out and replaced about generateValues (see above)
       */
      /*if (conditions != null || !conditions.isEmpty()) {
        Iterator i = conditions.iterator(); //iterate on the conditions
      
        int j = 1; //the number of the parameter in the prepared statement
        while (i.hasNext()) {
          Object condition = i.next();
      
          SqlHelper.addParameter(pstmt, j, condition); // add the condition to the prepared statement
      
          j++;
        }
        
      }*/

      // execute the select
      ResultSet rs = pstmt.executeQuery();

      if (rs == null) {
        log.error("executeSelectStatement() - no result returned.");
        throw new StatementException("Error! No result returned.");
      } else {
        results = (ArrayList) fetchListResults(rs);
      }

      rs.close();
    } catch (NamingException e) {
      log.error("executeSelectStatement() - NamingException looking up context... Statement: " + pstmt.getQueryString(), e);
      return null;
    } catch (SQLException e) {
      e.printStackTrace();
      log.error("executeSelectStatement() - SQLException looking up context... Statemet: " + pstmt.getQueryString(), e);
      return null;
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return (results);
  }

  /**
   * Performs update of existing data in a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @return boolean
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeUpdate()
   */
  public boolean executeUpdate() throws StatementException {
    Persistable entity = (Persistable) getDataBean(); //The data bean that is going to be updated. Needs to instantiate it to get the id (the id cannot cahnge, and will not be subject for update)

    if (entity == null) {
      log.error("executeUpdate() - No dataBean set. The DataBean must be set before this method is called by using ObjectStatement.setDataBean method.");
      throw new StatementException("Error! Could not update record, missing databean");
    }

    return executeUpdate(entity.getId());
  }

  /**
   * Performs update of existing data in a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @param entityId Integer
   * @return boolean
   * @throws StatementException
   * @see no.halogen.statements.ObjectStatement#executeUpdate(Integer)
   */
  public boolean executeUpdate(Integer entityId) throws StatementException {
    Connection conn = null;
    LoggableStatement pstmt = null;

    try {
      int result; // The return from the execution of the statement
      String whereClause = null;
      //If the where clause is specified. If wherClause is null, the key are used to update the correct row
      Persistable entity = (Persistable) getDataBean();
      //The data bean that is going to be updated. Needs to instantiate it to get the id (the id cannot cahnge, and will not be subject for update)

      // Gets connection to the database
      conn = SqlHelper.getConnection();

      //Creates a StringBuffer to contain the generated update statement, and the generates the statement
      StringBuffer statement = new StringBuffer("UPDATE ");

      statement.append(getTableName() + " SET " + getUpdateValuesString());
      //statement.append(" WHERE " + getKey() + " = " + entityId);

      whereClause = getWhereClause();

      if (whereClause == null) {
        statement.append(" WHERE " + getKey() + " = " + entityId);
      } else {
        statement.append(whereClause);
      }

      pstmt = new LoggableStatement(conn, statement.toString());

      /*
       * insert values for the enity that is going to be updated 
       */
      generateValues(pstmt);

      // execute the update
      result = pstmt.executeUpdate();

      if (result <= 0) {
        log.error("executeSelect() - Could not update record." + getTableName() + ", " + entityId);
        return false;
      }
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not update record.");
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context... Statement: " + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not update record.");
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return true;
  }

  /**
   * Creates a List to contain the results from a query.
   * There will be one item pr. result row in the list.
   * The object types in the List will be in accordance to the data from the result row
   *  
   * @author Frode Langseth
   * @version 0.1
   * @param rs ResultSet
   * @return List
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchListResults(ResultSet)
   */
  public List fetchListResults(ResultSet rs) throws SQLException {
    ArrayList results = new ArrayList();

    // as long as there are rows in the result set from the query
    while (rs.next() == true) {
      Object entity = null; // the object that will contain the data from a row from the query

      entity = fetchResults(rs); // fetch one result row

      if (entity != null) {
        results.add(entity); //adds the result object to the 
      }
    }

    return results;
  }
  /*    
  	public List fetchListResults(ResultSet rs) {
  		Object entity = getDataBean();
  		ArrayList results = null;
  		
  		if(entity != null) {
  			results = (ArrayList) fetchListResults(rs, entity);
  		}
  		
  		return results;
  	}
    */
  /* (non-Javadoc)
   * @see halogen.statements.ObjectStatement#fetchResults(java.sql.ResultSet)
   */
  //	public abstract boolean fetchResults(ResultSet rs, Object entity); 

  /**
   * Method fetchResults
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public abstract Object fetchResults(ResultSet rs) throws SQLException;

  /**
   * Initialises the object.
   *  
   * @author Frode Langseth
   * @version 0.1
   * 
   */
  public ObjectStatementImpl() {
    super();
  }

  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    Iterator i = getConditions().iterator();

    int j = 1;

    while (i.hasNext()) {
      SqlHelper.addParameter((LoggableStatement) pstmt, j, i.next());

      j++;
    }
  }

  /**
   * Used to get the connection for a statement. Usually not used, but if overriding classes needs to handle the connection themselves, it might be usefull
   * 
   * @author Frode Langseth
   * @version 0.1
   * @return Connection - a connection to a database
   * 
   */
  public Connection getConn() {
    return conn;
  }

  /**
   * Statements for some tables must connect to another database than default. 
   * By accessing this method, these statements can set the connection it needs.
   * 
   * @author Frode Langseth
   * @version 0.1
   * @param conn
   * 
   */
  public void setConn(Connection conn) {
    this.conn = conn;
  }

  /**
   * @return List
   * @see no.halogen.statements.ObjectStatement#getConditions()
   */
  public List getConditions() {
    return conditions;
  }

  /**
   * @param conditions
   * @see no.halogen.statements.ObjectStatement#setConditions(List)
   */
  public void setConditions(List conditions) {
    this.conditions = (ArrayList) conditions;
  }

  /**
   * Performs the deletion of data from a table.
   * Uses the properties of the specialized classes that inherits this class to generate the correct statement
   * 
   * @author Frode Langseth
   * @version 0.1
   * 
   * @param entityIds List
   * @return boolean
   * @throws StatementException
   */
  public boolean executeDelete(List entityIds) throws StatementException {
    Connection conn = null;
    LoggableStatement pstmt = null;

    try {
      int result; // The return from the execution of the statement
      String whereClause = null; //If the where clause is specified. If whereClause is null, the key are used to delete the correct row

      // Gets connection to the database
      conn = SqlHelper.getConnection();

      //Creates a StringBuffer to contain the generated delete statement, and the generates the statement
      StringBuffer statement = new StringBuffer("DELETE FROM ");

      statement.append(getTableName());

      whereClause = getWhereClause();

      if (whereClause == null) {
        statement.append(" WHERE " + getKey() + " IN (");

        Iterator i = entityIds.iterator();

        boolean firstIteration = true;
        while (i.hasNext()) {
          if (!firstIteration) {
            statement.append(", ");
          }

          statement.append("?");
          conditions.add(i.next());
        }

        statement.append(")");
      } else {
        statement.append(whereClause);
      }

      pstmt = new LoggableStatement(conn, statement.toString());

      // execute the delete
      result = pstmt.executeUpdate();

      if (result <= 0) {
        log.error("executeDelete() - Could not delete records." + getTableName());
        return false;
      }
    } catch (NamingException e) {
      log.error("executeDelete() - NamingException looking up context...", e);
      throw new StatementException("Error! Could not delete record.");
    } catch (SQLException e) {
      log.error("executeDelete() - SQLException looking up context..." + pstmt.getQueryString(), e);
      throw new StatementException("Error! Could not delete record.");
    } finally {
      SqlHelper.closeConnection(pstmt, conn);
    }

    return true;
  }

}
