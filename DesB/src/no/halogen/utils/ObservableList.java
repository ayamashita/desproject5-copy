package no.halogen.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;

/** A <CODE>List</CODE> that extends <CODE>Observable</CODE> in order to send
 * messages to the <CODE>Observer</CODE>s when it is updated.
 * <p>
 * WARNING: Calling remove-methods on this <CODE>List</CODE>s <CODE>Iterator</CODE>s
 * or <CODE>SubList</CODE>s will not notify observers on the changes.
 * @author Stian Eide
 */
public class ObservableList extends Observable implements List, Serializable {

  // TODO: Implement Iterator-wrappers and SubList-wrappers to facilitate 
  // notifications on remove-methods of these classes
  
  /** Creates an empty <CODE>ObservableList</CODE> with initial capacity of ten. */
  public ObservableList() {
    list = new ArrayList();
  }

  /** Creates an <CODE>ObservableList</CODE> without instantiating <CODE>list</CODE>
   * if <CODE>instantiateBackingList</CODE> is <CODE>false</CODE>. If not, it has the
   * same effect as <CODE>ObservableList()</CODE>.
   * @param instantiateBackingList <CODE>true</CODE> if <CODE>list</CODE> should be instantiated
   */  
  public ObservableList(boolean instantiateBackingList) {
    if(instantiateBackingList) {
      list = new ArrayList();
    }
  }
  
  /** Creates an empty <CODE>ObservableList</CODE> with the specified initial capacity.
   * @param initialCapacity the initial capacity of the list
   */  
  public ObservableList(int initialCapacity) {
    list = new ArrayList(initialCapacity);
  }
  
  /** Creates an <CODE>ObservableList</CODE> containing the elements of the specified
   * collection, in the order they are returned by the collection's iterator.
   * The ArrayList instance has an initial capacity of 110% the size of the
   * specified collection.
   * @param c the collection whose elements are to be placed into this list.
   * @throws NullPointerException if the specified collection is null
   */  
  public ObservableList(Collection c) {
    list = new ArrayList(c);
  }
  
  /** The backing <CODE>List</CODE> of this <CODE>List</CODE>. */  
  private List list;

  /** Set the backing list of this list.
   * @param list the backing list
   */  
  public void setBackingList(List list) {
    this.list = list;
  }
  
  public boolean add(Object o) {
    list.add(o);
    setChanged();
    notifyObservers();
    return(true);
  }
  
  public void add(int index, Object element) {
    list.add(index, element);
    setChanged();
    notifyObservers();
  }
  
  public boolean addAll(Collection c) {
    boolean changed = list.addAll(c);
    if(changed) {
      setChanged();
      notifyObservers();
    }
    return(changed);
  }
  
  public boolean addAll(int index, Collection c) {
    boolean changed = list.addAll(index, c);
    if(changed) {
      setChanged();
      notifyObservers();
    }
    return(changed);
  }
  
  public void clear() {
    list.clear();
    setChanged();
    notifyObservers();
  }
  
  public boolean contains(Object o) {
    return(list.contains(o));
  }
  
  public boolean containsAll(Collection c) {
    return(containsAll(c));
  }
  
  public Object get(int index) {
    return(list.get(index));
  }
  
  public int indexOf(Object o) {
    return(list.indexOf(o));
  }
  
  public boolean isEmpty() {
    return(list.isEmpty());
  }
  
  public Iterator iterator() {
    return(list.iterator());
  }
  
  public int lastIndexOf(Object o) {
    return(list.lastIndexOf(o));
  }
  
  public ListIterator listIterator() {
    return(list.listIterator());
  }
  
  public ListIterator listIterator(int index) {
    return(list.listIterator(index));
  }
  
  public boolean remove(Object o) {
    boolean changed = list.remove(o);
    if(changed) {
      setChanged();
      notifyObservers();
    }
    return(changed);
  }
  
  public Object remove(int index) {
    Object o = list.remove(index);
    setChanged();
    notifyObservers();
    return(o);
  }
  
  public boolean removeAll(Collection c) {
    boolean changed = list.removeAll(c);
    if(changed) {
      setChanged();
      notifyObservers();
    }
    return(changed);
  }
  
  public boolean retainAll(Collection c) {
    boolean changed = list.retainAll(c);
    if(changed) {
      setChanged();
      notifyObservers();
    }
    return(changed);
  }
  
  public Object set(int index, Object element) {
    Object old = list.set(index, element);
    if(!old.equals(element)) {
      setChanged();
      notifyObservers();
    }
    return(old);
  }
  
  public int size() {
    return(list.size());
  }
  
  public List subList(int fromIndex, int toIndex) {
    return(list.subList(fromIndex, toIndex));
  }
  
  public Object[] toArray() {
    return(list.toArray());
  }
  
  public Object[] toArray(Object[] a) {
    return(list.toArray(a));
  } 
}