package no.halogen.utils.table;

public class ExternalUrlTableColumn extends TableColumn {
	
	private String subProperty;
	public ExternalUrlTableColumn(String name, String labelKey, String href, String subProperty) {
		super(name, labelKey, null, href, null,null, null, null, null);
		this.subProperty = subProperty;
	}

	public ExternalUrlTableColumn(String name, String labelKey, String forward, String href, String page,
			String action, String paramName, String property, Integer maxSize) {
		super(name, labelKey, forward, href, page, action, paramName, property, maxSize);
		// TODO Auto-generated constructor stub
	}

	public ExternalUrlTableColumn(String name, String labelKey, Integer maxSize) {
		super(name, labelKey, maxSize);
		// TODO Auto-generated constructor stub
	}

	public String getSubProperty() {
		return subProperty;
	}

	public void setSubProperty(String subProperty) {
		this.subProperty = subProperty;
	}

}
