package no.halogen.utils.table;

import java.lang.IllegalAccessException;
import java.lang.NoSuchMethodException;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This is a column in a <CODE>Table</CODE>. Each column has a name, which is
 * used to extract the value of this column from the <CODE>TableSource</CODE>.
 * <p>
 * A link may be added to this column by specifying one of <CODE>forward</CODE>,
 * <CODE>href</CODE>, <CODE>page</CODE> or <CODE>action</CODE>.
 * 
 * @author Stian Eide
 */
public class TableColumn {

	private String subProperty;

	public TableColumn(String name, String labelKey, String subProperty, boolean notNeeded) {
		this(name, labelKey, null, null, null, null, null, null, null);
		this.subProperty = subProperty;
	}

	/**
	 * Creates a new <CODE>TableColumn</CODE>.
	 * 
	 * @param name
	 *            the name of this column
	 * @param labelKey
	 *            the key to the label heading in application resources
	 * @param maxSize
	 *            the maximum length of values. Longer values are clipped
	 */
	public TableColumn(String name, String labelKey, Integer maxSize) {
		this.name = name;
		this.labelKey = labelKey;
		this.maxSize = maxSize;
	}

	/**
	 * Creates a new <CODE>TableColumn</CODE>
	 * 
	 * @param name
	 *            the name of this colum
	 * @param forward
	 *            logical forward name for which to look up the context-relative
	 *            URI (if specified)
	 * @param href
	 *            URL to be utilized unmodified (if specified)
	 * @param page
	 *            module-relative page for which a URL should be created (if
	 *            specified)
	 * @param labelKey
	 *            the key to the label heading in application resources
	 * @param action
	 *            logical action name for which to look up the context-relative
	 *            URI (if specified)
	 * @param paramName
	 *            the name of the parameter, which is appended to links in this
	 *            colunm
	 * @param property
	 *            specify the property of the content of this column, which
	 *            value will be appended to links in this column
	 * @param maxSize
	 *            the maximum length of values. Longer values are clipped
	 */
	public TableColumn(String name, String labelKey, String forward, String href, String page, String action,
			String paramName, String property, Integer maxSize) {
		this.name = name;
		this.labelKey = labelKey;
		this.forward = forward;
		this.href = href;
		this.page = page;
		this.action = action;
		this.paramName = paramName;
		this.property = property;
		this.maxSize = maxSize;
		this.decorator = null;
	}

	private static Log log = LogFactory.getLog(TableColumn.class);
	private String name;
	private String labelKey;
	private String action;
	private String href;
	private String paramName;
	private String property;
	private Integer maxSize;
	private ColumnDecorator decorator;

	/** Holds value of property forward. */
	private String forward;

	/** Holds value of property page. */
	private String page;

	/**
	 * Returns the name of this column
	 * 
	 * @return the name in session where the table is stored
	 */
	public String getName() {
		return (name);
	}

	/**
	 * Sets the name of this column
	 * 
	 * @param name
	 *            New value of property name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the key to the label heading in application resources.
	 * 
	 * @return Value of property label.
	 */
	public String getLabelKey() {
		return (labelKey);
	}

	/**
	 * Sets the key to the label heading in application resources.
	 * 
	 * @param labelKey
	 *            the new value of labelKey
	 */
	public void setLabelKey(String labelKey) {
		this.labelKey = labelKey;
	}

	/**
	 * Compares two columns and returns true if their names are equal as defined
	 * by <CODE>java.lang.String</CODE>.
	 * 
	 * @param o
	 *            the column to compare to this
	 * @return <CODE>true</CODE> if the columns are equal
	 */
	public boolean equals(Object o) {
		if (o instanceof TableColumn && this.name != null && ((TableColumn) o).name != null
				&& ((TableColumn) o).name.equals(this.name)) {
			return (true);
		}
		return (false);
	}

	/**
	 * Returns the target struts action for links in this column
	 * 
	 * @return Value of property action.
	 */
	public String getAction() {
		return (action);
	}

	/**
	 * Sets the target struts action for links in this column
	 * 
	 * @param action
	 *            New value of property action.
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Returns the name of the parameter, which is appended to links in this
	 * colunm
	 * 
	 * @return Value of property paramName.
	 */
	public String getParamName() {
		return (paramName);
	}

	/**
	 * Sets the name of the parameter, which is appended to links in this colunm
	 * 
	 * @param paramName
	 *            New value of property paramName.
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * Returns the name of the property of the content of this column, which
	 * value will be appended to links in this column
	 * 
	 * @return Value of property property.
	 */
	public String getProperty() {
		return (property);
	}

	/**
	 * Sets the name of the property of the content of this column, which value
	 * will be appended to links in this column
	 * 
	 * @param property
	 *            New value of property property.
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * Returns the target url for links in this column
	 * 
	 * @return Value of property url.
	 */
	public String getHref() {
		return (href);
	}

	/**
	 * Sets the target url for links in this column
	 * 
	 * @param href
	 *            the value of href
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * Returns the value of <CODE>row</CODE> by using reflection to get the
	 * value of the property as specified in <CODE>name</CODE>.
	 * 
	 * @param row
	 *            the object from <CODE>TableSource</CODE> that represents the
	 *            current row
	 * @throws UnreadableRowValueException
	 *             if the value of this column could not be extracted from
	 *             <CODE>row</CODE>.
	 * @return an object representing the value of <CODE>row</CODE>.
	 */
	public Object getRowValue(Object row) throws UnreadableRowValueException {
		try {
			String method = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
			return (row.getClass().getMethod(method, null).invoke(row, null));
		} catch (Exception e) {
			log.error("Could not read row value for column '" + getName() + "'.", e);
			throw new UnreadableRowValueException("Could not read row value for column '" + getName() + "'.");
		}
	}

	/**
	 * Returns the maximum length for values. Longer column-values are clipped
	 * and appended with three dots ("...")
	 * 
	 * @return Value of property maxSize.
	 */
	public Integer getMaxSize() {
		return (maxSize);
	}

	/**
	 * Sets the maximum length for values. Longer column-values are clipped and
	 * appended with three dots ("...")
	 * 
	 * @param maxSize
	 *            New value of property maxSize.
	 */
	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}

	/**
	 * Getter for property decorator.
	 * 
	 * @return Value of property decorator.
	 * 
	 */
	public ColumnDecorator getDecorator() {
		return (decorator);
	}

	/**
	 * Setter for property decorator.
	 * 
	 * @param decorator
	 *            New value of property decorator.
	 * 
	 */
	public void setDecorator(ColumnDecorator decorator) {
		this.decorator = decorator;
	}

	/**
	 * Getter for property forward.
	 * 
	 * @return Value of property forward.
	 * 
	 */
	public String getForward() {
		return this.forward;
	}

	/**
	 * Setter for property forward.
	 * 
	 * @param forward
	 *            New value of property forward.
	 * 
	 */
	public void setForward(String forward) {
		this.forward = forward;
	}

	/**
	 * Getter for property page.
	 * 
	 * @return Value of property page.
	 * 
	 */
	public String getPage() {
		return this.page;
	}

	/**
	 * Setter for property page.
	 * 
	 * @param page
	 *            New value of property page.
	 * 
	 */
	public void setPage(String page) {
		this.page = page;
	}

	public String getSubProperty() {
		return subProperty;
	}

	public void setSubProperty(String subProperty) {
		this.subProperty = subProperty;
	}

}