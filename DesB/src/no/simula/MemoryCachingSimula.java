package no.simula;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.ListUtils;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.search.Criterium;
import no.halogen.search.CriteriumFactory;
import no.halogen.search.Search;
import no.halogen.search.SearchFactory;
import no.halogen.utils.ObservableList;
import no.simula.des.AdminModule;
import no.simula.des.AggregatedStudyReport;
import no.simula.des.Column;
import no.simula.des.DurationUnit;
import no.simula.des.File;
import no.simula.des.PersonalizedReport;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportSortOrder;
import no.simula.des.Study;
import no.simula.des.StudyMaterial;
import no.simula.des.StudyMaterialComparator;
import no.simula.des.StudyMaterialManager;
import no.simula.des.StudyType;
import no.simula.des.persistence.PersistentAdminModule;
import no.simula.des.persistence.PersistentColumn;
import no.simula.des.persistence.PersistentDurationUnit;
import no.simula.des.persistence.PersistentPersonalizedReport;
import no.simula.des.persistence.PersistentPrivilege;
import no.simula.des.persistence.PersistentReportCriterium;
import no.simula.des.persistence.PersistentSortOrder;
import no.simula.des.persistence.PersistentStudy;
import no.simula.des.persistence.PersistentStudyMaterial;
import no.simula.des.persistence.PersistentStudyReport;
import no.simula.des.persistence.PersistentStudyType;
import no.simula.des.search.CriteriumFactoryImpl;
import no.simula.des.search.SearchByFreetext;
import no.simula.des.search.SearchByResponsible;
import no.simula.des.search.SearchByStudyEndDate;
import no.simula.des.search.SearchByStudyType;
import no.simula.des.search.SearchFactoryImpl;
import no.simula.persistence.PersistentPerson;
import no.simula.persistence.PersistentPublication;
import no.simula.utils.FindUtils;
import no.simula.utils.PersonFamilyNameComparator;
import no.simula.utils.StudyTransformer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This is the only implementation of {@link Simula} in this distribution. This
 * particular implementation keeps all data in memory and changes to the objects
 * are both made persistent and reflected in the cache.
 * <p>
 * The exception is the content in study material of the type file. All methods
 * in the persistence service that returns a list of study material will return
 * study material of type file without content. Methods that return a single
 * file, however, will also return the content.
 */
public class MemoryCachingSimula implements Simula, Serializable {

	private PersistentPersonalizedReport persistentReports;
	private ObservableList reports;
	private PersistentSortOrder persistentSortOrders;
	private PersistentReportCriterium persistentReportCriteria;
	private ObservableList sortOptions;
	private ObservableList reportCriteria;
	private PersistentColumn persistentColumns;
	private ObservableList columns;

	/**
	 * Creates a new memory caching implementation of Simula. The cache will be
	 * populated with data from the persistent store.
	 * 
	 * @throws Exception
	 *             never (the throws clause should be removed)
	 */
	MemoryCachingSimula() throws Exception {

		persistentStudies = new PersistentStudy();
		persistentStudyMaterials = new PersistentStudyMaterial();
		persistentPublications = new PersistentPublication();
		persistentPersons = new PersistentPerson();
		persistentAggregatedStudyReport = new PersistentStudyReport();
		persistentAdminModule = new PersistentAdminModule();
		persistentDurationUnits = new PersistentDurationUnit();
		persistentStudyTypes = new PersistentStudyType();
		persistentReports = new PersistentPersonalizedReport();
		persistentSortOrders = new PersistentSortOrder();
		persistentReportCriteria = new PersistentReportCriterium();
		persistentColumns = new PersistentColumn();

		studies = new ObservableList(persistentStudies.find());
		studyTypes = new ObservableList(persistentStudyTypes.find());
		studyMaterials = new ObservableList(persistentStudyMaterials.find());
		durationUnits = new ObservableList(persistentDurationUnits.find());
		publications = new ObservableList(persistentPublications.find());
		persons = new ObservableList(persistentPersons.find());
		reports = new ObservableList(persistentReports.find());
		sortOptions = new ObservableList(persistentSortOrders.find());
		reportCriteria = new ObservableList(persistentReportCriteria.find());
		columns = new ObservableList(persistentColumns.find());
		sortPersons();

		List tmp = persistentAggregatedStudyReport.find();
		if (tmp == null || tmp.isEmpty()) {
			aggregatedStudyReport = null;
		} else {
			// Should only contain one item, so we fetch the first one
			aggregatedStudyReport = (AggregatedStudyReport) tmp.get(0);
		}

		tmp = persistentAdminModule.find();
		if (tmp == null || tmp.isEmpty()) {
			adminModule = null;
		} else {
			// Should only contain one item, so we fetch the first one
			adminModule = (AdminModule) tmp.get(0);
		}

		PersistentPrivilege pPrivs = new PersistentPrivilege();
		privileges = pPrivs.find();

		studyMaterialTypes = StudyMaterialManager.getInstance().getTypes();

		searchFactory = new SearchFactoryImpl();
		criteriumFactory = new CriteriumFactoryImpl();
	}

	private static final String CONFIG = "no/simula/simula.properties";
	private static final String UPDATE_INTERVAL = "simulaweb.update.interval";
	private static Log log = LogFactory.getLog(MemoryCachingSimula.class);

	// These data are internal to the DES application and will
	// be held in memory. Changes are written to the persistent
	// store as well as stored in memory.

	private PersistentStudy persistentStudies;
	private List studies;

	private PersistentStudyMaterial persistentStudyMaterials;
	private List studyMaterials;

	private PersistentStudyReport persistentAggregatedStudyReport;
	private AggregatedStudyReport aggregatedStudyReport;

	private PersistentAdminModule persistentAdminModule;
	private AdminModule adminModule;

	private PersistentDurationUnit persistentDurationUnits;
	private List durationUnits;

	private PersistentStudyType persistentStudyTypes;
	private List studyTypes;

	// These data are read-only, external to the application and
	// will change during the course of the application lifetime
	private PersistentPublication persistentPublications;
	private List publications;
	private PersistentPerson persistentPersons;
	private List persons;

	// These data are read-only and will not change in the
	// course of the application lifetime
	private List studyMaterialTypes;
	private List privileges;

	private SearchFactory searchFactory;
	private CriteriumFactory criteriumFactory;

	// ---------------------------------------------------------------------------
	// ///////////////// STUDY ///////////////////////
	// ---------------------------------------------------------------------------

	public Study addStudy(Study study) throws SimulaException {
		try {
			Integer id = persistentStudies.create(study);
			study.setId(id);
			studies.add(study);
			return (study);
		}

		catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to add new study.", cpoe);
			throw new SimulaException();
		}
	}

	public boolean deleteStudy(Integer id) throws SimulaException {
		try {
			Object o = studies.remove(studies.indexOf(new Study(id)));
			boolean deleted = persistentStudies.delete(id);
			if ((o != null) != deleted) {
				throw new SimulaException(
						"Failed to perform delete operation on study due to cache and persistence being unsynchronized.");
			}
			return (deleted);
		} catch (DeletePersistentObjectException dpoe) {
			log.error("Failed to delete study with id " + id + ".", dpoe);
			throw new SimulaException();
		}
	}

	public List getStudiesByCriteria(List criteria) {
		if (criteria == null) {
			return (studies);
		} else {
			Search search = searchFactory.create(STUDY_SEARCH, criteria);
			if (search == null) {
				return (null);
			} else {
				return (search.doSearch());
			}
		}
	}

	public List getStudiesByIds(List ids) {
		if (ids == null) {
			return (null);
		}
		List s = new ArrayList(ids.size());
		for (Iterator i = ids.iterator(); i.hasNext();) {
			s.add(getStudy((Integer) i.next()));
		}
		return (s);
	}

	public Study getStudy(Integer id) {
		return ((Study) studies.get(studies.indexOf(new Study(id))));
	}

	public List getStudiesByStudyMaterial(Integer id) {
		return (FindUtils.getStudiesByStudyMaterial(studies, getStudyMaterial(id)));
	}

	public List getStudiesByStudyType(Integer id) throws SimulaException {
		return (FindUtils.getStudiesByStudyType(studies, getStudyType(id)));
	}

	public List getStudiesByDurationUnit(Integer id) throws SimulaException {
		return (FindUtils.getStudiesByDurationUnit(studies, getDurationUnit(id)));
	}

	public boolean storeStudy(Study study) throws SimulaException {
		try {
			Object o = studies.set(studies.indexOf(study), study);
			boolean updated = persistentStudies.update(study);
			if ((o != null) != updated) {
				throw new SimulaException(
						"Failed to perform update operation on study due to cache and persistence being unsynchronized.");
			}
			return (updated);
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update study with id " + study.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	public Criterium getStudyByEndDateCriterium() {
		return (criteriumFactory.create(SearchByStudyEndDate.class.getName()));
	}

	public Criterium getStudyByResponsiblesCriterium() {
		return (criteriumFactory.create(SearchByResponsible.class.getName()));
	}

	public Criterium getStudyByStudyTypeCriterium() {
		return (criteriumFactory.create(SearchByStudyType.class.getName()));
	}

	public Criterium getStudyByFreetextCriterium() {
		return (criteriumFactory.create(SearchByFreetext.class.getName()));
	}

	public String getStudiesAsCSV(char fieldSeparator) {
		return (StudyTransformer.getCSV(studies, fieldSeparator));
	}

	// ---------------------------------------------------------------------------
	// ///////////////// STUDY MATERIAL ///////////////////////
	// ---------------------------------------------------------------------------

	public StudyMaterial addStudyMaterial(StudyMaterial material) throws SimulaException {
		try {
			Integer id = persistentStudyMaterials.create(material);
			material.setId(id);
			if (material instanceof File) {
				File file = (File) material;
				// Set content to null in cache to save memory
				file.setContent(null);
			}
			studyMaterials.add(material);
			return (material);
		}

		catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to add new study material.", cpoe);
			throw new SimulaException();
		}
	}

	public boolean deleteStudyMaterial(Integer id) throws SimulaException {
		try {
			Object o = studyMaterials.remove(studyMaterials.indexOf(new StudyMaterialComparator(id)));
			boolean deleted = persistentStudyMaterials.delete(id);
			if ((o != null) != deleted) {
				throw new SimulaException(
						"Failed to perform delete operation on study material due to cache and persistence being unsynchronized.");
			}
			return (deleted);
		} catch (DeletePersistentObjectException dpoe) {
			log.error("Failed to delete study material with id " + id + ".", dpoe);
			throw new SimulaException();
		}
	}

	public StudyMaterial getStudyMaterial(Integer id) {
		// Get from database instead of cache or files aren't fetched
		return ((StudyMaterial) persistentStudyMaterials.findByKey(id));
	}

	public List getStudyMaterialByIds(List ids) {
		if (ids == null) {
			return (null);
		}
		List sm = new ArrayList(ids.size());
		for (Iterator i = ids.iterator(); i.hasNext();) {
			sm.add(getStudyMaterial((Integer) i.next()));
		}
		return (sm);
	}

	public File getStudyMaterialByFilename(String name) {
		return ((File) persistentStudyMaterials.findByFilename(name));
	}

	public List getStudyMaterials() {
		return (studyMaterials);
	}

	public boolean storeStudyMaterial(StudyMaterial studyMaterial) throws SimulaException {
		try {
			boolean updated = persistentStudyMaterials.update(studyMaterial);
			if (studyMaterial instanceof File) {
				File file = (File) studyMaterial;
				// Set content to null in cache to save memory
				file.setContent(null);
			}
			Object o = studyMaterials.set(studyMaterials.indexOf(studyMaterial), studyMaterial);
			if ((o != null) != updated) {
				throw new SimulaException(
						"Failed to perform update operation on study material due to cache and persistence being unsynchronized.");
			}
			return (updated);
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update study material with id " + studyMaterial.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// ADMIN MODULE ///////////////////////
	// ---------------------------------------------------------------------------

	public AdminModule getAdminModule() {
		return (adminModule);
	}

	public boolean storeAdminModule(AdminModule adminModule) throws SimulaException {
		try {
			boolean successDb = false;
			if (this.adminModule == null) {
				Integer id = persistentAdminModule.create(adminModule);
				adminModule.setId(id);
			} else {
				successDb = persistentAdminModule.update(adminModule);
			}
			this.adminModule = adminModule;
			return (successDb);
		} catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to create admin module with id " + adminModule.getId() + ".", cpoe);
			throw new SimulaException();
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update admin module with id " + adminModule.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// AGGREGATED STUDY REPORT ///////////////////////
	// ---------------------------------------------------------------------------

	public AggregatedStudyReport getAggregatedStudyReport() {
		return (aggregatedStudyReport);
	}

	public boolean storeAggregatedStudyReport(AggregatedStudyReport report) throws SimulaException {
		try {
			this.aggregatedStudyReport = report;
			return (persistentAggregatedStudyReport.update(report));
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update aggregated study report with id " + report.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// PERSONS ///////////////////////
	// ---------------------------------------------------------------------------

	public Person getPerson(String id) {
		// change to webservice call
		return ((Person) ListUtils.get(persons, id));
	}

	public Person getPersonByEmail(String email) {
		return (FindUtils.getPersonByEmail(persons, email));
	}

	public List getPersons() {
		return (persons);
	}

	public Privilege getPrivilege(Integer id) {
		return ((Privilege) ListUtils.get(privileges, id));
	}

	public List getPrivileges() {
		return (privileges);
	}

	public List getPersonsByPrivilege(Privilege priv) {
		return (FindUtils.getPersonsByPrivilege(persons, priv));
	}

	public Integer updatePersons(List persons) throws SimulaException {
		try {
			int count = 0;
			for (Iterator i = persons.iterator(); i.hasNext();) {
				Person person = (Person) i.next();
				int index = this.persons.indexOf(person);
				if (index >= 0) {
					this.persons.set(index, person);
					count++;
				}
			}
			int dbCount = persistentPersons.update(persons);
			if (count != dbCount) {
				throw new SimulaException(
						"Failed to perform update on list of persons due to cache and persistence being unsynchronized.");
			}
			return (new Integer(count));
		}

		catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update list of persons.", upoe);
			throw new SimulaException("Failed to update list of persons.");
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// PUBLICATIONS ///////////////////////
	// ---------------------------------------------------------------------------

	public Publication getPublication(String id) {
		return ((Publication) ListUtils.get(publications, id));
	}

	public List getPublications() {
		return (publications);
	}

	// ---------------------------------------------------------------------------
	// ///////////////// STUDY TYPES ///////////////////////
	// ---------------------------------------------------------------------------

	public StudyType addStudyType(StudyType type) throws SimulaException {
		try {
			Integer id = persistentStudyTypes.create(type);
			type.setId(id);
			studyTypes.add(type);
			return (type);
		}

		catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to add new study type.", cpoe);
			throw new SimulaException();
		}
	}

	public boolean deleteStudyType(Integer id) throws SimulaException {
		try {
			Object o = studyTypes.remove(studyTypes.indexOf(new StudyType(id)));
			boolean deleted = persistentStudyTypes.delete(id);
			if ((o != null) != deleted) {
				throw new SimulaException(
						"Failed to perform delete operation on study type due to cache and persistence being unsynchronized.");
			}
			return (deleted);
		} catch (DeletePersistentObjectException dpoe) {
			log.error("Failed to delete study type with id " + id + ".", dpoe);
			throw new SimulaException();
		}
	}

	public List getStudyTypes() {
		return (studyTypes);
	}

	public StudyType getStudyType(Integer id) {
		return ((StudyType) studyTypes.get(studyTypes.indexOf(new StudyType(id))));
	}

	public List getStudyTypesByIds(List ids) {
		if (ids == null) {
			return (null);
		}
		List st = new ArrayList(ids.size());
		for (Iterator i = ids.iterator(); i.hasNext();) {
			st.add(getStudyType((Integer) i.next()));
		}
		return (st);
	}

	public boolean storeStudyType(StudyType type) throws SimulaException {
		try {
			Object o = studyTypes.set(studyTypes.indexOf(type), type);
			boolean updated = persistentStudyTypes.update(type);
			if ((o != null) != updated) {
				throw new SimulaException(
						"Failed to perform update operation on study type due to cache and persistence being unsynchronized.");
			}
			return (updated);
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update study type with id " + type.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// DURATION UNITS ///////////////////////
	// ---------------------------------------------------------------------------

	public DurationUnit addDurationUnit(DurationUnit unit) throws SimulaException {
		try {
			Integer id = persistentDurationUnits.create(unit);
			unit.setId(id);
			durationUnits.add(unit);
			return (unit);
		}

		catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to add new study type.", cpoe);
			throw new SimulaException();
		}
	}

	public boolean deleteDurationUnit(Integer id) throws SimulaException {
		try {
			Object o = durationUnits.remove(durationUnits.indexOf(new DurationUnit(id)));
			boolean deleted = persistentDurationUnits.delete(id);
			if ((o != null) != deleted) {
				throw new SimulaException(
						"Failed to perform delete operation on duration unit due to cache and persistence being unsynchronized.");
			}
			return (deleted);
		} catch (DeletePersistentObjectException dpoe) {
			log.error("Failed to delete duration unit with id " + id + ".", dpoe);
			throw new SimulaException();
		}
	}

	public List getDurationUnits() {
		return (durationUnits);
	}

	public DurationUnit getDurationUnit(Integer id) {
		return ((DurationUnit) durationUnits.get(durationUnits.indexOf(new DurationUnit(id))));
	}

	public List getDurationUnitsByIds(List ids) {
		if (ids == null) {
			return (null);
		}
		List st = new ArrayList(ids.size());
		for (Iterator i = ids.iterator(); i.hasNext();) {
			st.add(getDurationUnit((Integer) i.next()));
		}
		return (st);
	}

	public boolean storeDurationUnit(DurationUnit type) throws SimulaException {
		try {
			Object o = durationUnits.set(durationUnits.indexOf(type), type);
			boolean updated = persistentDurationUnits.update(type);
			if ((o != null) != updated) {
				throw new SimulaException(
						"Failed to perform update operation on duration unit due to cache and persistence being unsynchronized.");
			}
			return (updated);
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update duration unit with id " + type.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	// ---------------------------------------------------------------------------
	// ///////////////// HELPER METHODS ///////////////////////
	// ---------------------------------------------------------------------------

	private void sortPersons() {
		Collections.sort(this.persons, new PersonFamilyNameComparator());
	}

	/** Updates external data by reading them from the persistent store. */
	public void updateExternalData() {
		log.debug("Updating External Data");
		persons = persistentPersons.find();
		publications = persistentPublications.find();
	}

	public PersonalizedReport getReport(Integer reportId) throws SimulaException {
		return ((PersonalizedReport) reports.get(reports.indexOf(new PersonalizedReport(reportId))));
	}

	public List getReportCriteria() throws SimulaException {
		return reportCriteria;
	}

	public List getReportSortOptions() throws SimulaException {
		return sortOptions;
	}

	public boolean storeReport(PersonalizedReport report) throws SimulaException {
		try {
			Object o = reports.set(reports.indexOf(report), report);
			boolean updated = persistentReports.update(report);
			if ((o != null) != updated) {
				throw new SimulaException(
						"Failed to perform update operation on report due to cache and persistence being unsynchronized.");
			}
			return (updated);
		} catch (UpdatePersistentObjectException upoe) {
			log.error("Failed to update report with id " + report.getId() + ".", upoe);
			throw new SimulaException();
		}
	}

	public PersonalizedReport addReport(PersonalizedReport report) throws SimulaException {
		try {
			Integer id = persistentReports.create(report);
			report.setId(id);
			reports.add(report);
			return (report);
		}

		catch (CreatePersistentObjectException cpoe) {
			log.error("Failed to add new report.", cpoe);
			throw new SimulaException();
		}
	}

	public List getReportColumns() throws SimulaException {
		return columns;
	}

	public Column getReportColumn(Integer id) {
		return ((Column) columns.get(columns.indexOf(new Column(id))));
	}

	public List getReports() throws SimulaException {
		return reports;
	}

	public ReportCriterium getCriteriumById(Integer id) throws SimulaException {
		return ((ReportCriterium) reportCriteria.get(reportCriteria.indexOf(new ReportCriterium(id))));
	}

	public ReportSortOrder getSortOrderById(Integer id) throws SimulaException {
		return ((ReportSortOrder) sortOptions.get(sortOptions.indexOf(new ReportSortOrder(id))));
	}

	public List getReportsByIds(List deletableReportIds) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public List getReportsByOwner(String remoteUser) {
		PersistentPersonalizedReport ppr = new PersistentPersonalizedReport();

		return ppr.findByOwnerId(remoteUser);
	}

	public void deleteReport(Integer id) throws SimulaException{
		PersistentPersonalizedReport ppr = new PersistentPersonalizedReport();
		
		try {
			ppr.delete(id);
		} catch (DeletePersistentObjectException e) {
			e.printStackTrace();
			throw new SimulaException(e);
		}
	}

	public List getStudiesByCriteriaAndSorting(List criteria, List sorting) throws SimulaException {
		if (criteria == null) {
			return (studies);
		} else {
			Search search = searchFactory.create(STUDY_SEARCH, criteria, sorting);
			if (search == null) {
				return (null);
			} else {
				return (search.doSearch());
			}
		}
	}
}