package no.simula;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import no.halogen.persistence.Persistable;

/** A privilege that a {@link Person} may have in some system. More specifically
 * this is used in the Simula DES-application to model the privilege(s) that
 * persons have in this system.
 * <p>
 * A privilege has a name, which represents the system-name of this privilege. I.e
 * in a container manages security context like that of DES running on Tomcat, the
 * name of the privileges will be equal to the role-names. See the
 * Servlet-specification for more information on roles.
 */
public class Privilege implements Persistable, Serializable {

  /** Creates an empty privilege. */  
  public Privilege() {}
  
  /** Creates a privilege with the specified id.
   * @param id the id
   */  
  public Privilege(Integer id) {
    this.id = id;
  }
  
  /** Creates a privilege with the specified id and name.
   * @param id the id
   * @param name the name of the privilege
   */  
  public Privilege(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  private Integer id;
  private String name;
  
  /** Returns the name of this privilage
   * @return name of privilage.
   *
   */
  public String getName() {
    return(name);
  }  
  
  public Integer getId() {
    return(id);
  }
  
  public void setId(Integer id) {
    this.id = id;
  } 
  
  /** Sets the name of this privilege.
   * @param name the name of this privilege
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /** Returns whether this privilege is equal to another privilege. Two privileges are
   * equal if their ids are equal.
   * @param o another privilege
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(((Privilege)o).id.equals(this.id));
  }
}