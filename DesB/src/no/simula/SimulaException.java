package no.simula;

/** This exception is thrown if an error occurs in <CODE>Simula</CODE> and the
 * application layer needs to be informed of this.
 * <p>
 * If this exception is thrown, it should be logged as an error and the user should
 * be informed that an error occured. This exception may indicate a bug in the
 * <CODE>Simula</CODE>-implementation or the persistency service. Alternatively it
 * may indicate that there is a problem in the persistent store.
 * @author Stian Eide
 */
public class SimulaException extends Exception {
 
  public SimulaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SimulaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

/** Creates a new <CODE>SimulaException</CODE>. */  
  public SimulaException() {
    super();
  }
  
  /** Creates a new <CODE>SimulaException</CODE> with the specified message.
   * @param message a message that explains the error.
   */  
  public SimulaException(String message) {
    super(message);
  }
}