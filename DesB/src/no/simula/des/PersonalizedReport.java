package no.simula.des;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.Persistable;
import no.simula.Person;

public class PersonalizedReport implements Persistable, Serializable {

	public final static String REPORT_TABLE = "report";
	public final static String REPORT_ID = "report_id";
	public final static String REPORT_NAME = "report_name";
	public final static String PEOPLE_ID = "people_id";

	public final static String REPORT_COLUMN_TABLE = "report_column_rel";
	public final static String REPORT_RESPONSIBLE_TABLE = "report_responsible_rel";
	
	public final static String REPORT_COLUMN_ORDER = "report_column_order";
	
	private List columns;
	private List responsibles;
	private ReportSortOrder timeSortOrder;
	private ReportCriterium responsibleCriterium;
	private Integer id;
	private String name;

	private Person owner;
	
	public PersonalizedReport(){}

	public PersonalizedReport(Integer id) {
		super();
		this.id = id;
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
		this.columnNames = null;
	}

	public List getResponsibles() {
		return responsibles;
	}

	public void setResponsibles(List responsibles) {
		this.responsibles = responsibles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public ReportSortOrder getTimeSortOrder() {
		return timeSortOrder;
	}

	public void setTimeSortOrder(ReportSortOrder timeSortOrder) {
		this.timeSortOrder = timeSortOrder;
	}

	public ReportCriterium getResponsibleCriterium() {
		return responsibleCriterium;
	}

	public void setResponsibleCriterium(ReportCriterium responsibleCriteria) {
		this.responsibleCriterium = responsibleCriteria;
	}
	
	private HashMap columnNames = null;
	
	protected HashMap getColumnNamesMap() {
		if (columnNames == null) {
			columnNames = new HashMap();
			Iterator iter = this.getColumns().iterator();
			while (iter.hasNext()) {
				Column col = (Column)iter.next();
				columnNames.put(col.getName(),col);
			}
		}
		return columnNames;
	}
	
	public boolean containsColumn(String name) {
		return (getColumnNamesMap().keySet().contains(name));
	}
	
	public Column getColumnByName (String name) {
		if (getColumnNamesMap().containsKey(name)) {
			return (Column)getColumnNamesMap().get(name);
		}
		return null;
	}
	
	/** Returns whether to reports are equal. Two reports are by definition equal when
	   * their ids are equal.
	   * @return <CODE>true</CODE> if <CODE>this.id.equals(obj.id)</CODE>
	   * @param obj the other study
	   */
	  public boolean equals(Object obj) {
	    if(obj instanceof PersonalizedReport && ((PersonalizedReport)obj).id.equals(this.id)) {
	      return(true);
	    }
	    else {
	      return(false);
	    }
	  }
}
