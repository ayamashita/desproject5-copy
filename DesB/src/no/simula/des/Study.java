package no.simula.des;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import no.halogen.persistence.Persistable;
import no.simula.Person;
import no.simula.Publication;

/** A study is, in addition to {@link no.simula.Simula}, the main object of the
 * Simula DES application. A study contains study material and publications, and
 * has one or more persons as responsible.
 */
public class Study implements Persistable, Serializable {
  
  /** Creates an empty study. */  
  public Study() {
    studyMaterial = new ArrayList();
    publications = new ArrayList();
    responsibles = new ArrayList();
  }
  
  /** Creates a study with the specified id.
   * @param id the id
   */  
  public Study(Integer id) {
    this.id = id;
    studyMaterial = new ArrayList();
    publications = new ArrayList();
    responsibles = new ArrayList();
  }
  
  private Integer id;
  private String description;
  private Integer duration;
  private Date end;
  private String keywords;
  private String name;
  private Integer noOfProfessionals;
  private Integer noOfStudents;
  private String notes;
  private Date start;
  private Person lastEditedBy;
  private Person ownedBy;
  private List studyMaterial;
  private List publications;
  private List responsibles;
  
  /** Holds value of property type. */
  private StudyType type;
  
  /** Holds value of property durationUnit. */
  private DurationUnit durationUnit;
  
  /** Adds an publication to this study.
   * @param publication the publication
   */  
  public void addPublication(Publication publication) {
    if(!publications.contains(publication)) {
      publications.add(publication);
    }
  }
  
  /** Adds a responsible to this study
   * @param responsible the responsible person
   */  
  public void addResponsible(Person responsible) {
    if(!responsibles.contains(responsible)) {
      responsibles.add(responsible);
    }
  }
  
  /** Adds study material to this study.
   * @param material the study material
   */  
  public void addStudyMaterial(StudyMaterial material) {
    if(!this.studyMaterial.contains(material)) {
      this.studyMaterial.add(material);
//      material.addStudy(this);
    }
  }
  
  /** Returns a list of publications attached to this study.
   * @return a list of attached publications
   */  
  public List getPublications() {
    return(publications);
  }
  
  /** Returns a list of persons responsible for this study.
   * @return a list of responsible persons
   */  
  public List getResponsibles() {
    return(responsibles);
  }
  
  /** Returns a list of study material attached to this study.
   * @return a list of attached study material
   */  
  public List getStudyMaterial() {
    return(studyMaterial);
  }
  
  /** Detaches an attached publication from this study.
   * @param publication the publication to detach
   */  
  public void removePublication(Publication publication) {
    publications.remove(publication);
  }
  
  /** Removes a person as responsible for this study.
   * @param responsible the person to remove as responsible
   */  
  public void removeResponsible(Person responsible) {
    responsibles.remove(responsible);
  }
  
  /** Detaches an attached study material from this study.
   * @param material the study material to detach
   */  
  public void removeStudyMaterial(StudyMaterial material) {
    this.studyMaterial.remove(material);
//    material.removeStudy(this);
  }

  /** Getter for property description.
   * @return Value of property description.
   *
   */
  public String getDescription() {
    return(description);
  }
  
  /** Setter for property description.
   * @param description New value of property description.
   *
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  /** Getter for property duration.
   * @return Value of property duration.
   *
   */
  public Integer getDuration() {
    return(duration);
  }
  
  /** Setter for property duration.
   * @param duration New value of property duration.
   *
   */
  public void setDuration(Integer duration) {
    this.duration = duration;
  }
  
  /** Getter for property end.
   * @return Value of property end.
   *
   */
  public Date getEnd() {
    return(end);
  }
  
  /** Setter for property end.
   * @param end New value of property end.
   *
   */
  public void setEnd(Date end) {
    this.end = end;
  }
  
  /** Getter for property keywords.
   * @return Value of property keywords.
   *
   */
  public String getKeywords() {
    return(keywords);
  }
  
  /** Setter for property keywords.
   * @param keywords New value of property keywords.
   *
   */
  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }
  
  /** Getter for property lastEditedBy.
   * @return Value of property lastEditedBy.
   *
   */
  public Person getLastEditedBy() {
    return(lastEditedBy);
  }
  
  /** Setter for property lastEditedBy.
   * @param lastEditedBy New value of property lastEditedBy.
   *
   */
  public void setLastEditedBy(Person lastEditedBy) {
    this.lastEditedBy = lastEditedBy;
  }
  
  /** Getter for property name.
   * @return Value of property name.
   *
   */
  public String getName() {
    return(name);
  }
  
  /** Setter for property name.
   * @param name New value of property name.
   *
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /** Returns the number of professional participants in this study.
   * @return the number of professional participants
   */
  public Integer getNoOfProfessionals() {
    return(noOfProfessionals);
  }
  
  /** Sets the number of professional participants in this study.
   * @param noOfProfessionals the number of professional participants
   */
  public void setNoOfProfessionals(Integer noOfProfessionals) {
    this.noOfProfessionals = noOfProfessionals;
  }
  
  /** Return the number of student participants in this study.
   * @return the number of student participants
   */
  public Integer getNoOfStudents() {
    return(noOfStudents);
  }
  
  /** Sets the number of student participants in this study.
   * @param noOfStudents the number of student participants
   */
  public void setNoOfStudents(Integer noOfStudents) {
    this.noOfStudents = noOfStudents;
  }
  
  /** Getter for property notes.
   * @return Value of property notes.
   *
   */
  public String getNotes() {
    return(notes);
  }
  
  /** Setter for property notes.
   * @param notes New value of property notes.
   *
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }
  
  /** Getter for property ownedBy.
   * @return Value of property ownedBy.
   *
   */
  public Person getOwnedBy() {
    return(ownedBy);
  }
  
  /** Setter for property ownedBy.
   * @param ownedBy New value of property ownedBy.
   *
   */
  public void setOwnedBy(Person ownedBy) {
    this.ownedBy = ownedBy;
  }
  
  /** Getter for property start.
   * @return Value of property start.
   *
   */
  public Date getStart() {
    return(start);
  }
  
  /** Setter for property start.
   * @param start New value of property start.
   *
   */
  public void setStart(Date start) {
    this.start = start;
  }

  /** Getter for property id.
   * @return Value of property id.
   *
   */
  public Integer getId() {
    return(id);
  }

  /** Setter for property id.
   * @param id New value of property id.
   *
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /** Returns whether to studies are equal. Two studies are by definition equal when
   * their ids are equal.
   * @return <CODE>true</CODE> if <CODE>this.id.equals(obj.id)</CODE>
   * @param obj the other study
   */
  public boolean equals(Object obj) {
    if(obj instanceof Study && ((Study)obj).id.equals(this.id)) {
      return(true);
    }
    else {
      return(false);
    }
  }
  
  /** Setter for property responsibles.
   * @param responsibles New value of property responsibles.
   *
   */
  public void setResponsibles(List responsibles) {
    this.responsibles = responsibles;
  }
  
  /** Setter for property publications.
   * @param publications New value of property publications.
   *
   */
  public void setPublications(List publications) {
    this.publications = publications;
  }  
  
  /** Getter for property type.
   * @return Value of property type.
   *
   */
  public StudyType getType() {
    return(type);
  }
  
  /** Setter for property type.
   * @param type New value of property type.
   *
   */
  public void setType(StudyType type) {
    this.type = type;
  }
  
  /** Getter for property durationUnit.
   * @return Value of property durationUnit.
   *
   */
  public DurationUnit getDurationUnit() {
    return(durationUnit);
  }
  
  /** Setter for property durationUnit.
   * @param durationUnit New value of property durationUnit.
   *
   */
  public void setDurationUnit(DurationUnit durationUnit) {
    this.durationUnit = durationUnit;
  }
  
  /** Setter for property studyMaterial.
   * @param studyMaterial New value of property materials.
   *
   */
  public void setStudyMaterial(List studyMaterial) {
    this.studyMaterial = studyMaterial;
  }
  
  public String getDurationString() {
	  return getDuration() + " " + getDurationUnit().getName(); 
  }
}