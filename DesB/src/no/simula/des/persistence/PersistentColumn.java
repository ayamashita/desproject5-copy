package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.Column;
import no.simula.des.Study;
import no.simula.des.statements.ColumnStatement;
import no.simula.des.statements.ReportColumnRelStatement;
import no.simula.des.statements.StudyStatement;

import org.apache.log4j.Logger;

public class PersistentColumn implements PersistentObject {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(PersistentSortOrder.class);

	public List find() {
		List results = new ArrayList(); // will contain the results

		Column column = new Column();

		ColumnStatement statement = new ColumnStatement();

		statement.setDataBean(column);

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of all sort orders failed", e);
			return (null);
		}

		return (results);
	}

	public Object findByKey(Integer id) {
		Object result = new Object();
		Column column = new Column();

		ColumnStatement statement = new ColumnStatement();

		statement.setDataBean(column);

		try {
			result = (ColumnStatement) statement.executeSelect(id);
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of column " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof Column) {
			column = (Column) result;

			if (column.getId() == null || !column.getId().equals(id)) {
				column.setId(id);
			}

		}
		return (column);
	}

	public List findByKeys(List entityIds) {
		List results = new ArrayList(); // will contain the results

		if (!entityIds.isEmpty()) {

			if (entityIds == null) {
				entityIds = new ArrayList(0);
			}

			StringBuffer stmtWhere = new StringBuffer();

			Column column = new Column();

			ColumnStatement columnStmt = new ColumnStatement();

			columnStmt.setDataBean(column);

			if (!entityIds.isEmpty()) {
				stmtWhere.append(" WHERE " + Column.COLUMN_ID + " IN (");

				Iterator i = entityIds.iterator();
				boolean firstIteration = true;

				while (i.hasNext()) {
					if (!firstIteration) {
						stmtWhere.append(", ");
					}

					stmtWhere.append("?");

					columnStmt.getConditions().add(i.next());
					firstIteration = false;
				}
				
				stmtWhere.append(")");
			}

			columnStmt.setWhereClause(stmtWhere.toString());
			try {
				results = (ArrayList) columnStmt.executeSelect();
			} catch (StatementException e) {
				log.error("findBykeys() - Fetching of columms " + entityIds.toString() + " failed!!", e);
				return (null);
			}
		}

		return (results);

	}

	// *** These methods should be never called since this is dictionary table
	public Integer create(Object instance) throws CreatePersistentObjectException {
		return null;
	}

	public boolean delete(Integer id) throws DeletePersistentObjectException {
		return false;
	}

	public boolean update(Object instance) throws UpdatePersistentObjectException {
		return false;
	}

	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		return false;
	}

}
