package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.Persistable;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.Person;
import no.simula.des.Column;
import no.simula.des.PersonalizedReport;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportSortOrder;
import no.simula.des.Study;
import no.simula.des.statements.PersonalizedReportStatement;
import no.simula.des.statements.ReportColumnRelStatement;
import no.simula.des.statements.ReportResponsibleRelStatement;
import no.simula.des.statements.StudyStatement;
import no.simula.persistence.PersistentPerson;

import org.apache.log4j.Logger;

public class PersistentPersonalizedReport implements PersistentObject {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(PersistentPersonalizedReport.class);

	public Integer create(Object instance) throws CreatePersistentObjectException {
		Integer reportId = new Integer(0);

		PersonalizedReport report = (PersonalizedReport) instance;

		PersonalizedReportStatement reportStmt = new PersonalizedReportStatement();
		reportStmt.setDataBean(report);

		try {
			// store report in db
			reportId = reportStmt.executeInsert();

			// store report columns
			setColumns(reportId, report);

			// store report responsibles
			setResponsibles(reportId, report);
		} catch (StatementException e) {
			log.error(e.getMessage());
			e.printStackTrace();

			throw new CreatePersistentObjectException(e);
		}

		return reportId;
	}

	private void setResponsibles(Integer reportId, PersonalizedReport report) throws StatementException {
		ReportResponsibleRelStatement reportRespRelStmt = new ReportResponsibleRelStatement();

		List responsibles = report.getResponsibles();

		if (!responsibles.isEmpty()) {
			Iterator i = (Iterator) responsibles.iterator();
			List valuePair = new ArrayList();
			Person person = null;

			while (i.hasNext()) {
				valuePair.clear();

				valuePair.add(reportId);

				person = (Person) i.next();
				valuePair.add(person.getId());

				reportRespRelStmt.setDataBean(valuePair);

				reportRespRelStmt.executeInsert();
			}
		}

	}

	private void setColumns(Integer reportId, PersonalizedReport report) throws StatementException {
		ReportColumnRelStatement reportColRelStmt = new ReportColumnRelStatement();

		List responsibles = report.getColumns();

		if (!responsibles.isEmpty()) {
			Iterator i = (Iterator) responsibles.iterator();
			List valuePair = new ArrayList();
			Column column = null;

			while (i.hasNext()) {
				valuePair.clear();

				valuePair.add(reportId);

				column = (Column) i.next();
				valuePair.add(column.getId());

				reportColRelStmt.setDataBean(valuePair);

				reportColRelStmt.executeInsert();
			}
		}

	}

	public boolean delete(Integer id) throws DeletePersistentObjectException {

		boolean result = false;

		ReportResponsibleRelStatement reportRespRelStmt = new ReportResponsibleRelStatement();

		try {
			result = reportRespRelStmt.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to people");

			throw new DeletePersistentObjectException();
		}

		reportRespRelStmt = null;

		ReportColumnRelStatement reportColRelStmt = new ReportColumnRelStatement();

		try {
			result = reportColRelStmt.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to report columns");

			throw new DeletePersistentObjectException();
		}

		reportColRelStmt = null;

		PersonalizedReportStatement studyStatement = new PersonalizedReportStatement();

		/* Delete the study */
		try {
			result = studyStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete report");

			throw new DeletePersistentObjectException();
		}

		return (result);
	}

	public List find() {
		List results = new ArrayList(); // will contain the results

		List row = new ArrayList();

		PersonalizedReport report = new PersonalizedReport();

		PersonalizedReportStatement studyStatement = new PersonalizedReportStatement();

		studyStatement.setDataBean(report);

		try {
			results = (ArrayList) studyStatement.executeSelect();
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of all reports failed", e);
			return (null);
		}

		populateReports(results);

		return (results);

	}

	private void populateReports(List results) {
		Object result = null;
		List row = null;
		PersonalizedReport report = null;

		// Fetches the result row by row, and populates it with the remaining
		// data
		Iterator iRow = results.iterator();

		while (iRow.hasNext()) {
			result = (Object) iRow.next();

			if (result instanceof List) {
				row = (List) result;
				report = (PersonalizedReport) row.get(0); // gets the study,
															// populated with
				// data
			} else if (result instanceof PersonalizedReport) {
				report = (PersonalizedReport) result;
			}

			populateReport(report);
		}

	}

	public Object findByKey(Integer id) {
		Object result = new Object();
		PersonalizedReport report = new PersonalizedReport();

		PersonalizedReportStatement reportStatement = new PersonalizedReportStatement();

		reportStatement.setDataBean(report);

		try {
			result = (PersonalizedReport) reportStatement.executeSelect(id);
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of study " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof PersonalizedReport) {
			report = (PersonalizedReport) result;

			if (report.getId() == null || !report.getId().equals(id)) {
				report.setId(id);
			}

			populateReport(report);
		}
		return (report);
	}

	private void populateReport(PersonalizedReport report) {
		if (report.getTimeSortOrder() != null && report.getTimeSortOrder().getId() != null) {
			report.setTimeSortOrder(getSortOrder(report.getTimeSortOrder().getId()));
		}

		if (report.getResponsibleCriterium() != null && report.getResponsibleCriterium().getId() != null) {
			report.setResponsibleCriterium(getResponsibleCriterium(report.getResponsibleCriterium().getId()));
		}

		if (report.getOwner() != null && report.getOwner().getId() != null) {
			report.setOwner(getPerson(report.getOwner().getId()));
		}

		report.setColumns(getColumns(report.getId()));
		report.setResponsibles(getResponsibles(report.getId()));
	}

	private Person getPerson(String id) {
		PersistentPerson persistentPerson = new PersistentPerson();

		return (Person) (persistentPerson.findByKey(id));
	}

	private ReportCriterium getResponsibleCriterium(Integer id) {
		PersistentReportCriterium persistentReportCriterium = new PersistentReportCriterium();
		return (ReportCriterium) persistentReportCriterium.findByKey(id);
	}

	private ReportSortOrder getSortOrder(Integer id) {
		PersistentSortOrder persistentSortOrder = new PersistentSortOrder();
		return (ReportSortOrder) persistentSortOrder.findByKey(id);
	}

	private List getResponsibles(Integer id) {
		PersistentPerson persistentPerson = new PersistentPerson();

		return (persistentPerson.findResponsibleByReportId(id));
	}

	private List getColumns(Integer id) {
		ReportColumnRelStatement statement = new ReportColumnRelStatement();

		ArrayList columnIds = new ArrayList(); // List to receive result from
		Integer colId = new Integer(0);

		statement.setDataBean(colId);

		statement.setWhereClause(" WHERE report_id = ?");
		statement.getConditions().add(id);

		try {
			columnIds = (ArrayList) statement.executeSelect();

		} catch (StatementException e) {
			log.error("getColumns() - Fetching of columns related to report " + id + " failed!!", e);
			return (null);
		}

		PersistentColumn pc = new PersistentColumn();
		return pc.findByKeys(columnIds);
	}

	public List findByKeys(List entityIds) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean update(Object instance) throws UpdatePersistentObjectException {
		return update(((Persistable) instance).getId(), instance);
	}

	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		PersonalizedReport report = (PersonalizedReport) instance;
		boolean result = false;

		// update responsibles
		ReportResponsibleRelStatement respStmt = new ReportResponsibleRelStatement();
		try {
			result = respStmt.executeDelete(id);
			setResponsibles(id, report);
		} catch (StatementException e) {
			log.error("Update() - could not delete responsibles for report with id " + id);
			log.error(e);
		}

		// update columns
		ReportColumnRelStatement colStmt = new ReportColumnRelStatement();
		try {
			result = colStmt.executeDelete(id);
			setColumns(id, report);
		} catch (StatementException e) {
			log.error("Update() - could not delete columns for report with id " + id);
			log.error(e);
		}

		PersonalizedReportStatement prs = new PersonalizedReportStatement();

		prs.setDataBean(report);

		try {
			result = prs.executeUpdate(id);
		} catch (StatementException e) {
			log.error("Update() - could not report with id " + id);
			log.error(e);
			throw new UpdatePersistentObjectException(e);
		}

		return result;
	}

	public List findByOwnerId(String ownerId) {
		PersonalizedReportStatement prs = new PersonalizedReportStatement();

		PersonalizedReport report = new PersonalizedReport();

		prs.setDataBean(report);
		prs.setWhereClause(" WHERE " + PersonalizedReport.PEOPLE_ID + " = ?");
		prs.getConditions().add(ownerId);

		List results = new ArrayList();
		try {
			results = prs.executeSelect();
		} catch (StatementException e) {
			log.error("Error while fetching reports for person : " + ownerId);
			log.error(e);
		}

		populateReports(results);

		return results;
	}

}
