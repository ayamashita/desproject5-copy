package no.simula.des.persistence;

import java.util.List;

import junit.framework.TestCase;

public class PersistentPersonalizedReportTest extends TestCase {
	public void testSearchByOwner() throws Exception {
		PersistentPersonalizedReport search = new PersistentPersonalizedReport();
		List result = search.findByOwnerId("aiko");
		assertTrue(result.size() == 1);
	}
}
