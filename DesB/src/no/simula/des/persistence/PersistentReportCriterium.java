package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.ReportCriterium;
import no.simula.des.statements.ReportCriteriumStatement;

import org.apache.log4j.Logger;

public class PersistentReportCriterium implements PersistentObject {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(PersistentReportCriterium.class);

	public List find() {
		List results = new ArrayList(); // will contain the results

		ReportCriterium criterium = new ReportCriterium();

		ReportCriteriumStatement criteriumStatement = new ReportCriteriumStatement();

		criteriumStatement.setDataBean(criterium);

		try {
			results = (ArrayList) criteriumStatement.executeSelect();
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of all criteria failed", e);
			return (null);
		}

		return (results);
	}

	public Object findByKey(Integer id) {
		Object result = new Object();
		ReportCriterium criterium = new ReportCriterium();

		ReportCriteriumStatement critStmt = new ReportCriteriumStatement();

		critStmt.setDataBean(criterium);

		try {
			result = (ReportCriterium) critStmt.executeSelect(id);
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of criterium " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof ReportCriterium) {
			criterium = (ReportCriterium) result;

			if (criterium.getId() == null || !criterium.getId().equals(id)) {
				criterium.setId(id);
			}

		}
		return (criterium);
	}

	public List findByKeys(List entityIds) {
		// TODO Auto-generated method stub
		return null;
	}

	// *** These methods should be never called since this is dictionary table
	public Integer create(Object instance) throws CreatePersistentObjectException {
		return null;
	}

	public boolean delete(Integer id) throws DeletePersistentObjectException {
		return false;
	}

	public boolean update(Object instance) throws UpdatePersistentObjectException {
		return false;
	}

	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		return false;
	}

}
