/**
 * @(#) PersistentStudy.java
 */

package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.Persistable;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.ObjectJoinableStatement;
import no.halogen.statements.ObjectJoinableStatementImpl;
import no.halogen.statements.StatementException;
import no.simula.Person;
import no.simula.Publication;
import no.simula.des.DurationUnit;
import no.simula.des.Study;
import no.simula.des.StudyType;
import no.simula.des.statements.DurationUnitStatement;
import no.simula.des.statements.PersonStudyRelStatement;
import no.simula.des.statements.StudyKeywordsStatement;
import no.simula.des.statements.StudyMaterialStudyRelStatement;
import no.simula.des.statements.StudyPersonRelStatement;
import no.simula.des.statements.StudyPublicationRelStatement;
import no.simula.des.statements.StudyStatement;
import no.simula.des.statements.StudyStudyMaterialRelStatement;
import no.simula.des.statements.StudyTypeStatement;
import no.simula.persistence.PersistentPerson;
import no.simula.persistence.PersistentPublication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 */

public class PersistentStudy implements PersistentObject {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(PersistentStudy.class);

	/**
	 * Constructor for PersistentStudy
	 */
	public PersistentStudy() {
		super();
	}

	/**
	 * Creates a new study object
	 * 
	 * 
	 * @param instance -
	 *            The study that will be saved
	 * @return the identificator of the new study (the identificator is
	 *         automaticly generated and unique)
	 * 
	 * @throws CreatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#create(Object)
	 */
	public Integer create(Object instance) throws CreatePersistentObjectException {
		Integer studyId = new Integer(0);

		Study study = (Study) instance;

		StudyStatement studyStatement = new StudyStatement();

		/* Tells the statement what study object to save */
		studyStatement.setDataBean(study);

		/* Save the study */
		try {
			studyId = studyStatement.executeInsert();

			setKeywords(studyId, study);
			setMaterial(studyId, study);
			setPublications(studyId, study);
			setResponsibles(studyId, study);

		} catch (StatementException se) {
			if (log.isTraceEnabled()) {
				log.trace("Couldn't create study");
			}

			se.printStackTrace();

			throw new CreatePersistentObjectException();
		}

		return (studyId);
	}

	/**
	 * Deletes a existing study NBNB!! This is not part of transaction controll,
	 * it really should be ......
	 * 
	 * @param id -
	 *            The id of the study to delete
	 * 
	 * @return boolean
	 * @throws DeletePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(Integer id) throws DeletePersistentObjectException {
		boolean result = false;

		StudyStudyMaterialRelStatement studyStudyMaterialRelStatement = new StudyStudyMaterialRelStatement();

		try {
			result = studyStudyMaterialRelStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to study material");

			throw new DeletePersistentObjectException();
		}

		studyStudyMaterialRelStatement = null;

		StudyPublicationRelStatement studyPublicationRelStatement = new StudyPublicationRelStatement();

		try {
			result = studyPublicationRelStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to study material");

			throw new DeletePersistentObjectException();
		}

		StudyPersonRelStatement studyPersonRelStatement = new StudyPersonRelStatement();

		try {
			result = studyPersonRelStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to study responsibles");

			throw new DeletePersistentObjectException();
		}

		StudyKeywordsStatement studyKeywordsStatement = new StudyKeywordsStatement();

		studyKeywordsStatement.setWhereClause(" WHERE sk_stu_id = ?");
		studyKeywordsStatement.getConditions().add(id);

		try {
			result = studyKeywordsStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete relations to study material", se);

			throw new DeletePersistentObjectException();
		}

		StudyStatement studyStatement = new StudyStatement();

		/* Delete the study */
		try {
			result = studyStatement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete study");

			throw new DeletePersistentObjectException();
		}

		return (result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see halogen.persistence.PersistentObject#findByKey(int)
	 */
	/**
	 * Method findByKey
	 * 
	 * @param id
	 *            Integer
	 * @return Object
	 * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
	 */
	public Object findByKey(Integer id) {
		StringBuffer stmtWhere = new StringBuffer();

		Object result = new Object();
		Study study = new Study();

		StudyStatement studyStatement = new StudyStatement();

		studyStatement.setDataBean(study);

		try {
			result = (Study) studyStatement.executeSelect(id);
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of study " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof Study) {
			study = (Study) result;

			if (study.getId() == null || !study.getId().equals(id)) {
				study.setId(id);
			}

			populateStudy(study);
		}
		return (study);
	}

	/**
	 * Updates a existing study
	 * 
	 * @param instance -
	 *            the study to update
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Object)
	 */
	public boolean update(Object instance) throws UpdatePersistentObjectException {
		Study study = (Study) instance;

		return (update(study.getId(), instance));
	}

	/**
	 * Updates a existing study
	 * 
	 * @param id -
	 *            The id of the study to update
	 * @param instance -
	 *            the study to update
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
	 */
	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		boolean result = false;

		Study study = (Study) instance;
		/*
		 * Update Study material relations
		 */

		// First, remove existing keywords relations, then set the new ones
		StudyKeywordsStatement studyKeywordsStatement = new StudyKeywordsStatement();

		try {
			result = studyKeywordsStatement.executeDelete(id);

			setKeywords(id, study);
		} catch (StatementException e) {
			log.error("update() - Could not delete keywords from study.");
		}

		// Then, remove existing study material relations, then set the new ones
		StudyStudyMaterialRelStatement studyStudyMaterialStatement = new StudyStudyMaterialRelStatement();

		try {
			result = studyStudyMaterialStatement.executeDelete(id);

			setMaterial(id, study);
		} catch (StatementException e) {
			log.error("update() - Could not delete study material from person.");
		}

		// And, remove existing publication relations, then set the new ones
		StudyPublicationRelStatement studyPublicationStatement = new StudyPublicationRelStatement();

		try {
			result = studyPublicationStatement.executeDelete(id);

			setPublications(id, study);
		} catch (StatementException e) {
			log.error("update() - Could not delete publication from person.");
		}

		// Almost there now , remove existing responsible relations, then set
		// the new ones
		PersonStudyRelStatement personStudyStatement = new PersonStudyRelStatement();

		try {
			result = personStudyStatement.executeDelete(id);

			setResponsibles(id, study);
		} catch (StatementException e) {
			log.error("update() - Could not delete publication from person.");
		}

		StudyStatement studyStatement = new StudyStatement();

		/* Tells the statement what study object to update */
		studyStatement.setDataBean(study);

		/* Update the study */
		try {
			result = studyStatement.executeUpdate(id);
		} catch (StatementException se) {
			log.error("Couldn't update study");

			se.printStackTrace();

			throw new UpdatePersistentObjectException();
		}

		return (result);
	}

	/*
	 * Get keywords related to a study
	 */
	/**
	 * Method getKeywords
	 * 
	 * @param id
	 *            Integer
	 * @return String
	 */
	private String getKeywords(Integer id) {
		StringBuffer keywords = new StringBuffer();
		ArrayList keywordsList = null;
		List keyword = new ArrayList();

		StudyKeywordsStatement studyKeywordsStatement = new StudyKeywordsStatement();
		studyKeywordsStatement.setDataBean(keyword);
		studyKeywordsStatement.setWhereClause(" WHERE " + studyKeywordsStatement.getKey() + " = ?");
		studyKeywordsStatement.getConditions().add(id);

		try {
			keywordsList = (ArrayList) studyKeywordsStatement.executeSelect();
		} catch (StatementException e) {
			log.error("getKeywords() - Fetching keywords related to study " + id + " failed!!", e);
			return (null);
		}

		/*
		 * For each keyword, populate a string with the keywords, separated by
		 * comma
		 */
		if (keywordsList != null) {
			/*
			 * The first keyword is fetched outside the for loop, as a comma
			 * first in the keywords string is an error
			 */
			Iterator iter = keywordsList.iterator();

			List row = null; // The row contains both the study id and the
								// actual keyword
			boolean firstIteration = true;

			while (iter.hasNext()) {
				if (!firstIteration) {
					keywords.append(", ");
				}

				row = (ArrayList) iter.next();
				keywords.append(row.get(1));

				firstIteration = false;
			}
		}

		return (keywords.toString());
	}

	/**
	 * Method getPublications
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getPublications(Integer id) {
		PersistentPublication persistentPublication = new PersistentPublication();

		return (persistentPublication.findByStudy(id));
	}

	/**
	 * Method getPerson
	 * 
	 * @param id
	 *            Integer
	 * @return Person
	 */
	private Person getPerson(String id) {
		PersistentPerson persistentPerson = new PersistentPerson();

		return (Person) (persistentPerson.findByKey(id));
	}

	/**
	 * Method getResponsibles
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getResponsibles(Integer id) {
		PersistentPerson persistentPerson = new PersistentPerson();

		return (persistentPerson.findResponsibleByStudy(id));
	}

	/**
	 * Method getMaterial
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getMaterial(Integer id) {
		PersistentStudyMaterial persistentStudyMaterial = new PersistentStudyMaterial();

		return (persistentStudyMaterial.findByStudy(id));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKeys(java.util.List)
	 */
	/**
	 * Method findByKeys
	 * 
	 * @param entityIds
	 *            List
	 * @return List
	 * @see no.halogen.persistence.PersistentObject#findByKeys(List)
	 */
	public List findByKeys(List entityIds) {
		List results = new ArrayList(); // will contain the results

		if (!entityIds.isEmpty()) {
			List row = new ArrayList();

			if (entityIds == null) {
				entityIds = new ArrayList(0);
			}

			StringBuffer stmtWhere = new StringBuffer();

			Study study = new Study();

			StudyStatement studyStatement = new StudyStatement();

			studyStatement.setDataBean(study);

			if (!entityIds.isEmpty()) {
				stmtWhere.append("WHERE stu_id IN (");

				Iterator i = entityIds.iterator();
				boolean firstIteration = true;

				while (i.hasNext()) {
					if (!firstIteration) {
						stmtWhere.append(", ");
					}

					stmtWhere.append("?");

					studyStatement.getConditions().add(i.next());

					firstIteration = false;
				}
				
				stmtWhere.append(")");
			}

			studyStatement.setWhereClause(stmtWhere.toString()); // tells the
																	// statement
																	// how to
																	// join the
																	// tables

			try {
				results = (ArrayList) studyStatement.executeSelect();
			} catch (StatementException e) {
				log.error("findBykey() - Fetching of studies " + entityIds.toString() + " failed!!", e);
				return (null);
			}

			populateStudies(results);
		}

		return (results);

	}

	/**
	 * Method populateStudies
	 * 
	 * @param results
	 *            List
	 */
	private void populateStudies(List results) {
		Object result = null;
		List row = null;
		Study study = null;

		// Fetches the result row by row, and populates it with the remaining
		// data
		Iterator iRow = results.iterator();

		while (iRow.hasNext()) {
			result = (Object) iRow.next();

			if (result instanceof List) {
				row = (List) result;
				study = (Study) row.get(0); // gets the study, populated with
											// data
			} else if (result instanceof Study) {
				study = (Study) result;
			}

			populateStudy(study);
		}
	}

	/**
	 * Method populateStudy
	 * 
	 * @param study
	 *            Study
	 */
	private void populateStudy(Study study) {
		/*
		 * Gets the study type, and populates the study with it
		 */
		study.setType(getStudyType(study.getId()));

		/*
		 * Gets the duration unit, and populates the study with it
		 */
		study.setDurationUnit(getDurationUnit(study.getId()));

		Person ownedBy = getPerson(study.getOwnedBy().getId());
		if (ownedBy != null && ownedBy instanceof Person) {
			study.setOwnedBy(ownedBy);
		}

		if (study.getLastEditedBy() != null) {
			Person lastEditedBy = getPerson(study.getLastEditedBy().getId());

			if (lastEditedBy != null && lastEditedBy instanceof Person) {
				study.setLastEditedBy(lastEditedBy);
			}
		}

		study.setKeywords(getKeywords(study.getId()));
		study.setPublications(getPublications(study.getId()));
		study.setResponsibles(getResponsibles(study.getId()));
		study.setStudyMaterial(getMaterial(study.getId()));
	}

	/**
	 * Method setResponsibles
	 * 
	 * @param studyId
	 *            Integer
	 * @param study
	 *            Study
	 * @throws StatementException
	 */
	private void setResponsibles(Integer studyId, Study study) throws StatementException {
		StudyPersonRelStatement studyPersonRelStatement = new StudyPersonRelStatement();

		List responsibles = study.getResponsibles();

		if (!responsibles.isEmpty()) {
			Iterator i = (Iterator) responsibles.iterator();
			List valuePair = new ArrayList();
			Person person = null;
			Integer j;

			while (i.hasNext()) {
				valuePair.clear();

				valuePair.add(studyId);

				person = (Person) i.next();
				valuePair.add(person.getId());

				studyPersonRelStatement.setDataBean(valuePair);

				j = studyPersonRelStatement.executeInsert();
			}
		}
	}

	/**
	 * Method setPublications
	 * 
	 * @param studyId
	 *            Integer
	 * @param study
	 *            Study
	 * @throws StatementException
	 */
	private void setPublications(Integer studyId, Study study) throws StatementException {
		StudyPublicationRelStatement studyPublicationRelStatement = new StudyPublicationRelStatement();

		List publications = study.getPublications();

		if (!publications.isEmpty()) {
			Iterator i = (Iterator) publications.iterator();
			List valuePair = new ArrayList();
			Publication publication = null;
			Integer j;

			while (i.hasNext()) {
				valuePair.clear();

				valuePair.add(studyId);

				publication = (Publication) i.next();
				valuePair.add(publication.getId());

				studyPublicationRelStatement.setDataBean(valuePair);

				j = studyPublicationRelStatement.executeInsert();
			}
		}
	}

	/**
	 * Method setMaterial
	 * 
	 * @param studyId
	 *            Integer
	 * @param study
	 *            Study
	 * @throws StatementException
	 */
	private void setMaterial(Integer studyId, Study study) throws StatementException {
		StudyStudyMaterialRelStatement studyStudyMaterialRelStatement = new StudyStudyMaterialRelStatement();

		List studyMaterials = study.getStudyMaterial();

		if (!studyMaterials.isEmpty()) {
			Iterator i = (Iterator) studyMaterials.iterator();
			List valuePair = new ArrayList();
			Persistable studyMaterial = null;
			Integer j;

			while (i.hasNext()) {
				valuePair.clear();

				valuePair.add(studyId);

				studyMaterial = (Persistable) i.next();
				valuePair.add(studyMaterial.getId());

				studyStudyMaterialRelStatement.setDataBean(valuePair);

				j = studyStudyMaterialRelStatement.executeInsert();
			}
		}
	}

	/**
	 * Method setKeywords
	 * 
	 * @param studyId
	 *            Integer
	 * @param study
	 *            Study
	 * @throws StatementException
	 */
	private void setKeywords(Integer studyId, Study study) throws StatementException {
		StudyKeywordsStatement statement = new StudyKeywordsStatement();
		String keywords = new String();
		List valuePair = new ArrayList();

		keywords = study.getKeywords();

		if (keywords != null) {
			StringTokenizer st = new StringTokenizer(keywords);

			while (st.hasMoreTokens()) {
				valuePair.clear();
				valuePair.add(studyId);
				valuePair.add(st.nextToken(",").trim());

				statement.setDataBean(valuePair);

				statement.executeInsert();
			}
		}
	}

	/**
	 * Finding all Studies
	 * 
	 * @return - All studies in the database
	 * 
	 * @see no.halogen.persistence.PersistentObject#find()
	 */
	public List find() {
		List results = new ArrayList(); // will contain the results

		List row = new ArrayList();

		Study study = new Study();

		StudyStatement studyStatement = new StudyStatement();

		studyStatement.setDataBean(study);

		try {
			results = (ArrayList) studyStatement.executeSelect();
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of all studies failed", e);
			return (null);
		}

		populateStudies(results);

		return (results);

	}

	/**
	 * Finds study material by a study id, that is, finds the study material
	 * related to a study
	 * 
	 * @param id -
	 *            a study id
	 * @return List with populated study material objects.
	 * 
	 */

	public List findByStudyMaterial(Integer id) {
		List queryCollection = new ArrayList();
		List results = new ArrayList();
		Study study = null; // dataBean that will contain the values from the
							// search

		StringBuffer stmt = new StringBuffer();

		StudyStatement studyStatement = new StudyStatement();
		StudyMaterialStudyRelStatement studyMaterialStudyRelStatement = new StudyMaterialStudyRelStatement();

		studyStatement.setDataBean(study);

		ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
		queryCollection.add(studyStatement);
		queryCollection.add(studyMaterialStudyRelStatement);

		stmt.append(" WHERE stusm_sm_id = ?");
		statement.getConditions().add(id);

		stmt.append(" AND stusm_stu_id = stu_id");

		statement.setWhereClause(stmt.toString());

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("findByStudyMaterial() - Fetching of Study for study material with id " + id + " failed!!", e);
			return (null);
		}

		/*
		 * Populate the studies in the result with study type, duration unit,
		 * study material, publications, responsibles, etc
		 */
		populateStudies(results);

		return (results);
	}

	/*
	 * Get study type related to a study
	 */
	/**
	 * Method getStudyType
	 * 
	 * @param id
	 *            Integer
	 * @return StudyType
	 */
	private StudyType getStudyType(Integer id) {
		ArrayList queryCollection = new ArrayList(); // will contain all the
														// objects representing
														// the tables included
														// in the statement
		ArrayList results = new ArrayList(); // will contain the results
		ArrayList rows = new ArrayList();

		ObjectJoinableStatement statement = new ObjectJoinableStatementImpl();

		StudyType studyType = new StudyType();

		StudyTypeStatement studyTypeStatement = new StudyTypeStatement();
		StudyStatement studyStatement = new StudyStatement();

		studyTypeStatement.setDataBean(studyType);

		queryCollection.add(studyTypeStatement);
		queryCollection.add(studyStatement);

		statement.setQueryCollection(queryCollection);

		statement.setWhereClause("WHERE stu_id = ?  AND stu_sty_id = sty_id");
		statement.getConditions().add(id);

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("getStudyType() - Fetching study type related to study " + id + " failed!!", e);
			return (null);
		}

		/*
		 * For each keyword, populate a string with the keywords, separated by
		 * comma
		 */
		if (results != null) {

			/*
			 * The first uses a joined statement to fetch the study type First,
			 * get the row result, from the joined statement Then, get the study
			 * type from the row
			 */
			Iterator row = results.iterator();

			while (row.hasNext()) {
				rows = (ArrayList) row.next();

				Iterator resultIter = rows.iterator();

				while (resultIter.hasNext()) {
					Object result = resultIter.next();

					if (result instanceof StudyType) {
						studyType = (StudyType) result;
					}
				}
			}
		}

		return (studyType);
	}

	/*
	 * Get duration unit related to a study
	 */
	/**
	 * Method getDurationUnit
	 * 
	 * @param id
	 *            Integer
	 * @return DurationUnit
	 */
	private DurationUnit getDurationUnit(Integer id) {
		ArrayList queryCollection = new ArrayList(); // will contain all the
														// objects representing
														// the tables included
														// in the statement
		ArrayList results = new ArrayList(); // will contain the results
		ArrayList rows = new ArrayList();

		ObjectJoinableStatement statement = new ObjectJoinableStatementImpl();

		DurationUnit durationUnit = new DurationUnit();

		DurationUnitStatement durationUnitStatement = new DurationUnitStatement();
		StudyStatement studyStatement = new StudyStatement();

		durationUnitStatement.setDataBean(durationUnit);

		queryCollection.add(durationUnitStatement);
		queryCollection.add(studyStatement);

		statement.setQueryCollection(queryCollection);

		statement.setWhereClause("WHERE stu_id = ? AND stu_du_id = du_id");
		statement.getConditions().add(id);

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("getStudyType() - Fetching duration unit related to study " + id + " failed!!", e);
			return (null);
		}

		/*
		 * For each keyword, populate a string with the keywords, separated by
		 * comma
		 */
		if (results != null) {

			/*
			 * The first uses a joined statement to fetch the study type First,
			 * get the row result, from the joined statement Then, get the study
			 * type from the row
			 */
			Iterator row = results.iterator();

			while (row.hasNext()) {
				rows = (ArrayList) row.next();

				Iterator resultIter = rows.iterator();

				while (resultIter.hasNext()) {
					Object result = resultIter.next();

					if (result instanceof DurationUnit) {
						durationUnit = (DurationUnit) result;
					}
				}
			}
		}

		return (durationUnit);
	}
}
