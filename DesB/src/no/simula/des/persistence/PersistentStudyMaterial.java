/*
 * Created on 30.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.persistence.*;
import no.halogen.persistence.PersistentObject;
import no.halogen.statements.ObjectJoinableStatementImpl;
import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.StudyMaterial;
import no.simula.des.statements.StudyMaterialFileAttrStatement;
import no.simula.des.statements.StudyMaterialLinkStatement;
import no.simula.des.statements.StudyMaterialStatement;
import no.simula.des.statements.StudyMaterialFileStatement;
import no.simula.des.statements.StudyStudyMaterialRelStatement;
import no.simula.des.statements.utils.StudyMaterialStatementFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyMaterial implements PersistentObject {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentStudyMaterial.class);

  /**
   * Creates a new study material object
   * 
   * @param instance - The study material that will be saved
   * @return the identificator of the new study material (the identificator is automaticly generated and unique)
   * 
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
    Integer studyMaterialId = new Integer(0);

    StudyMaterial studyMaterial = (StudyMaterial) instance;

    StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

    /* Tells the statement what study object to save */
    studyMaterialStatement.setDataBean(studyMaterial);

    /* Save the study material*/
    try {
      studyMaterialId = studyMaterialStatement.executeInsert();
    } catch (StatementException se) {
      log.error("Couldn't create study material");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }

    setMaterialType(studyMaterialId, instance);

    return (studyMaterialId);
  }

  /**
   * Method setMaterialType
   * @param studyMaterialId Integer
   * @param instance Object
   * @throws CreatePersistentObjectException
   */
  private void setMaterialType(Integer studyMaterialId, Object instance) throws CreatePersistentObjectException {
    ObjectStatementImpl materialStatement = StudyMaterialStatementFactory.create(instance);

    Persistable entity = (Persistable) instance;
    entity.setId(studyMaterialId);

    materialStatement.setDataBean(instance);

    /* Save the study material*/
    try {
      Integer i = materialStatement.executeInsert();
    } catch (StatementException se) {
      log.error("Couldn't create study material");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }
  }

  /**
   * Deletes existing study material
   * 
   * @param id - The id of the study material to delete
   * 
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Integer id) throws DeletePersistentObjectException {
    boolean result = false;

    /*
     * It's unknown if the study material is a file or link, must try to delete from both
     */

    StudyMaterialFileStatement studyMaterialFileStatement = new StudyMaterialFileStatement();

    try {
      result = studyMaterialFileStatement.executeDelete(id);
    } catch (StatementException e) {
      if (log.isInfoEnabled()) {
        log.info("Could not delete file for study material " + id, e);
      }
    }

    /*
     * ... and then the link ...
     */
    if (!result) {
      StudyMaterialLinkStatement studyMaterialLinkStatement = new StudyMaterialLinkStatement();

      try {
        studyMaterialLinkStatement.executeDelete(id);
      } catch (StatementException e) {
        if (log.isInfoEnabled()) {
          log.info("Could not delete link for study material " + id, e);
          throw new DeletePersistentObjectException();
        }
      }
    }

    StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

    /* Delete the study material*/
    try {
      result = studyMaterialStatement.executeDelete(id);
    } catch (StatementException se) {
      log.error("Couldn't delete study material");

      se.printStackTrace();

      throw new DeletePersistentObjectException();
    }

    return (result);
  }

  /**
   * Finds study material by it's id/key
   * @param id - The study material's id/key
   * @return Populated StudyMaterial object
   * 
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Integer id) {
    List queryCollection = new ArrayList();
    List results = new ArrayList();
    StudyMaterial studyMaterial = null; //dataBean that will contain the values from the search

    /*
     * Since StudyMaterial can be either a file or link, two statements must be executed.
     * First, get the potential Files related to a study
     */
    StudyMaterialFileStatement studyMaterialFileStatement = new StudyMaterialFileStatement();
    StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

    studyMaterial = new File();
    studyMaterial.setId(id);

    studyMaterialFileStatement.setDataBean(studyMaterial);

    ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
    queryCollection.add(studyMaterialFileStatement);
    queryCollection.add(studyMaterialStatement);

    statement.setQueryCollection(queryCollection);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKey() - Fetching of Study Material (File) with id " + id + " failed!!", e);
      return (null);
    }

    /*
     * If there were no study material available, check if there might be a link ...
     */
    if (results.isEmpty()) {
      queryCollection.clear();

      StudyMaterialLinkStatement studyMaterialLinkStatement = new StudyMaterialLinkStatement();

      studyMaterial = new Link();
      studyMaterial.setId(id);

      studyMaterialLinkStatement.setDataBean(studyMaterial);

      statement = null;
      statement = new ObjectJoinableStatementImpl();
      queryCollection.add(studyMaterialLinkStatement);
      queryCollection.add(studyMaterialStatement);

      statement.setQueryCollection(queryCollection);

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKey() - Fetching of Study Material (Link) with id " + id + " failed!!", e);
        return (null);
      }
    }

    if (results.isEmpty()) {
      return (null);
    }
		
		List row = (List) results.get(0);

    return (row.get(0));
  }

  /**
   * Finds study material by a list of identities/key
   * @param entityIds - A List of study material ids, each id as an Integer. If the List is empty, all publications will be returned 
   * @return List with populated Study material objects. 
   * 
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    ArrayList results = null;

    if (!entityIds.isEmpty()) {
      List queryCollection = new ArrayList();
      List resultsFile = new ArrayList();
      List resultsLink = new ArrayList();
      StudyMaterial studyMaterial = null; //dataBean that will contain the values from the search

      StringBuffer stmt = new StringBuffer();

      /*
       * Since StudyMaterial can be either a file or link, two statements must be executed.
       * First, get the potential Files related to a study
       */
      StudyMaterialFileAttrStatement studyMaterialFileAttrStatement = new StudyMaterialFileAttrStatement();
      StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

      studyMaterial = new File();

      studyMaterialFileAttrStatement.setDataBean(studyMaterial);

      ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
      queryCollection.add(studyMaterialFileAttrStatement);
      queryCollection.add(studyMaterialStatement);

      statement.setQueryCollection(queryCollection);

      if (entityIds != null) {
        stmt.append(" WHERE smf_sm_id IN (");

        Iterator i = entityIds.iterator();

        boolean firstIteration = true;
        while (i.hasNext()) {
          if (!firstIteration) {
            stmt.append(", ");
          }

          stmt.append("? ");
          statement.getConditions().add(i.next());
        }
        // the join between sm_studymaterial and smf_smfile
        stmt.append(" AND sm_id = smf_sm_id");
      } else { //No parameters, then just the join between sm_studymaterial and smf_smfile, and get all files
        stmt.append(" WHERE sm_id = smf_sm_id");
      }

      statement.setWhereClause(stmt.toString());

      try {
        resultsFile = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of Study Material (File) with id " + entityIds + " failed!!", e);
        return (null);
      }

      /*
       * Even if Files were found, since a List of Ids is input, there might be links as well ... 
       */
      queryCollection.clear();
      stmt.setLength(0);

      StudyMaterialLinkStatement studyMaterialLinkStatement = new StudyMaterialLinkStatement();

      studyMaterial = new Link();

      studyMaterialLinkStatement.setDataBean(studyMaterial);

      statement = null;
      statement = new ObjectJoinableStatementImpl();
      queryCollection.add(studyMaterialLinkStatement);
      queryCollection.add(studyMaterialStatement);

      statement.setQueryCollection(queryCollection);

      if (entityIds != null) {
        statement.getConditions().clear();

        stmt.append(" WHERE sml_sm_id IN (");

        Iterator i = entityIds.iterator();

        boolean firstIteration = true;
        while (i.hasNext()) {
          if (!firstIteration) {
            stmt.append(", ");
          }

          stmt.append("? ");
          statement.getConditions().add(i.next());
        }
        // the join between sm_studymaterial and sml_smlink
        stmt.append(" AND sm_id = sml_sm_id");
      } else { //No parameters, then just the join between sm_studymaterial and sml_smlink, and get all files
        stmt.append(" WHERE sm_id = sml_sm_id");
      }

      statement.setWhereClause(stmt.toString());

      try {
        resultsLink = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKey() - Fetching of Study Material (Link) with id " + entityIds + " failed!!", e);
        return (null);
      }

      Iterator iFile = resultsFile.iterator();

      while (iFile.hasNext()) {
        List row = (List) iFile.next();
        results.add(row.get(0));
      }

      Iterator iLink = resultsLink.iterator();

      while (iLink.hasNext()) {
        List row = (List) iLink.next();
        results.add(row.get(0));
      }
    }
    return (results);
  }

  /**
   * Finds study material by a study id, that is, finds the study material related to a study
   * @param id - a study id 
   * @return List with populated study material objects.
   * 
   */

  public List findByStudy(Integer id) {
    List queryCollection = new ArrayList();
    List results = new ArrayList();
    List resultsFile = new ArrayList();
    List resultsLink = new ArrayList();
    StudyMaterial studyMaterial = null; //dataBean that will contain the values from the search

    StringBuffer stmt = new StringBuffer();

    /*
     * Since StudyMaterial can be either a file or link, two statements must be executed.
     * First, get the potential Files related to a study
     */
    StudyMaterialFileAttrStatement studyMaterialFileAttrStatement = new StudyMaterialFileAttrStatement();
    StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();
    StudyStudyMaterialRelStatement studyStudyMaterialRelStatement = new StudyStudyMaterialRelStatement();

    studyMaterial = new File();

    studyMaterialFileAttrStatement.setDataBean(studyMaterial);

    ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
    queryCollection.add(studyMaterialFileAttrStatement);
    queryCollection.add(studyMaterialStatement);
    queryCollection.add(studyStudyMaterialRelStatement);

    statement.setQueryCollection(queryCollection);

    stmt.append(" WHERE stusm_stu_id = ?");
    statement.getConditions().add(id);

    stmt.append(" AND stusm_sm_id = sm_id");
    stmt.append(" AND sm_id = smf_sm_id");

    statement.setWhereClause(stmt.toString());

    try {
      resultsFile = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKeys() - Fetching of Study Material (File) for study with id " + id + " failed!!", e);
      return (null);
    }

    /*
     * Even if Files were found, since a List of Ids is input, there might be links as well ... 
     */
    queryCollection.clear();

    studyMaterialStatement = null;
    studyStudyMaterialRelStatement = null;

    StudyMaterialLinkStatement studyMaterialLinkStatement = new StudyMaterialLinkStatement();
    studyMaterialStatement = new StudyMaterialStatement();
    studyStudyMaterialRelStatement = new StudyStudyMaterialRelStatement();

    studyMaterial = new Link();

    studyMaterialLinkStatement.setDataBean(studyMaterial);

    statement.getConditions().clear();
    statement = null;

    statement = new ObjectJoinableStatementImpl();
    queryCollection.add(studyMaterialLinkStatement);
    queryCollection.add(studyMaterialStatement);
    queryCollection.add(studyStudyMaterialRelStatement);

    statement.setQueryCollection(queryCollection);

    stmt = new StringBuffer();

    stmt.append(" WHERE stusm_stu_id = ?");
    statement.getConditions().add(id);

    stmt.append(" AND stusm_sm_id = sm_id");
    stmt.append(" AND sm_id = sml_sm_id");

    statement.setWhereClause(stmt.toString());

    try {
      resultsLink = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKey() - Fetching of Study Material (Link) for study with id " + id + " failed!!", e);
      return (null);
    }

    Iterator iFile = resultsFile.iterator();

    while (iFile.hasNext()) {
      List row = (List) iFile.next();
      results.add(row.get(0));
    }

    Iterator iLink = resultsLink.iterator();

    while (iLink.hasNext()) {
      List row = (List) iLink.next();
      results.add(row.get(0));
    }

    return (results);
  }

  /**
   * Updates existing study material
   * 
   * @param id - The id of the study material to update
   * @param instance - the study material to update
   * 
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
    /*
     * Must perform update in two steps:
     * - first the general study material update
     * - then the specialized (file or link) 
     */

    boolean result = false;

    StudyMaterialStatement statement = new StudyMaterialStatement();

    statement.setDataBean(instance);

    /* Update the study material */
    try {
      result = statement.executeUpdate(id);
    } catch (StatementException se) {
      log.error("Couldn't update study material");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    }

    /*
     * Since the existing material type might change (e.g. from File to Link, or the other way), we deletes the existing type, and creates a new entity in the database
     */
    ObjectStatementImpl materialStatement = StudyMaterialStatementFactory.create(instance);

    try {
      result = materialStatement.executeDelete(id);

      setMaterialType(id, instance);
    } catch (StatementException se) {
      log.error("Couldn't update study material");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    } catch (CreatePersistentObjectException e) {
      log.error("update() - Could not create new study material type during update of: " + id, e);
      throw new UpdatePersistentObjectException();
    }

    return (result);
  }

  /**
   * Updates existing study material
   * 
   * @param instance - the study material to update
   * 
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
    Persistable entity = (Persistable) instance;

    return (update(entity.getId(), instance));
  }

  /**
   * Finding all Studiy Material
   * @return - All study material in the database
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {

    List results = new ArrayList();

    List queryCollection = new ArrayList();
    List resultsFile = new ArrayList();
    List resultsLink = new ArrayList();
    StudyMaterial studyMaterial = null; //dataBean that will contain the values from the search

    StringBuffer stmt = new StringBuffer();

    /*
     * Since StudyMaterial can be either a file or link, two statements must be executed.
     * First, get the potential Files related to a study
     */
    StudyMaterialFileAttrStatement studyMaterialFileAttrStatement = new StudyMaterialFileAttrStatement();
    StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

    studyMaterial = new File();

    studyMaterialFileAttrStatement.setDataBean(studyMaterial);

    ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
    queryCollection.add(studyMaterialFileAttrStatement);
    queryCollection.add(studyMaterialStatement);

    stmt.append(" WHERE sm_id = smf_sm_id");

    statement.setQueryCollection(queryCollection);
    statement.setWhereClause(stmt.toString());

    try {
      resultsFile = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKeys() - Fetching of all Study Material (File) failed!!", e);
      return (null);
    }

    /*
     * Even if Files were found, since a List of Ids is input, there might be links as well ... 
     */
    queryCollection.clear();
    stmt.setLength(0);

    StudyMaterialLinkStatement studyMaterialLinkStatement = new StudyMaterialLinkStatement();

    studyMaterial = new Link();

    studyMaterialLinkStatement.setDataBean(studyMaterial);

    statement = null;
    statement = new ObjectJoinableStatementImpl();
    queryCollection.add(studyMaterialLinkStatement);
    queryCollection.add(studyMaterialStatement);

    stmt.append(" WHERE sm_id = sml_sm_id");

    statement.setQueryCollection(queryCollection);
    statement.setWhereClause(stmt.toString());
    statement.setQueryCollection(queryCollection);

    try {
      resultsLink = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKey() - Fetching of all Study Material (Link) failed!!", e);
      return (null);
    }

    Iterator iFile = resultsFile.iterator();

    while (iFile.hasNext()) {
      if (results == null) {
        results = new ArrayList();
      }

      List row = (List) iFile.next();
      results.add(row.get(0));
    }

    Iterator iLink = resultsLink.iterator();

    while (iLink.hasNext()) {
      if (results == null) {
        results = new ArrayList();
      }

      List row = (List) iLink.next();
      results.add(row.get(0));
    }

    return (results);

  }

  /**
   * Finds study material by a list of identities/key
   * 
   * @param fileName - A List of study material ids, each id as an Integer. If the List is empty, all publications will be returned
   * @return List with populated Study material objects. 
   * 
   */
  public Object findByFilename(String fileName) {
    File studyMaterial = new File(); //dataBean that will contain the values from the search
    List results = new ArrayList();

    if (fileName.length() > 0) {
      List queryCollection = new ArrayList();

      StringBuffer stmt = new StringBuffer();

      /*
       * Since StudyMaterial can be either a file or link, two statements must be executed.
       * First, get the potential Files related to a study
       */
      StudyMaterialFileStatement studyMaterialFileStatement = new StudyMaterialFileStatement();
      StudyMaterialStatement studyMaterialStatement = new StudyMaterialStatement();

      studyMaterial = new File();

      studyMaterialFileStatement.setDataBean(studyMaterial);

      ObjectJoinableStatementImpl statement = new ObjectJoinableStatementImpl();
      queryCollection.add(studyMaterialFileStatement);
      queryCollection.add(studyMaterialStatement);

      statement.setQueryCollection(queryCollection);

      stmt.append(" WHERE smf_filename = ?");

      statement.getConditions().add(fileName);

      // the join between sm_studymaterial and smf_smfile
      stmt.append(" AND sm_id = smf_sm_id");

      statement.setWhereClause(stmt.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of Study Material (File) with filename " + fileName + " failed!!", e);
        return (null);
      }
    }

    if (!results.isEmpty()) {
      List row = (List) results.get(0);
      studyMaterial = (File) row.get(0);
    }

    return (studyMaterial);
  }
}
