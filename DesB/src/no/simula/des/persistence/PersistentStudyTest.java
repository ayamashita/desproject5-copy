/*
 * Created on 08.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.Publication;
import no.simula.des.Link;
import no.simula.des.Study;
import junit.framework.TestCase;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyTest extends TestCase {
  /**
   * Field persistentStudy
   */
  private PersistentStudy persistentStudy = null;

  /**
   * Field persons
   */
  private List persons;
  /**
   * Field studies
   */
  private List studies;
  /**
   * Field publications
   */
  private List publications;
  /**
   * Field privileges
   */
  private List privileges;
  /**
   * Field studyMaterials
   */
  private List studyMaterials;

  /**
   * Field nextStudyId
   */
  private int nextStudyId;
  /**
   * Field nextStudyMaterialId
   */
  private int nextStudyMaterialId;

  /**
   * Field createdStudyId
   */
  private Integer createdStudyId;

  /**
   * Method main
   * @param args String[]
   */
  public static void main(String[] args) {
  }

  /*
   * @see TestCase#setUp()
   */
  /**
   * Method setUp
   * @throws Exception
   */
  protected void setUp() throws Exception {
    super.setUp();

    /*persistentStudy = new PersistentStudy();

    persons = new ArrayList(4);
    List privs = new ArrayList(2);
    Privilege studyAdmin = new Privilege(new Integer(0), "study-admin");
    privs.add(studyAdmin);
    persons.add(new Person(new Integer(1), "Bj�rn", "Hjulstad", "bjorn.hjulstad@halogen.no", new ArrayList(privs)));
    persons.add(new Person(new Integer(3), "Arve", "Kval�y", "arve.kvaloy@halogen.no", new ArrayList(privs)));

    privs.add(new Privilege(new Integer(1), "db-admin"));
    persons.add(new Person(new Integer(2), "Frode", "Langseth", "frode.langseth@halogen.no", new ArrayList(privs)));

    privileges = new ArrayList(privs);

    privs.remove(studyAdmin);
    persons.add(new Person(new Integer(0), "Stian", "Eide", "stian.eide@halogen.no", new ArrayList(privs)));

    publications = new ArrayList(5);
    publications.add(new Publication(new Integer(605), "Eliminating Over-Confidence in Software Development Effort Estimates", "Magne J�rgensen and Kjetil Mol�kken"));
    publications.add(
      new Publication(
        new Integer(554),
        "Expert Estimation of the Effort of Web-Development Projects: Are Software Professionals in Technical Roles More Optimistic Than Those in Non-Technical Roles",
        "Kjetil Mol�kken and Magne J�rgensen"));
    publications.add(
      new Publication(new Integer(572), "Applying Use Cases to Design versus Validate Class Diagrams � A Controlled Experiment Using a Professional Modelling Tool", "Bente Anda and Dag Sj�berg"));
    publications.add(new Publication(new Integer(583), "Empirical Studies of Construction and Application of Use Case Models", "Bente Anda"));
    publications.add(new Publication(new Integer(596), "Dynamic Coupling Measurement for Object-Oriented Software: Detailed Analyses", "Erik Arisholm, Lionel Briand and Audun F�yen"));

    studyMaterials = new ArrayList(3);
    studyMaterials.add(new Link(new Integer(0), "Halogen has a lot of interesting stuff", "http://www.halogen.no/"));
    studyMaterials.add(new Link(new Integer(1), "Birdstep could be the winner of tomorrow", "http://www.birdstep.com/"));
    studyMaterials.add(new Link(new Integer(2), "Get smart at the University of Oslo", "http://www.uio.no/"));

    studies = getStudies();*/
  }

  /**
   * Method testCreate
   */
  public void testCreate() {
    try {
      createdStudyId = persistentStudy.create(studies.get(10));
    } catch (CreatePersistentObjectException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(20));
    } catch (CreatePersistentObjectException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(30));
    } catch (CreatePersistentObjectException e2) {
      // TODO Auto-generated catch block
      e2.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(40));
    } catch (CreatePersistentObjectException e3) {
      // TODO Auto-generated catch block
      e3.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(50));
    } catch (CreatePersistentObjectException e4) {
      // TODO Auto-generated catch block
      e4.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(60));
    } catch (CreatePersistentObjectException e5) {
      // TODO Auto-generated catch block
      e5.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(70));
    } catch (CreatePersistentObjectException e6) {
      // TODO Auto-generated catch block
      e6.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(80));
    } catch (CreatePersistentObjectException e7) {
      // TODO Auto-generated catch block
      e7.printStackTrace();
    }
    try {
      createdStudyId = persistentStudy.create(studies.get(90));
    } catch (CreatePersistentObjectException e8) {
      // TODO Auto-generated catch block
      e8.printStackTrace();
    }
  }

  /**
   * Method testDelete
   */
  public void testDelete() {
    Study entity = (Study) studies.get(20);

    try {
      persistentStudy.delete(entity.getId());
    } catch (DeletePersistentObjectException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    /*		
        List entities = new ArrayList(3);
        
        entities.add(studies.get(30));
    		entities.add(studies.get(40));
    		entities.add(studies.get(50));
    		
    		persistentStudy.
    		
    		try {
          studyStatement.executeDelete(entities);
        } catch (StatementException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }*/
  }

  /**
   * Method testFindByKey
   */
  public void testFindByKey() {
    Study study = (Study) persistentStudy.findByKey(new Integer(16));

  }

  /*
   * Class to test for void update(Object)
   */
  /**
   * Method testUpdateObject
   */
  public void testUpdateObject() {

  }

  /*
   * Class to test for void update(Integer, Object)
   */
  /**
   * Method testUpdateIntegerObject
   */
  public void testUpdateIntegerObject() {
    Study study = (Study) studies.get(70);

    study.setDescription("Frodetest 70");

    try {
      persistentStudy.update(study);
    } catch (UpdatePersistentObjectException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    study = (Study) studies.get(90);

    study.setDescription("Frodetest 90");

    try {
      persistentStudy.update(study.getId(), study);
    } catch (UpdatePersistentObjectException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

  }

  /**
   * Method testFindByKeys
   */
  public void testFindByKeys() {
    List entities = new ArrayList(2);
    List studies = new ArrayList(2);

    entities.add(studies.get(60));
    entities.add(studies.get(90));

    studies = persistentStudy.findByKeys(entities);
  }

  /**
   * Method testFind
   */
  public void testFind() {
    List studies = new ArrayList();

    studies = persistentStudy.find();
  }

  /**
   * Method testFindByStudyMaterial
   */
  public void testFindByStudyMaterial() {
  }

  //---------------------------------------------------------------------------
  ///////////////////           TEST METHODS            ///////////////////////
  //---------------------------------------------------------------------------

  // This method returns a list of fictional studies for testing purposes

  // Copied from Simula.getStudies, author Stian Eide
  /**
   * Method getStudies
   * @return List
   */
  private List getStudies() {
    long lowerBound = new Date(99, 0, 1).getTime();
    long upperBound = System.currentTimeMillis();

    Random rnd = new Random();
    int count;
    List studies = new ArrayList(100);
    for (int i = 0; i < 100; i++) {
      Study study = new Study(new Integer(i));
      study.setName("Test Study Number " + i);
      study.setDescription("Description for study " + i);
      count = rnd.nextInt(3) + 1;
      for (int j = 0; j < count; j++) {
        study.addResponsible((Person) persons.get(rnd.nextInt(4)));
      }
      count = rnd.nextInt(4) + 1;
      for (int j = 0; j < count; j++) {
        study.addPublication((Publication) publications.get(rnd.nextInt(5)));
      }
      long start = (long) (rnd.nextFloat() * (upperBound - lowerBound));
      long end = (long) (rnd.nextFloat() * (upperBound - (lowerBound + start)));
      study.setStart(new Date(lowerBound + start));
      study.setEnd(new Date(lowerBound + start + end));
      study.setStudyMaterial(studyMaterials);
      studies.add(study);
      nextStudyId = i + 1;
    }
    return (studies);
  }
}
