package no.simula.des.presentation.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableRenderer;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceArrayList;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.presentation.web.DateColumnDecorator;
import no.simula.des.presentation.web.actions.Constants;
import no.simula.utils.StudyTransformer;

import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.fop.apps.Driver;
import org.w3c.dom.Document;

/**
 * Returns either a specific study or information on all studies as PDF
 * documents. This servlet users apache fop (see http://xml.apache.org/fop/) to
 * transform formatting objects (see
 * http://www.w3.org/TR/2001/REC-xsl-20011015/) into PDF.
 * 
 * The formatting objects are made by transforming XML using XSLT.
 * 
 * @author Stian Eide
 */
public class PDFServlet extends HttpServlet {

	/** The jakarta-commons logger */
	private static Log log = LogFactory.getLog(PDFServlet.class);

	/** The path to the xslt-document transforming xml for studies into fo */
	private static final String TRANSFORMER_STUDIES = "resources/fo-studies.xsl";
	/** The path to the xslt-document transforming xml for a study into fo */
	private static final String TRANSFORMER_STUDY = "resources/fo-study.xsl";

	// FOP can't use commons-logging
	/** The logger used by fop */
	protected Logger fopLog;
	protected TransformerFactory transformerFactory;

	/**
	 * Initializes the servlet.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		fopLog = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
		transformerFactory = TransformerFactory.newInstance();
	}

	/**
	 * Destroys the servlet.
	 */
	public void destroy() {
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		try {

			String arg = request.getPathInfo();
			Document source;
			String transformerSource;

			if (arg.startsWith("/studies.pdf")) {
				transformerSource = TRANSFORMER_STUDIES;
				TableRenderer renderer = (TableRenderer) getServletContext().getAttribute(Constants.TABLE_RENDERER);
				Table table = (Table) request.getSession().getAttribute(Constants.TABLE_SEARCH_STUDIES);
				source = renderer.getXML(table);
			} else if (arg.startsWith("/report.pdf")) {
				transformerSource = TRANSFORMER_STUDIES;
				TableRenderer renderer = (TableRenderer) getServletContext().getAttribute(Constants.TABLE_RENDERER);
				Table resultTable = (Table) request.getSession().getAttribute(Constants.TABLE_QUERY_REPORT);

				Table table = createStandardTable(resultTable.getSource().getContent());

				 source = renderer.getXML(table);
			} else {
				transformerSource = TRANSFORMER_STUDY;
				Integer id = new Integer(request.getParameter("id"));
				Simula simula = SimulaFactory.getSimula();
				source = StudyTransformer.getXML(simula.getStudy(id));
			}

			// Debug to console
			// Transformer transformer = transformerFactory.newTransformer();
			// transformer.transform(new DOMSource(source), new
			// StreamResult(System.err));

			// Setup FOP
			Driver driver = new Driver();
			driver.setLogger(fopLog);
			driver.setRenderer(Driver.RENDER_PDF);

			// Setup a buffer to obtain the content length
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			driver.setOutputStream(out);

			// Setup Transformer
			ClassLoader cl = PDFServlet.class.getClassLoader();
			Source xsltSrc = new StreamSource(cl.getResourceAsStream(transformerSource));
			Transformer transformer = transformerFactory.newTransformer(xsltSrc);

			// Make sure the XSL transformation's result is piped through to FOP
			Result result = new SAXResult(driver.getContentHandler());

			// Setup input
			Source dataSource = new DOMSource(source);

			// Start the transformation and rendering process
			transformer.transform(dataSource, result);

			// Prepare response
			response.setContentType("application/pdf");
			response.setContentLength(out.size());

			// Send content to Browser
			response.getOutputStream().write(out.toByteArray());
			response.getOutputStream().flush();

			out.close();
		}

		catch (Exception e) {
			log.error("Failed to render PDF document.", e);
			response.sendError(500);
		}
	}

	private Table createStandardTable(List tableContent) {
		TableSource source = new TableSourceArrayList(tableContent);
		List columns = new ArrayList(6);
		// This column has a link to the logical forward "this". Which will be
		// set to current action by the relevant action mapping
		SortableTableColumn name = new SortableTableColumn("name", "studies.table.heading.name", null, "?view=true",
				null, null, "id", "id", new Integer(50));
		SortableTableColumn type = new SortableTableColumn("type", "studies.table.heading.type", null);
		SortableTableColumn end = new SortableTableColumn("end", "studies.table.heading.end", null);
		DateColumnDecorator dateDecorator = new DateColumnDecorator();
		end.setDecorator(dateDecorator);
		columns.add(name);
		columns.add(type);
		columns.add(end);
		columns.add(new TableColumn("responsibles", "studies.table.heading.responsibles", "url", false));
		columns.add(new TableColumn("description", "studies.table.heading.description", null));
		columns.add(new TableColumn("publications", "studies.table.heading.publications", "url", false));
		return new Table(columns, source, 10, name);

	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 */
	public String getServletInfo() {
		return "Short description";
	}
}