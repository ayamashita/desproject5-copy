package no.simula.des.presentation.web.actions;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.Column;
import no.simula.des.PersonalizedReport;
import no.simula.des.Study;
import no.simula.des.presentation.web.forms.ReportForm;
import no.simula.des.presentation.web.forms.StudyForm;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PersonalizedReportEditAction extends Action {
	private static final String FORWARD_RETURN = "return";
	private static final String FORWARD_EDIT = "edit";
	private static Logger log = Logger.getLogger(PersonalizedReportEditAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (isCancelled(request)) {
			return (mapping.findForward(FORWARD_RETURN));
		}

		ReportForm reportForm = (ReportForm) form;
		ActionErrors errors = null;
		Simula simula = SimulaFactory.getSimula();

		if (saveChanges(request)) {

			errors = validate(simula, reportForm);
			if (errors == null) {

				PersonalizedReport report;

				// Get existing study from cache to allow tables in session to
				// be
				// updated automatically (they point to the same object in
				// memory)
				if (isNewReport(reportForm)) {
					report = new PersonalizedReport();
				} else {
					report = simula.getReport(reportForm.getId());
				}
				
				BeanUtils.copyProperties(report, reportForm);
				report.setOwner(simula.getPerson(reportForm.getOwnerId()));
				report.setTimeSortOrder(simula.getSortOrderById(reportForm.getSortOrderId()));
				report.setResponsibleCriterium(simula.getCriteriumById(reportForm.getCriteriumId()));

				if (isNewReport(reportForm)) {
					simula.addReport(report);
				} else {
					simula.storeReport(report);
				}

				return (mapping.findForward(FORWARD_RETURN));
			}
		}

		if (isInitialRequest(request)) {
			log.info("Initial request");

			if (isNew(request)) {
				reportForm.setId(null);
				reportForm.setName(null);
				reportForm.setCriteriumId(null);
				reportForm.setOwnerId(request.getRemoteUser());
				reportForm.setSortOrderId(null);
				reportForm.setResponsibles(new ArrayList(0));
				reportForm.setColumns(new ArrayList(0));
			} else {
				Integer id = Integer.valueOf(request.getParameter("id"));
				PersonalizedReport source = simula.getReport(id);
				
				if (!request.getRemoteUser().equals(source.getOwner().getId())) {
					request.setAttribute("noaccess", Boolean.TRUE);
					return mapping.findForward(FORWARD_EDIT);
				}
				
				
				BeanUtils.copyProperties(reportForm, source);
				reportForm.setOwnerId(source.getOwner().getId());
				reportForm.setSortOrderId(source.getTimeSortOrder().getId());
				reportForm.setCriteriumId(source.getResponsibleCriterium().getId());
				
				

			}

			reportForm.setPersons(new ArrayList(simula.getPersons()));
			reportForm.getPersons().removeAll(reportForm.getResponsibles());

			reportForm.setAllColumns(new ArrayList(simula.getReportColumns()));
			reportForm.getAllColumns().removeAll(reportForm.getColumns());

		}

		else if (addResponsibles(request)) {
			String[] add = reportForm.getAddResponsible();
			for (int i = 0; i < add.length; i++) {
				Person person = simula.getPerson(add[i]);
				if (!reportForm.getResponsibles().contains(person)) {
					reportForm.getResponsibles().add(person);
				}
				reportForm.getPersons().remove(person);
			}
			reportForm.sortResponsibles();
		}

		else if (removeResponsibles(request)) {
			String[] remove = reportForm.getRemoveResponsible();
			for (int i = 0; i < remove.length; i++) {
				Person person = simula.getPerson(remove[i]);
				if (!reportForm.getPersons().contains(person)) {
					reportForm.getPersons().add(person);
				}
				reportForm.getResponsibles().remove(person);
			}
			reportForm.sortPersons();
		} else if (addColumn(request)) {
			Integer[] add = reportForm.getAddColumn();
			for (int i = 0; i < add.length; i++) {
				Column column = simula.getReportColumn(add[i]);
				// Person person = simula.getPerson(add[i]);
				if (!reportForm.getColumns().contains(column)) {
					reportForm.getColumns().add(column);
				}
				reportForm.getAllColumns().remove(column);
			}
		}

		else if (removeColumns(request)) {
			Integer[] remove = reportForm.getRemoveColumn();
			for (int i = 0; i < remove.length; i++) {
				Column column = simula.getReportColumn(remove[i]);
				if (!reportForm.getAllColumns().contains(column)) {
					reportForm.getAllColumns().add(column);
				}
				reportForm.getColumns().remove(column);
			}
		}

		List sortOptions = simula.getReportSortOptions();
		request.setAttribute(Constants.BEAN_SORT_OPTIONS, sortOptions);
		List reportCriteria = simula.getReportCriteria();
		request.setAttribute(Constants.BEAN_REPORT_CRITERIA, reportCriteria);

		return (mapping.findForward(FORWARD_EDIT));
	}

	private boolean isInitialRequest(HttpServletRequest request) {
		return (request.getParameter("new") != null || request.getParameter("id") != null);
	}

	private boolean isNew(HttpServletRequest request) {
		return (request.getParameter("new") != null);
	}

	private boolean addResponsibles(HttpServletRequest request) {
		return (request.getParameter("addResp") != null);
	}

	private boolean removeResponsibles(HttpServletRequest request) {
		return (request.getParameter("removeResp") != null);
	}

	private boolean addColumn(HttpServletRequest request) {
		return (request.getParameter("addCol") != null);
	}

	private boolean removeColumns(HttpServletRequest request) {
		return (request.getParameter("removeCol") != null);
	}

	private boolean saveChanges(HttpServletRequest request) {
		return (request.getParameter("save") != null);
	}

	private ActionErrors validate(Simula simula, ReportForm form) throws SimulaException {
		ActionErrors errors = new ActionErrors();

		if (form.getName() == null || form.getName().length() == 0) {
			ActionError error = new ActionError("errors.required", "name");
			errors.add("name", error);
		}

		for (Iterator i = simula.getReports().iterator(); i.hasNext();) {
			PersonalizedReport study = (PersonalizedReport) i.next();
			if ((form.getId() == null || (form.getId() != null && !form.getId().equals(study.getId())))
					&& study.getName().trim().equals(form.getName().trim())) {
				ActionError error = new ActionError("errors.name.equal", "report", form.getName());
				errors.add("name", error);
				break;
			}
		}

		if (form.getResponsibles().size() == 0) {
			ActionError error = new ActionError("errors.minoccur", "1 responsible");
			errors.add("responsibles", error);
		}
		
		if (form.getColumns().size() == 0) {
			ActionError error = new ActionError("errors.minoccur", "1 column");
			errors.add("columns", error);
		}

		if (errors.isEmpty()) {
			return (null);
		} else {
			return (errors);
		}
	}

	private boolean empty(String string) {
		return (string == null || string.length() == 0);
	}
	
	  private boolean isNewReport(ReportForm form) {
		    return(form.getId() == null);
		  }  
}
