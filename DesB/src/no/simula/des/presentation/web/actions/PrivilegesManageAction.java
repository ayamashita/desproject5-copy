package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import no.halogen.struts.ActionForwardUtil;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.presentation.web.forms.PrivilegeSubForm;
import no.simula.des.presentation.web.forms.PrivilegesForm;
import no.simula.des.presentation.web.forms.StudyForm;
import org.apache.commons.beanutils.BeanUtils;

/** Handles the process of managing privileges. The presentation layer bean
 * <CODE>PrivilegesForm</CODE> contains a list of all privileges (represented by
 * <CODE>PrivilegesSubForm</CODE>) and a list of all persons that has no
 * privilege. The list of persons is updated when privileges are granted and revoked
 * from persons in the GUI to reflect this. The form also contains a field that
 * keeps track on the currently selected privilege.
 * <p>
 * When persons are granted a privilege, the person-objects are moved from the list
 * of persons to the current privilege (<CODE>PrivilegeSubForm</CODE>). The
 * opposite happens when a privilege is revoked from a person.
 * <p>
 * When changes are saved, all privileges are iterated and lists of all persons
 * that are added and removed from the current privilege are constructed. Then the
 * person-objects are updated in memory.
 * <p>
 * A list of all persons that are affected by the change is compiled and this list
 * of persons are sent to the persistence service in order to make the changes
 * persistent.
 * <p>
 * Returns <CODE>manage</CODE> for displaying the GUI for managing privileges.
 * Returns <CODE>return</CODE> when changes are saved successfully or the process
 * is cancelled.
 */
public class PrivilegesManageAction extends Action {
  
  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
        
    PrivilegesForm form = (PrivilegesForm)actionForm;
    Simula simula = SimulaFactory.getSimula();

    if(saveChanges(request)) {

      // Use Set to avoid duplicate entries
      Set changed = new TreeSet();
      
      // Iterate over every registered Privilege
      for(Iterator i = form.getAll().iterator(); i.hasNext();) {
        
        List temp;
        
        // Find persons that are added to the current privilege
        PrivilegeSubForm priv = (PrivilegeSubForm)i.next();
        temp = new ArrayList(priv.getPersons());
        List privileged = simula.getPersonsByPrivilege(priv);
        if(privileged != null) {
          temp.removeAll(privileged);
        }
        changed.addAll(temp);
        
        // Update person in memory
        for(Iterator j = temp.iterator(); j.hasNext();) {
          ((Person)j.next()).grantPrivilege(priv);
        }
        
        // Find persons that are removed from the current privilege
        temp = new ArrayList(privileged);
        temp.removeAll(priv.getPersons());
        changed.addAll(temp);
        
        // Update persons in memory
        for(Iterator j = temp.iterator(); j.hasNext();) {
          Person person = (Person)j.next();
          Principal user = request.getUserPrincipal();
          if(
            user != null && 
            person.equals(simula.getPersonByEmail(user.getName())) && 
            priv.getName().equals(Constants.PRIVILEGE_DB_ADMIN)
          ) {
            changed.remove(person);
          }
          else {
            person.revokePrivilege(priv);
          }
        }
      }
      
      // Store updated persons in database
      simula.updatePersons(new ArrayList(changed));
      return(mapping.findForward("return"));
    }

    ///////////////////////////////////////////////////////
    // Mutually exclusive actions that directs the user
    // to the main view of this action
    
    if(isInitialRequest(request)) {
      List persons = new ArrayList(simula.getPersons());
      List privileges = new ArrayList();
      for(Iterator i = simula.getPrivileges().iterator(); i.hasNext();) {
        Privilege priv = (Privilege)i.next();
        PrivilegeSubForm privForm = new PrivilegeSubForm();
        privForm.setId(priv.getId());
        privForm.setName(priv.getName());
        privForm.setPersons(new ArrayList(simula.getPersonsByPrivilege(priv)));
        privileges.add(privForm);
        persons.removeAll(privForm.getPersons());
      }
      form.setAll(privileges);
      form.setSelected((PrivilegeSubForm)form.getAll().get(0));
      form.setPersons(persons);
    }
    
    else if(changePrivilege(request)) {
      form.setSelected(form.getPrivilege(form.getPrivilegeId()));
    }
    
    else if(addPersons(request)) {
      String[] add = form.getAddPersons();
      for(int i = 0; i < add.length; i++) {
        Person person = simula.getPerson(add[i]);
        if(!form.getSelected().getPersons().contains(person)) {
          form.getSelected().getPersons().add(person);
        }
        form.getPersons().remove(person);
      }
      form.getSelected().sortPersons();
    }

    else if(removePersons(request)) {
      String[] remove = form.getRemovePersons();
      for(int i = 0; i < remove.length; i++) {
        Person person = simula.getPerson(remove[i]);
        
        // Check if user is revoking his own db-admin privilege
        Principal user = request.getUserPrincipal();
        if(
          user != null && 
          person.equals(simula.getPersonByEmail(user.getName())) && 
          form.getSelected().getName().equals(Constants.PRIVILEGE_DB_ADMIN)
        ) {
          ActionError error = new ActionError("errors.privileges.self");
          ActionErrors errors = new ActionErrors();
          errors.add(ActionErrors.GLOBAL_MESSAGE, error);
          saveErrors(request, errors);
        }
        else {
          if(!form.getPersons().contains(person)) {
            form.getPersons().add(person);
          }
          form.getSelected().getPersons().remove(person);
        }
      }
      form.sortPersons();
    }
    
    // Make sure the select element always reflects the selected privilege
    form.setPrivilegeId(form.getSelected().getId());
    return(mapping.findForward("manage"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameterMap().size() == 0);
  }
  
  private boolean addPersons(HttpServletRequest request) {
    return(request.getParameter("add") != null);
  }

  private boolean removePersons(HttpServletRequest request) {
    return(request.getParameter("remove") != null);
  }
  
  private boolean changePrivilege(HttpServletRequest request) {
    return(request.getParameter("change") != null);
  }
  
  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }
}