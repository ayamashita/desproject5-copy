package no.simula.des.presentation.web.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.halogen.search.Criterium;
import no.halogen.search.SortOrder;
import no.halogen.search.SortOrderImpl;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSource;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.Column;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.PersonalizedReport;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportQueryTableSource;
import no.simula.des.StudyMaterial;
import no.simula.des.presentation.web.DateColumnDecorator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportQueryAction extends Action  {
	private static final String STUDY_END_DATE = "stu_study.stu_end";
	private static final String SORT_ORDER_UNSORTED = "-";
	private static final String STUDY_TYPE_ID = "stu_study.stu_sty_id";

	/**
	 * Execute this action
	 * 
	 * @param mapping
	 *            the action mapping that match the current request
	 * @param actionForm
	 *            the action form linked to the current action mapping
	 * @param request
	 *            the current servlet request object
	 * @param response
	 *            the current servlet response object
	 * @throws Exception
	 *             if the action could not be successfully completed
	 * @return an action forward from the action mapping depending on the
	 *         outcome of this action
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Simula simula = SimulaFactory.getSimula();
		Table table = null;

		ActionErrors errors = null;
		
		String reportId = request.getParameter("id");

		errors = new ActionErrors();
		Integer id = null;
		if (reportId != null && reportId.length() != 0) {

			try {
				id = new Integer(Integer.parseInt(reportId));
			} catch (NumberFormatException nfe) {
				errors.add("id", new ActionError("Report id must be a number!"));
			}
		} else {
			errors.add("id", new ActionError("Report id must be a number!"));
		}
		if (errors.size() > 0) {
			saveErrors(request, errors);
			return mapping.findForward("error");
		}

		PersonalizedReport report = simula.getReport(id);

		if (report != null) {

			// For some reason, an ArrayList won't work in this case, hence
			// the Vector
			List criteria = new Vector(4);
			String[] responsibleIds = new String[report.getResponsibles().size()];

			for (int i = 0; i < report.getResponsibles().size(); i++) {
				responsibleIds[i] = ((Person) report.getResponsibles().get(i)).getId();
			}

			criteria.add(processStudyByResponsiblesCriteria(simula, responsibleIds));
			criteria.remove(null);
			((Vector) criteria).trimToSize();

			ArrayList sorting = new ArrayList();

			sorting.add(new SortOrderImpl(STUDY_TYPE_ID, SortOrder.SORT_ASC));

			if (!report.getTimeSortOrder().getSortOrderName().equals(SORT_ORDER_UNSORTED)) {
				sorting.add(new SortOrderImpl(STUDY_END_DATE, report.getTimeSortOrder().getSortOrderName()));
			}

			TableSource source;
			if (report.getResponsibleCriterium().getId().intValue() == ReportCriterium.REPORT_CRITERIA_AND_ID) {
				source = new ReportQueryTableSource(criteria, sorting, responsibleIds);
			} else {
				source = new ReportQueryTableSource(criteria, sorting);
			}

			if (table == null) {
				List columns = new ArrayList(report.getColumns().size());
				// This column has a link to the logical forward "this".
				// Which will be set to current action by the relevant
				// action mapping
				SortableTableColumn dummyCol = new SortableTableColumn("name", "studies.table.heading.name", null,
						"?view=true", null, null, "id", "id", new Integer(50));

				Iterator colIterator = report.getColumns().iterator();

				//
				while (colIterator.hasNext()) {
					Column col = (Column) colIterator.next();
					TableColumn tableColumn = null;
					if (col.getName().equals("study_end") || col.getName().equals("study_start")) {
						tableColumn = new TableColumn(col.getProperty(), "studies.table.heading." + col.getProperty(),
								null);
						tableColumn.setDecorator(new DateColumnDecorator());
					} else if (col.getName().equals("study_publications") || col.getName().equals("study_responsible")
							|| col.getName().equals("study_files")) {
						//          columns.add(new TableColumn("publications", "studies.table.heading.publications", null, "http://www.simula.no/publication_one.php", null, null, "publication_id", "id", new Integer(50)));
						tableColumn =new TableColumn(col.getProperty(), "studies.table.heading."+ col.getProperty(), null, "?material=true", null, null, "id", "id", new Integer(50)); 
							
							//new TableColumn(col.getProperty(), "studies.table.heading." + col.getProperty(),
								//"url", false);
					} else {
						tableColumn = new TableColumn(col.getProperty(), "studies.table.heading." + col.getProperty(),
								null);
					}

					if (tableColumn != null) {
						columns.add(tableColumn);
					}
				}

				table = new Table(columns, source, 10, dummyCol);
				request.getSession().setAttribute(Constants.TABLE_QUERY_REPORT, table);
			}

			return (mapping.findForward("list"));
		}

		if (errors != null) {
			saveErrors(request, errors);
		}

		return (mapping.findForward("manage"));
	}

	private Criterium processStudyByResponsiblesCriteria(Simula simula, String[] responsibles) throws SimulaException {
		if (responsibles == null || responsibles.length == 0
				|| (responsibles.length == 1 && responsibles[0].length() == 0)) {
			return (null);
		}
		Criterium crit = simula.getStudyByResponsiblesCriterium();
		List attr = Arrays.asList(responsibles);
		crit.addAttributes(attr);
		return (crit);
	}

}
