package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import no.halogen.presentation.web.HttpSessionTable;

import no.halogen.search.SearchFactory;
import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.ObservableList;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceArrayList;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.StudyMaterial;
import no.simula.des.presentation.web.DateColumnDecorator;
import no.simula.des.search.SearchFactoryImpl;

/**
 * Displays the table of registered studies and handles dispatching of requests
 * to other actions or views used to manage studies.
 * 
 * Returns:
 * 
 * <ul>
 * <li><CODE>new</CODE> to display an empty study form</li>
 * <li><CODE>edit</CODE> to display a study form with values</li>
 * <li><CODE>delete</CODE> to confirm deletion of selected studies</li>
 * <li><CODE>success</CODE> to display the table of registered studies</li>
 * </ul>
 * 
 * Please note that all actions linked from the main view of manage studies is
 * dispatched through this action
 */
public class ReportsManageAction extends Action {

	/**
	 * Execute this action
	 * 
	 * @param mapping
	 *            the action mapping that match the current request
	 * @param actionForm
	 *            the action form linked to the current action mapping
	 * @param request
	 *            the current servlet request object
	 * @param response
	 *            the current servlet response object
	 * @throws Exception
	 *             if the action could not be successfully completed
	 * @return an action forward from the action mapping depending on the
	 *         outcome of this action
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Simula simula = SimulaFactory.getSimula();
		Table table = (Table) request.getSession().getAttribute(Constants.TABLE_MANAGE_REPORTS);
		
		
		if (request.getParameter("material") != null) {
			Integer id = null;
			ActionErrors errors = new ActionErrors();
			String materialId = request.getParameter("id");
			if (materialId != null && materialId.length() != 0) {

				try {
					id = new Integer(Integer.parseInt(materialId));
				} catch (NumberFormatException nfe) {
					errors.add("id", new ActionError("Report id must be a number!"));
				}
			} else {
				errors.add("id", new ActionError("Report id must be a number!"));
			}
			if (errors.size() > 0) {
				saveErrors(request, errors);
				return mapping.findForward("error");
			}

			StudyMaterial sm = simula.getStudyMaterial(id);
			
			String link = getLink(request,sm);
			
			return new ActionForward(link);
		}
 
		

		if (deleteReports(request)) {
			String reportId = request.getParameter("id");
			ActionErrors errors = new ActionErrors();
			Integer id = null;
			if (reportId != null && reportId.length() != 0) {
				
				try {
					id = new Integer(Integer.parseInt(reportId));
				} catch (NumberFormatException nfe) {
					errors.add("id", new ActionError("Report id must be a number!"));
				}
			} else {
				errors.add("id", new ActionError("Report id must be a number!"));
			}
			if (errors.size() > 0) {
				saveErrors(request, errors);
				return mapping.findForward("error");
			}
			
			simula.deleteReport(id);
		}

		if (newReport(request)) {
			return (mapping.findForward("new"));
		}

		if (editReport(request)) {
			ActionForward forward = mapping.findForward("edit");
			forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
			return (forward);
		}
		
		if (queryReport(request)) {
			ActionForward forward = mapping.findForward("query");
			forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
			return (forward);
		}

		List original = simula.getReportsByOwner(request.getRemoteUser());
		TableSource source = new TableSourceArrayList(original);

		if (table == null) {
			List columns = new ArrayList(4);
			SortableTableColumn name = new SortableTableColumn("name", "report.table.heading.name", null);
			TableColumn edit = new TableColumn(null, "report.table.link.edit", null, null, null,
					"/adm/report/manage?edit=true", "id", "id", new Integer(10));
			TableColumn delete = new TableColumn(null, "report.table.link.delete", null, null, null,
					"/adm/report/manage?delete=true", "id", "id", new Integer(10));
			TableColumn query = new TableColumn(null, "report.table.link.query", null, null, null,
					"/adm/report/manage?query=true", "id", "id", new Integer(10));
			columns.add(name);
			columns.add(edit);
			columns.add(delete);
			columns.add(query);
			// add column for querying

			table = new Table(columns, source, 10, name);
			request.getSession().setAttribute(Constants.TABLE_MANAGE_REPORTS, table);
		} else {
			table.setSource(source);
			table.setCurrentPage(1);
		}

		String page = request.getParameter("page");

		if (page != null && page.length() > 0) {
			table.setCurrentPage(Integer.parseInt(page));
		}

		return (mapping.findForward("list"));
	}

	private boolean deleteReports(HttpServletRequest request) {
		return (request.getParameter("delete") != null);
	}

	private boolean newReport(HttpServletRequest request) {
		return (request.getParameter("new") != null);
	}

	private boolean editReport(HttpServletRequest request) {
		return (request.getParameter("edit") != null);
	}
	
	private boolean queryReport(HttpServletRequest request) {
		return (request.getParameter("query") != null);
	}
	

	private String getLink(HttpServletRequest request, StudyMaterial sm) {
		if (sm.getType().equalsIgnoreCase("link")) {
			return ((Link)sm).getUrl();
		} else if (sm.getType().equalsIgnoreCase("file")) {
			return "/file/" + sm.getId() + "/" + ((File)sm).getName();
		}
		return null;
	}
}