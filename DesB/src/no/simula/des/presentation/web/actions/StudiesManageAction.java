package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import no.halogen.presentation.web.HttpSessionTable;

import no.halogen.search.SearchFactory;
import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.ObservableList;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceArrayList;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.presentation.web.DateColumnDecorator;
import no.simula.des.search.SearchFactoryImpl;

/** Displays the table of registered studies and handles dispatching of
 * requests to other actions or views used to manage studies.
 *
 * Returns:
 *
 * <ul>
 *  <li><CODE>new</CODE> to display an empty study form</li>
 *  <li><CODE>edit</CODE> to display a study form with values</li>
 *  <li><CODE>delete</CODE> to confirm deletion of selected studies</li>
 *  <li><CODE>success</CODE> to display the table of registered studies</li>
 * </ul>
 *
 * Please note that all actions linked from the main view of manage studies
 * is dispatched through this action
 */
public class StudiesManageAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {    
    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_MANAGE_STUDIES);

    if(deleteStudies(request)) {
      // Firstly, we check if any studies are chosen. If this is the case,
      // the deletableStudies is populated with the ids in the request. If not
      // an error message is displayed.
      List deletableStudieIds = new ArrayList();
      for(Iterator i = request.getParameterMap().keySet().iterator(); i.hasNext();) {
        Object o = i.next();
        if(o instanceof String) {
          String paramName = (String)o;
          if(paramName.startsWith("id[")) {
            String id = paramName.substring(paramName.indexOf("[")+1, paramName.indexOf("]"));
            deletableStudieIds.add(Integer.valueOf(id));
          }
        }
      }
      if(deletableStudieIds.isEmpty()) {
        ActionError error = new ActionError("errors.delete.empty", "studies");
        ActionErrors errors = new ActionErrors();
        errors.add(ActionErrors.GLOBAL_ERROR, error);
        saveErrors(request, errors);
      }
      else {
        List deletableStudies = simula.getStudiesByIds(deletableStudieIds);
        request.getSession().setAttribute(Constants.DELETE_STUDIES, deletableStudies);
        return(mapping.findForward("delete"));
      }
    }

    if(newStudy(request)) {
      return(mapping.findForward("new"));
    }
    
    if(editStudy(request)) {
      ActionForward forward = mapping.findForward("edit");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }

    if(table == null) {
      List columns = new ArrayList(6);
      SortableTableColumn name = new SortableTableColumn("name", "studies.table.heading.name", null, null, null, "/adm/studies/manage?edit=true", "id", "id", new Integer(50));
      SortableTableColumn type = new SortableTableColumn("type", "studies.table.heading.type", null);
      SortableTableColumn end = new SortableTableColumn("end", "studies.table.heading.end", null);
      DateColumnDecorator dateDecorator = new DateColumnDecorator();
      end.setDecorator(dateDecorator);
      columns.add(name);
      columns.add(type);
      columns.add(end);
      columns.add(new TableColumn("responsibles", "studies.table.heading.responsibles", "url", false));
      columns.add(new TableColumn("description", "studies.table.heading.description", null));
      //TODO change the url here
      columns.add(new TableColumn("publications", "studies.table.heading.publications", "url", false));

      ObservableList original = (ObservableList)simula.getStudiesByCriteria(null);
      TableSource source = new TableSourceArrayList(original);
      table = new HttpSessionTable(columns, source, 10, name, original);
      original = null;
      request.getSession().setAttribute(Constants.TABLE_MANAGE_STUDIES, table);
    }

    String sortBy = request.getParameter("sortBy");
    String page = request.getParameter("page");

    if(sortBy != null && sortBy.length() > 0) {
      table.sortBy(sortBy);
    }

    else if(page != null && page.length() > 0) {
      table.setCurrentPage(Integer.parseInt(page));
    }
    
    else {
      table.sortBy("name", true);
    }
    return(mapping.findForward("success"));
  }
  
  private boolean deleteStudies(HttpServletRequest request) {
    return(request.getParameter("delete") != null);
  }
    
  private boolean newStudy(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }
  
  private boolean editStudy(HttpServletRequest request) {
    return(request.getParameter("edit") != null);
  }
}