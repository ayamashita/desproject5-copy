package no.simula.des.presentation.web.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.halogen.search.Criterium;
import no.halogen.utils.table.ExternalUrlTableColumn;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSource;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.StudySearchTableSource;
import no.simula.des.presentation.web.DateColumnDecorator;
import no.simula.des.presentation.web.forms.StudySearchForm;
import no.simula.utils.PersonFamilyNameComparator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Handles the process of displaying the search form and performing the search for
 * studies.
 * <p>
 * Studies may be searched for by free text, study type, responsible and date.
 * Combining two or more will only return studies that match all criteria.
 * <p>
 * The result is put in a table that is stored in session and displayed by
 * <CODE>StudiesSearchResultAction</CODE>.
 * <p>
 * Return <CODE>search</CODE> to display the search form and <CODE>result</CODE> to
 * display the result from the search.
 */
public class StudiesSearchAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_SEARCH_STUDIES);
    StudySearchForm form = (StudySearchForm)actionForm;
    ActionErrors errors = null;

    List types = simula.getStudyTypes();
    request.setAttribute(Constants.BEAN_STUDY_TYPES, types);
    List persons = new ArrayList(simula.getPersons());
    Collections.sort(persons, new PersonFamilyNameComparator());
    request.setAttribute(Constants.BEAN_PERSONS, persons);    
    
    if(isSearch(request)) {
      
      // A "hack" to reset values of the select boxes when no items are selected.
      if(request.getParameterValues("types") == null || request.getParameterValues("types").length == 0) {
        form.setTypes(new Integer[0]);
      }
      if(request.getParameterValues("responsibles") == null || request.getParameterValues("responsibles").length == 0) {
        form.setResponsibles(new String[0]);
      }
      
      errors = validate(form);
      if(errors == null) {
      
        // For some reason, an ArrayList won't work in this case, hence the Vector
        List criteria = new Vector(4);
        criteria.add(processStudyByEndDateCriteria(simula, form.getStart(), form.getEnd()));
        criteria.add(processStudyByFreetextCriteria(simula, form.getSearchText()));
        criteria.add(processStudyByResponsiblesCriteria(simula, form.getResponsibles()));
        criteria.add(processStudyByStudyTypeCriteria(simula, form.getTypes()));
        criteria.remove(null);
        ((Vector)criteria).trimToSize();

        TableSource source = new StudySearchTableSource(criteria);

        if(table == null) {
          List columns = new ArrayList(6);
          // This column has a link to the logical forward "this". Which will be set to current action by the relevant action mapping
          SortableTableColumn name = new SortableTableColumn("name", "studies.table.heading.name", null, "?view=true", null, null, "id", "id", new Integer(50));
          SortableTableColumn type = new SortableTableColumn("type", "studies.table.heading.type", null);
          SortableTableColumn end = new SortableTableColumn("end", "studies.table.heading.end", null);
          DateColumnDecorator dateDecorator = new DateColumnDecorator();
          end.setDecorator(dateDecorator);
          columns.add(name);
          columns.add(type);
          columns.add(end);
          columns.add(new TableColumn("responsibles", "studies.table.heading.responsibles", "url", false));
          columns.add(new TableColumn("description", "studies.table.heading.description", null));
          columns.add(new TableColumn("publications", "studies.table.heading.publications", "url", false));
          table = new Table(columns, source, 10, name);
          request.getSession().setAttribute(Constants.TABLE_SEARCH_STUDIES, table);
        }
        else {
          table.setSource(source);
          table.setCurrentPage(1);
        }

        return(mapping.findForward("result"));
      }
    }

    else if(clearFields(request)) {
      form.setSearchText(null);
      form.setResponsibles(new String[0]);
      form.setTypes(new Integer[0]);
      form.setStartDay(null);
      form.setStartMonth(null);
      form.setStartYear(null);
      form.setEndDay(null);
      form.setEndMonth(null);
      form.setEndYear(null);
    }

    if(errors != null) {
      saveErrors(request, errors);
    }
    
    return(mapping.findForward("search"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private Criterium processStudyByEndDateCriteria(Simula simula, Date start, Date end) throws SimulaException {
    Criterium crit = simula.getStudyByEndDateCriterium();
    if(start == null && end == null) {
      return(null);
    }
    if(start == null) {
      start = new Date(0L);
    }
    if(end == null) {
      end = new Date(System.currentTimeMillis());
    }
    crit.addAttribute(start);
    crit.addAttribute(end);
    return(crit);
  }

  private Criterium processStudyByResponsiblesCriteria(Simula simula, String[] responsibles) throws SimulaException {
    if(responsibles == null || responsibles.length == 0 || (responsibles.length == 1 && responsibles[0].length() == 0)) {
      return(null);
    }
    Criterium crit = simula.getStudyByResponsiblesCriterium();
    List attr = Arrays.asList(responsibles);
    crit.addAttributes(attr);
    return(crit);
  }
  
  private Criterium processStudyByStudyTypeCriteria(Simula simula, Integer[] types) throws SimulaException {
    if(types == null || types.length == 0 || (types.length == 1 && types[0].intValue() == -1)) {
      return(null);
    }
    Criterium crit = simula.getStudyByStudyTypeCriterium();
    crit.addAttributes(Arrays.asList(types));
    return(crit);
  }

  private Criterium processStudyByFreetextCriteria(Simula simula, String text) throws SimulaException {
    if(text == null || text.length() == 0) {
      return(null);
    }
    Criterium crit = simula.getStudyByFreetextCriterium();
    crit.addAttribute(text);
    return(crit);
  }
  
  private ActionErrors validate(StudySearchForm form) {
    ActionErrors errors = new ActionErrors();

    Calendar validator = Calendar.getInstance();
    validator.setLenient(false);
    
    if(!empty(form.getStartYear()) || !empty(form.getStartMonth()) || !empty(form.getStartDay())) {
      try {
        validator.set(
          Integer.parseInt(form.getStartYear()) - 1900, 
          Integer.parseInt(form.getStartMonth()) - 1,
          Integer.parseInt(form.getStartDay())
        );
        validator.getTime();      
      } catch(Exception e) {
        ActionError error = new ActionError("errors.date", "start");
        errors.add("start", error);
      }
    }
    
    if(!empty(form.getEndYear()) || !empty(form.getEndMonth()) || !empty(form.getEndDay())) {
      try {
        validator.set(
          Integer.parseInt(form.getEndYear()) - 1900,
          Integer.parseInt(form.getEndMonth()) - 1, 
          Integer.parseInt(form.getEndDay())
        );
        validator.getTime();
      } catch(Exception e) {
        ActionError error = new ActionError("errors.date", "end");
        errors.add("end", error);
      }
    }
    
    if(form.getStart() != null && form.getEnd() != null) {
      try {
        if(form.getStart().after(form.getEnd())) {
          ActionError error = new ActionError("errors.date.startafterend");
          errors.add("end", error);
        }
      } catch(Exception e) {
        // Do nada. Illegal dates are handled by the two preceeding tests
      }
    }
        
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }    
  }
  
  private boolean empty(String string) {
    return(string == null || string.length() == 0);
  }
  
  private boolean viewStudyDetails(HttpServletRequest request) {
    return(request.getParameter("view") != null);
  }

  private boolean clearFields(HttpServletRequest request) {
    return(request.getParameter("clear") != null);
  }

  private boolean isSearch(HttpServletRequest request) {
    return(request.getParameter("search") != null);
  }
}