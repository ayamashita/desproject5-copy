package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import no.simula.Privilege;
import no.simula.utils.PersonFamilyNameComparator;

/** Holds information about one specific privilige to be displayed when managing
 * privileges.
 * @author Stian Eide
 */
public class PrivilegeSubForm extends Privilege {
  
  /** Holds value of property persons. */
  private List persons;
  
  /** Getter for property persons.
   * @return Value of property persons.
   *
   */
  public List getPersons() {
    return(persons);
  }
  
  /** Setter for property persons. This list is always sorted.
   * @param persons New value of property persons.
   *
   */
  public void setPersons(List persons) {
    if(persons == null) {
      this.persons = null;
    }
    else {
      this.persons = new ArrayList(persons);
      sortPersons();
    }
  }
  
  /** Returns the name of this privilage
   * @return name of privilage.
   *
   */
  public String getName() {
    return(super.getName());
  }  
  
  /** Getter for property id.
   * @return Value of property id.
   *
   */
  public Integer getId() {
    return(super.getId());
  }
  
  /** Setter for property id.
   * @param id New value of property id.
   *
   */
  public void setId(Integer id) {
    super.setId(id);
  } 
  
  /** Setter for property name.
   * @param name New value of property name.
   *
   */
  public void setName(String name) {
    super.setName(name);
  }

  /** Sorts the persons who have this privilege by their family name. */  
  public void sortPersons() {
    Collections.sort(this.persons, new PersonFamilyNameComparator());
  }
  
}