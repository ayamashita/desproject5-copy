package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import no.simula.Publication;
import no.simula.des.DurationUnit;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/** Holds data that is used when attaching and detaching publications to and from a
 * study.
 */
public class StudyPublicationRelationForm extends ActionForm {
    
  private ArrayList detachedPublications;
  private ArrayList attachedPublications;
  
  /** Getter for property attachedPublications.
   * @return Value of property attachedPublications.
   *
   */
  public List getAttachedPublications() {
    return(attachedPublications);
  }  
  
  /** Setter for property attachedPublications.
   * @param attachedPublications New value of property attachedPublications.
   *
   */
  public void setAttachedPublications(List attachedPublications) {
    this.attachedPublications = new ArrayList(attachedPublications);
  }

  /** Getter for property detachedPublications.
   * @return Value of property detachedPublications.
   *
   */
  public List getDetachedPublications() {
    return(detachedPublications);
  }
  
  /** Setter for property detachedPublications.
   * @param detachedPublications New value of property detachedPublications.
   *
   */
  public void setDetachedPublications(List detachedPublications) {
    this.detachedPublications = new ArrayList(detachedPublications);
  }

//  public void detachPublication(Publication publication) {
//    if(!detachedPublications.contains(publication)) {
//      detachedPublications.add(publication);
//      if(attachedPublications.remove(publication)) {
//        attachedPublications.trimToSize();
//      }
//    }
//  }
//  
//  public void attachPublication(Publication publication) {
//    if(!attachedPublications.contains(publication)) {
//      attachedPublications.add(publication);
//      if(detachedPublications.remove(publication)) {
//        detachedPublications.trimToSize();
//      }
//    }
//  }  
}