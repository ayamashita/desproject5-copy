/**
 * @(#) SearchByFreetextImpl.java
 */

package no.simula.des.search;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.search.Criterium;
import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumFactory;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.search.*;

/**
 * @author Frode Langseth
 */
public class SearchByFreetextImpl extends CriteriumImpl implements SearchByFreetext {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByFreetext.class);

  /**
   * Field subCriteria
   */
  private final ArrayList subCriteria = new ArrayList();
  /**
   * Field DATABASE_COLUMN
   */
  private final String DATABASE_COLUMN = "stu_name, ";
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" BETWEEN ");

  /**
   * Field searchByStudyName
   */
  private SearchByStudyName searchByStudyName = null;
  /**
   * Field searchByStudyDescription
   */
  private SearchByStudyDescription searchByStudyDescription = null;
  /**
   * Field searchByMaterialTitle
   */
  private SearchByMaterialTitle searchByMaterialTitle = null;
  /**
   * Field searchByPublicationTitle
   */
  private SearchByPublicationTitle searchByPublicationTitle = null;
  /**
   * Field searchByPublicationAuthor
   */
  private SearchByPublicationAuthor searchByPublicationAuthor = null;
  /**
   * Field searchByStudyKeyword
   */
  private SearchByStudyKeyword searchByStudyKeyword = null;
  
  private SearchByResponsible searchByResponsible = null;

  /**
   * Constructor for SearchByFreetextImpl
   */
  public SearchByFreetextImpl() {
    CriteriumFactory criteriumFactory = new CriteriumFactoryImpl();

    searchByStudyName = (SearchByStudyName) criteriumFactory.create(SearchByStudyName.class.getName());
    subCriteria.add(searchByStudyName);
    searchByStudyDescription = (SearchByStudyDescription) criteriumFactory.create(SearchByStudyDescription.class.getName());
    subCriteria.add(searchByStudyDescription);
    searchByMaterialTitle = (SearchByMaterialTitle) criteriumFactory.create(SearchByMaterialTitle.class.getName());
    subCriteria.add(searchByMaterialTitle);
    searchByPublicationTitle = (SearchByPublicationTitle) criteriumFactory.create(SearchByPublicationTitle.class.getName());
    subCriteria.add(searchByPublicationTitle);
    searchByPublicationAuthor = (SearchByPublicationAuthor) criteriumFactory.create(SearchByPublicationAuthor.class.getName());
    subCriteria.add(searchByPublicationAuthor);
    searchByStudyKeyword = (SearchByStudyKeyword) criteriumFactory.create(SearchByStudyKeyword.class.getName());
    subCriteria.add(searchByStudyKeyword);
    searchByResponsible = (SearchByResponsible)criteriumFactory.create(SearchByResponsible.class.getName());
    subCriteria.add(searchByResponsible);
  }

  /* (non-Javadoc)
   * @see halogen.search.Criterium#addAttributes(halogen.search.List)
   */
  /**
   * Method addAttributes
   * @param attributes List
   * @see no.halogen.search.Criterium#addAttributes(List)
   */
  public void addAttributes(List attributes) {
    searchByStudyName.addAttributes(attributes);
    searchByStudyDescription.addAttributes(attributes);
    searchByMaterialTitle.addAttributes(attributes);
    searchByPublicationTitle.addAttributes(attributes);
    searchByPublicationAuthor.addAttributes(attributes);
    searchByStudyKeyword.addAttributes(attributes);
    searchByResponsible.addAttributes(attributes);
  }

  /* (non-Javadoc)
   * @see halogen.search.Criterium#addAttributes(halogen.search.List)
   */
  /**
   * Method addAttribute
   * @param attribute Object
   * @see no.halogen.search.Criterium#addAttribute(Object)
   */
  public void addAttribute(Object attribute) {
    searchByStudyName.addAttribute(attribute);
    searchByStudyDescription.addAttribute(attribute);
    searchByMaterialTitle.addAttribute(attribute);
    searchByPublicationTitle.addAttribute(attribute);
    searchByPublicationAuthor.addAttribute(attribute);
    searchByStudyKeyword.addAttribute(attribute);
    searchByResponsible.addAttribute(attribute);
  }

  /**
   * Since this is a composite criterium, generateWhereClause returns a concatenated string from the sub criteria's generateWhereStatements
   * 
   * Since Publication is in another database than the default, it must be special trated, and will not participate as a part of the returned whereClause
   * 
   * @return Generated return clause 
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() {
    StringBuffer whereStatement = new StringBuffer();

    try {
      whereStatement.append(searchByStudyName.generateWhereClause());
      whereStatement.append(" OR " + searchByStudyDescription.generateWhereClause());
      whereStatement.append(" OR " + searchByMaterialTitle.generateWhereClause());
    } catch (CriteriumException e) {
      log.error("generateWhereClause() - Error while fetching the where clause for freetext search ...", e);
      return (null);
    }

    return (whereStatement.toString());
  }

  /**
   * Since this is a composite criterium, generateWhereClause is not implemented, and returns null.
   * Instead, generateWhereClause must be called on each of the sub criteria 
   * @return ObjectStatement
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return null;
  }

  /**
   * Gets the statements for the sub criteria of this composite criterium
   * 
   * @return List of statements, related to the sub criteria
   * @see no.halogen.search.CompositeCriterium#getStatements()
   */
  public List getStatements() {
    ArrayList statements = new ArrayList();

    Iterator i = subCriteria.iterator();

    while (i.hasNext()) {
      statements.add(i.next());
    }

    return (statements);
  }

  /**
   * Returns a list containing the sub criteria that forms this composite criteria
   * @return A list of Criteria
   * 
   * @see no.halogen.search.CompositeCriterium#getCriteria()
   * 
   */
  public List getCriteria() {
    return (subCriteria);
  }

  /**
   * Returns a sub criterium, indicated by index
   * @param index - The number (1 .. x) of the criterium to get
   * @return A Criterium, that is a sub criterium for this composite criterium
   * 
   * @see no.halogen.search.CompositeCriterium#getCriterium(java.lang.Integer)
   * 
   */
  public Criterium getCriterium(Integer index) {
    return (Criterium) (subCriteria.get(index.intValue()));
  }

}
