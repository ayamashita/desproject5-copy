/**
 * @(#) SarchBystudyEndDateImpl.java
 * Criteria for searching on the study end date
 * The criteria might be:
 *  - a interval (two values)
 *  - start date to todays date (one value)
 *  - many intervals (more than two values). The values will be paired into intervals, if there's an odd number ov attribute values, the last will be regarded as an interval between value and todays date   
 *    
 */

package no.simula.des.search;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.des.statements.StudyStatement;

/**
 * @author Frode Langseth
 */
public class SearchByStudyEndDateImpl extends CriteriumImpl implements SearchByStudyEndDate {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByStudyEndDate.class);

  /**
   * Field DATABASE_COLUMN
   */
  private final String DATABASE_COLUMN = "stu_end";
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" BETWEEN ");

  /** 
   * Generates the criteria and "?" character as value representators as a part of a SQL where clause
   * Due to use of prepared statements, the value attributes must be fetched when a prepared statement is instatiated and will use the criterium
   * The method does not generate the start of the clause (WHERE), nor the operator (AND, OR ..)
   * @return String
   * @throws CriteriumException
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() throws CriteriumException {
    StringBuffer statement = new StringBuffer();

    if (getAttributes() == null) {
      log.error("generateWhereClause - No attributes in SearchByStudyEndDate criterium");
      throw new CriteriumException("Error! No attributes ...");
    }

    statement.append("("); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    Iterator i = getAttributes().iterator();

    /* 
     * Fetches pair of end date attributes (interval)
     */
    boolean firstIteration = true;
    while (i.hasNext()) {
      Object attribute = i.next(); //the first date in an interval

      if (!firstIteration) { // if more than one internal pair
        statement.append(" OR ");
      }

      statement.append(" " + getCriteriumColumn() + " BETWEEN ? ");

      if (i.hasNext()) { // the second date in an interval
        attribute = i.next();

        statement.append(" AND ? ");
      } else { // if no second date, use today's date
        statement.append(" AND CURDATE()");
      }

      firstIteration = false;
    }

    statement.append(")"); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    return (statement.toString());
  }

  /**
   * @return Database columns
   */
  private String getCriteriumColumn() {
    return (DATABASE_COLUMN);
  }

  /**
   * Returns the statement object that represents the database table tha attribute relates to
   * 
   * @return An object statement 
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return (new StudyStatement());
  }
}