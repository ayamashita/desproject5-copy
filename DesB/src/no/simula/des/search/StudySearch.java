/**
 * @(#) StudySearch.java
 */

package no.simula.des.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import no.halogen.search.CompositeCriterium;
import no.halogen.search.Criterium;
import no.halogen.search.CriteriumException;
import no.halogen.search.Search;
import no.halogen.search.SortOrder;
import no.halogen.statements.ObjectJoinableStatement;
import no.halogen.statements.ObjectJoinableStatementImpl;
import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.simula.Publication;
import no.simula.des.Study;
import no.simula.des.persistence.PersistentStudy;
import no.simula.des.statements.PublicationStudyRelStatement;
import no.simula.des.statements.StudyKeywordsStatement;
import no.simula.des.statements.StudyMaterialStatement;
import no.simula.des.statements.StudyMaterialStudyRelStatement;
import no.simula.des.statements.StudyPersonRelStatement;
import no.simula.des.statements.StudyStatement;
import no.simula.search.SearchByPublicationAuthor;
import no.simula.search.SearchByPublicationTitle;
import no.simula.statements.PublicationPeopleRelStatement;
import no.simula.statements.PublicationStatement;
import no.simula.wsclient.SimulaWSClient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 */
public class StudySearch implements Search, Serializable {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(StudySearch.class);

	/**
	 * Field criteria
	 */
	private ArrayList criteria = new ArrayList();

	private ArrayList sortOrders = new ArrayList();

	/**
	 * Adds a search criterium to the criteria list
	 * 
	 * @param attribute -
	 *            The attribute, with value(s), to add the criteria
	 * 
	 * @see no.halogen.search.Search#addCriterium(Criterium)
	 */
	public void addCriterium(Criterium attribute) {
		criteria.add(attribute);
	}

	/**
	 * @see halogen.search.Search#doSearch()
	 */
	public List doSearch() {
		List criteria = null; // The criteria that is set for the search
		List expandedCriteria = new ArrayList();
		// since some criteria might be composite criteria (consists of two or
		// more sub criteria), this list contains all the criteria, including
		// eventual sub criteria

		/*
		 * special case, since publication is in a different database than
		 * default, and since it's possible to search in one of the table's
		 * columns (publication title)
		 */
		PublicationStatement publicationStatement = new PublicationStatement();

		/*
		 * special case, since publication is in a different database than
		 * default, and since it's possible to search in one of the table's
		 * columns (publication title)
		 */
		PublicationPeopleRelStatement publicationPeopleStatement = new PublicationPeopleRelStatement();

		/*
		 * uses joinable statement, even if there might be only one statement
		 * invloved in a particulaer search (probably must flexible ...)
		 */
		StudyKeywordsStatement studyKeywordsStatement = new StudyKeywordsStatement();
		StudyPersonRelStatement studyPersonRelStatement = new StudyPersonRelStatement();
		ObjectJoinableStatement studyMaterialStatement = new ObjectJoinableStatementImpl();
		ObjectJoinableStatement statement = new ObjectJoinableStatementImpl();

		Set studyIdResults = new HashSet();
		Set studyIdFreetext = new HashSet();
		List results = new ArrayList();

		List publicationTitleCriteria = new ArrayList();
		List publicationAuthorCriteria = new ArrayList();
		List responsibleCriteria = new ArrayList();

		boolean freeTextSearch = false;

		/*
		 * Expanding sub riteria related to a composite criteria, and adds them
		 * to a list together with all other criteria
		 */
		if (getCriteria() != null) {
			criteria = getCriteria();

			Iterator i = criteria.iterator();

			while (i.hasNext()) {
				Object o = i.next();

				// Removes ctriterium that is null
				if (o != null) {
					if (o instanceof SearchByFreetext) {
						freeTextSearch = true;
					}

					if (o instanceof CompositeCriterium) {
						CompositeCriterium criterium = (CompositeCriterium) o;
						expandedCriteria.addAll((Collection) criterium.getCriteria());

					} else {
						expandedCriteria.add(o);
					}
				}
			}

			Iterator crIter = expandedCriteria.iterator();

			while (freeTextSearch && crIter.hasNext()) {
				Object o = crIter.next();
				if (o instanceof SearchByPublicationAuthor) {
					publicationAuthorCriteria.add(o);
					crIter.remove();
				} else if (o instanceof SearchByPublicationTitle) {
					publicationTitleCriteria.add(o);
					crIter.remove();
				} else if (o instanceof SearchByResponsible) {
					responsibleCriteria.add(o);
					crIter.remove();
				}
			}
		}

		/*
		 * Populates statements
		 */

		if (!generateStatement(expandedCriteria, statement, publicationStatement, publicationPeopleStatement,
				studyPersonRelStatement, studyKeywordsStatement, studyMaterialStatement)) {
			log.error("Could not generate statements and prepared statement values");

			return (null);
		}

		if (publicationTitleCriteria != null && publicationTitleCriteria.size() != 0) {
			try {
				studyIdFreetext.addAll(searchPublicationTitle(publicationTitleCriteria));
			} catch (Exception e) {
				log.error("doSearch - Searching for study publications by title failed!!", e);
				return (null);
			}
		}

		if (publicationAuthorCriteria != null && publicationAuthorCriteria.size() != 0) {
			try {
				studyIdFreetext.addAll(searchPublicationAuthor(publicationAuthorCriteria));
			} catch (Exception e) {
				log.error("doSearch - Searching for study publications by author failed!!", e);
				return (null);
			}
		}

		if (responsibleCriteria != null && responsibleCriteria.size() != 0) {
			try {
				studyIdFreetext.addAll(searchResponsible(responsibleCriteria));
			} catch (Exception e) {
				log.error("doSearch - Searching for study responsibles by name failed!!", e);
				return (null);
			}
		}
		/*
		 * Tries to execute the studyMaterialStatement, the result will be a
		 * list of study ids, that are related to the matching study material
		 */
		if (studyMaterialStatement.getWhereClause() != null) {
			try {
				studyIdFreetext.addAll((ArrayList) searchStudyMaterial(studyMaterialStatement));
			} catch (StatementException e) {
				log.error("doSearch - Searching for study material by title failed!!", e);
				return (null);
			}
		}

		/*
		 * Tries to execute the studyKeywordsStatement, the result will be a
		 * list of study ids, that are related to the matching responsible
		 */
		if (studyKeywordsStatement.getWhereClause() != null) {
			try {
				studyIdFreetext.addAll((ArrayList) searchKeywords(studyKeywordsStatement));
			} catch (StatementException e) {
				log.error("doSearch - Searching for study keywords!!", e);
				return (null);
			}
		}
		/*
		 * Adds publication ids (if any) to the search for studies
		 */
		if (!studyIdResults.isEmpty()) {
			StringBuffer extendWhereClause = new StringBuffer(statement.getWhereClause());

			if (extendWhereClause.length() > 0) {
				extendWhereClause.append(" AND ");
			} else {
				extendWhereClause.append(" WHERE ");
			}
			extendWhereClause.append(" stu_id IN (");

			Iterator j = studyIdResults.iterator();

			boolean firstIteration = true;
			while (j.hasNext()) {
				if (!firstIteration) {
					extendWhereClause.append(", ");
				}

				extendWhereClause.append("?");

				statement.getConditions().add(j.next());

				firstIteration = false;
			}

			extendWhereClause.append(")");

			statement.setWhereClause(extendWhereClause.toString());
		}

		/*
		 * Builds the freetext part of the search, both from other tables
		 * (publications, study material, e.g.) and the freetext fields in the
		 * study table
		 * 
		 */
		List freetextValues = new ArrayList();
		String generatedFreetextClause = generateFreetextStatement(expandedCriteria, freetextValues);

		StringBuffer freetextWhereClause = new StringBuffer();

		// Is there freetext parameters at all??
		if (generatedFreetextClause.length() > 0) {
			if (statement.getWhereClause().length() > 0) {
				freetextWhereClause.append(" AND (");
			} else {
				freetextWhereClause.append(" WHERE (");
			}

			// StringBuffer freetextWhereClause = new
			// StringBuffer(statement.getWhereClause());

			statement.getConditions().addAll(freetextValues);
			freetextWhereClause.append(generatedFreetextClause);

			if (!studyIdFreetext.isEmpty()) {

				/*
				 * if (freetextWhereClause.length() > 0) {
				 * freetextWhereClause.append(" OR "); } else {
				 * freetextWhereClause.append(" WHERE ("); }
				 */
				freetextWhereClause.append(" OR stu_id IN (");

				Iterator j = studyIdFreetext.iterator();

				boolean firstIteration = true;
				while (j.hasNext()) {
					if (!firstIteration) {
						freetextWhereClause.append(", ");
					}

					freetextWhereClause.append("?");

					statement.getConditions().add(j.next());

					firstIteration = false;
				}
				freetextWhereClause.append(")");
			}

			freetextWhereClause.append(")");

			StringBuffer whereClause = new StringBuffer(statement.getWhereClause());

			whereClause.append(freetextWhereClause);

			statement.setWhereClause(whereClause.toString());
		}

		if (sortOrders != null && sortOrders.size() > 0) {
			StringBuffer sorting = new StringBuffer(" ORDER BY ");

			SortOrder so = (SortOrder) sortOrders.get(0);

			sorting.append(so.getSortColumn()).append(" ").append(so.getSortOrder()).append(" ");
			for (int i = 1; i < sortOrders.size(); i++) {
				so = (SortOrder) sortOrders.get(i);
				sorting.append(", ").append(so.getSortColumn()).append(" ").append(so.getSortOrder()).append(" ");
			}
			statement.setOrderBy(sorting.toString());
		}

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("doSearch() - Fetching of studies " + statement.getWhereClause() + " failed!!", e);
			return (null);
		}

		/*
		 * The List to contains the studies with all populated data
		 */
		List populatedStudies = new ArrayList();

		PersistentStudy persistentStudy = new PersistentStudy();

		Object o = null; // will contain the data entity from the row
		Study study = null;

		Iterator resultIterator = results.iterator();

		// Each row might contain different data bean types, but only Study are
		// of interest

		//we want to add each study only once!
		HashSet existingIds = new HashSet();
		while (resultIterator.hasNext()) {
			List row = (List) resultIterator.next();

			Iterator iRow = row.iterator();

			while (iRow.hasNext()) {
				o = iRow.next();

				if (o instanceof Study) {
					study = (Study) o;
					if (existingIds.add(study.getId())) {
						populatedStudies.add(persistentStudy.findByKey(study.getId()));
					}
				}
			}
		}

		return (populatedStudies);
	}

	private Collection searchResponsible(List responsibleCriteria) throws Exception {
		List peopleResult = new ArrayList();
		Iterator crIter = responsibleCriteria.iterator();

		SimulaWSClient client = SimulaWSClient.getInstance();

		while (crIter.hasNext()) {
			SearchByResponsible cr = (SearchByResponsible) crIter.next();
			Iterator attrIterator = cr.getAttributes().iterator();
			while (attrIterator.hasNext()) {
				String criterium = (String) attrIterator.next();
				peopleResult.addAll(client.getPeopleIdsByName(getWSQueryString(criterium)));
			}
		}

		List studyIds = getStudyIdsForPeople(peopleResult);

		return studyIds;
	}

	private String getWSQueryString(String criterium) {
		String queryStr = criterium;
		if (criterium.startsWith("%") && criterium.endsWith("%")) {
			queryStr = criterium.substring(1, criterium.length() - 1);
		}
		return queryStr;
	}

	private List searchPublicationTitle(List publicationTitleCriteria) throws Exception {
		List publicationResult = new ArrayList();
		Iterator crIter = publicationTitleCriteria.iterator();

		SimulaWSClient client = SimulaWSClient.getInstance();

		while (crIter.hasNext()) {
			SearchByPublicationTitle cr = (SearchByPublicationTitle) crIter.next();
			Iterator attrIterator = cr.getAttributes().iterator();
			while (attrIterator.hasNext()) {
				String criterium = (String) attrIterator.next();
				publicationResult.addAll(client.getPublicationIdsByTitle(getWSQueryString(criterium)));
			}
		}

		List studyIds = getStudyIdsForPublications(publicationResult);

		return studyIds;
	}

	private List searchPublicationAuthor(List publicationAuthorCriteria) throws Exception {

		List publicationResult = new ArrayList();
		Iterator crIter = publicationAuthorCriteria.iterator();

		SimulaWSClient client = SimulaWSClient.getInstance();

		while (crIter.hasNext()) {
			SearchByPublicationAuthor cr = (SearchByPublicationAuthor) crIter.next();
			Iterator attrIterator = cr.getAttributes().iterator();
			while (attrIterator.hasNext()) {
				String criterium = (String) attrIterator.next();
				publicationResult.addAll(client.getPublicationIdsByAuthor(getWSQueryString(criterium)));
			}
		}

		List studyIds = getStudyIdsForPublications(publicationResult);

		return (studyIds);
	}

	private List getStudyIdsForPeople(List peopleIds) throws StatementException {
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();

		if (!peopleIds.isEmpty()) {
			StudyPersonRelStatement studyStmt = new StudyPersonRelStatement();
			whereClause.append(" WHERE people_id IN (");

			Iterator i = peopleIds.iterator();
			boolean firstIteration = true;
			while (i.hasNext()) {
				if (!firstIteration) {
					whereClause.append(", ");
				}

				whereClause.append("?");
				String peopleId = (String) i.next();

				studyStmt.getConditions().add(peopleId);
				firstIteration = false;
			}

			whereClause.append(")");
			studyStmt.setWhereClause(whereClause.toString());
			studyIds = (List) studyStmt.executeSelect();
		}

		return studyIds;
	}

	private List getStudyIdsForPublications(List publicationResult) throws StatementException {
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();

		if (!publicationResult.isEmpty()) {
			PublicationStudyRelStatement studyStatement = new PublicationStudyRelStatement();
			// studyStatement.setDataBean(new Study()); //else, the publication
			// id will be fetched ....
			/*
			 * Gets the publication ids. to use in the next search, which will
			 * get the studies related to the found publications
			 */
			whereClause.append(" WHERE publication_id IN (");
			Iterator i = publicationResult.iterator();
			boolean firstIteration = true;
			while (i.hasNext()) {
				if (!firstIteration) {
					whereClause.append(", ");
				}

				whereClause.append("?");
				String publicationId = (String) i.next();
				// Gets the publication id (see
				// PublicationPeopleRelStatement.fetchResult()) from author
				// object
				studyStatement.getConditions().add(publicationId);
				firstIteration = false;
			}

			whereClause.append(")");
			studyStatement.setWhereClause(whereClause.toString());
			studyIds = (List) studyStatement.executeSelect();
		}
		return studyIds;
	}

	/**
	 * @param expandedCriteria
	 * @param freetextValues
	 * @return String
	 */
	private String generateFreetextStatement(List expandedCriteria, List freetextValues) {
		StringBuffer whereClause = new StringBuffer();

		Iterator i = expandedCriteria.iterator();
		Object o = null;

		// StringBuffer whereClause = new
		// StringBuffer(statement.getWhereClause().toString());

		boolean firstIteration = true;
		while (i.hasNext()) {
			o = i.next();

			if (o instanceof SearchByStudyName || o instanceof SearchByStudyDescription) {
				/*
				 * if (firstIteration) { if (whereClause.length() > 0) {
				 * whereClause.append(" AND ("); } else { whereClause.append("
				 * WHERE ("); } } else { whereClause.append(" OR "); }
				 */
				if (!firstIteration) {
					whereClause.append(" OR ");
				}

				Criterium freetextCriterium = (Criterium) o;

				try {
					whereClause.append(freetextCriterium.generateWhereClause());

					/*
					 * Populates the conditions in the joinable statement. This
					 * will later exchange the "?" charaters in the prepared
					 * statement with the values ...
					 */
					Iterator j = freetextCriterium.getAttributes().iterator();
					while (j.hasNext()) {
						freetextValues.add(j.next());
					}
				} catch (CriteriumException e) {
					log.error("generateFreetextStatement() - Error fetching where clause for freetext statement", e);
					return (null);
				}

				firstIteration = false;
			}
		}

		// statement.setWhereClause(whereClause.toString());

		return (whereClause.toString());
	}

	/**
	 * Populates the statements, and sets the where clauses, before search is
	 * executed. The method may result in two separate statements being
	 * populated, one search for publication in the simulaweb database, one
	 * other search for studies in the DES database
	 * 
	 * @param criteria -
	 *            The search criteria (epxanded)
	 * @param statement -
	 *            The search statement going to be executed in the default
	 *            database
	 * @param publicationStatement -
	 *            The search statement going to be executed for publications in
	 *            the simulaweb database
	 * 
	 * @param publicationPeopleStatement
	 *            PublicationPeopleRelStatement
	 * @param studyPersonRelStatement
	 *            StudyPersonRelStatement
	 * @param studyKeywordsStatement
	 *            StudyKeywordsStatement
	 * @param studyMaterialStatement
	 *            ObjectJoinableStatement
	 * @return booleand
	 */
	private boolean generateStatement(List criteria, ObjectJoinableStatement statement,
			PublicationStatement publicationStatement, PublicationPeopleRelStatement publicationPeopleStatement,
			StudyPersonRelStatement studyPersonRelStatement, StudyKeywordsStatement studyKeywordsStatement,
			ObjectJoinableStatement studyMaterialStatement) {
		/*
		 * will be populated with the statements going to be added to statement.
		 * Using a Hashmap to ensure that each statement is reffered once, even
		 * if several criteria is related to the same statement
		 */
		HashMap statements = new HashMap(2);

		StringBuffer whereClause = new StringBuffer();

		/*
		 * Adds the StudyStatement, as search in the study table should be
		 * performed anyway, independent of the criteria. This ensures that a
		 * search without criteria returns all studies
		 */
		StudyStatement studyStatement = new StudyStatement();
		studyStatement.setDataBean(new Study());

		statements.put(studyStatement.getClass(), studyStatement);

		Iterator i = criteria.iterator();

		boolean firstIteration = true;
		while (i.hasNext()) {
			Object o = i.next();

			if (o instanceof SearchByPublicationTitle) {
				if (publicationStatement == null) {
					publicationStatement = new PublicationStatement();
				}

				publicationStatement.setDataBean(new Publication());

				SearchByPublicationTitle criterium = (SearchByPublicationTitle) o;

				try {
					publicationStatement.setWhereClause(" WHERE " + criterium.generateWhereClause());

					/*
					 * Populates the conditions in the publication statement.
					 * This will later exchange the "?" charaters in the
					 * prepared statement with the values ...
					 */
					Iterator j = criterium.getAttributes().iterator();

					while (j.hasNext()) {
						publicationStatement.getConditions().add(j.next());
					}
				} catch (CriteriumException e) {
					log.error("generateStatement() - Error fetching where clause for publication statement", e);
					return (false);
				}
			} else if (o instanceof SearchByStudyKeyword) {
				if (studyKeywordsStatement == null) {
					studyKeywordsStatement = new StudyKeywordsStatement();
				}

				studyKeywordsStatement.setDataBean(new ArrayList());

				SearchByStudyKeyword criterium = (SearchByStudyKeyword) o;

				try {
					studyKeywordsStatement.setWhereClause(" WHERE " + criterium.generateWhereClause());

					/*
					 * Populates the conditions in the keywrods statement. This
					 * will later exchange the "?" charaters in the prepared
					 * statement with the values ...
					 */
					Iterator j = criterium.getAttributes().iterator();

					while (j.hasNext()) {
						studyKeywordsStatement.getConditions().add(j.next());
					}
				} catch (CriteriumException e) {
					log.error("generateStatement() - Error fetching where clause for keyword statement", e);
					return (false);
				}
			} else if (o instanceof SearchByMaterialTitle) {
				List queryCollection = new ArrayList(2);
				StringBuffer studyMaterialWhereClause = new StringBuffer();

				if (studyMaterialStatement == null) {
					studyMaterialStatement = new ObjectJoinableStatementImpl();
				}

				StudyMaterialStatement materialStatement = new StudyMaterialStatement();
				StudyMaterialStudyRelStatement studyIdsStatement = new StudyMaterialStudyRelStatement();

				// studyIdsStatement.setDataBean(new Integer(0));

				queryCollection.add(studyIdsStatement);
				queryCollection.add(materialStatement);

				studyMaterialStatement.setQueryCollection(queryCollection);

				SearchByMaterialTitle criterium = (SearchByMaterialTitle) o;

				try {
					studyMaterialWhereClause.append(" WHERE " + criterium.generateWhereClause());
					studyMaterialWhereClause.append(" AND stusm_sm_id = sm_id");

					studyMaterialStatement.setWhereClause(studyMaterialWhereClause.toString());

					/*
					 * Populates the conditions in the publication statement.
					 * This will later exchange the "?" charaters in the
					 * prepared statement with the values ...
					 */
					Iterator j = criterium.getAttributes().iterator();

					while (j.hasNext()) {
						studyMaterialStatement.getConditions().add(j.next());
					}
				} catch (CriteriumException e) {
					log.error("generateStatement() - Error fetching where clause for publication statement", e);
					return (false);
				}
			} else if (o instanceof SearchByPublicationAuthor) {
				if (publicationPeopleStatement == null) {
					publicationPeopleStatement = new PublicationPeopleRelStatement();
				}

				// publicationPeopleStatement.setDataBean(new Publication());

				SearchByPublicationAuthor criterium = (SearchByPublicationAuthor) o;

				try {
					publicationPeopleStatement.setWhereClause(" WHERE " + criterium.generateWhereClause());

					/*
					 * Populates the conditions in the publication author
					 * statement. This will later exchange the "?" charaters in
					 * the prepared statement with the values ...
					 */
					Iterator j = criterium.getAttributes().iterator();

					while (j.hasNext()) {
						publicationPeopleStatement.getConditions().add(j.next());
					}
				} catch (CriteriumException e) {
					log.error("generateStatement() - Error fetching where clause for publication statement", e);
					return (false);
				}
				/*
				 * } else if (o instanceof SearchByResponsible) { if
				 * (studyPersonRelStatement == null) { studyPersonRelStatement =
				 * new StudyPersonRelStatement(); }
				 * 
				 * //publicationPeopleStatement.setDataBean(new Publication());
				 * 
				 * SearchByResponsible criterium = (SearchByResponsible) o;
				 * 
				 * try { studyPersonRelStatement.setWhereClause(" WHERE " +
				 * criterium.generateWhereClause());
				 */
				/*
				 * Populates the conditions in the publication author statement.
				 * This will later exchange the "?" charaters in the prepared
				 * statement with the values ...
				 */
				/*
				 * Iterator j = criterium.getAttributes().iterator();
				 * 
				 * while (j.hasNext()) {
				 * studyPersonRelStatement.getConditions().add(j.next()); } }
				 * catch (CriteriumException e) { log.error("generateStatement() -
				 * Error fetching where clause for responsible statement", e);
				 * return (false); }
				 */
			} else {
				Criterium criterium = (Criterium) o;

				ObjectStatementImpl entityStatement = null;

				if (statements.containsKey(criterium.getStatement().getClass())) {
					entityStatement = (ObjectStatementImpl) statements.get(criterium.getStatement().getClass());
				} else {
					entityStatement = (ObjectStatementImpl) criterium.getStatement();

					if (entityStatement instanceof StudyStatement) {
						entityStatement.setDataBean(new Study());
					}

					statements.put(criterium.getStatement().getClass(), entityStatement); // Adds
					// the
					// statement
					// related
					// to
					// the
					// criterium,
					// using
					// the
					// statement
					// class
					// as
					// key
				}

				// Starts to build the study table search statement
				if (!(o instanceof SearchByStudyName) && !(o instanceof SearchByStudyDescription)) {
					if (firstIteration) {
						whereClause.append(" WHERE ");
					} else {
						whereClause.append(" AND ");
					}

					try {
						whereClause.append(criterium.generateWhereClause());

						// If the criterium is study responsible, add the join
						// condition between responsible table and study table
						if (criterium instanceof SearchByResponsible) {
							whereClause.append(" AND stu_resp_id = stu_id ");
						}
						/*
						 * Populates the conditions in the joinable statement.
						 * This will later exchange the "?" charaters in the
						 * prepared statement with the values ...
						 */
						Iterator j = criterium.getAttributes().iterator();
						while (j.hasNext()) {
							statement.getConditions().add(j.next());
						}

					} catch (CriteriumException e) {
						log.error("generateStatement() - Error fetching where clause for criterium "
								+ criterium.getClass().getName(), e);
						return (false);
					}
					firstIteration = false;
				}
			}
		}

		Iterator statementsIterator = statements.values().iterator();
		List queries = new ArrayList();
		while (statementsIterator.hasNext()) {
			ObjectStatementImpl joinStatement = (ObjectStatementImpl) statementsIterator.next();
			queries.add(joinStatement);
		}

		statement.setQueryCollection(queries);
		// Adds the statements to the JoinableStatement
		statement.setWhereClause(whereClause.toString());
		// Adds the conditions to the statement
		return (true);
	}

	/**
	 * Gets the publications that satisifies the criterium When the publications
	 * are fetched, we get the studies related to that publications
	 * 
	 * @author Frode Langseth
	 * @version 0.1
	 * @param publicationStatement -
	 *            the Statement to search in the publication table
	 * @return A list of study ids
	 * 
	 * @throws StatementException
	 */
	private List searchPublicationTitle(PublicationStatement publicationStatement) throws StatementException {
		List publicationResults = new ArrayList();
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();
		publicationResults = (List) publicationStatement.executeSelect();
		if (!publicationResults.isEmpty()) {
			PublicationStudyRelStatement studyStatement = new PublicationStudyRelStatement();
			// studyStatement.setDataBean(new Study()); //else, the publication
			// id will be fetched ....
			/*
			 * Gets the publication ids. to use in the next search, which will
			 * get the studies related to the found publications
			 */
			whereClause.append(" WHERE publication_id IN (");
			Iterator i = publicationResults.iterator();
			boolean firstIteration = true;
			while (i.hasNext()) {
				if (!firstIteration) {
					whereClause.append(", ");
				}

				whereClause.append("?");
				Publication publication = (Publication) i.next();
				studyStatement.getConditions().add(publication.getId());
				firstIteration = false;
			}

			whereClause.append(")");
			studyStatement.setWhereClause(whereClause.toString());
			studyIds = (List) studyStatement.executeSelect();
		}

		return (studyIds);
	} /*
		 * (non-Javadoc)
		 * 
		 * @see halogen.search.Search#getCriteria()
		 */

	/**
	 * Method getCriteria
	 * 
	 * @return List
	 * @see no.halogen.search.Search#getCriteria()
	 */
	public List getCriteria() {
		return (criteria);
	}

	/**
	 * Gets the publications that satisifies the criterium When the publications
	 * are fetched, we get the studies related to that publications
	 * 
	 * @author Frode Langseth
	 * @version 0.1
	 * @param publicationPeopleStatement -
	 *            the Statement to search in the publication table
	 * @return A list of study ids
	 * 
	 * @throws StatementException
	 */
	private List searchPublicationAuthor(PublicationPeopleRelStatement publicationPeopleStatement)
			throws StatementException {
		List publicationResults = new ArrayList();
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();
		// The statement is already populated (in generateStatement(), so it's
		// just to execute
		publicationResults = (List) publicationPeopleStatement.executeSelect();
		if (!publicationResults.isEmpty()) {
			PublicationStudyRelStatement studyStatement = new PublicationStudyRelStatement();
			// studyStatement.setDataBean(new Study()); //else, the publication
			// id will be fetched ....
			/*
			 * Gets the publication ids. to use in the next search, which will
			 * get the studies related to the found publications
			 */
			whereClause.append(" WHERE publication_id IN (");
			Iterator i = publicationResults.iterator();
			boolean firstIteration = true;
			while (i.hasNext()) {
				if (!firstIteration) {
					whereClause.append(", ");
				}

				whereClause.append("?");
				List author = (List) i.next();
				// Gets the publication id (see
				// PublicationPeopleRelStatement.fetchResult()) from author
				// object
				studyStatement.getConditions().add(author.get(1));
				firstIteration = false;
			}

			whereClause.append(")");
			studyStatement.setWhereClause(whereClause.toString());
			studyIds = (List) studyStatement.executeSelect();
		}

		return (studyIds);
	}

	/**
	 * Gets the publications that satisifies the criterium When the publications
	 * are fetched, we get the studies related to that publications
	 * 
	 * @author Frode Langseth
	 * @version 0.1
	 * @param studyMaterialStatement -
	 *            the Statement to search in the publication table
	 * @return A list of study ids
	 * 
	 * @throws StatementException
	 */
	private List searchStudyMaterial(ObjectJoinableStatement studyMaterialStatement) throws StatementException {
		List studyMaterialResults = new ArrayList();
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();
		// The statement is already populated (in generateStatement(), so it's
		// just to execute
		studyMaterialResults = (List) studyMaterialStatement.executeSelect();
		if (!studyMaterialResults.isEmpty()) {
			List row = null;
			Iterator i = studyMaterialResults.iterator();
			Iterator iRow = null; // since it is a joinable statement, the
			// first iteration will get rows ....
			while (i.hasNext()) {
				row = (ArrayList) i.next();
				iRow = row.iterator();
				while (iRow.hasNext()) {
					studyIds.add(iRow.next());
				}
			}
		}

		return (studyIds);
	}

	/**
	 * Gets the publications that satisifies the criterium When the publications
	 * are fetched, we get the studies related to that publications
	 * 
	 * @author Frode Langseth
	 * @version 0.1
	 * @param studyPersonRelStatement -
	 *            the Statement to search in the publication table
	 * @return A list of study ids
	 * 
	 * @throws StatementException
	 */
	private List searchResponsible(StudyPersonRelStatement studyPersonRelStatement) throws StatementException {
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();
		// The statement is already populated (in generateStatement(), so it's
		// just to execute
		studyIds = (List) studyPersonRelStatement.executeSelect();
		return (studyIds);
	}

	/**
	 * Gets the keywords that satisifies the criterium When the keywords are
	 * fetched, we get the studies related to that keywords
	 * 
	 * @author Frode Langseth
	 * @version 0.1
	 * @param studyKeywordsStatement -
	 *            the Statement to search in the keywords table
	 * @return A list of study ids
	 * 
	 * @throws StatementException
	 */
	private List searchKeywords(StudyKeywordsStatement studyKeywordsStatement) throws StatementException {
		List keywordsResults = new ArrayList();
		List studyIds = new ArrayList();
		StringBuffer whereClause = new StringBuffer();
		keywordsResults = (List) studyKeywordsStatement.executeSelect();

		if (!keywordsResults.isEmpty()) {
			Iterator i = keywordsResults.iterator();

			List row = null;
			while (i.hasNext()) {
				row = (ArrayList) i.next();

				studyIds.add(row.get(0));
			}
		}

		return (studyIds);
	}

	public void addSortOrder(SortOrder order) {
		sortOrders.add(order);

	}

	public List getSortOrders() {
		return sortOrders;
	}

}
