package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.simula.des.ReportCriterium;
import no.simula.des.StudyType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.regexp.RE;

public class ReportCriteriumStatement extends ObjectStatementImpl {

	private static Log log = LogFactory.getLog(PersonalizedReportStatement.class);

	private final String BEAN_NAME = ReportCriterium.class.getName();

	private final String INSERT_COLUMNS = "report_criteria_name";

	private final String INSERT_VALUES = "(?)";

	private final String KEY = "report_criteria_id";

	private final String SELECT_COLUMNS = "report_criteria_id, report_criteria_name";

	private final String TABLE_NAME = "report_criteria";

	private final String UPDATE_VALUES = "report_criteria_name = ?";

	public Object fetchResults(ResultSet rs) throws SQLException {

		ReportCriterium reportCriterium = null;

		try {
			if (getDataBean() != null) {
				reportCriterium = (ReportCriterium) getDataBean().getClass().newInstance();
			} else {
				reportCriterium = new ReportCriterium();
			}
		} catch (InstantiationException e) {
			log.error("fetchResults() - Could not create new report criterium data object");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			log.error("fetchResults() - Could not create new report criterium data object");
			e.printStackTrace();
		}

		reportCriterium.setId(new Integer(rs.getInt(ReportCriterium.REPORT_CRITERIA_ID)));
		reportCriterium.setCriteriaName(rs.getString(ReportCriterium.REPORT_CRITERIA_NAME));

		return (reportCriterium);
	}

	public String getInsertColumnNames() {
		return INSERT_COLUMNS;
	}

	public String getInsertValues() {
		return INSERT_VALUES;
	}

	public String getKey() {
		return KEY;
	}

	public String getSelectColumnNames() {
		return SELECT_COLUMNS;
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getUpdateValuesString() {
		return UPDATE_VALUES;
	}

	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		} else {
			ReportCriterium entity = (ReportCriterium) getDataBean();

			pstmt.setString(1, entity.getCriteriaName());
		}
	}

	//******************** these methods should never be called ***************************************
	public boolean executeDelete() throws StatementException {
		return false;
	}

	public boolean executeDelete(Integer entityId) throws StatementException {
		return false;
	}

	public boolean executeDelete(List entityIds) throws StatementException {
		return false;
	}

	public boolean executeDelete(String entityId) throws StatementException {
		return false;
	}

	public Integer executeInsert() throws StatementException {
		return null;
	}

	public boolean executeUpdate() throws StatementException {
		return false;
	}

	public boolean executeUpdate(Integer entityId) throws StatementException {
		return false;
	}

}
