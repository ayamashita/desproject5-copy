/*
 * Created on 29.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyPublicationRelStatement extends ObjectStatementImpl {
  private static Log log = LogFactory.getLog(StudyPublicationRelStatement.class);

  private final String BEAN_NAME = "List";

  private final String INSERT_COLUMNS = "stu_pub_id, publication_id";

  private final String INSERT_VALUES = "(?, ?)";

  private final String KEY = "stu_pub_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "stu_pub_id, publication_id";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "study_publication_rel";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "stu_pub_id = ?, publication_id = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Object entity = null;

    try {
      if (getDataBean() instanceof Integer) {
        entity = getDataBean();
      } else if (getDataBean() == null) {
        entity = new Integer(0);
      } else {
        entity = getDataBean().getClass().newInstance();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study publication rel data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study publication rel data object");
      e.printStackTrace();
    }

    if (entity instanceof String) {
      entity = rs.getString("publication_id");
    }

    return entity;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      List entity = (List) getDataBean();
      Integer studyId = (Integer) entity.get(0);
      String publicationId = (String) entity.get(1);

      pstmt.setInt(1, studyId.intValue());
      pstmt.setString(2, publicationId);
    }
  }

}
