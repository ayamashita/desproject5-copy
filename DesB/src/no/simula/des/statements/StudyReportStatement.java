/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.des.AggregatedStudyReport;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyReportStatement extends ObjectStatementImpl {
  private static Log log = LogFactory.getLog(StudyReportStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "String";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "sr_date, sr_graph";

  private final String INSERT_VALUES = "(?, ?)";

  private final String KEY = "sr_id";

  private final String SELECT_COLUMNS = "sr_id, sr_date, sr_graph";

  private final String TABLE_NAME = "sr_studyreports";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "sr_date = ?, sr_graph = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }
  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatement#fetchResults(java.sql.ResultSet)
   */
  /**
   * Method fetchResults
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    AggregatedStudyReport studyReport = null;

    try {
      if (getDataBean() != null) {
        studyReport = (AggregatedStudyReport) getDataBean().getClass().newInstance();
      } else {
        studyReport = new AggregatedStudyReport();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study report data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study report data object");
      e.printStackTrace();
    }

    studyReport.setId(new Integer(rs.getInt("rs_id")));
    studyReport.setDate(rs.getDate("rs_date"));

    Blob data = rs.getBlob("rs_graph");
    studyReport.setBitmap(data.getBytes(1L, (int) data.length()));

    return (studyReport);
  }

  /**
   * Populates the prepared statement string for an update pr insert statement with values from the data bean
   * 
   * Used e.g. by INSERT statements
   * 
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      AggregatedStudyReport studyReport = (AggregatedStudyReport) getDataBean();

      pstmt.setDate(1, new Date(studyReport.getDate().getYear(), studyReport.getDate().getMonth(), studyReport.getDate().getDay()));

      ByteArrayInputStream inStream = new java.io.ByteArrayInputStream(studyReport.getBitmap());
      pstmt.setBinaryStream(2, inStream, studyReport.getBitmap().length);
    }
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

}
