/**
 * @(#) PersistentPerson.java
 */

package no.simula.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.des.persistence.PersistentPrivilege;
import no.simula.des.statements.PersonPrivilegesRelStatement;
import no.simula.des.statements.PersonStudyRelStatement;
import no.simula.des.statements.PrivilegePersonsRelStatement;
import no.simula.des.statements.ReportResponsibleRelStatement;
import no.simula.statements.PersonStatement;
import no.simula.wsclient.SimulaWSClient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 */
public class PersistentPerson implements PersistentObject {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(PersistentPerson.class);

	/**
	 * Constructor for PersistentPerson
	 */
	public PersistentPerson() {

	}

	/**
	 * Constructor for PersistentPerson
	 * 
	 * @param persons
	 *            Person[]
	 */
	public PersistentPerson(final Person[] persons) {

	}

	/**
	 * The Person object itself is in the simula database, and will not be
	 * created. But the privileges related to a person might be updated, so this
	 * method will update that relation
	 * 
	 * @param instance
	 * @return null
	 * 
	 * @see no.halogen.persistence.PersistentObject#create(Object)
	 */
	public Integer create(Object instance) {
		Person person = (Person) instance;

		setPrivilege(person.getId(), person);

		return (null);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never delete it ...
	 * The method is not implemented
	 * 
	 * @param id
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(String id) {
		return (false);
	}

	/**
	 * Finds a person by it's id/key
	 * 
	 * @param id -
	 *            The person's id/key
	 * @return Populated Person object
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
	 */
	public Object findByKey(String id) {
		Object result = new Object();
		Person person = new Person(); // dataBean to populate with data
		List privileges = null;

		try {
			person = SimulaWSClient.getInstance().getPeopleById(id);
		} catch (Exception e) {
			log.error("findByKey() - Fetching of a person with id " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof Person) {
			person = (Person) result;

			privileges = getPersonPrivileges(person.getId());
			person.setPrivileges(privileges);
		}

		return (person);
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param id -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public List findResponsibleByStudy(Integer id) {
		List personIds = new ArrayList();
		List persons = new ArrayList();
		List privileges = null;

		personIds = (ArrayList) getPersonsByStudy(id);

		persons = (ArrayList) findByKeys(personIds);

		if (persons != null) {
			Iterator i = persons.iterator();

			while (i.hasNext()) {
				Person entity = (Person) i.next();

				privileges = getPersonPrivileges(entity.getId());

				entity.setPrivileges(privileges);
			}
		} else {
			persons = new ArrayList();
		}

		return (persons);
	}

	/**
	 * The Person object itself is in the simula database, and will not be
	 * updated. But the privileges related to a person might be updated, so this
	 * method will update that relation
	 * 
	 * @param id
	 *            Integer
	 * @param instance
	 * 
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
	 */
	public boolean update(String id, Object instance) {
		boolean result = false;

		Person person = (Person) instance;

		/*
		 * First, delete the existing privileges
		 */
		PersonPrivilegesRelStatement statement = new PersonPrivilegesRelStatement();

		try {
			result = statement.executeDelete(id);

			result = setPrivilege(id, person);
		} catch (StatementException e) {
			log.error("update() - Could not delete privileges from person.");
		}

		return (result);
	}

	/**
	 * Method setPrivilege
	 * 
	 * @param id
	 *            Integer
	 * @param person
	 *            Person
	 * @return boolean
	 */
	private boolean setPrivilege(String id, Person person) {
		PersonPrivilegesRelStatement statement = new PersonPrivilegesRelStatement();

		return (setPrivilege(id, person, statement));
	}

	/**
	 * Method setPrivilege
	 * 
	 * @param id
	 *            Integer
	 * @param person
	 *            Person
	 * @param statement
	 *            PersonPrivilegesRelStatement
	 * @return boolean
	 */
	private boolean setPrivilege(String id, Person person, PersonPrivilegesRelStatement statement) {
		List privileges = person.getPrivileges();

		/*
		 * There's no databean representing the privilege-person, a list is
		 * created, and populated with the privilege and person ids
		 */
		if (!privileges.isEmpty()) {
			Iterator i = privileges.iterator();
			while (i.hasNext()) {
				List entity = new ArrayList();

				Privilege privilege = (Privilege) i.next();

				entity.add(privilege.getId());
				entity.add(id);

				statement.setDataBean(entity);

				try {
					statement.executeInsert();
				} catch (StatementException e) {
					log.error("update() - Could not delete privileges from person.");
				}
			}
		}

		return (true);
	}

	/**
	 * Method getPersonsByStudy
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getPersonsByStudy(Integer id) {

		ArrayList persons = new ArrayList(); // List to receive result from
		// the query

		/*
		 * Integer personId is the "dataBean". Since the tables are in two
		 * different databases, two queries must be performed. First one to
		 * fetch ids for the Simulaweb.People database, then one to get the
		 * Person information from Simulaweb. Since the first query won't return
		 * any populated objects, String in an ArrayList will contain the result
		 * (person ids)
		 */
		String personId = "";

		PersonStudyRelStatement studyPersonRelStatement = new PersonStudyRelStatement();

		studyPersonRelStatement.setDataBean(personId);

		studyPersonRelStatement.setWhereClause(" WHERE stu_resp_id = ?");
		studyPersonRelStatement.getConditions().add(id);

		try {
			persons = (ArrayList) studyPersonRelStatement.executeSelect();
		} catch (StatementException e) {
			log.error("findByStudy() - Fetching of persons related to study " + id + " failed!!", e);
			return (null);
		}

		return (persons == null) ? new ArrayList() : persons;
	}

	/**
	 * Finds persons by a list of identities/key
	 * 
	 * @param entityIds -
	 *            A List of perons id's, each id as an Integer
	 * @return List with populated Person objects. If the List is empty, all
	 *         persons will be returned
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKeys(List)
	 */
	public List findByKeys(List entityIds) {
		List persons = new ArrayList();

		if (!entityIds.isEmpty()) {

			List privileges = new ArrayList();

			try {
				persons = (List) SimulaWSClient.getInstance().getPeopleByIds(entityIds);
			} catch (Exception e) {
				log.error("findByKeys() - Fetching list of persons: " + entityIds.toString(), e);
				return (null);
			}

			Iterator i = persons.iterator();

			while (i.hasNext()) {
				Person entity = (Person) i.next();

				privileges = getPersonPrivileges(entity.getId());

				entity.setPrivileges(privileges);
			}
		}

		return (persons);
	}

	/**
	 * @param personId -
	 *            the id of the person to fetch privileges for
	 * @return A list of privileges
	 */
	private List getPersonPrivileges(String personId) {
		List condition = new ArrayList();
		PrivilegePersonsRelStatement statement = new PrivilegePersonsRelStatement();
		List results = new ArrayList();
		PersistentPrivilege persistentPrivilege = null;
		List privileges = new ArrayList();

		statement.setWhereClause(" WHERE people_id = ?");

		condition.add(personId);
		statement.setConditions(condition);

		try {
			results = (List) statement.executeSelect();
		} catch (StatementException e) {
			log.error("getPersonPrivileges() - Error when fetching privileges for person " + personId, e);
			return (null);
		}

		persistentPrivilege = new PersistentPrivilege();
		privileges = persistentPrivilege.findByKeys(results);

		return (privileges);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Object)
	 */
	public boolean update(Object instance) throws UpdatePersistentObjectException {
		Person person = (Person) instance;

		return (update(person.getId(), instance));
	}

	/**
	 * Finding all Persons
	 * 
	 * @return - All persons in the database
	 * 
	 * @see no.halogen.persistence.PersistentObject#find()
	 */
	public List find() {
		List persons = null;

		List privileges = new ArrayList();

		try {
			persons = (List) SimulaWSClient.getInstance().getAllPeople();
		} catch (Exception e) {
			log.error("findByKeys() - Fetching list of all persons failed", e);
			return (null);
		}

		Iterator i = persons.iterator();

		while (i.hasNext()) {
			Person entity = (Person) i.next();

			privileges = getPersonPrivileges(entity.getId());

			entity.setPrivileges(privileges);
		}

		return (persons);
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param email -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public Object findPersonByEmail(String email) {
		List persons = new ArrayList();

		try {
			persons = (List) SimulaWSClient.getInstance().getPeopleByEmail(email);
		} catch (Exception e) {
			log.error("findPersonByEmail() - Statement failed for person with email adress: " + email, e);
			return (null);
		}

		return (persons.get(0));
	}

	/**
	 * Method getPersonsByPrivilege
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getPersonsByPrivilege(Integer id) {
		ArrayList queryCollection = new ArrayList();
		ArrayList persons = new ArrayList(); // List to receive result from
		// the query

		/*
		 * Integer personId is the "dataBean". Since the tables are in two
		 * different databases, two queries must be performed. First one to
		 * fetch ids for the Simulaweb.People database, then one to get the
		 * Person information from Simulaweb. Since the first query won't return
		 * any populated objects, String in an ArrayList will contain the result
		 * (person ids)
		 */
		Integer personId = new Integer(0);

		PersonPrivilegesRelStatement statement = new PersonPrivilegesRelStatement();

		statement.setDataBean(personId);

		statement.setWhereClause(" WHERE pr_id = ?");
		statement.getConditions().add(id);

		try {
			persons = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("findByStudy() - Fetching of persons related to study " + id + " failed!!", e);
			return (null);
		}

		return (persons);
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param id -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public List findPersonsByPrivilege(Integer id) {
		List personIds = new ArrayList();
		List persons = new ArrayList();
		List privileges = null;

		personIds = (ArrayList) getPersonsByPrivilege(id);

		persons = (ArrayList) findByKeys(personIds);

		Iterator i = persons.iterator();

		while (i.hasNext()) {
			Person entity = (Person) i.next();

			privileges = getPersonPrivileges(entity.getId());

			entity.setPrivileges(privileges);
		}

		return (persons);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instances
	 * @return int
	 * @throws UpdatePersistentObjectException
	 */
	public int update(List instances) throws UpdatePersistentObjectException {
		int updated = 0;

		Iterator i = instances.iterator();

		while (i.hasNext()) {
			if (update(i.next())) {
				updated++;
			}
		}

		return (updated);
	}

	// ******************* A list of uninplemented methods ***********/
	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		throw new UpdatePersistentObjectException("Method not implemented!");
	}

	public Object findByKey(Integer id) {
		throw new RuntimeException("Method not implemented!");
	}

	/**
	 * As this entity is in the simulaweb database, DES will never delete it ...
	 * The method is not implemented
	 * 
	 * @param id
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(Integer id) throws DeletePersistentObjectException {
		throw new DeletePersistentObjectException("Method not allowed!");
	}

	/**
	 * Returns a list of fully populated Person objects for the given report
	 * 
	 * @param id
	 * @return
	 */
	public List findResponsibleByReportId(Integer id) {
		ArrayList peopleIds = new ArrayList(); // List to receive result from

		//asdf
		String personId = "";

		ReportResponsibleRelStatement studyPersonRelStatement = new ReportResponsibleRelStatement();

		studyPersonRelStatement.setDataBean(personId);

		studyPersonRelStatement.setWhereClause(" WHERE report_id = ?");
		studyPersonRelStatement.getConditions().add(id);

		try {
			peopleIds = (ArrayList) studyPersonRelStatement.executeSelect();
		} catch (StatementException e) {
			log.error("findByStudy() - Fetching of persons related to study " + id + " failed!!", e);
			return (null);
		}

		return findByKeys(peopleIds);

	}
}
