/*
 * Created on 22.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.persistence;

import java.util.ArrayList;

import no.simula.Person;
import junit.framework.TestCase;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentPersonTest extends TestCase {

	private PersistentPerson persistentPerson;
	private Person person;
	private ArrayList persons;
	
	private String personId;

	public void testFindResponsibleByStudy() {
		//TODO Implement findResponsibleByStudy().
		Integer studyId = new Integer(1);
		
		persistentPerson.findResponsibleByStudy(studyId);
	}

	public void testCreate() {
		//TODO Implement create().
		persistentPerson.create(person);
	}

	public void testDelete() {
		//TODO Implement delete().
	
		persistentPerson.delete(personId);
	}

	public void testFindByKey() {
		//TODO Implement findByKey().
		
		person = (Person) persistentPerson.findByKey(personId);
	}

	public void testUpdate() throws Exception {
		//TODO Implement update().
		
		persistentPerson.update(personId, person);
	}

	/** (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
//		super.setUp();
//		
//		persistentPerson = new PersistentPerson();
//				
//		personId = new Integer(1);
//		person = new Person(personId, "Frode", "Langseth", "frode.langseth@halogen.no", null);
	}

	/** (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
		
		persistentPerson = null;
		person = null;
		
		personId = null;
	}

	public void testFindResponsibleByStudy(Integer studyId) {
		//TODO Implement findResponsibleByStudy().
		persons = (ArrayList) persistentPerson.findResponsibleByStudy(studyId);
	}

}
