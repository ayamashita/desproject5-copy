/*
 * Created on 29.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.Publication;
import no.simula.des.statements.StudyPublicationRelStatement;
import no.simula.statements.PublicationPeopleRelStatement;
import no.simula.statements.PublicationStatement;
import no.simula.wsclient.SimulaWSClient;

/**
 * @author Frode Langseth
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentPublication implements PersistentObject {
	/**
	 * Field log
	 */
	private static Log log = LogFactory.getLog(PersistentPublication.class);

	/**
	 * As this entity is in the simulaweb database, DES will never create it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * @return null
	 * 
	 * @throws CreatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#create(Object)
	 */
	public Integer create(Object instance) throws CreatePersistentObjectException {
		return null;
	}

	/**
	 * As this entity is in the simulaweb database, DES will never delete it ...
	 * The method is not implemented
	 * 
	 * @param id
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(Integer id) {
		return (false);
	}

	/**
	 * Finds a publication by it's id/key
	 * 
	 * @param id -
	 *            The publication's id/key
	 * @return Populated Publication object
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
	 */
	public Object findByKey(String id) {
		Publication publication = null;

		try {
			publication = SimulaWSClient.getInstance().getPublicationById(id);
		} catch (Exception e) {
			log.error("findByKey() - Fetching of publication with id " + id + " failed!!", e);
			return (null);
		}
		
		return (publication);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param id
	 *            Integer
	 * @param instance
	 * 
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
	 */
	public boolean update(String id, Object instance) {
		return (false);
	}

	/**
	 * Finds publications by a list of identities/key
	 * 
	 * @param entityIds -
	 *            A List of publication ids, each id as an Integer
	 * @return List with populated Publication objects. If the List is empty,
	 *         all publications will be returned
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKeys(List)
	 */
	public List findByKeys(List entityIds) {
		List publications = new ArrayList(0);

		if (entityIds != null && !entityIds.isEmpty()) {

			try {
				publications = SimulaWSClient.getInstance().getPublicationsByIds(entityIds);
			} catch (Exception e) {
				log.error("findByKeys() - Fetching list of publications: " + entityIds.toString(), e);
				return (null);
			}
		}

		return (publications);
	}

	/**
	 * Finds publications by a study id, that is, finds the publications related
	 * to a study
	 * 
	 * @param id -
	 *            a study id
	 * @return List with populated Publication objects. If the List is empty,
	 *         all publications will be returned
	 * 
	 */

	public List findByStudy(Integer id) {
		List publicationIds = new ArrayList();
		List publications = new ArrayList(0);

		publicationIds = (ArrayList) getPublicationsByStudy(id);

		publications = (ArrayList) findByKeys(publicationIds);

		return (publications);
	}

	/**
	 * @param id
	 * @return ArrayList
	 */
	private ArrayList getPublicationsByStudy(Integer id) {
		ArrayList queryCollection = new ArrayList();
		ArrayList publications = new ArrayList(); // List to receive result
													// from the query

		/*
		 * String publicationId is the "dataBean". Since the tables are in two
		 * different databases, two queries must be performed. First, one to
		 * fetch ids for the Simulaweb.Publication database. Then, another one
		 * to get the Publication information from Simulaweb. Since the first
		 * query won't return any populated objects, String in an ArrayList will
		 * contain the result (publication ids)
		 */
		String publicationId = "";

		StudyPublicationRelStatement studyPublicationRelStatement = new StudyPublicationRelStatement();

		studyPublicationRelStatement.setDataBean(publicationId);

		studyPublicationRelStatement.setWhereClause(" WHERE stu_pub_id = ?");
		studyPublicationRelStatement.getConditions().add(id);

		try {
			publications = (ArrayList) studyPublicationRelStatement.executeSelect();
		} catch (StatementException e) {
			log.error("getPublicationsByStudy() - Fetching of publications related to study " + id + " failed!!", e);
			return (null);
		}

		return (publications);
	}

	/**
	 * @param publicationId -
	 *            Id of a publication to find related authors
	 * @return Comma separated list of author names
	 */
	private String getAuthors(String publicationId) {
		List condition = new ArrayList();
		List results = new ArrayList();
		StringBuffer authors = new StringBuffer();

		PublicationPeopleRelStatement statement = new PublicationPeopleRelStatement();

		statement.setWhereClause(" WHERE publication_id = ?");
		condition.add(publicationId);

		statement.setConditions(condition);

		try {
			results = (List) statement.executeSelect();
		} catch (StatementException e) {
			log.error("getAuthors() - Failed to get authors for publication " + publicationId, e);
			return (null);
		}

		Iterator i = results.iterator();

		boolean firstIteration = true;
		while (i.hasNext()) {
			List author = (List) i.next();

			if (!firstIteration) {
				authors.append(", ");
			}

			// gets the author name
			authors.append(author.get(0));

			firstIteration = false;
		}

		return (authors.toString());
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Object)
	 */
	public boolean update(Object instance) throws UpdatePersistentObjectException {
		Publication publication = (Publication) instance;

		return (update(publication.getId(), instance));
	}

	/**
	 * Finding all Publications
	 * 
	 * @return - All publicatoins in the database
	 * 
	 * @see no.halogen.persistence.PersistentObject#find()
	 */
	public List find() {
		List publications = null;
		
		try {
			publications = SimulaWSClient.getInstance().getAllPublications();
		} catch (Exception e) {
			log.error("findByKeys() - Fetching list of all publications failed", e);
			return (null);
		}
		
		return (publications);
	}

	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		return false;
	}

	public Object findByKey(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

}
