/**
 * @(#) PersonStatement.java
 */

package no.simula.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.halogen.utils.sql.SqlHelper;
import no.simula.Person;

/**
 * @author Frode Langseth
 * @deprecated THIS CLASS SHOULD NOT BE USED!!!!
 */
public class PersonStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersonStatement.class);

  //private String DATASOURCE = "jdbc:mysql://localhost:3306/simula?user=root&autoReconnect=true";
  /**
   * Field DATASOURCE
   */
  private String DATASOURCE = SqlHelper.getSimulaJNDIName();

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "people";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "";

  /**
   * Field KEY
   */
  private final String KEY = "people_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "people_id, people_first_name, people_family_name, people_email";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "people";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "";

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatement#fetchResults(java.sql.ResultSet)
   */
  /**
   * Method fetchResults
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Person person = null;

    try {
      if (getDataBean() != null) {
        person = (Person) getDataBean().getClass().newInstance();
      } else {
        person = new Person();
      }
    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new person data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new person data object");
      e.printStackTrace();
    }

    person.setId(new Integer(rs.getInt("people_id")));
    person.setFirstName(rs.getString("people_first_name"));
    person.setFamilyName(rs.getString("people_family_name"));
    person.setEmail(rs.getString("people_email"));

    return (person);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateUpdateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateUpdateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateUpdateValues(PreparedStatement pstmt) throws SQLException {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  /**
   * Method getInsertColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getKey()
   */
  /**
   * Method getKey
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    // TODO Auto-generated method stub
    return (KEY);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  /**
   * Method getSelectColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    // TODO Auto-generated method stub
    return (SELECT_COLUMNS);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getTableName()
   */
  /**
   * Method getTableName
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    // TODO Auto-generated method stub
    return (TABLE_NAME);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  /**
   * Method getUpdateValuesString
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    // TODO Auto-generated method stub
    return null;
  }

  /**
  	* Gets connection to the Simulaweb database, then executes super.executeSelect()
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public List executeSelect() throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect();
  }

  /**
  	* Gets connection to the Simulaweb database, then executes super.executeSelect()
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public Object executeSelect(Integer entityId) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return (super.executeSelect(entityId));
  }

  /**
  	* Gets connection to the Simulaweb database, then executes super.executeSelect()
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public List executeSelect(List entityIds) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return (super.executeSelect(entityIds));
  }

  /**
  	* DES shall not manage any content in Simulaweb, so this method is empy
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public boolean executeDelete() throws StatementException {
    return (false);
  }

  /**
  	* DES shall not manage any content in Simulaweb, so this method is empy
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public boolean executeDelete(Integer entityId) throws StatementException {
    return (false);
  }

  /**
  	* DES shall not manage any content in Simulaweb, so this method is empy
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public Integer executeInsert() throws StatementException {
    return (null);
  }

  /**
  * DES shall not manage any content in Simulaweb, so this method is empy
  * 
  * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  */
  public boolean executeUpdate() throws StatementException {
    return (false);
  }

  /**
  * DES shall not manage any content in Simulaweb, so this method is empy
  * 
  * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  */
  public boolean executeUpdate(Integer entityId) throws StatementException {
    return (false);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    // TODO Auto-generated method stub
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    }
  }

}
