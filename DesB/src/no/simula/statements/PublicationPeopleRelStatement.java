/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.halogen.utils.sql.SqlHelper;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PublicationPeopleRelStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PublicationPeopleRelStatement.class);

  //private String DATASOURCE = "jdbc:mysql://localhost:3306/simula?user=root&autoReconnect=true";
  /**
   * Field DATASOURCE
   */
  private String DATASOURCE = SqlHelper.getSimulaJNDIName();

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "java.util.String";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "";

  /**
   * Field KEY
   */
  private final String KEY = "publication_author_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "external_name, publication_id";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "publication_author";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "";

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  /**
   * Method getInsertColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getKey()
   */
  /**
   * Method getKey
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  /**
   * Method getSelectColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getTableName()
   */
  /**
   * Method getTableName
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  /**
   * Method getUpdateValuesString
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return null;
  }

  /**
   * Creates a list that contains author name and publication id
   * 
   * @rs - Resultset row
   * @param rs ResultSet
   * @return The author name and publication id
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    List author = null;

    try {
      if (getDataBean() != null) {
        author = (List) getDataBean().getClass().newInstance();
      } else {
        author = new ArrayList();
      }
    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new publication people rel type data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new publication people rel type data object");
      e.printStackTrace();
    }

    author.add(rs.getString("external_name"));
    author.add(new Integer(rs.getInt("publication_id")));

    return (author);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    }
  }

  /**
  	* Gets connection to the Simulaweb database, then executes super.executeSelect()
  	* 
  	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
  	*/
  public List executeSelect() throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect();
  }

  /**
  * Gets connection to the Simulaweb database, then executes super.executeSelect()
  * 
  * @see no.halogen.statements.ObjectStatement#executeSelect(java.util.List)
  */
  public List executeSelect(List entityIds) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect(entityIds);
  }

  /**
   * Gets connection to the Simulaweb database, then executes super.executeSelect()
   * 
   * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
   */
  public Object executeSelect(Integer entityId) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect(entityId);
  }
}
