package no.simula.wsclient;

public interface StringIdObject {

	public String getId();
	public void setId(String id);
}
