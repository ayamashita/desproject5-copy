<%@page import="java.util.*"%>
<%
    // Get status from the session object to check if user has already logged in
    if ( true ) {   // Create a local scope for this file
    	String status = (String)session.getAttribute("status");
    	
    	if (status == null || status.length() == 0 || !status.equalsIgnoreCase("ok")) {
    		response.sendRedirect(request.getContextPath() + "/jsp/login.jsp");
    		return;
    	}
    }

%>