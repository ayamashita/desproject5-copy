<!-- THIS IS THE MAIN PAGE FOR DES ADMINS --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@include file="check_login.jsp" %>
<%@include file="../header.jsp" %>
<%
	// First get some stats about the user
  
    HttpSession sess = request.getSession();
   
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
    //System.err.println("----------: " + new Date().toString());
    //System.err.println("username=" + username + ", status=" + status);
    //System.err.println("Actions=" + DB.getInstance().getAllUserActionsAsString(actions));
    if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">


<p>
    No Access, please login
</p>
</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

        
<%      return;
    } else { %>

<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="40%">
<p>
<span class="path">studies > db admin</span>


<p>
Welcome to SIMULA DES Data & Access Maintenance. Here you can maintain system data, configure user types and their access privileges, and
clean up the files uploaded to the server. 
</p>

<ul type="square">
<li>
<p class="bodytext-bold">
<a title="Click to maintain Study Types in the system" href="edit_studytype.jsp">Study types</a>
</p>
</li>
<li>
<p class="bodytext-bold">
<a title="Click to maintain Units in the system" href="edit_unit.jsp">Units</a>
</p>
</li>
<li>
<p class="bodytext-bold">
<a title="Click to maintain Actions in the system" href="edit_action.jsp">Actions</a>
</p>
</li>
<li>
<p class="bodytext-bold">
<a title="Click to maintain User Types in the system, and their access rights" href="edit_usertype.jsp">User types</a>
</p>
</li>
<li>
<p class="bodytext-bold">
<a title="Click to execute a cleanup of any orphan files uploaded to the server" href="delete_orphan_files.jsp">Orphan File Cleanup</a>
</p>
</li>
</ul>
</td>

<td valign="top" width="30%">
<br><br><br>
<jsp:include page="../menu.jsp" />
</td>

<td width="15%">
&nbsp;
</td>

</tr>





	

</td>


<td width="15%">
&nbsp;
</td>

</tr>

</table>


<% } %>


<!-- Include standard footer -->
