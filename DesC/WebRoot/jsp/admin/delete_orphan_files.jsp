<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*" %>
<%@include file="check_login.jsp" %>
<%
	int numDeleted = 0;
String action = request.getParameter("action");
if (action != null && action.equals("delete")){
    numDeleted = DB.getInstance().deleteOrphanFiles();
}
%>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	// First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    String pageTitle = "Main";
   
    if ( !DB.getInstance().listContainsAction(actions, "DELETE_ORPHAN_FILES")) {
%>

        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>

      No Access, please login
</p>
</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;
    } else { %>



<!-- MAIN BODY -->
<tr>

<td width="15%">
&nbsp;
</td>

<td valign="top" width="40%">
<p>
<span class="path">studies > db admin > cleanup orphan files</span>
<p>
<%
if (action != null && action.equals("delete")){
%>
Deleted <%=numDeleted %> orphan files.<p>
<a href="dataAdmin_main.jsp">Back</a>
<%
}
else{
%>

Orphan files are files that have been uploaded but not connected to any study.
Pressing the button will delete all orphan files from the database. Note that
this will also delete any files from currently logged in users who are in the process 
of creating a new study, but have yet not commited that study. 
<p>
<form action="delete_orphan_files.jsp">
    <input type="hidden" name="action" value="delete">
    <input class="input" type="submit" value="Delete orphan files">
</form>
<% } %>
</td>


<td valign="top" width="30%">
<br><br><br>
<jsp:include page="../menu.jsp" />
</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>


<% } %>


<!-- Include standard footer -->
<br><br><br><br>
<jsp:include page="../footer.jsp" />

