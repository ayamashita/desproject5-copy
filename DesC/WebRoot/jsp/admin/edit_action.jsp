<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@include file="check_login.jsp" %>
<%
	// First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    // Get the action from the request
    String action = request.getParameter("action");
    String nextAction = null;
    if ( action == null || action.length() == 0 ) {
        action = "list";
    }

    // Set the pagetitle and name of this page
    String pageTitle = "Actions";
    String message = "";
    String pgName = "edit_action.jsp";

    // Set the type of master data this file applies to and some other
    // variables
    Action instance = null;
    List entityList = null;
    String entityName = "Action";
    Class entityClass = Action.class;

    // Get the id if this is a delete or edit operation
    String idStr = (String)request.getParameter("id");
    int id = NumberUtils.stringToInt(idStr, -1);

    // Do preprocessing depending on operation
    if ( "list".equalsIgnoreCase(action))  { 
        entityList = MasterDataDAO.getInstance().getList(entityClass);
    } else if ( "new".equalsIgnoreCase(action)) {
        pageTitle = "New " + entityName;
        nextAction = "create";
    } else if ( "edit".equalsIgnoreCase(action)) {
        pageTitle = "Edit " + entityName;
        nextAction = "update";
        if ( id != -1 ) {
            instance = (Action)MasterDataDAO.getInstance().get(id, entityClass);
        }
    } else if ( "create".equalsIgnoreCase(action)) {
        // Create a new instance of studytype in db
        message = "<p>" + entityName + " created. " + "<a href=\"" + pgName + "\">Back</a></p>";
        instance = Action.createAction(request.getParameterMap());
        MasterDataDAO.getInstance().newInstance(instance);
    } else if ( "delete".equalsIgnoreCase(action)) {
        // Delete studytype in db
        message = "<p>" + entityName + " deleted. " + "<a href=\"" + pgName + "\">Back</a></p>";
        if ( id != -1 ) {
            MasterDataDAO.getInstance().delete(id, entityClass);
        }
    } else if ( "update".equalsIgnoreCase(action)) {
        // Update studytype in db
        message = "<p>" + entityName + " updated. " + "<a href=\"" + pgName + "\">Back</a></p>";
        instance = Action.createAction(request.getParameterMap());
        instance.setActionId(id);
        MasterDataDAO.getInstance().update(instance);
    }
%>
<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>
<p>
    No Access, please login

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%
	return;
    } else {
%>


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > db admin > actions</span>

<p>
Actions are the various functions that have restricted access in the system. For instance, access to the Main Administrator page is restricted to users of user type that has the privilege "VIEW_MAIN".
Creating a new action will not have any effect, unless combined with conditioning the display of a function, page or information. Please refer to the system documentation for details on these issues.
</p>

<%
	if ( "list".equalsIgnoreCase(action) || "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%>
    
  	<br><span class="content"><a title="Click to create a new action" href="<%=pgName%>?action=new">Create new</a> &nbsp; | &nbsp; <a title="Click to list all actions" href="<%=pgName%>?action=list">List All</A></span><br><br>

<%
	}
%>  



<%
  	if ( "list".equalsIgnoreCase(action))  {
  %>  <%-- List all entities --%>

<table cellpadding="5" cellspacing="1">


<tr>
  <td class="label">Name</td>
  <td class="label">Description</td>
  <td class="label">&nbsp;</td> 
</tr>


<%
	int size = entityList != null ? entityList.size() : 0;
Action st = null;
for ( int i = 0;i < size;i++) {
st = (Action)entityList.get(i); 
int tmpid = st.getActionId();
%>
<tr>
    <td class="content"><%=st.getActionName()%></td>
    <td class="content"><%=st.getActionDesc()%></td>
    <td class="content">
        <a title="Click to edit this action" href="<%=pgName%>?action=edit&id=<%=tmpid%>">Edit</a>&nbsp;
        <!--<a href="<%=pgName%>?action=delete&id=<%=tmpid%>">[Delete]</a>-->
    </td>
</TR>
<%
	}
%>
<tr>
                     <td colspan="3"><input class="input" type="button" onCLick="javascript:document.location.href='/DesC/jsp/admin/dataAdmin_main.jsp';" value="Back"></td>
                </tr>
</table>


<%
	} else if ( "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%> <%-- Present empty form --%>

            <form action="<%=pgName%>" method="POST">
            <input type="hidden" name="action" value="<%=nextAction%>"/>
            <input type="hidden" name="id" value="<%=instance != null ? instance.getActionId() : -1%>"/>
            <table>
                <tr>
                    <td class="label2">Name: </td>
                    <td title="Input a name for the action (do not change existing ones)" class="content2"><input class="input" type="text" name="<%=Action.ACTION_NAME%>" 
                        value="<%=instance != null ? instance.getActionName() : ""%>">
                    </td>
                </tr>
                <tr>
                    <td class="label2">Description: </td>
                    <td title="Input a description of the action" class="content2"><input class="input" type="text" name="<%=Action.ACTION_DESC%>" 
                        value="<%=instance != null ? instance.getActionDesc() : ""%>">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="input" type="submit" value="Confirm >">&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='/jsp/admin/edit_action.jsp';" value="Cancel"></td>
                </tr>
            </table>
            </form>
        <% } else if ( "create".equalsIgnoreCase(action)) { %>  <%-- Create new instance in database--%>
            <%=message%>
        <% } else if ( "delete".equalsIgnoreCase(action)) { %> <%-- Delete instance from database--%>
            <%=message%>
        <% } else if ( "update".equalsIgnoreCase(action)) { %> <%-- Update instance in database--%>
            <%=message%>
        <% } %>
        
    
        </td>
<td width="15%">
&nbsp;
</td>

    </tr>
</table>

<% } %>


