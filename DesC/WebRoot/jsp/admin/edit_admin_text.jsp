<!-- THIS IS THE EDIT ADMIN TEXT PAGE --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@include file="check_login.jsp" %>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	String action = request.getParameter("action");

if (action != null){
	// Get request parameters
	String message = request.getParameter("message");

	if (action.equals("update")){
		// no need for to validate input
        // insert message into database 
        DB.getInstance().updateAdminMessage(message);
		// Go back to front page
		
		//out.print(backLink);
	} // end if action is update
} // end if action is set
//else{ // no action

%>





<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > edit text</span>


<p>Type message to appear on administration start page, then press the 'Confirm' button to continue. Feel free to use HTML for better style.</p>



<form action="edit_admin_text.jsp" method="POST">

<%-- Get message from database and display it for editing--%>
<p>
<center>
<textarea class="input" cols="130" rows="30" name="message">
<%=DB.getInstance().getAdminMessage()%>
</textarea>
</center>
</p>

<p>
<input type="hidden" name="action" value="update">
<input class="input" type="submit" value="Confirm >">&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='<% request.getContextPath(); %>/DesC/jsp/admin/main.jsp';" class="content" value="Back">
</p>
</form>


</td>
<td width="15%">
&nbsp;
</td>

</tr>

</table>





<!-- Include standard footer -->

