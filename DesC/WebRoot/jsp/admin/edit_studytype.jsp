<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@include file="check_login.jsp" %>
<%
	// First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");

    // Get the action from the request
    String action = request.getParameter("action");
    String nextAction = null;
    if ( action == null || action.length() == 0 ) {
        action = "list";
    }

    // Set the pagetitle and name of this page
    String pageTitle = "Study Types";
    String message = "";
    String pgName = "edit_studytype.jsp";

    // Set the type of master data this file applies to and some other
    // variables
    StudyType instance = null;
    List entityList = null;
    String entityName = "Study Type";
    Class entityClass = StudyType.class;

    // Get the id if this is a delete or edit operation
    String idStr = (String)request.getParameter("id");
    int id = NumberUtils.stringToInt(idStr, -1);

    // Get the allowed actions for this user
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
        response.sendRedirect("main.jsp");
    }

    // Do preprocessing depending on operation
    if ( "list".equalsIgnoreCase(action))  { 
        entityList = MasterDataDAO.getInstance().getList(entityClass);
    } else if ( "new".equalsIgnoreCase(action)) {
        pageTitle = "New " + entityName;
        nextAction = "create";
    } else if ( "edit".equalsIgnoreCase(action)) {
        pageTitle = "Edit " + entityName;
        nextAction = "update";
        if ( id != -1 ) {
            instance = (StudyType)MasterDataDAO.getInstance().get(id, StudyType.class);
        }
    } else if ( "create".equalsIgnoreCase(action)) {
        // Create a new instance of studytype in db
        message = "<p>" + entityName + " created. " + "<a href=\"" + pgName + "\">Back</a></p>";
        instance = StudyType.createStudyType(request.getParameterMap());
        MasterDataDAO.getInstance().newInstance(instance);
    } else if ( "delete".equalsIgnoreCase(action)) {
        // Delete studytype in db
        message = "<p>" + entityName + " deleted. " + "<a href=\"" + pgName + "\">Back</a></p>";
        if ( id != -1 ) {
            MasterDataDAO.getInstance().delete(id, entityClass);
        }
    } else if ( "update".equalsIgnoreCase(action)) {
        // Update studytype in db
        message = "<p>" + entityName + " updated. " + "<a href=\"" + pgName + "\">Back</a></p>";
        instance = StudyType.createStudyType(request.getParameterMap());
        instance.setStudyTypeId(id);
        MasterDataDAO.getInstance().update(instance);
    }
%>
<!-- Include standard header -->
<jsp:include page="../header.jsp" />



<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > db admin > study types</span>

<%
	if ( "list".equalsIgnoreCase(action) || "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%>
    
  	<br><span class="content"><a title="Click to create a new study type" href="<%=pgName%>?action=new">Create new</a> &nbsp; | &nbsp; <a title="Click to list all study types" href="<%=pgName%>?action=list">List All</A></span><br><br>

<%
	}
%>  



<%
  	if ( "list".equalsIgnoreCase(action))  {
  %>  <%-- List all entities --%>

<table cellpadding="5" cellspacing="1">


<tr>
  <td class="label">Name</td>
  <td class="label">Description</td>
  <td class="label">&nbsp;</td> 
</tr>


<%
	int size = entityList != null ? entityList.size() : 0;
StudyType st = null;
for ( int i = 0;i < size;i++) {
st = (StudyType)entityList.get(i); 
int tmpid = st.getStudyTypeId();
%>
<tr>
    <td class="content"><%=st.getStudyTypeName()%></td>
    <td class="content"><%=st.getStudyTypeDesc()%></td>
    <td class="content">
        <a title="Click to edit this study type" href="<%=pgName%>?action=edit&id=<%=tmpid%>">Edit</a>&nbsp;
        <!--<a href="<%=pgName%>?action=delete&id=<%=tmpid%>">[Delete]</a>-->
    </td>
</TR>

<%
	}
%>
<tr>
                     <td colspan="3"><input class="input" type="button" onCLick="javascript:document.location.href='/DesC/jsp/admin/dataAdmin_main.jsp';" value="Back"></td>
                </tr>
</table>


<%
	} else if ( "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%> <%-- Present empty form --%>

            <form action="<%=pgName%>" method="POST">
            <input type="hidden" name="action" value="<%=nextAction%>"/>
            <input type="hidden" name="id" value="<%=instance != null ? instance.getStudyTypeId() : -1%>"/>
            <table>
                <tr>
                    <td class="label2">Name: </td>
                    <td title="Input the name of the study type" class="content2"><input class="input" type="text" name="<%=StudyType.STUDY_TYPE_NAME%>" 
                        value="<%=instance != null ? instance.getStudyTypeName() : ""%>">
                    </td>
                </tr>
                <tr>
                    <td class="label2">Description: </td>
                    <td title="Input a description of the study type" class="content2"><input class="input" type="text" name="<%=StudyType.STUDY_TYPE_DESC%>" 
                        value="<%=instance != null ? instance.getStudyTypeDesc() : ""%>"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input title="Click to save the study type" class="input" type="submit" value="Confirm >">&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='/DES/jsp/admin/edit_studytype.jsp';" value="Cancel"></td>
                </tr>
            </table>
            </form>
        <% } else if ( "create".equalsIgnoreCase(action)) { %>  <%-- Create new instance in database--%>
            <%=message%>
        <% } else if ( "delete".equalsIgnoreCase(action)) { %> <%-- Delete instance from database--%>
            <%=message%>
        <% } else if ( "update".equalsIgnoreCase(action)) { %> <%-- Update instance in database--%>
            <%=message%>
        <% } %>
        
   
        </td>
<td width="15%">
&nbsp;
</td>

    </tr>
</table>




