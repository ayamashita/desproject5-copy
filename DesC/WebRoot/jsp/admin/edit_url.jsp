<%@ page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="org.apache.commons.lang.*, java.io.*, java.util.*, no.machina.simula.*"%>
<%@include file="check_login.jsp" %>

<%
	String errorMsg = "";
String action = request.getParameter("action");
System.err.println("edit_url: action = " + action);
String studyAction = request.getParameter("study_action");
System.err.println("edit_url: studyAction = " + studyAction);
String studyIdParam = request.getParameter("study_id");
int studyId = -1;
try{
    studyId = Integer.parseInt(studyIdParam);
}
catch(NumberFormatException nfe){
}

if (action != null && action.equals("add")){
    String url = StringUtils.defaultString(request.getParameter("url"));
    String urlDesc = StringUtils.defaultString(request.getParameter("url_desc"));

    if (StringUtils.isNotBlank(url)){
        // Validate url
        if (Validator.isUrl(url)) {
            // set default desc = url if it's blank
            if (StringUtils.isBlank(urlDesc)){
                urlDesc = url;
            }

            // add to session
            List urls = (List) session.getAttribute("urls");
            if (urls == null){
                urls = new ArrayList();
            }
            urls.add(new Url(url, urlDesc));
            session.setAttribute("urls", urls);
        }
        else{
            errorMsg = "Not a valid URL. Remember to include the scheme, e.g. http, https, ftp.";
        }
    }
}
else {
    if (action != null && action.equals("remove")){
        String urlIdParam = request.getParameter("url_id");
        int urlId = 0;
        try{
            urlId =  Integer.parseInt(urlIdParam);
            // Remove fileId from session
            List urls = (ArrayList) session.getAttribute("urls");
            urls.remove(urlId);
            session.setAttribute("urls", urls);
        }
        catch(NumberFormatException nfe){
            errorMsg = "Error! url_id is not an integer.";
        }
    }
}
%>


<!-- Include standard header -->
<jsp:include page="../header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > cerate new > add url</span>


<%
	if (StringUtils.isNotBlank(errorMsg)){
%>
    <p>
    <%=errorMsg%>
    </p>
<%
	}
%>

<p>

Here you can add URLs that will be attached to the study. 
Provide a description/title of the URL (the default descrition is the URL itself). Then click the 'Add URL' button to confirm.

<p>

<form action="edit_url.jsp" method="POST">
    <span class="label2">URL Description:</span> <input class="input" type="text" name="url_desc" value=""><p>
    <%
    	if (studyAction != null && studyAction.equals("edit")){
    %>
        <input type="hidden" name="study_id" value="<%=studyId%>">
        <input type="hidden" name="study_action" value="<%=studyAction%>">
    <%
    	}
    %>
    <span class="label2">URL:</span> <input class="input" type="text" name="url" size=40> <p>
    <input type="hidden" name="action" value="add">
    <input class="input" type=submit value="Add URL"><p>
</form>

<p>


<form action="study_edit.jsp" method="POST">
<%
	// Get file ids from session
    List  urls = (List) session.getAttribute("urls");
    if (urls != null){
%>
    <fieldset>
    <legend class="fieldheader">URL List</legend>
        <table>
<%
	// List urls
        String editParameters = ""; // if it's an existing study, study_action and study_id must be passed on
        if (studyAction != null && studyAction.equals("edit")){
            editParameters = "&study_action=" + studyAction + "&study_id=" + studyId;
        }
        int urlId = 0;
        for(ListIterator i = urls.listIterator(); i.hasNext(); ){
            Url u = (Url) i.next();
%>
            <tr><td><a href="<%=u.getUrl() %>" target="_blank"><%=u.getDesc()%><a></td><td> 
            <a href="edit_url.jsp?action=remove&url_id=<%=urlId %><%=editParameters %>">Remove</a></td></tr>
            <%-- input type="hidden" name="url" value="<%=u.getUrl() %>;<%=u.getDesc() %>" --%>
        <%
            urlId++;
        }
%>
        </table>
    </fieldset>
<%
    }
%>


    <%-- Set attributes --%>
    <input type="hidden" name="study_name" value="<%=session.getAttribute("study_name") %>">
    <input type="hidden" name="study_type_id" value="<%=session.getAttribute("study_type_id") %>">
    <input type="hidden" name="study_duration" value="<%=session.getAttribute("study_duration") %>">
    <input type="hidden" name="study_duration_unit_id" value="<%=session.getAttribute("study_duration_unit_id") %>">
    <input type="hidden" name="study_start_date" value="<%=session.getAttribute("study_start_date") %>">
    <input type="hidden" name="study_end_date" value="<%=session.getAttribute("study_end_date") %>">
    <input type="hidden" name="study_desc" value="<%=session.getAttribute("study_desc") %>">
    <input type="hidden" name="study_keywords" value="<%=session.getAttribute("study_keywords") %>">
    <input type="hidden" name="number_of_students" value="<%=session.getAttribute("number_of_students") %>">
    <input type="hidden" name="number_of_professionals" value="<%=session.getAttribute("number_of_professionals") %>">

    <%
     String[] studyResponsibleIds = (String[]) session.getAttribute("study_responsible_ids");
     if (studyResponsibleIds != null){
         for (int i = 0; i < studyResponsibleIds.length; i++){
        %>
            <input type="hidden" name="study_responsible_ids" value="<%=studyResponsibleIds[i]%>">
        <%
         }
     }

     String[] studyPublicationIds = (String[]) session.getAttribute("study_publication_ids");
     if (studyPublicationIds != null){
         for (int i = 0; i < studyPublicationIds.length; i++){
        %>
            <input type="hidden" name="study_publication_ids" value="<%=studyPublicationIds[i]%>">
        <%
         }
     }


    if (studyAction != null && studyAction.equals("edit")){
    %>
        <input type="hidden" name="action" value="edit_with_file">
        <input type="hidden" name="study_id" value="<%=studyId %>">
        <%--input type="hidden" name="action" value="<%=studyAction %>" --%>
    <%
    }
    else{
    %>
        <input type="hidden" name="action" value="insert_with_file">
    <%
    }
    %>
    <p>
    <input class="input" type="submit" value="Finished">
</form>




	

</td>



<td width="15%">
&nbsp;
</td>

</tr>

</table>






<!-- Include standard footer -->
<jsp:include page="../footer.jsp" />

