<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="check_login.jsp" %>
<%
	// First get some stats about the user
    HttpSession sess = request.getSession();
    Logger log = Logger.getLogger(this.getClass());

    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    // Get the action from the request
    String action = request.getParameter("action");
    String nextAction = null;
    if ( action == null || action.length() == 0 ) {
        action = "list";
    }
    log.debug("edit_user: " + action);

    // Set the pagetitle and name of this page
    String pageTitle = "Users";
    String pgName = "edit_user.jsp";

    // Set the type of master data this file applies to and some other
    // variables
    People instance = null;
    List entityList = null;
    String entityName = "User";
    Class entityClass = People.class;

    // Get the id if this is a delete or edit operation
    String idStr = (String)request.getParameter("id");
    int id = NumberUtils.stringToInt(idStr, -1);

    // Get the usertypes available
    List userTypes = MasterDataDAO.getInstance().getList(UserType.class);

    // Do preprocessing depending on operation
    if ( "list".equalsIgnoreCase(action))  { 
        entityList = MasterDataDAO.getInstance().getList(entityClass);
    } else if ( "new".equalsIgnoreCase(action)) {
        //pageTitle = "New " + entityName;
        //nextAction = "create";
    } else if ( "edit".equalsIgnoreCase(action)) {
        //pageTitle = "Edit " + entityName;
        //nextAction = "update";
        //if ( id != -1 ) {
        //    instance = (User)MasterDataDAO.getInstance().get(id, entityClass);
        //}
    } else if ( "create".equalsIgnoreCase(action)) {
        // Create a new instance of studytype in db
        //pageTitle = entityName + " created." + "<a href=\"" + pgName + "\">Back</a>";
        //instance = User.createAction(request.getParameterMap());
        //MasterDataDAO.getInstance().newInstance(instance);
    } else if ( "delete".equalsIgnoreCase(action)) {
        // Delete studytype in db
        //pageTitle = entityName + " deleted." + "<a href=\"" + pgName + "\">Back</a>";
        //if ( id != -1 ) {
        //   MasterDataDAO.getInstance().delete(id, entityClass);
        //}
    } else if ( "update".equalsIgnoreCase(action)) {
        // Get the list of people and usertypes in database
        entityList = MasterDataDAO.getInstance().getList(entityClass);

        // Get all the incoming attribute names
        Enumeration enume = request.getParameterNames();
        log.debug("Iterating attributes");

        // Iterate over all attributes and check if changed
        int count = 0;
        while ( enume.hasMoreElements() ) {
            // Parse the request variables
            Object value = enume.nextElement();
            String paramName = (String)value;
            log.debug("Checking " + paramName);
            if ( !paramName.startsWith("user_type_id")) {
                continue;
            }
            int lastIndex = paramName.lastIndexOf("_");
            String peopleIdStr = paramName.substring(lastIndex+1);
            String userTypeIdStr = request.getParameter(paramName);
            DB.getInstance().modifyUserType(peopleIdStr, userTypeIdStr, entityList);
            count++;
        }
        log.debug("Done with " + count + " attributes");


        response.sendRedirect(pgName + "?action=done");
        
    }
%>
<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%
	return;
    } else {
%>


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > user privileges</span>


<%
	if ( "done".equalsIgnoreCase(action) ) {
%>
<br>

<p>
User privileges updated. Click <a href="main.jsp">here</a> to return to main menu.

<%
	}
%>


<%
	if ( "list".equalsIgnoreCase(action) ) {
%>
<br>

<p>
From the list below, you can grant access rights to the DES system. Set the user type in the 'DES Usertype' column. Then, click the 'Update' button to confirm your changes.
<p>

<form action="edit_user.jsp" method="post">
<table cellpadding="5" cellspacing="1">
<tr>
  <td class="label">Name</td>
  <td class="label">Position</td>
  <td class="label">DES Usertype</td> 
</tr>

<%
	int size = entityList != null ? entityList.size() : 0;
People st = null;
for ( int i = 0;i < size;i++) {
st = (People)entityList.get(i); 
String tmpid = st.getPeopleId();
int peopleUserTypeId = st.getUserTypeId();
%>
<tr>
    
        <input type="hidden" name="action" value="update"/>
    <td class="content"><%=st.getPeopleFirstName()%>&nbsp;<%=st.getPeopleFamilyName()%> </td>
    <td class="content"><%=st.getPeoplePosition()%></td>
    <td class="content">
        <select name="user_type_id_<%=tmpid%>">
            <option value="-1">- None -</option>
            <%
            	int typesize = userTypes != null ? userTypes.size() : 0;
                           for ( int j = 0;j < typesize;j++) {
                               UserType tmptype = (UserType)userTypes.get(j);
                               int typeid  = tmptype.getUserTypeId();
                               String isSelected = typeid == peopleUserTypeId ? "selected" : "";
                               String typename = tmptype.getUserTypeName();
            %>
                <option value="<%=typeid%>" <%=isSelected%>>
                    <%=typename%>
                </option>
            <% } %>
        </SELECT>
    </td>
    <td class="content">
        <!--input type="hidden" name="<%=People.PEOPLE_ID_DB%>" value="<%=tmpid%>"/-->
    </td>
    
    
</TR>
<% } %>

<tr>
                     <td colspan="3"><input class="input" type="submit" value="Confirm >"/>&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='../admin/main.jsp';" value="Cancel"></td>
                </tr>
<% } %>





</table>

</form>

</td>
<td width="15%">
&nbsp;
</td>
</tr>
</table>

<% } %>

<!-- Include standard footer -->
<jsp:include page="../footer.jsp" />
