<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="check_login.jsp" %>
<%
	Logger log = Logger.getLogger(this.getClass());
	
    // First get some stats about the user
    // First get some stats about the user
    HttpSession sess = request.getSession();

    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    // Get the action from the request
    String action = request.getParameter("action");
    String nextAction = null;
    if ( action == null || action.length() == 0 ) {
        action = "list";
    }

    // Set the pagetitle and name of this page
    String pageTitle = "User Types";
    String message = "";
    String pgName = "edit_usertype.jsp";

    // Set the type of master data this file applies to and some other
    // variables
    UserType instance = null;
    List entityList = null;
    String entityName = "User Type";
    Class entityClass = UserType.class;

    // Get the id if this is a delete or edit operation
    String idStr = (String)request.getParameter("id");
    int id = NumberUtils.stringToInt(idStr, -1);

    // Additional lists of actions for the usertype
    List validActions = null;
    List allActions = null;
    List invalidActions = new ArrayList();

    // Do preprocessing depending on operation
    if ( "list".equalsIgnoreCase(action))  { 
        entityList = MasterDataDAO.getInstance().getList(entityClass);
    } else if ( "new".equalsIgnoreCase(action)) {
        pageTitle = "New " + entityName;
        nextAction = "create";
    } else if ( "edit".equalsIgnoreCase(action)) {
        pageTitle = "Edit " + entityName;
        nextAction = "update";
        if ( id != -1 ) {
            instance = (UserType)MasterDataDAO.getInstance().get(id, entityClass);
            // Get the allowed actions
            validActions = DB.getInstance().getUserActions(id);
            log.info("Got " + validActions.size() + " valid actions for id " + id);
            allActions = MasterDataDAO.getInstance().getList(Action.class);
            // Get the not allowed actions
            int size = allActions.size();
            for ( int i = 0;i < size;i++) {
                if ( !validActions.contains(allActions.get(i))) {
                    invalidActions.add(allActions.get(i));
                }
            }
        }
    } else if ( "create".equalsIgnoreCase(action)) {
        // Create a new instance of studytype in db
        message = "<p>" + entityName + " created. To configure the access rights for this usertype, edit it from the list " + "<a href=\"" + pgName + "\">here</a></p>";
        instance = UserType.createUserType(request.getParameterMap());
        MasterDataDAO.getInstance().newInstance(instance);
    } else if ( "delete".equalsIgnoreCase(action)) {
        // Delete studytype in db
        message = "<p>" + entityName + " deleted. " + "<a href=\"" + pgName + "\">Back</a></p>";
        if ( id != -1 ) {
            MasterDataDAO.getInstance().delete(id, entityClass);
        }
    } else if ( "update".equalsIgnoreCase(action)) {
        // Update studytype in db
        message = "<p>" + entityName + " updated. " + "<a href=\"" + pgName + "\">Back</a></p>";
        instance = UserType.createUserType(request.getParameterMap());
        instance.setUserTypeId(id);
        MasterDataDAO.getInstance().update(instance);
    } else if ( "addid".equalsIgnoreCase(action)) {
        // Get the action id and usertype id
        String actionId = request.getParameter("action_id");
        String userTypeId = request.getParameter("user_type_id");
        DB.getInstance().addActionToUserType(actionId, userTypeId);
        response.sendRedirect(pgName + "?action=edit&id=" + userTypeId);
    } else if ( "removeid".equalsIgnoreCase(action)) {
        // Get the action id and usertype id
        String actionId = request.getParameter("action_id");
        String userTypeId = request.getParameter("user_type_id");
        DB.getInstance().removeActionToUserType(actionId, userTypeId);
        response.sendRedirect(pgName + "?action=edit&id=" + userTypeId);
    }
%>
<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>
</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%
	return;
    } else {
%>

<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > db admin > usertypes</span>



<%
	if ( "list".equalsIgnoreCase(action) || "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%>
    
  	<br><span class="content"><a title="Click to create a new user type" href="<%=pgName%>?action=new">Create new</a> &nbsp; | &nbsp; <a title="CLick to list all user types" href="<%=pgName%>?action=list">List All</A></span><br><br>

<%
	}
%>  



<%
  	if ( "list".equalsIgnoreCase(action))  {
  %>  <%-- List all entities --%>

<table cellpadding="5" cellspacing="1">


<tr>
  <td class="label">Name</td>
  <td class="label">Description</td>
  <td class="label">&nbsp;</td> 
</tr>


<%
	int size = entityList != null ? entityList.size() : 0;
UserType st = null;
for ( int i = 0;i < size;i++) {
st = (UserType)entityList.get(i); 
int tmpid = st.getUserTypeId();
%>
<tr>
    <td class="content"><%=st.getUserTypeName()%></td>
    <td class="content"><%=st.getUserTypeDesc()%></td>
    <td class="content">
        <a title="Click to edit this user type" href="<%=pgName%>?action=edit&id=<%=tmpid%>">Edit</a>&nbsp;
        <!--<a href="<%=pgName%>?action=delete&id=<%=tmpid%>">[Delete]</a>-->
    </td>
</TR>
<%
	}
%>
<tr>
    	<td class="content" colspan="3">
		<input class="input" type="button" onCLick="javascript:document.location.href='../admin/dataAdmin_main.jsp';" value="Back">
	</td>
</tr>

</table>


<%
	} else if ( "new".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
%> <%-- Present empty form --%>
      
            <form action="<%=pgName%>" method="POST">
            <input type="hidden" name="action" value="<%=nextAction%>"/>
            <input type="hidden" name="id" value="<%=instance != null ? instance.getUserTypeId() : -1%>"/>
            <table>
                <tr>
                    <td class="label2">Name: </td>
                    <td title="Input a nem for the user type" class="content2"><input class="input" type="text" name="<%=UserType.USER_TYPE_NAME%>" 
                        value="<%=instance != null ? instance.getUserTypeName() : ""%>">
                    </td>
                </tr>
                <tr>
                    <td class="label2">Description: </td>
                    <td title="Input a description of the user type" class="content2"><input class="input" type="text" name="<%=UserType.USER_TYPE_DESC%>" 
                        value="<%=instance != null ? instance.getUserTypeDesc() : ""%>">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input title="Click to save the user type" class="input" type="submit" value="Confirm >">&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='/DES/jsp/admin/edit_usertype.jsp';" class="content" value="Cancel"></td>
                    </form>
                </tr>
                <% if ( id != -1 ) { %>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                    <form action="<%=pgName%>" method="POST">
                    <input type="hidden" name="action" value="removeid"/>
                    <input type="hidden" name="user_type_id" value="<%=id%>"/>
                <tr>
                    <td colspan="3"><p>Below, you can configure the access rights of this user type, by granting/un-granting access to actions below.</p></td>
                </tr>
                <tr>
                    <td title="List of actions that the usertype can perform" class="label">Granted Actions:</td>
                    <td class="label">&nbsp;</td>
                    <td title="List of actions that the usertype can NOT perform" class="label">Available Actions:</td>
                </tr>
                    <tr>
                    <td rowspan="2">
                        <SELECT name="action_id" width="90" size="3" multiple="no">
                            <%
                                int allSize = validActions != null ? validActions.size() : 0;
                                for ( int i = 0;i < allSize;i++) {
                                    Action act = (Action)validActions.get(i);
                                    int tmpId = act.getActionId();
                                    String tmpName = act.getActionName();
                                    
                            %>
                            <OPTION value="<%=tmpId%>"><%=tmpName%></OPTION>
                            <% } %>
                        </SELECT>
                    </td>
                    <td align="center" title="Click to remove the selected actions from the list of allowed actions" valign="top"><input type="submit" value="->"></td>
                    </form>

                    <form action="<%=pgName%>" method="POST">
                    <input type="hidden" name="action" value="addid"/>
                    <input type="hidden" name="user_type_id" value="<%=id%>"/>
                    <td rowspan="2">
                        <SELECT name="action_id" width="90" size="3" multiple="no">
                            <%
                                int invSize = invalidActions != null ? invalidActions.size() : 0;
                                for ( int i = 0;i < invSize;i++) {
                                    Action act = (Action)invalidActions.get(i);
                                    int tmpId = act.getActionId();
                                    String tmpName = act.getActionName();
                                    
                            %>
                            <OPTION value="<%=tmpId%>"><%=tmpName%></OPTION>
                            <% } %>
                        </SELECT>
                     </td>
                </tr>
                <tr>
                    <td align="center" title="Click to grant access to the action" valign="top"><input type="submit" value="<-"></td>
                </tr>
                </form>
                <% } %>
            </table>
            
        <% } else if ( "create".equalsIgnoreCase(action)) { %>  <%-- Create new instance in database--%>
            <%=message%>
        <% } else if ( "delete".equalsIgnoreCase(action)) { %> <%-- Delete instance from database--%>
            <%=message%>
        <% } else if ( "update".equalsIgnoreCase(action)) { %> <%-- Update instance in database--%>
            <%=message%>
        <% } %>
        
   
        </td>
<td width="15%">
&nbsp;
</td>

    </tr>
</table>

<% } %>

<!-- Include standard footer -->

