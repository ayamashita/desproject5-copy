<!-- THIS IS THE STUDY SEARCH RESULTS PAGE -->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="no.machina.simula.*, java.util.*, java.text.*, java.sql.SQLException" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.apache.log4j.Logger"%>


<%-- Include standard header --%>
<jsp:include page="../header.jsp" />
<%
    // First get some stats about the user
    HttpSession sess = request.getSession();
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
%>

<%
String feedbackMsg = "";
DB db = DB.getInstance();

// Set initial values for parameters
String studyTypeParam = "";
String earlyEndDate = "";
String lateEndDate = "";
String sortByParam = "";
String sortOrderParam = "";
String[] studyResponsiblesParam = null;
String[] studyResponsibles = null;

Logger log = Logger.getLogger(this.getClass());

String startRecordParam = null;
String endRecordParam = null;
// Set default range to be 0 to 'MaxRecordsInSearchList'
int startRecord = 0;
String maxRecordsPerPageParam = application.getInitParameter("MaxRecordsInSearchList");
int maxRecordsPerPage = 0;
maxRecordsPerPage = Integer.parseInt(maxRecordsPerPageParam); // no try-catch, want this to fail if MaxRecordsInSearchList is not set.
int endRecord = maxRecordsPerPage;

String action = request.getParameter("action");

List studies = (List)session.getAttribute("report_query_result");
PersonalizedReport report = (PersonalizedReport)session.getAttribute("report_query_definition");

// Get sorting parameters
int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
sortByParam = request.getParameter("sort_by");
sortOrderParam = request.getParameter("sort_order");
try{ sortBy = Integer.parseInt(sortByParam); }
catch (NumberFormatException nfe){}
try{ sortOrder = Integer.parseInt(sortOrderParam); }
catch (NumberFormatException nfe){}




// Get parameters for first and last record to show
startRecordParam = request.getParameter("start_record");
endRecordParam = request.getParameter("end_record");
try{
    startRecord = Integer.parseInt(startRecordParam);
    endRecord = Integer.parseInt(endRecordParam);
}
catch(NumberFormatException nfe){
}
%>


<% // Check security constraints. Is user authorized?
if ( !(DB.getInstance().listContainsAction(actions, "PERSONALIZED_REPORT"))) { 
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;}%>
  
 <!-- MAIN BODY -->


<tr>

<td width="5%">
&nbsp;
</td>

<td width="80%" valign="top">

<p>
<span class="path">admin > reports > query result</span>


<p>

<%
if (studies.size() < 1){ // if there where no hits
%>
    Your search did not return any studies. <a href="report_list.jsp">Back</a> to report selection page.<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%
}
else{
%>
    Currently showing results <b><%=startRecord + 1%></b> - <b><%=endRecord <= (studies.size() - 1)? endRecord : studies.size()%></b> of <b><%=studies.size()%></b>.
    Select another <a href="<%=request.getContextPath() %>/PersonalizedReport?report_action=list">report</a>.
    <p>
    <table cellpadding="5" cellspacing="1">
    <form name="deleter" action="list_studies.jsp" method="POST">

    <%
       if ( feedbackMsg != "" ) {
    %>
        <tr>
        <td colspan="8"><%=feedbackMsg %></td>
        </tr>
    <% } %>

    </td></tr>
    <%
    // Construct sort link
    String sortLink = "list_studies.jsp?";

    // Set sort order to opposite of current
    %>
    <tr>
    	<%if (report.containsColumn("study_name")) {%>
      <td class="label"><%=report.getColumnByName("study_name").getDisplayName() %></td>
      <%} %>
      	<%if (report.containsColumn("study_type")) {%>
      <td class="label">
               <%=report.getColumnByName("study_type").getDisplayName() %>
      </td>
      <%} %>
      	<%if (report.containsColumn("study_end_date")) {%>
      <td class="label">
               <%=report.getColumnByName("study_end_date").getDisplayName() %>
      </td>
      <%} %>
      	<%if (report.containsColumn("study_responsible")) {%>
      <td class="label"><%=report.getColumnByName("study_responsible").getDisplayName() %></td>
      <%} %>
      	<%if (report.containsColumn("study_desc")) {%>
      <td class="label"><%=report.getColumnByName("study_desc").getDisplayName() %></td>
      <%} %>
      <%if (report.containsColumn("study_publications")) {%>
      <td class="label"><%=report.getColumnByName("study_publications").getDisplayName() %></td>
      <%} %>
  
      <%if (report.containsColumn("keywords")) {%>
      <td class="label"><%=report.getColumnByName("keywords").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("no_of_students")) {%>
      <td class="label"><%=report.getColumnByName("no_of_students").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("no_of_professionals")) {%>
      <td class="label"><%=report.getColumnByName("no_of_professionals").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("study_notes")) {%>
      <td class="label"><%=report.getColumnByName("study_notes").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("study_duration")) {%>
      <td class="label"><%=report.getColumnByName("study_duration").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("study_files")) {%>
      <td class="label"><%=report.getColumnByName("study_files").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("study_owner")) {%>
      <td class="label"><%=report.getColumnByName("study_owner").getDisplayName() %></td>
      <%} %>
      
      <%if (report.containsColumn("last_edited_by")) {%>
      <td class="label"><%=report.getColumnByName("last_edited_by").getDisplayName() %></td>
      <%} %>
    </tr>

    <%-- for each study in table. Listing all studies --%>
    <%
    // First check startRecord to avoid index out of bounds
    if (startRecord > studies.size() -1){
        startRecord = studies.size() - 1;
    }
    for (ListIterator i = studies.listIterator(startRecord); i.hasNext(); ) {
            Study study = (Study) i.next();
            if (i.nextIndex() > endRecord){
                break;
            }
            int studyId = study.getId();
    %>

    <tr>
    <%if (report.containsColumn("study_name")) {%>
      <td valign="top" class="content"><a href="../view_study.jsp?id=<%=study.getId() %>"><%=study.getName() %></a></td>
<%} %>
<%if (report.containsColumn("study_type")) {%>
      <td valign="top" class="content"><%=db.getStudyTypeName(study.getTypeId()) %></td>
      <%} %> 
      <%if (report.containsColumn("study_end_date")) {%>
      <td valign="top" class="content"><%=study.getEndAsString(application.getInitParameter("DateFormat")) %></td>
      <%} %>
      <%if (report.containsColumn("study_responsible")) {%>
      <td valign="top" class="content">
        <%
            List responsibles = study.getResponsibles();
            for (ListIterator j = responsibles.listIterator(); j.hasNext(); ) {
                Responsible r = (Responsible) j.next();
                out.println("<a href=\""+r.getUrl()+"\">" + r.getFirstName() + " " + r.getFamilyName() + "</a>");
                // add a line break if there's more coming
                if (j.hasNext()) out.println("<br>");
            }
        %>
      </td>
      <%} %>
       <%if (report.containsColumn("study_desc")) {%>
      <td valign="top" class="content">
        <%
            // Abbreviate desc before printing. Max. 3 lines and 200 characters.
            String htmlDesc = study.getDesc().replaceAll("\\n", "<br>"); 
            int pos = htmlDesc.indexOf("<br>");
            if (pos != -1) {
            	pos = htmlDesc.indexOf("<br>", pos + 1);
            	log.debug("Study.getShortDesc: second pos = " + pos);
            }
            if (pos != -1) {
            	pos = htmlDesc.indexOf("<br>", pos + 1);
            	log.debug("Study.getShortDesc: third pos = " + pos);
            }

            if (pos != -1) {htmlDesc = htmlDesc.substring(0, pos - 3) + "...";}
            htmlDesc = StringUtils.abbreviate(htmlDesc, 200);
            out.println(htmlDesc);
        %>
      </td>
      <%} %>
      <%if (report.containsColumn("study_publications")) {%>
      <td valign="top" class="content">
        <%
            List publications = study.getPublications();
            for (ListIterator k = publications.listIterator(); k.hasNext(); ) {
                Publication p = (Publication) k.next();

                out.println("<a href=\"" + p.getUrl() + "\" title=\"" + URLDecoder.decode(p.getTitle()) + "\">" + 
                URLDecoder.decode(p.getTitle()) + "</a>");
                // add a line break if there's more coming
                if (k.hasNext()) out.println("<br>");
            }
        %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("keywords")) {%>
      	<td valign="top" class="content">
      		<%=StringUtils.defaultString(study.getKeywords()).replaceAll("\\n", "<br>") %>
		</td>
      <%} %>
      
      <%if (report.containsColumn("no_of_students")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getNoOfStudentsAsString()) %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("no_of_professionals")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getNoOfProfessionalsAsString()) %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("study_notes")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getStudyNotes()).replaceAll("\\n", "<br>") %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("study_duration")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getDurationAsString())+ " " + (study.getUnitId() == -1 ? "" : study.getUnit()) %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("study_files")) {%>
      <td valign="top" class="content">
		    <%
		    List files = db.getFilesForStudy(studyId);
		    for(ListIterator li = files.listIterator(); li.hasNext(); ){
		        FileInfo fi = (FileInfo) li.next();
			%>
			        <a href="../GetFile?file_id=<%=fi.getId()%>"><%=fi.getDesc()%></a> <br>
			<%
			    }
			%>
			
			<%
			    List urls = db.getUrlsForStudy(studyId);
			    for(ListIterator li = urls.listIterator(); li.hasNext(); ){
			        Url u = (Url) li.next();
			%>
			        <a href="<%=u.getUrl()%>" target="_blank"><%=u.getDesc()%></a> <br>
			<%
			    }
			%>
      </td>
      <%} %>
      
      <%if (report.containsColumn("study_owner")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getOwner()) %>
      </td>
      <%} %>
      
      <%if (report.containsColumn("last_edited_by")) {%>
      <td valign="top" class="content">
      <%=StringUtils.defaultString(study.getLastEditedBy()) %>
      </td>
      <%} %>

    </tr>

    <%
    } // end for each study
    %>
    <tr></tr>
    <tr>
        <%
        // Build 'previous' and 'next' links
        String nextLink = "&nbsp;";
        String previousLink = "&nbsp;";
        // Set up bounds for 'previous' and 'next'
        int previousStartRecord = startRecord - maxRecordsPerPage;
        int previousEndRecord = endRecord - maxRecordsPerPage;
        int nextStartRecord = startRecord + maxRecordsPerPage;
        int nextEndRecord = endRecord + maxRecordsPerPage;


        // Set links if start end end are within bounds
        if (previousStartRecord >= 0){
            previousLink="<a href=\"" + sortLink + "start_record=" + previousStartRecord 
                        + "&end_record=" + previousEndRecord + "\">&lt;- Previous</a>";
        }
        if (nextEndRecord <= (studies.size() - 1 + maxRecordsPerPage)){
            nextLink="<a href=\"" + sortLink +"start_record=" + nextStartRecord 
                        + "&end_record=" + nextEndRecord + "\">Next -&gt;</a>";
        }
        %>
        <td colspan="7" align="left" valign="top" nowrap class="content"><%=previousLink %></td>
        <td valign="top" class="content" nowrap align="right"><%=nextLink %></td></tr>
    </table>
 
    <% int size = studyResponsibles != null ? studyResponsibles.length : 0;
       for ( int i = 0;i < size;i++) {
    %>
        <input type="hidden" name="study_responsibles" value="<%=studyResponsibles[i]%>"/>
    <% } %>

    </form>

	<% if (startRecord == 0){ // only output on the first page %>
    <form action="<%=request.getContextPath()%>/Report.pdf" method="post">
    <input type="hidden" name="format" value="report_pdf"/>
    <input type="hidden" name="report_id" value="<%=report.getId() %>"/>
  
    <% size = studyResponsibles != null ? studyResponsibles.length : 0;
       for ( int i = 0;i < size;i++) {
    %>
        <input type="hidden" name="study_responsibles" value="<%=studyResponsibles[i]%>"/>
    <% } %>
    <input class="input" title="Click to print the studies to PDF format (requires Adobe Acrobat Reader)" type="submit" value="Print all studies to PDF">
    </form>

 
    <% } // end only show on first page%>
    </p>

    </td>

    <td width="5%">
    &nbsp;
    </td>

    </tr>

    </table>
<%
} // end if there where hits
%>


<!-- Include standard footer -->
<br><br><br><br><br><br>
<jsp:include page="footer.jsp" />
