<%@page import="org.apache.commons.httpclient.*"%>
<%@page import="org.apache.commons.httpclient.methods.*"%>
<%@page import="org.apache.commons.httpclient.protocol.*"%>
<%@page import="org.apache.commons.httpclient.cookie.*"%>
<%@page import="org.apache.commons.httpclient.contrib.ssl.*"%>
<%@page import="org.apache.commons.httpclient.contrib.utils.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html"%>
<%

    // Then remove the local session values
    HttpSession sess = request.getSession();
    sess.setAttribute("status", "NOTOK");
    sess.removeAttribute("status");
    sess.removeAttribute("simula_username");
    sess.removeAttribute("simula_userid");
    sess.invalidate();
    response.sendRedirect(request.getContextPath()+"/jsp/search.jsp");
%>