<!-- THIS IS THE MAIN PAGE FOR DES ADMINS --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@include file="check_login.jsp" %>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />
<%
	// First get some stats about the user

    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if (status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    String pageTitle = "Main";
   
    if ( !DB.getInstance().listContainsAction(actions, "VIEW_MAIN")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>

      No Access, please login
</p>
</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%
	return;
    } else {
%>



<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td valign="top" width="40%">
<p>
<span class="path">studies > admin main</span>


<%-- Get admin message from database and display it --%>
<%=DB.getInstance().getAdminMessage()%>



<!-- Only available for DB Admins -->
<%
	if ( DB.getInstance().listContainsAction(actions, "EDIT_TEXT")) {
%>
<p class="bodytext-bold">
<a title="Click to edit the text on this page (HTML)" href="edit_admin_text.jsp">Edit text on this page</a>
</p>
<% } %>


	

</td>


<td valign="top" width="30%">
<br><br><br>
<jsp:include page="../menu.jsp" />
</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>

<% } %>


<!-- Include standard footer -->
<br><br><br><br>
<jsp:include page="../footer.jsp" />
