<!-- THIS IS THE MAIN PAGE FOR DES ADMINS -->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="no.machina.simula.*, java.util.*, java.text.*, java.sql.SQLException" %>
<%@ page import="org.apache.commons.lang.*, org.apache.commons.lang.time.*" %>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="check_login.jsp" %>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />
<script language="JavaScript" src="selectbox.js"></script>
<%
	Logger log = Logger.getLogger(this.getClass());
	
    // First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
%>

<%

String actionValue = "insert"; // default action to pass on to receiving page
String action = request.getParameter("action");

log.debug("report_edit: action = " + action);
String path = "reports > create report";

String reportId = "";
String reportName = "";
String reportSorting = "";
String responsibleCriteria = "";
List reportColumns = new ArrayList();
List reportResponsibles = new ArrayList();
HashSet reportColumnsSet = null;
HashSet reportResponsiblesSet = null;
boolean dataInvalid = false;

PersonalizedReportUtils reportUtils = PersonalizedReportUtils.getInstance();

 %>


<% // Check security constraints. Is user authorized?
if ( !(DB.getInstance().listContainsAction(actions, "PERSONALIZED_REPORT"))) { 
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;
    } else { // user is authorized %>


<!-- MAIN BODY -->

<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>

 
<p>
Specify the report by filling out the form below. All fields marked with an asterisk (*) are required.<br>
Press the "Save" button to save the report. 
</p>

<table width="100%" cellspacing="5" cellpadding="5">
<form action="<%=request.getContextPath()%>/PersonalizedReport" method="POST">
<% 
	String fatalError = (String)session.getAttribute("report_fatal_error");
	
	if (fatalError != null && fatalError.length() > 0) {
		session.removeAttribute("report_fatal_error");
	%>
	<tr>
        <td colspan="4">
    	<strong>
    		Fatal Error occured:<br/>
    		<%=fatalError %>
    	</strong>	
	<%
	} 

	List errors = (List)request.getSession().getAttribute("report_validation_errors");
	if (errors != null && errors.size() > 0){ // There were errors in the input 
		session.removeAttribute("report_validation_errors");
	%>
    <tr>
        <td colspan="4">
        	<ul>
            <%  // Output error messages.
                dataInvalid = true;
                
                for (int i = 0; i < errors.size(); i++){
                    {out.println("<li>"+ errors.get(i) + "</li>");}
                }
            %>
            </ul>
        </td>
    </tr>
<% } %>

<%
	reportId = request.getParameter("report_id");
	String nextAction = "insert";
	
 	if (dataInvalid) {
 		reportName = (String) session.getAttribute("error_report_name");
		reportSorting = (String) session.getAttribute("error_report_time_sort");
		responsibleCriteria = (String)session.getAttribute("error_report_resp_criteria");
		
		reportResponsibles = new ArrayList();
		String[] respArr = (String[])session.getAttribute("error_report_responsible");
		reportResponsiblesSet = new HashSet();
		if (respArr != null && respArr.length > 0) {
			for (int i=0; i < respArr.length; i++) {
				reportResponsiblesSet.add(respArr[i]);
			}
		}
		
		reportColumns = new ArrayList();
		String[] colArr = (String[])session.getAttribute("error_report_columns");
		reportColumnsSet = new HashSet();
		if (colArr != null && colArr.length > 0) {
			for (int i=0; i < colArr.length; i++) {
				reportColumnsSet.add(colArr[i]);
			}
		}
		reportId = (String)session.getAttribute("error_report_id");
		
		if (reportId != null && reportId.length() > 0 ) {
			nextAction = "update";
		}
		
		
		session.removeAttribute("error_report_name");
		session.removeAttribute("error_report_time_sort");
		session.removeAttribute("error_report_resp_criteria");
		session.removeAttribute("error_report_responsible");
		session.removeAttribute("error_report_columns");
		session.removeAttribute("error_report_id");
 	}
 	else {
			if (action != null && action.equalsIgnoreCase("edit") && reportId != null && reportId.length() > 0) {
				nextAction = "update";
				
				//Read report data
				PersonalizedReport report = (PersonalizedReport)request.getSession().getAttribute("report_cached_object");
				if (report == null || !(report.getId()+"").equalsIgnoreCase(reportId)) {
					report = reportUtils.getReport(reportId);
					
					if (report == null) {
						%>
						 <tr>
						 <td colspan="2">
						 	Error while loading report with id = <%=reportId %>.
						 </td>
						 </tr>
						<%
						
						report = new PersonalizedReport();
						report.setOwner(userid);
					}
				}
				
				if (!report.getOwner().equals(userid)) {
				%>
				        <tr>
							<td width="15%">
							&nbsp;
							</td>
							
							<td width="70%">
							<br><br>
							
							<p>
							    You do not have access to this report!
							</p>
							
							</td>
							
							<td width="15%">&nbsp;</td>
						</tr>
							
					</table>
				<%
				}
				
				reportName = report.getName();
				reportSorting = report.getTimeSortOrder().getSortOrderName();
				responsibleCriteria = report.getResponsibleCriteria().getCriteriaName();
				
				reportResponsibles = report.getResponsibles();
				reportResponsiblesSet = new HashSet();
				for (int i=0; i < reportResponsibles.size(); i++) {
					reportResponsiblesSet.add(((Responsible)reportResponsibles.get(i)).getPeopleId());
				}
				
				reportColumns = report.getColumns();
				reportColumnsSet = new HashSet();
				for (int i=0; i < reportColumns.size(); i++) {
					reportColumnsSet.add(((Column)reportColumns.get(i)).getName());
				}
			} 
	}	
	//remove the cached object after reading data	
	session.removeAttribute("report_cached_object");
	
 %>

<tr>
<td class="label2" valign="top">
Report name: *
</td>
<td class="content2">
<input name="report_name" size="73" maxlength="128" class="content" value="<%=StringUtils.defaultString(reportName) %>">
</td>
</tr>


<tr>
<td class="label2" valign="top">
Sorting by time: *
</td>
<td class="content2">
<select name="report_time_sort" class="content">
	<%
		List sortOpts = reportUtils.getAllSortOrder();
		
		Iterator sortIter = sortOpts.iterator();
		while (sortIter.hasNext()) {
			ReportSortOrder order = (ReportSortOrder)sortIter.next();
			out.print("<option value=\"");
			out.print(order.getSortOrderName());
			out.print("\"");
			if (reportSorting.equalsIgnoreCase(order.getSortOrderName())) {
				out.print(" selected=\"true\" ");
			}
			out.print(">");
			out.print(order.getSortOrderDesc());
			out.println("</option>");
		}
		
	 %>
</select>
</td>
</tr>

<tr>
<td class="label2" valign="top">
Report columns: *
</td>
<td class="content2">
<table border="0" width="100%"> <!-- table for aligning select boxes and buttons -->
<tr><td width="47%">
<span class="label2">Available</span><br>
<select ondblclick="moveSelectedOptions(this.form['report_all_columns'],this.form['report_columns'],true)" style="width:100%" class="input" name="report_all_columns" size="8" multiple>
<%
	List allColumns = reportUtils.getAvailableColumns();
	
	if (reportColumnsSet != null) {
	    if (dataInvalid) {
		    for (int i = 0; i < allColumns.size(); i++) {
		    	Column col = (Column)allColumns.get(i);
		    	if (reportColumnsSet.contains(col.getName())) {
					allColumns.set(i,null);
					reportColumns.add(col);   		
		    	}
		    }
	    }
	    else {
		    for (int i = 0; i < allColumns.size(); i++) {
		    	Column col = (Column)allColumns.get(i);
		    	if (reportColumnsSet.contains(col.getName())) {
					allColumns.set(i,null);   		
		    	}
		    }
	    }
	}
	
	for (int i =0; i < allColumns.size(); i++) {
	Column col = (Column)allColumns.get(i);
		if (col != null) {
		
		out.print("<option value=\"" + col.getName() + "\" >"+col.getDisplayName()+"</option>" );
		}
	}
 %>
</select>
</td><td width="5%" valign="middle" align="center"  class="content2">
<input type="button" value="->" onclick="moveSelectedOptions(this.form['report_all_columns'],this.form['report_columns'],true)"><br><br>
<input type="button" value="<-" onclick="moveSelectedOptions(this.form['report_columns'],this.form['report_all_columns'],true)">
</td><td width="47%">
<span class="label2">Selected</span><br>
<select ondblclick="moveSelectedOptions(this.form['report_columns'],this.form['report_all_columns'],true)" style="width:100%" class="input" name="report_columns" size="8" multiple>
<%
	for (int i = 0; i < reportColumns.size(); i++) {
	Column col = (Column)reportColumns.get(i);
		out.print("<option value=\"" + col.getName() +"\">"+ col.getDisplayName() +"</option>" );
	}
 %>
</select>
</td></tr>
</table> <!-- end table for select box alignment -->
</td>
</tr>

<tr>
<td class="label2" valign="top">
Responsible criteria: *
</td>
<td class="content2">

<select name="report_resp_criteria" class="input">
	<%
		List allCriteria = reportUtils.getAllCriteria();
		
		Iterator crIter = allCriteria.iterator();
		while (crIter.hasNext()) {
			ReportCriteria order = (ReportCriteria)crIter.next();
			out.print("<option value=\"");
			out.print(order.getCriteriaName());
			out.print("\"");
			if (responsibleCriteria.equalsIgnoreCase(order.getCriteriaName())) {
				out.print(" selected=\"true\" ");
			}
			out.print("\">");
			out.print(order.getCriteriaName());
			out.println("</option>");
		}
		
	 %>
</select>
</td>
</tr>

<tr>
<td class="label2" valign="top">
Responsible persons: *
</td>
<td class="content2">
<table border="0" width="100%"> <!-- table for aligning select boxes and buttons -->
<tr><td width="47%">
<span class="label2">Available</span><br>
<select ondblclick="moveSelectedOptions(this.form['report_all_responsible'],this.form['report_responsible'],true)" style="width:100%" class="input" name="report_all_responsible" size="8" multiple>
<%
	List allResponsibles = reportUtils.getAllResponsibles();
     
    // Loop through list of all and move selected to selected list
    if (reportResponsiblesSet != null){
    	if (dataInvalid) {
    		for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
	            Responsible resp = (Responsible) i.next();
	            if (reportResponsiblesSet.contains(resp.getPeopleId())){ // if it's selected
	                i.remove();
	                reportResponsibles.add(resp);
	            }
	        }
    	}
    	else {
	        for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
	            Responsible resp = (Responsible) i.next();
	            if (reportResponsiblesSet.contains(resp.getPeopleId())){ // if it's selected
	                i.remove();
	            }
	        }
        }
    }
    
    // Output list of all responsibles, except any selected
    for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
        Responsible resp = (Responsible) i.next(); %>
        <option value="<%=resp.getPeopleId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%>
<%  }
    %>
</select>
</td><td width="5%" valign="middle" align="center"  class="content2">
<input type="button" value="->" onclick="moveSelectedOptions(this.form['report_all_responsible'],this.form['report_responsible'],true)"><br><br>
<input type="button" value="<-" onclick="moveSelectedOptions(this.form['report_responsible'],this.form['report_all_responsible'],true)">
</td><td width="47%">
<span class="label2">Selected</span><br>
<select ondblclick="moveSelectedOptions(this.form['report_responsible'],this.form['report_all_responsible'],true)" style="width:100%" class="input" name="report_responsible" size="8" multiple>
    <%for (ListIterator i = reportResponsibles.listIterator(); i.hasNext(); ){
        Responsible resp = (Responsible) i.next(); %>
        <option value="<%=resp.getPeopleId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%>
<%  }
    %>
</select>
</td></tr>
</table> <!-- end table for select box alignment -->
</td>
</tr>



<tr>
<td class="content2">
&nbsp;
</td>
<td class="content2">
<input class="input" type="submit" name="confirm"  onclick="selectAllOptions(this.form['report_columns']); selectAllOptions(this.form['report_responsible'])" class="content" value="Save">&nbsp;
<input class="input" type="button" onCLick="javascript:document.location.href='/DesC/PersonalizedReport?report_action=list';" class="content" value="Back">
</td>
</tr>
<input type="hidden" name="report_id" value="<%=reportId %>">
<input type="hidden" name="report_action" value="<%=nextAction %>">
</form>
</table>

</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>



<% } // end user is authorized %>


