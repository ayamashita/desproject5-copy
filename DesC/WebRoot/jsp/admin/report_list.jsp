<%@page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@page import="no.machina.simula.*, java.util.*"%>
<%@page import="org.apache.commons.lang.math.*"%>
<%@include file="check_login.jsp" %>
<%
	// First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }

    // Get the action from the request
    String action = request.getParameter("action");
    String nextAction = null;
    if ( action == null || action.length() == 0 ) {
        action = "list";
    }

    // Set the pagetitle and name of this page
    String pageTitle = "Report list";
    String message = "";
    String pgName = "report_list.jsp";
    
    List reports = (List)request.getSession().getAttribute("report_definition_list");

%>
<!-- Include standard header -->
<jsp:include page="../header.jsp" />

<%
	if ( !DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) {
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>
<p>
    No Access, please login

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%
	return;
    } else {
%>


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > db admin > actions</span>

<p>
Personalized reports.</p>
<p>
Create new <a href="<%=request.getContextPath() %>/jsp/admin/report_edit.jsp"> report</a>
</p>



<table cellpadding="5" cellspacing="1">

<% 
	String fatalError = (String)session.getAttribute("report_fatal_error");
	
	if (fatalError != null && fatalError.length() > 0) {
		session.removeAttribute("report_fatal_error");
	%>
	<tr>
        <td colspan="4">
    	<strong>
    		Fatal Error occured:<br/>
    		<%=fatalError %>
    	</strong>	
	<%
	} 

	List errors = (List)request.getSession().getAttribute("report_validation_errors");
	if (errors != null && errors.size() > 0){ // There were errors in the input 
		session.removeAttribute("report_validation_errors");
	%>
    <tr>
        <td colspan="4">
        	<ul>
            <%  // Output error messages.  
                for (int i = 0; i < errors.size(); i++){
                    {out.println("<li>"+ errors.get(i) + "</li>");}
                }
            %>
            </ul>
        </td>
    </tr>
<% } 
	String deleteMessage = (String)session.getAttribute("report_delete_message");
	
	if (deleteMessage != null && deleteMessage.length() > 0) {
		session.removeAttribute("report_delete_message");
	%>
	<tr>
        <td colspan="4">
    	<strong>
    		<%=deleteMessage %>
    	</strong>	
<%}%>



<tr>
  <td class="label">Name</td>
  <td class="label">&nbsp;</td>
  <td class="label">&nbsp;</td>
  <td class="label">&nbsp;</td>
</tr>


<%
	int size = reports != null ? reports.size() : 0;
PersonalizedReport st = null;
for ( int i = 0;i < size;i++) {
st = (PersonalizedReport)reports.get(i); 
int tmpid = st.getId();
%>
<tr>
	<td class="content">
		<%=st.getName() %>
	</td>
	<td class="content">
        <a title="Click to edit this action" href="<%=request.getContextPath()%>/PersonalizedReport?report_action=query&report_id=<%=tmpid%>">Query</a>&nbsp;
    </td>
    <td class="content">
        <a title="Click to edit this action" href="report_edit.jsp?action=edit&report_id=<%=tmpid%>">Edit</a>&nbsp;
    </td>
    <td class="content">
        <a title="Click to edit this action" href="<%=request.getContextPath()%>/PersonalizedReport?report_action=delete&report_id=<%=tmpid%>">Delete</a>&nbsp;
    </td>
</TR>
<%
	}
%>
<tr>
                     <td colspan="3"><input class="input" type="button" onCLick="javascript:document.location.href='/DesC/jsp/admin/main.jsp';" value="Back"></td>
                </tr>
</table>
    
        </td>
<td width="15%">
&nbsp;
</td>

    </tr>
</table>

<% } %>


