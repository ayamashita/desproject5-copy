<!-- THIS IS THE MAIN PAGE FOR DES ADMINS -->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="no.machina.simula.*, java.util.*, java.text.*, java.sql.SQLException" %>
<%@ page import="org.apache.commons.lang.*, org.apache.commons.lang.time.*" %>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="check_login.jsp" %>

<!-- Include standard header -->
<jsp:include page="../header.jsp" />
<script language="JavaScript" src="selectbox.js"></script>
<%
	Logger log = Logger.getLogger(this.getClass());
	
    // First get some stats about the user
    HttpSession sess = request.getSession();
    
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
%>

<%
	DB db = DB.getInstance();
// Set up default study for view
Study study = new Study();
String studyName = "";
String studyTypeId = "";
String studyDuration = "";
String studyDurationUnitId = "";
String studyStartDate = "";
String studyEndDate = "";
String studyDesc = "";
String studyKeywords = "";
String numberOfStudents = "";
String numberOfProfessionals = "";
HashSet studyResponsibleIdsHS = null;
HashSet studyPublicationIdsHS = null;
String studyNotes = "";
    boolean test = false;
String studyId = "";
String creator = "";
String lastEditedBy = "";
String creationDateString = "";
String lastAuditDateString = "";

String dateFormat = application.getInitParameter("DateFormat");
String[] errorMsgs = new String[20]; // holds error messages.
int errorNo = -1; // counter for errorMsgs
boolean validated = true; // initially assume all input validated ok
String actionValue = "insert"; // default action to pass on to receiving page
String action = request.getParameter("action");
System.err.println("study_edit: action = " + action);
String path = "studies > create study";


if (action != null){
    if (action.equals("edit")){
    	path = "studies > search > search results > edit study";
    	log.debug(path);
        studyId = request.getParameter("study_id");
        actionValue = "update";
        study = db.getSingleStudy(Integer.parseInt(studyId));
        
        // Set up view
        studyName = study.getName();
        studyTypeId = study.getTypeIdAsString();
        studyDuration = study.getDurationAsString();
        studyDurationUnitId = study.getUnitIdAsString();
        studyStartDate = study.getStartAsString(dateFormat);
        studyEndDate = study.getEndAsString(dateFormat);
        studyDesc = study.getDesc();
        studyKeywords = study.getKeywords();
        numberOfStudents = study.getNoOfStudentsAsString();
        numberOfProfessionals = study.getNoOfProfessionalsAsString();
        studyResponsibleIdsHS = study.getResponsibleIds();
        studyPublicationIdsHS = study.getPublicationIds();
        studyNotes = study.getStudyNotes();

        // Get URLs and add to sessioin
        List urls = db.getUrlsForStudy(Integer.parseInt(studyId));
        session.setAttribute("urls", urls);

        // Get audit info
        Audit firstAudit = db.getFirstAudit(Integer.parseInt(studyId));
        Audit lastAudit = db.getMostRecentAudit(Integer.parseInt(studyId));

        // Check that there is an audit and get and format the data.
        // Only need to check for firstAudit, since if there isn't one, there
        // will not be a lastAudit either.
        if (firstAudit != null){
            People firstPeople = db.getPeople(firstAudit.getPeopleId());
            creator = firstPeople.getPeopleFirstName() + " " + firstPeople.getPeopleFamilyName();
            People lastPeople = db.getPeople(lastAudit.getPeopleId());
            lastEditedBy = lastPeople.getPeopleFirstName() + " " + lastPeople.getPeopleFamilyName();

            // get and format audit dates
            DateFormat df = new SimpleDateFormat(dateFormat, new Locale("en", "GB"));
            Date creationDate = firstAudit.getAuditDate();
            Date lastAuditDate = lastAudit.getAuditDate();
            creationDateString = "on " + df.format(creationDate);
            lastAuditDateString = "on " + df.format(lastAuditDate);
        }
        else{
            creator = "No information recorded.";
            lastEditedBy = "No information recorded.";
        }
    }
    else{
        path = "new";
        log.debug("new study");
        // Get input
        studyName = request.getParameter("study_name");
        studyTypeId = request.getParameter("study_type_id");
        studyDuration = request.getParameter("study_duration");
        studyDurationUnitId = request.getParameter("study_duration_unit_id");
        studyStartDate = request.getParameter("study_start_date");
        studyEndDate = request.getParameter("study_end_date");
        studyDesc = request.getParameter("study_desc");
        studyKeywords = request.getParameter("study_keywords");
        numberOfStudents = request.getParameter("number_of_students");
        numberOfProfessionals = request.getParameter("number_of_professionals");
        String[] studyResponsibleIds = request.getParameterValues("study_responsible_ids");
        if (studyResponsibleIds != null){
            studyResponsibleIdsHS = new HashSet(Arrays.asList(studyResponsibleIds));
        }
        String[] studyPublicationIds = request.getParameterValues("study_publication_ids");
        if (studyPublicationIds != null){
            studyPublicationIdsHS = new HashSet(Arrays.asList(studyPublicationIds));
        }
        studyNotes = request.getParameter("study_notes");

        // If it's an existing file, get the study id
        if (action.equals("edit_with_file") || action.equals("update")){
            studyId = request.getParameter("study_id");
            actionValue = "update";
        }

        if(action.equals("insert") || action.equals("update")){
            // validate and convert input
            if(StringUtils.isBlank(studyName)){
                errorMsgs[++errorNo] = "Study Name is required.";
                validated = false;
            }
            else if(action.equals("insert") && db.doesStudyNameExist(studyName)){
                errorMsgs[++errorNo] = "The Study Name " + studyName + " is allready used. Study Name must be unique.";
                validated = false;
            }
            if(StringUtils.isBlank(studyTypeId)){
                errorMsgs[++errorNo] = "Study Type is required.";
                validated = false;
            }
            else{
                if (!Validator.isNumericLargerThan(studyTypeId, 0)){
                    errorMsgs[++errorNo] = "Study Type Id must be an integral number larger than 0.";
                    validated = false;
                }
            }
            if(StringUtils.isNotBlank(studyDuration)){
                if (!Validator.isNumericLargerThan(studyDuration, 0)){
                    errorMsgs[++errorNo] = "Study Duration must be an integral number larger than 0.";
                    validated = false;
                }
                // Check study duration unit id only if study duration is supplied
                if (!Validator.isNumericLargerThan(studyDurationUnitId, 0)){
                    errorMsgs[++errorNo] = "Study Duration Unit Id must be an integral number larger than 0.";
                    validated = false;
                }
            }

            if(StringUtils.isNotBlank(studyStartDate)){
                if (!Validator.isDate(studyStartDate, dateFormat)){
                    errorMsgs[++errorNo] = "Start Date must be on the format " + dateFormat + ".";
                    validated = false;
                }
            }
            else{
                studyStartDate = null;
            }
            if (!Validator.isDate(studyEndDate, dateFormat)){
                errorMsgs[++errorNo] = "End Date is required and must be on the format " + dateFormat + ".";
                validated = false;
            }
            if(StringUtils.isBlank(studyDesc)){
                errorMsgs[++errorNo] = "Study Description is required.";
                validated = false;
            }
            if(StringUtils.isBlank(studyKeywords)){studyKeywords = null;}

            if(StringUtils.isBlank(studyNotes)){studyNotes = null;}
            if(StringUtils.isNotBlank(numberOfStudents)){
                if (!Validator.isNumericLargerThan(numberOfStudents, -1)){
                    errorMsgs[++errorNo] = "Number of students must be an integral number larger than -1.";
                    validated = false;
                }
            }
            else{
                numberOfStudents = "";
            }
            if(StringUtils.isNotBlank(numberOfProfessionals)){
                if (!Validator.isNumericLargerThan(numberOfProfessionals, -1)){
                    errorMsgs[++errorNo] = "Number of professionals must be an integral number larger than -1.";
                    validated = false;
                }
            }
            else{
                numberOfProfessionals = "";
            }
            if(studyResponsibleIds == null){
                errorMsgs[++errorNo] = "A study is required to have at least one study responsible.";
                validated = false;
            }
            //else{
            //    for(int i = 0; i < studyResponsibleIds.length; i++){
            //        if(!Validator.isNumericLargerThan(studyResponsibleIds[i], 0)){
            //           errorMsgs[++errorNo] = "All responsible ids must be integral numbers larger than 0.";
            //            validated = false;
            //            break;
            //       }
            //    }
            //}
            //if(studyPublicationIds != null){
            //   for(int i = 0; i < studyPublicationIds.length; i++){
            //        if(!Validator.isNumericLargerThan(studyPublicationIds[i], -1)){
            //            errorMsgs[++errorNo] = "All publication ids must be integral numbers larger than -1.";
            //            validated = false;
            //            break;
            //        }
            //    }
            //}
            if (validated == true){ // input is OK.
		System.out.print("VALIDATED OK");
                int studyTypeIdInt = Integer.parseInt(studyTypeId);
                int studyDurationInt = -1;
                try{
                    studyDurationInt = Integer.parseInt(studyDuration);
                }
                catch(NumberFormatException nfe){
                }
                int studyDurationUnitIdInt = -1;
                // only try to set this to valid id if study duration is set
                if (studyDurationInt > -1){
                    try{
                        studyDurationUnitIdInt = Integer.parseInt(studyDurationUnitId);
                    }
                    catch(NumberFormatException nfe){
                    }
                }

                Date start = Converter.stringToDate(studyStartDate, application.getInitParameter("DateFormat"));
                log.debug("study_edit: studyEndDate = " + studyEndDate);
                Date end = Converter.stringToDate(studyEndDate, application.getInitParameter("DateFormat"));
                log.debug("study_edit: end = " + end);
                //int[] studyResponsibleIdsInt = Converter.stringArrayToIntArray(studyResponsibleIds);
                //int[] studyPublicationIdsInt = Converter.stringArrayToIntArray(studyPublicationIds);

                int numberOfStudentsInt = -1;
                try{
                    numberOfStudentsInt = Integer.parseInt(numberOfStudents);
                }
                catch(NumberFormatException nfe){
                }

                int numberOfProfessionalsInt = -1;
                try{
                    numberOfProfessionalsInt = Integer.parseInt(numberOfProfessionals);
                }
                catch(NumberFormatException nfe){
                }



                if (action.equals("insert")){
                    // get user id for auditing
                    String uid = (String)session.getAttribute("simula_userid");
                    
                    if (uid == null || uid.length() == 0) {
                    	System.err.println("Could not get user id while inserting. The study was not inserted.");
                        throw new ServletException("Could not get user id while inserting. The study was not inserted.");
                    }
                    
                    // insert the study and audit
                    int newStudyId = db.insertStudy(studyName, studyTypeIdInt, studyDurationInt, studyDurationUnitIdInt, 
                            start, end, studyDesc, studyKeywords, numberOfStudentsInt,
                            numberOfProfessionalsInt, studyNotes, studyResponsibleIds,
                            studyPublicationIds);
                    db.auditStudy(newStudyId, uid);
                    
                    // insert the study's URLs
                    List urlList = (List) session.getAttribute("urls");
                    if (urlList != null){ // if there are URLs insert them
                        for (Iterator i = urlList.iterator(); i.hasNext(); ){
                            Url newUrl = (Url) i.next();
                            db.insertUrl(newStudyId, newUrl.getUrl(), newUrl.getDesc());
                        }
                    }
                    // Remove urls from session
                    session.removeAttribute("urls");
                    // insert the study's files
                    HashSet files = (HashSet) session.getAttribute("fileIds");
                    if (files != null){ // if there are files insert them
                        for (Iterator i = files.iterator(); i.hasNext(); ){
                            Integer newFileId = (Integer) i.next();
                            db.insertFileForStudy(newStudyId, newFileId.intValue());
                        }
                        // Remove files from session
                        session.removeAttribute("fileIds");
                        // Remove study attributes
                        session.removeAttribute("study_name");
                        session.removeAttribute("study_type_id");
                        session.removeAttribute("study_duration");
                        session.removeAttribute("study_duration_unit_id");
                        session.removeAttribute("study_start_date");
                        session.removeAttribute("study_end_date");
                        session.removeAttribute("study_desc");
                        session.removeAttribute("study_keywords");
                        session.removeAttribute("number_of_students");
                        session.removeAttribute("number_of_professionals");
                        session.removeAttribute("study_responsible_ids");
                        session.removeAttribute("study_publication_ids");
                    }
                }
                else if (action.equals("update")){
                    studyId = request.getParameter("study_id");

                    // get user id for auditing
                    String uid = (String)session.getAttribute("simula_userid");
                    
                    if (uid == null || uid.length() == 0) {
                    	System.err.println("Could not get user id while updating. The study was not updated.");
                        throw new ServletException("Could not get user id while updating. The study was not updated.");
                    }
                    
                    // update the study and audit
                    db.updateStudy(Integer.parseInt(studyId), studyName, studyTypeIdInt, studyDurationInt, studyDurationUnitIdInt, 
                            start, end, studyDesc, studyKeywords, numberOfStudentsInt,
                            numberOfProfessionalsInt, studyNotes, studyResponsibleIds,
                            studyPublicationIds);
                    db.auditStudy(Integer.parseInt(studyId), uid);

                    // update the study's URLs
                    db.deleteUrlsForStudy(Integer.parseInt(studyId));
                    List urlList = (List) session.getAttribute("urls");
                    if (urlList != null){ // if there are URLs insert them
                        for (Iterator i = urlList.iterator(); i.hasNext(); ){
                            Url newUrl = (Url) i.next();
                            db.insertUrl(Integer.parseInt(studyId), newUrl.getUrl(), newUrl.getDesc());
                        }
                    }
                    // Remove urls from session
                    session.removeAttribute("urls");

                    // Remove files from session
                    session.removeAttribute("fileIds");

                    // remove session attributes
                    session.removeAttribute("study_name");
                    session.removeAttribute("study_type_id");
                    session.removeAttribute("study_duration");
                    session.removeAttribute("study_duration_unit_id");
                    session.removeAttribute("study_start_date");
                    session.removeAttribute("study_end_date");
                    session.removeAttribute("study_desc");
                    session.removeAttribute("study_keywords");
                    session.removeAttribute("number_of_students");
                    session.removeAttribute("number_of_professionals");
                    session.removeAttribute("study_responsible_ids");
                    session.removeAttribute("study_publication_ids");
                }
            } // end if validated
            // didn't validate so if it's an update we need to try again.
            // not necessary for insert, since that is the default action
            if (action.equals("update")){ actionValue = "update"; } 
        } // end action is insert or update (i.e. not coming from file upload.
    } // end action other than edit
} // end if action is set
%>


<% // Check security constraints. Is user authorized?
if ( !(DB.getInstance().listContainsAction(actions, "CREATE_STUDY") || DB.getInstance().listContainsAction(actions, "EDIT_STUDY"))) { 
%>
        <tr>
<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<p>
    No Access, please login
</p>

</td>

<td width="15%">&nbsp;</td>
</tr>

</table>

<%  return;
    } else { // user is authorized %>


<!-- MAIN BODY -->

<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>

<span class="path"><%=path%></span>


 
<p>
Specify the Study by filling out the form below. All fields marked with an asterisk (*) are required.<br>
Press the "Save" button to save the study. 
</p>

<table width="100%" cellspacing="5" cellpadding="5">
<form action="StudyEdit" method="POST">
<%  if (validated == false){ // There were errors in the input %>
    <tr>
        <td colspan="2">
            <%  // Output error messages.
                for (int i = 0; i < errorMsgs.length; i++){
                    if (errorMsgs[i] != null) {out.println(errorMsgs[i] + "<br/>");}
                }
            %>
        </td>
    </tr>
<% } %>

<tr>
<td class="label2" valign="top">
Study name: *
</td>
<td class="content2">
<input name="study_name" size="73" maxlength="128" class="content" value="<%=StringUtils.defaultString(studyName) %>">
</td>
</tr>


<tr>
<td class="label2" valign="top">
Study type: *
</td>
<td class="content2">
<select name="study_type_id" class="content">
<%
	List studyTypeList = MasterDataDAO.getInstance().getList(StudyType.class);
for (ListIterator i = studyTypeList.listIterator(); i.hasNext(); ){
    StudyType st = (StudyType) i.next();
%>
    <option value="<%=st.getStudyTypeId()%>"
<%
        if (studyTypeId.equals(new Integer(st.getStudyTypeId()).toString())){
            out.print(" selected");
        }
    out.print(">" + st.getStudyTypeName());
}
%>
</select>
</td>
</tr>


<tr>
<td class="label2" valign="top">
Study duration: 
</td>
<td class="content2">
<input maxlength="4" size="4" name="study_duration" class="content" value="<%=StringUtils.defaultString(studyDuration)%>"> 

<select name="study_duration_unit_id" class="input">
<%
	List unitList = MasterDataDAO.getInstance().getList(Unit.class);
for (ListIterator i = unitList.listIterator(); i.hasNext(); ){
    Unit u = (Unit) i.next();
    out.print("<option value=\"" + u.getUnitId() + "\"");
        if (studyDurationUnitId != null && studyDurationUnitId.equals(new Integer(u.getUnitId()).toString())){
            out.print(" selected");
        }
    out.print(">" + u.getUnitName());
}
%>
</select>
</td>
</tr>


<tr>
<td class="label2" valign="top">
Study period: 
</td>
<td class="content2">
<input size="11" maxlength="11" name="study_start_date" class="input" value="<%=StringUtils.defaultString(studyStartDate) %>"> - 
*<input size="11" maxlength="11" name="study_end_date" class="input" value="<%=StringUtils.defaultString(studyEndDate) %>"> &nbsp; ( format: dd-MMM-yyyy, e.g. 02-Oct-2003 )
</td>
</tr>


<tr>
<td class="label2" valign="top">
Study description: *
</td>
<td class="content2">
<textarea maxlength="1024" class="input" name="study_desc" cols="75" rows="12"><%=StringUtils.defaultString(studyDesc) %></textarea>
</td>
</tr>


<tr>
<td class="label2" valign="top">
Keywords: 
</td>
<td class="content2">
<textarea class="input" name="study_keywords" cols="75" rows="4" maxlength="256"><%=StringUtils.defaultString(studyKeywords) %></textarea>
</td>
</tr> 


<tr>
<td class="label2" valign="top">
Number of students: 
</td>
<td class="content2">
<input size="4" maxlength="4" name="number_of_students" class="input" value="<%=StringUtils.defaultString(numberOfStudents) %>">
</td>
</tr>

<tr>
<td class="label2" valign="top">
Number of professionals: 
</td>
<td class="content2">
<input size="4" maxlength="4" name="number_of_professionals" class="input" value="<%=StringUtils.defaultString(numberOfProfessionals) %>">
</td>
</tr>

<tr>
<td class="label2" valign="top">
Responsible persons: *
</td>
<td class="content2">
<table border="0" width="100%"> <!-- table for aligning select boxes and buttons -->
<tr><td width="47%">
<span class="label2">Available</span><br>
<select ondblclick="moveSelectedOptions(this.form['study_responsible_ids_all'],this.form['study_responsible_ids'],true)" style="width:100%" class="input" name="study_responsible_ids_all" size="8" multiple>
    <%
    List allResponsibles = db.getAllResponsibles();
    List selectedResponsibles = new ArrayList();
    
    // Loop through list of all and move selected to selected list
    if (studyResponsibleIdsHS != null){
        for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
            Responsible resp = (Responsible) i.next();
            if (studyResponsibleIdsHS.contains(resp.getPeopleId())){ // if it's selected
                // move it
                selectedResponsibles.add(resp);
                i.remove();
            }
        }
    }
    
    // Output list of all responsibles, except any selected
    for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
        Responsible resp = (Responsible) i.next(); %>
        <option value="<%=resp.getPeopleId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%>
<%  }
    %>
</select>
</td><td width="5%" valign="middle" align="center"  class="content2">
<input type="button" value="->" onclick="moveSelectedOptions(this.form['study_responsible_ids_all'],this.form['study_responsible_ids'],true)"><br><br>
<input type="button" value="<-" onclick="moveSelectedOptions(this.form['study_responsible_ids'],this.form['study_responsible_ids_all'],true)">
</td><td width="47%">
<span class="label2">Selected</span><br>
<select ondblclick="moveSelectedOptions(this.form['study_responsible_ids'],this.form['study_responsible_ids_all'],true)" style="width:100%" class="input" name="study_responsible_ids" size="8" multiple>
    <%
    // Output list of selected responsibles
    for (ListIterator i = selectedResponsibles.listIterator(); i.hasNext(); ){
        Responsible resp = (Responsible) i.next(); %>
        <option value="<%=resp.getPeopleId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%>
<%  }
    %>
    
</select>
</td></tr>
</table> <!-- end table for select box alignment -->
</td>
</tr>


<tr>
<td class="label2" valign="top">
Related publications: 
</td>
<td class="content2">

<table border="0" width="100%"> <!-- table for aligning select boxes and buttons -->
<tr><td width="47%">
<span class="label2">Available</span><br>
<select ondblclick="moveSelectedOptions(this.form['study_publication_ids_all'],this.form['study_publication_ids'],true)" style="width:100%" class="input" name="study_publication_ids_all" size="8" multiple>
    <%
    List allPublications = db.getAllPublications();
    List selectedPublications = new ArrayList();
    
    // Loop through list of all and move selected to selected list
    if (studyPublicationIdsHS != null){
        for (ListIterator i = allPublications.listIterator(); i.hasNext(); ){
            Publication pub = (Publication) i.next();
            if (studyPublicationIdsHS.contains(pub.getId())){ // if it's selected
                // move it
                selectedPublications.add(pub);
                i.remove();
            }
        }
    }
    
    // Output list of all publications, except any selected
    for (ListIterator i = allPublications.listIterator(); i.hasNext(); ){
        Publication pub = (Publication) i.next(); %>
        <option value="<%=pub.getId()%>"><%=StringUtils.abbreviate(URLDecoder.decode(pub.getTitle()), 65)%>
<%  }
    %>
</select>
</td><td valign="middle" align="center"  class="content2">
<input type="button" value="->" onclick="moveSelectedOptions(this.form['study_publication_ids_all'],this.form['study_publication_ids'],true)"><br><br>
<input type="button" value="<-" onclick="moveSelectedOptions(this.form['study_publication_ids'],this.form['study_publication_ids_all'],true)">
</td><td width="47%">
<span class="label2">Selected</span><br>
<select ondblclick="moveSelectedOptions(this.form['study_publication_ids'],this.form['study_publication_ids_all'],true)" style="width:100%" class="input" name="study_publication_ids" size="8" multiple>
    <%
    // Output list of selected publication
    for (ListIterator i = selectedPublications.listIterator(); i.hasNext(); ){
        Publication pub = (Publication) i.next(); %>
        <option value="<%=pub.getId()%>"><%=StringUtils.abbreviate(URLDecoder.decode(pub.getTitle()), 40)%>
<%  }
    %>
    
</select>
</td></tr>
</table> <!-- end table for select box alignment -->




</td>
</tr>

<tr>
<td class="label2" valign="top">
Related files: 
</td>
<td class="content2">
<%
if (action != null && (action.equals("edit") || action.equals("edit_with_file"))){
%>    
    <input type="hidden" name="study_action" value="edit">
    <input type="hidden" name="study_id" value="<%=studyId %>">
<%
}
%>
<input class="input" type="submit" name="add_file" onclick="selectAllOptions(this.form['study_responsible_ids']); selectAllOptions(this.form['study_publication_ids'])" value="Add and remove files"><br>
<br>
<%
int numFiles = 0;
if (StringUtils.isNotBlank(studyId)){
    System.err.println("studyId is not blank");
    if (StringUtils.isNumeric(studyId)){
        int sId = Integer.parseInt(studyId);
        List files = db.getFilesForStudy(sId);
        if (files != null){
            numFiles = files.size();
        }
    }
}
else{
    System.err.println("studyId is blank");
    HashSet files = (HashSet) session.getAttribute("fileIds");
    if (files != null){
        numFiles = files.size();
    }
}
if (numFiles >0){
%>
There are <%=numFiles %> file(s) connected to this study.
<%
}
%>
</td>
</tr>


<tr>
<td class="label2" valign="top">
Related URLs: 
</td>
<td class="content2">
<input class="input" type="submit" name="edit_url" onclick="selectAllOptions(this.form['study_responsible_ids']); selectAllOptions(this.form['study_publication_ids'])" value="Add and remove URLs"><br>
<br>
<%
// Get URLs from session
int numUrls = 0;
List urls = (List) session.getAttribute("urls");
if (urls != null){
    numUrls = urls.size();
}
if (numUrls > 0){
%>
There are <%=numUrls %> URL(s) connected to this study.
<%
}
%>

</td>
</tr>


<tr>
<td class="label2" valign="top">
Study notes: 
</td>
<td class="content2">
<textarea maxlength="1024" class="input" name="study_notes" cols="75" rows="12"><%=StringUtils.defaultString(studyNotes) %></textarea>
</td>
</tr>

<tr>
<%
if (action != null && action.equals("edit")){ // show audit info
%>
    <td class="label2" valign="top">
    Study owner: 
    </td>
    <td class="content2">
    <%=creator %> <%=creationDateString %>
    </td>
    </tr>
    <tr>
    <td class="label2" valign="top">
    Last edited by: 
    </td>
    <td class="content2">
    <%=lastEditedBy %> <%=lastAuditDateString %>
    </td>
    </tr>
<%
}
%>
<tr>
<td class="content2">
&nbsp;
</td>
<td class="content2">
<input class="input" type="submit" name="confirm"  onclick="selectAllOptions(this.form['study_responsible_ids']); selectAllOptions(this.form['study_publication_ids'])" class="content" value="Save">&nbsp;<input class="input" type="button" onCLick="javascript:document.location.href='OnLeave?redirect=/DesC/jsp/admin/main.jsp';" class="content" value="Back">
</td>
</tr>
<input type="hidden" name="study_id" value="<%=studyId %>">
<input type="hidden" name="action" value="<%=actionValue %>">
</form>
</table>

</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>



<% } // end user is authorized %>


