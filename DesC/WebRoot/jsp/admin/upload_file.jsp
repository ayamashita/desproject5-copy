<%@ page contentType="text/html"%>
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="org.apache.commons.fileupload.*, org.apache.commons.lang.*, java.io.*, java.util.*, no.machina.simula.*"%>
<%@include file="check_login.jsp" %>
<%
	try{
String errorMsg = "";
String fieldName = "";
String fileName = "";
String fileDesc = "";
long sizeInBytes = -1;
File f = null;

int threshold = -1; 
String thresholdInit = application.getInitParameter("Threshold");
application.log("upload_file: Retrieved MaxFileSizeInKb. thresholdInit = " + thresholdInit);
try{
    threshold = Integer.parseInt(thresholdInit);
}
catch(NumberFormatException nfe){
}
// convert threshold to kb if it is not -1
if (threshold != -1){
    threshold = threshold * 1024;
}

int maxUploadSize = -1; // Default is unlimited file size
String maxUploadSizeInit = application.getInitParameter("MaxFileSizeKb");
application.log("upload_file: Retrieved MaxFileSizeInKb. maxUploadSizeInit = " + maxUploadSizeInit);
try{
    maxUploadSize = Integer.parseInt(maxUploadSizeInit);
}
catch(NumberFormatException nfe){
}
// convert maxUploadSize to kb if it is not -1
if (maxUploadSize != -1){
    maxUploadSize = maxUploadSize * 1024;
}

application.log("upload_file: Parsed thresholdInit. threshold = " + threshold);
DB db = DB.getInstance();
String action = request.getParameter("action");
String studyAction = request.getParameter("study_action");
String studyIdParam = request.getParameter("study_id");
application.log("upload_file: action = " + action + ", study_action = " + studyAction + ", study_id = " + studyIdParam);
int studyId = -1;
try{
    studyId = Integer.parseInt(studyIdParam);
}
catch(NumberFormatException nfe){
}
application.log("upload_file: Parsed studyIdParam. studyId = " + studyId);
boolean isMultipartContent = FileUpload.isMultipartContent(request);
application.log("upload_file: isMultipartContent = " + isMultipartContent);

if (isMultipartContent){
    try{
        // Create a new file upload handler
        application.log("upload_file: About to create new DiskFileUpload.");
        DiskFileUpload upload = new DiskFileUpload();
        application.log("upload_file: Created new DiskFileUpload.");
        if (threshold != -1){
            upload.setSizeThreshold(threshold);
        }
        application.log("upload_file: Threshold is set to : " + threshold);
        upload.setSizeMax(maxUploadSize);
        application.log("upload_file: Max = " + upload.getSizeMax());

        // Parse the request
        List /* FileItem */ items = upload.parseRequest(request);
        application.log("upload_file: Has parsed request.");
        application.log("upload_file: items contains " + items.size() + " elements");
        // Process the uploaded items
        Iterator iter = items.iterator();
        application.log("upload_file: About to iterate over items.");
        while (iter.hasNext()) {
            application.log("upload_file: Iteration over items. Start.");
            FileItem item = (FileItem) iter.next();
            application.log("upload_file: Got next item.");

            // Process a file upload
            if (item.isFormField()) {
                application.log("upload_file: Item is form field.");
                if(item.getFieldName().equals("study_id")){
                    application.log("upload_file: Item is study_id.");
                    studyIdParam = item.getString();
                    try{
                        studyId = Integer.parseInt(studyIdParam);
                        application.log("upload_file: studyId = " + studyId);
                    }
                    catch(NumberFormatException nfe){
                        throw new ServletException("Processing file upload: Study id param is not an integer.");
                    }
                }
                if(item.getFieldName().equals("study_action")){
                    application.log("upload_file: Item is study_action.");
                    studyAction = item.getString();
                        application.log("upload_file: studyAction = " + studyAction);
                }
                if(item.getFieldName().equals("file_desc")){
                    application.log("upload_file: Item is file_desc.");
                    fileDesc = item.getString();
                        application.log("upload_file: fileDesc = " + fileDesc);
                }
            }
            else{ // it's a file
                application.log("upload_file: Starting processing of file.");
                fileName = item.getName();
                // Strip path info (from IE and Opera on Windows).
                fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
                // Create File-object for logging info.
                f = new File(fileName);
                application.log("upload_file: Created new File.");
                if (f == null) {application.log("f is null.");}
                else {application.log("f is not null.");}

                sizeInBytes = item.getSize();
                if (sizeInBytes < 1){ // empty file
                    errorMsg = "File is empty.";
                    break;
                }
                int size = (int) sizeInBytes;

                if (StringUtils.isBlank(fileDesc)){
                    fileDesc = fileName;
                }
                
                application.log("upload_file: About to add file: name = " + f.getName() + " canonicalPath = " + f.getCanonicalPath() 
                                + " size = " + size + " fileDesc = " + fileDesc);
                int fileId = db.insertFile(fileName, item.getContentType(), fileDesc, size, item.getInputStream());

                // Clear file description
                fileDesc = "";

                // If editing study, connect file to study
                if (studyAction != null && studyAction.equals("edit")){
                    application.log("upload_file: studyAction is edit, About to connect file to study.");
                    db.insertFileForStudy(studyId, fileId);
                    application.log("upload_file: Connected file to study.");
                }
                else{
                    application.log("upload_file: new study, About to add file id to session.");
                    // Add fileId to session
                    HashSet fileIds = (HashSet) session.getAttribute("fileIds");
                    if (fileIds == null){
                        fileIds = new HashSet();
                    }
                    fileIds.add(new Integer(fileId));
                    session.setAttribute("fileIds", fileIds);
                    application.log("upload_file: added file ids to session.");
               }
            }
        }
    }
    catch(FileUploadException fue){
        errorMsg = "Error uploading file: " + fue.getMessage();
    }
}
else {
    if (action != null && action.equals("remove")){
        String fileIdParam = request.getParameter("file_id");
        int fileId = 0;
        try{
            fileId =  Integer.parseInt(fileIdParam);
        }
        catch(NumberFormatException nfe){
        }
        //db.deleteFileForStudy(studyId, fileId);
        db.deleteFile(fileId);

        // If editing study, delete file from study
        if (studyAction != null && studyAction.equals("edit")){
            db.deleteFileForStudy(studyId, fileId);
        }
        else{
            // Remove fileId from session
            HashSet fileIds = (HashSet) session.getAttribute("fileIds");
            fileIds.remove(new Integer(fileId));
            session.setAttribute("fileIds", fileIds);
        }
    }
}
%>


<!-- Include standard header -->
<jsp:include page="../header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > create/edit study > upload files</span>

<% if (StringUtils.isNotBlank(errorMsg)){ %>
    <p>
    <%=errorMsg %>
    </p>
<% } %>

<p>

Here you can upload files that will be attached to the study. Provide a description/title of the file (if you want to overwrite the file name), then browse to locate the file you want to upload. Then click the 'Add file' button to confirm.

<p>

<form action="upload_file.jsp" method="POST" encType="multipart/form-data">
    <span class="label2">File Description:</span> <input class="input" type="text" name="file_desc" value=""><p>
    <%
    if (studyAction != null && studyAction.equals("edit")){
    %>
        <input type="hidden" name="study_id" value="<%=studyId %>">
        <input type="hidden" name="study_action" value="<%=studyAction %>">
    <%
    }
    %>
    <span class="label2">File:</span> <input class="input" type=file name="file" size=40> <p>
    <input class="input" type=submit value="Add file"><p>
    
</form>

<p>

<%
if (studyAction != null && studyAction.equals("edit")){
    // Get list of files
    List files = db.getFilesForStudy(studyId);
    if (!files.isEmpty()){
%>
    <fieldset>
    <legend class="fieldheader">File list</legend>
        <table>
            <tr><td>Name</td><td>Description</td><td>Size</td><td>&nbsp;</td></tr>
<%
	for(ListIterator i = files.listIterator(); i.hasNext(); ){
            FileInfo fi = (FileInfo) i.next();
%>
            <tr><td><%=fi.getName()%></td><td><%=fi.getDesc()%></td><td><%=fi.getSize()%></td><td> 
            <a href="upload_file.jsp?action=remove&study_action=<%=studyAction%>&file_id=<%=fi.getId()%>&study_id=<%=studyId%>">Remove file</a></td></tr>
<%
	}
%>
        </table>
    </fieldset>
<%
	}
}
else{
    // Get file ids from session
    HashSet fileIds = (HashSet) session.getAttribute("fileIds");
    if (fileIds != null){
%>
    <fieldset>
    <legend class="fieldheader">File list</legend>
        <table>
            <tr><td>Name</td><td>Description</td><td>Size</td><td>&nbsp;</td></tr>
<%
	// List files
        for(Iterator i = fileIds.iterator(); i.hasNext(); ){
            Integer fId = (Integer) i.next();
            FileInfo fi = db.getFileInfo(fId.intValue());
%>
            <tr><td><%=fi.getName() %></td><td><%=fi.getDesc() %></td><td><%=fi.getSize() %></td><td> 
            <a href="upload_file.jsp?action=remove&file_id=<%=fi.getId()%>">Remove file</a></td></tr>
        <%
        }
%>
        </table>
    </fieldset>
<%
    }
}
%>

<form action="study_edit.jsp" method="POST">
    <%-- Set attributes --%>
    <input type="hidden" name="study_name" value="<%=session.getAttribute("study_name") %>">
    <input type="hidden" name="study_type_id" value="<%=session.getAttribute("study_type_id") %>">
    <input type="hidden" name="study_duration" value="<%=session.getAttribute("study_duration") %>">
    <input type="hidden" name="study_duration_unit_id" value="<%=session.getAttribute("study_duration_unit_id") %>">
    <input type="hidden" name="study_start_date" value="<%=session.getAttribute("study_start_date") %>">
    <input type="hidden" name="study_end_date" value="<%=session.getAttribute("study_end_date") %>">
    <input type="hidden" name="study_desc" value="<%=session.getAttribute("study_desc") %>">
    <input type="hidden" name="study_keywords" value="<%=session.getAttribute("study_keywords") %>">
    <input type="hidden" name="number_of_students" value="<%=session.getAttribute("number_of_students") %>">
    <input type="hidden" name="number_of_professionals" value="<%=session.getAttribute("number_of_professionals") %>">

    <%
     String[] studyResponsibleIds = (String[]) session.getAttribute("study_responsible_ids");
     if (studyResponsibleIds != null){
         for (int i = 0; i < studyResponsibleIds.length; i++){
        %>
            <input type="hidden" name="study_responsible_ids" value="<%=studyResponsibleIds[i]%>">
        <%
         }
     }

     String[] studyPublicationIds = (String[]) session.getAttribute("study_publication_ids");
     if (studyPublicationIds != null){
         for (int i = 0; i < studyPublicationIds.length; i++){
        %>
            <input type="hidden" name="study_publication_ids" value="<%=studyPublicationIds[i]%>">
        <%
         }
     }


    if (studyAction != null && studyAction.equals("edit")){
    %>
        <input type="hidden" name="action" value="edit_with_file">
        <input type="hidden" name="study_id" value="<%=studyId %>">
        <%--input type="hidden" name="action" value="<%=studyAction %>" --%>
    <%
    }
    else{
    %>
        <input type="hidden" name="action" value="insert_with_file">
    <%
    }
    %>
    <input class="input" type="submit" value="Finished">
</form>




	

</td>



<td width="15%">
&nbsp;
</td>

</tr>

</table>







<%
}
catch(Throwable t){
    application.log(t.getMessage(), t);
    out.println("Error: " + t.getMessage());
}
%>
