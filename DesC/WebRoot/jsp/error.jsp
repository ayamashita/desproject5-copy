<!-- THIS IS THE MAIN PAGE FOR DES ADMINS --->
<%@page import="org.apache.commons.lang.math.*"%>
<%@page import="org.apache.commons.lang.exception.ExceptionUtils"%>
<%@page import="no.machina.simula.engine.*" %>
<!-- Include standard header -->
<jsp:include page="header.jsp" />
<%
    int errorCode = NumberUtils.stringToInt(Converter.getFirstString(request.getParameter("error")));
    String errorMsg = "";
    String details = (String)request.getSession().getAttribute("error.message");
    Throwable ex = (Throwable)request.getSession().getAttribute("error.exception");
    Throwable root = ExceptionUtils.getRootCause(ex);
    String rootStack = ExceptionUtils.getFullStackTrace(root);
    String exStack = ExceptionUtils.getFullStackTrace(ex);
    String exDetails = ex != null ? ex.getClass().getName() + ": " + ex.getMessage() : "No exception details";
    String rootDetails = root != null ? root.getClass().getName() + ": " + root.getMessage() : "No root exception details";

    switch(errorCode) {
        case 1: 
            errorMsg = "Problems creating report, no format specified";
            break;
        case 2:
            errorMsg = "Creating report failed because of exception";
            
       default:
            errorMsg = "An unexpected error occured";
            break;
    }
%>

<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<br><br>

<fieldset>
<legend class="fieldheader"><b>Simula DES Administration</b></legend>

    <h3>Error</h3>
    <p><%=errorMsg%></p>

    <h4>Exception</h3>
    <p><%=exDetails%></p>
    <p><%=exStack%></p>

    <h3>Root cause</h3>
    <p><%=rootDetails%></p>
    <p><%=rootStack%></p>
</fieldset>
</td>


<td width="15%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<jsp:include page="footer.jsp" />
