<!-- THIS IS THE STUDY SEARCH RESULTS PAGE -->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<%@ page import="no.machina.simula.*, java.util.*, java.text.*, java.sql.SQLException" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.apache.log4j.Logger"%>


<%-- Include standard header --%>
<jsp:include page="header.jsp" />
<%
    // First get some stats about the user
    HttpSession sess = request.getSession();
    String status = (String)sess.getAttribute("status");
    //String username = (String)sess.getAttribute("simula_username");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
%>

<%
String feedbackMsg = "";
DB db = DB.getInstance();

// Set initial values for parameters
int studyType = 0;
Date early = null;
Date late = null;
String studyTypeParam = "";
String earlyEndDate = "";
String lateEndDate = "";
String sortByParam = "";
String sortOrderParam = "";
String[] studyResponsiblesParam = null;
String[] studyResponsibles = null;

Logger log = Logger.getLogger(this.getClass());

String startRecordParam = null;
String endRecordParam = null;
// Set default range to be 0 to 'MaxRecordsInSearchList'
int startRecord = 0;
String maxRecordsPerPageParam = application.getInitParameter("MaxRecordsInSearchList");
int maxRecordsPerPage = 0;
maxRecordsPerPage = Integer.parseInt(maxRecordsPerPageParam); // no try-catch, want this to fail if MaxRecordsInSearchList is not set.
int endRecord = maxRecordsPerPage;

String action = request.getParameter("action");
if (action != null){
    if (action.equals("delete") && DB.getInstance().listContainsAction(actions, "DELETE_STUDY")){
		// User chose to delete. Gather which entities were checked
		// and delete them.
		String[] toDelete = request.getParameterValues("toDelete");

		if (toDelete != null && toDelete.length > 0){ // Check that at least one check box was checked
            int numDeleted = 0;
            // convert string array to int array
            int[] toDeleteIds = Converter.stringArrayToIntArray(toDelete);
            // Delete
            numDeleted = db.deleteStudies(toDeleteIds);
            feedbackMsg = "Deleted " + numDeleted + " record(s).";
		}
		else {feedbackMsg = "No records were selected for deletion.";}
	}
}

// Do a search
List studies;

// Get sorting parameters
int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
sortByParam = request.getParameter("sort_by");
sortOrderParam = request.getParameter("sort_order");
try{ sortBy = Integer.parseInt(sortByParam); }
catch (NumberFormatException nfe){}
try{ sortOrder = Integer.parseInt(sortOrderParam); }
catch (NumberFormatException nfe){}

// Get free text search, if this parameter is non-empty we default to do a free text search
String searchString = request.getParameter("free_text_search");
log.info("list_studies: searchString = " + searchString);
if (StringUtils.isNotBlank(searchString)){
    log.debug("list_studies: isNotBlank(searchString) = true");
    studies = db.getStudies(searchString, sortBy, sortOrder);
}
else{ // do a search based on the input parameters
    
    // Get and format parameters

    // Study Type
    studyType = 0; // default study type is "Any"
    studyTypeParam = request.getParameter("study_type_id");
    if (studyTypeParam != null){
        try{
            studyType = Integer.parseInt(studyTypeParam);
        }
        catch(NumberFormatException nfe){
        }
    }
    

    // Earliest End Date
    early = null;
    DateFormat df = new SimpleDateFormat(application.getInitParameter("DateFormat"), new Locale("en", "GB"));
    earlyEndDate = request.getParameter("early_end_date");
    
    if (StringUtils.isNotBlank(earlyEndDate)){
        earlyEndDate = earlyEndDate.trim();
        // If it's just a year, set it to the beginning of the year
        if (StringUtils.isNumeric(earlyEndDate)){
            int earlyEndDateInt = Integer.parseInt(earlyEndDate);
            if (earlyEndDateInt > 0 && earlyEndDateInt < 9999){
                earlyEndDate = "01-jan-" + earlyEndDate;
            }
        }
        try{
            early = df.parse(earlyEndDate);
        }
        catch(ParseException pe){
            // do nothing, date will be null which gives default
        }
    }

    // Latest End Date
    late = null;
    lateEndDate = request.getParameter("late_end_date");
    
    if (StringUtils.isNotBlank(lateEndDate)){
        lateEndDate = lateEndDate.trim();
        // If it's just a year, set it to the end of the year
        if (StringUtils.isNumeric(lateEndDate)){
            log.debug("list_studies: lateEndDate is Numeric");
            int lateEndDateInt = Integer.parseInt(lateEndDate);
            if (lateEndDateInt > 0 && lateEndDateInt < 9999){
                lateEndDate = "31-dec-" + lateEndDate;
            }
        }
        try{
            late = df.parse(lateEndDate);
        }
        catch(ParseException pe){
            // do nothing, date will be null which gives default
        }
    }

    // Study Responsibles
    studyResponsiblesParam = request.getParameterValues("study_responsibles");
    if (studyResponsiblesParam != null && !ArrayUtils.contains(studyResponsiblesParam, "0")){
    	studyResponsibles = studyResponsiblesParam;
	}
    // Get studies from database
    try{
        studies = db.getStudies(studyType, early, late, studyResponsibles, sortBy, sortOrder);
    }
    catch(SQLException se){
        out.println("<tr><td>" + se.getMessage() + "</td></tr>");
        out.println("<tr><td>" + db.lastStmt + "</td></tr>");
        return;
    }
} // end else (not free text search)


// Get parameters for first and last record to show
startRecordParam = request.getParameter("start_record");
endRecordParam = request.getParameter("end_record");
try{
    startRecord = Integer.parseInt(startRecordParam);
    endRecord = Integer.parseInt(endRecordParam);
}
catch(NumberFormatException nfe){
}
%>






<!-- MAIN BODY -->


<tr>

<td width="5%">
&nbsp;
</td>

<td width="80%" valign="top">

<p>
<span class="path">studies > search > search results</span>


<p>

<%
if (studies.size() < 1){ // if there where no hits
%>
    Your search did not return any studies. <a href="search.jsp">Try again</a><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%
}
else{
%>
    Currently showing results <b><%=startRecord + 1%></b> - <b><%=endRecord <= (studies.size() - 1)? endRecord : studies.size()%></b> of <b><%=studies.size()%></b>.
    Try another <a href="search.jsp">search</a>.
    <p>
    <table cellpadding="5" cellspacing="1">
    <form name="deleter" action="list_studies.jsp" method="POST">

    <%
       if ( feedbackMsg != "" ) {
    %>
        <tr>
        <td colspan="8"><%=feedbackMsg %></td>
        </tr>
    <% } %>

    </td></tr>
    <%
    // Construct sort link
    String sortLink = "";
    if (StringUtils.isBlank(searchString)){
        sortLink = "list_studies.jsp?study_type_id=" + studyTypeParam
                       + "&early_end_date=" + earlyEndDate 
                       + "&late_end_date=" + lateEndDate;
    }
    else{
        sortLink = "list_studies.jsp?free_text_search=" + searchString;
    }

    // Add all responsible ids
    if (studyResponsibles != null){
        for (int i = 0; i < studyResponsibles.length; i++){
            sortLink += "&study_responsibles=" + studyResponsibles[i];
        }
    }
    else{
        sortLink += "&study_responsibles=0"; // Sett to "any"
    }

    // Set sort order to opposite of current
    int newSortOrder = sortOrder;
    if (sortOrder == DB.SORT_ORDER_ASCENDING){
        newSortOrder = DB.SORT_ORDER_DESCENDING;
    }
    else if (sortOrder == DB.SORT_ORDER_DESCENDING){
        newSortOrder = DB.SORT_ORDER_ASCENDING;
    }
    %>
    <tr>
      <% if (DB.getInstance().listContainsAction(actions, "DELETE_STUDY")) { %>
        <td class="label">Select</td>
      <% } %>
      <td class="label">
            <a title="Click to sort by Study Name" class="menulink2" href="<%=sortLink%>&sort_by=<%=DB.SORT_STUDIES_STUDY_NAME%>&sort_order=<%=newSortOrder%>">
                Study Name
            </a>
      </td>
      <td class="label">
            <a title="Click to sort by Study Type" class="menulink2" href="<%=sortLink%>&sort_by=<%=DB.SORT_STUDIES_STUDY_TYPE_ID%>&sort_order=<%=newSortOrder%>">
                Type of Study
            </a>
      </td>
      <td class="label">
            <a title="Click to sort by Study End Date" class="menulink2" href="<%=sortLink%>&sort_by=<%=DB.SORT_STUDIES_STUDY_END_DATE%>&sort_order=<%=newSortOrder%>">
                End of Study
            </a>
      </td>
      <td class="label">Study Responsibles</td>
      <td class="label">Study Description</td>
      <td class="label">Publications</td>
      <% if (DB.getInstance().listContainsAction(actions, "EDIT_STUDY")) { %>
      <td class="label">&nbsp;</td> 
        <% } %>
    </tr>

    <%-- for each study in table. Listing all studies --%>
    <%
    // First check startRecord to avoid index out of bounds
    if (startRecord > studies.size() -1){
        startRecord = studies.size() - 1;
    }
    for (ListIterator i = studies.listIterator(startRecord); i.hasNext(); ) {
            Study study = (Study) i.next();
            if (i.nextIndex() > endRecord){
                break;
            }
    %>

    <tr>
      <% if (DB.getInstance().listContainsAction(actions, "DELETE_STUDY")) { %>
        <td valign="top" class="content"><input class="input" type="checkbox" name="toDelete" value="<%=study.getId() %>"></td>
      <% } %>
      <td valign="top" class="content"><a href="view_study.jsp?id=<%=study.getId() %>"><%=study.getName() %></a></td>

      <td valign="top" class="content"><%=db.getStudyTypeName(study.getTypeId()) %></td> 
      <td valign="top" class="content"><%=study.getEndAsString(application.getInitParameter("DateFormat")) %></td>
      <td valign="top" class="content">
        <%
            List responsibles = study.getResponsibles();
            for (ListIterator j = responsibles.listIterator(); j.hasNext(); ) {
                Responsible r = (Responsible) j.next();
                out.println("<a href=\""+r.getUrl()+"\">" + r.getFirstName() + " " + r.getFamilyName() + "</a>");
                // add a line break if there's more coming
                if (j.hasNext()) out.println("<br>");
            }
        %>
      </td>
      <td valign="top" class="content">
        <%
            // Abbreviate desc before printing. Max. 3 lines and 200 characters.
            String htmlDesc = study.getDesc().replaceAll("\\n", "<br>"); 
            int pos = htmlDesc.indexOf("<br>");
            if (pos != -1) {
            	pos = htmlDesc.indexOf("<br>", pos + 1);
            	log.debug("Study.getShortDesc: second pos = " + pos);
            }
            if (pos != -1) {
            	pos = htmlDesc.indexOf("<br>", pos + 1);
            	log.debug("Study.getShortDesc: third pos = " + pos);
            }

            if (pos != -1) {htmlDesc = htmlDesc.substring(0, pos - 3) + "...";}
            htmlDesc = StringUtils.abbreviate(htmlDesc, 200);
            out.println(htmlDesc);
        %>
      </td>
      <td valign="top" class="content">
        <%
            List publications = study.getPublications();
            for (ListIterator k = publications.listIterator(); k.hasNext(); ) {
                Publication p = (Publication) k.next();

                out.println("<a href=\"" + p.getUrl() + "\" title=\"" + URLDecoder.decode(p.getTitle()) + "\">" + 
                URLDecoder.decode(p.getTitle()) + "</a>");
                // add a line break if there's more coming
                if (k.hasNext()) out.println("<br>");
            }
        %>
      </td>
      <% if (DB.getInstance().listContainsAction(actions, "EDIT_STUDY")) { %>
        <td valign="top" class="content"><a href="admin/study_edit.jsp?action=edit&study_id=<%=study.getId() %>">Edit study</a></td>
      <% } %>
    </tr>

    <%
    } // end for each study
    %>
    <tr></tr>
    <tr>
        <%
        // Build 'previous' and 'next' links
        String nextLink = "&nbsp;";
        String previousLink = "&nbsp;";
        // Set up bounds for 'previous' and 'next'
        int previousStartRecord = startRecord - maxRecordsPerPage;
        int previousEndRecord = endRecord - maxRecordsPerPage;
        int nextStartRecord = startRecord + maxRecordsPerPage;
        int nextEndRecord = endRecord + maxRecordsPerPage;


        // Set links if start end end are within bounds
        if (previousStartRecord >= 0){
            previousLink="<a href=\"" + sortLink + "&sort_by=" + sortBy + "&sort_order=" + sortOrder
                        + "&start_record=" + previousStartRecord 
                        + "&end_record=" + previousEndRecord + "\">&lt;- Previous</a>";
        }
        if (nextEndRecord <= (studies.size() - 1 + maxRecordsPerPage)){
            nextLink="<a href=\"" + sortLink + "&sort_by=" + sortBy + "&sort_order=" + sortOrder 
                        +"&start_record=" + nextStartRecord 
                        + "&end_record=" + nextEndRecord + "\">Next -&gt;</a>";
        }
        %>
        <td colspan="7" align="left" valign="top" nowrap class="content"><%=previousLink %></td>
        <td valign="top" class="content" nowrap align="right"><%=nextLink %></td></tr>
    </table>

    <p>
    <input type="hidden" name="early_end_date" value="<%=earlyEndDate%>"/>
    <input type="hidden" name="late_end_date" value="<%=lateEndDate%>"/>
    <input type="hidden" name="study_type_id" value="<%=studyTypeParam%>"/>
    <input type="hidden" name="sort_by" value="<%=sortByParam%>"/>
    <input type="hidden" name="sort_order" value="<%=sortOrderParam%>"/>
    <input type="hidden" name="free_text_search" value="<%=StringUtils.defaultString(searchString)%>"/>
    <% int size = studyResponsibles != null ? studyResponsibles.length : 0;
       for ( int i = 0;i < size;i++) {
    %>
        <input type="hidden" name="study_responsibles" value="<%=studyResponsibles[i]%>"/>
    <% } %>
    <input type="hidden" name="action" value="delete">

    <% if (DB.getInstance().listContainsAction(actions, "DELETE_STUDY")) { %>
        <input class="input" type="button" onclick="javascript:if(confirm('Are you sure you want to delete the selected studies? This action cannot be undone.'))document.deleter.submit();" value="Delete selected studies">
    <% } %>
    </form>

	<% if (startRecord == 0){ // only output on the first page %>
    <form action="../Report.pdf" method="post">
    <input type="hidden" name="format" value="pdf"/>
    <input type="hidden" name="early_end_date" value="<%=earlyEndDate%>"/>
    <input type="hidden" name="late_end_date" value="<%=lateEndDate%>"/>
    <input type="hidden" name="study_type_id" value="<%=studyTypeParam%>"/>
    <input type="hidden" name="sort_by" value="<%=sortByParam%>"/>
    <input type="hidden" name="sort_order" value="<%=sortOrderParam%>"/>
    <input type="hidden" name="free_text_search" value="<%=searchString%>"/>
    <% size = studyResponsibles != null ? studyResponsibles.length : 0;
       for ( int i = 0;i < size;i++) {
    %>
        <input type="hidden" name="study_responsibles" value="<%=studyResponsibles[i]%>"/>
    <% } %>
    <input class="input" title="Click to print the studies to PDF format (requires Adobe Acrobat Reader)" type="submit" value="Print all studies to PDF">
    </form>

    <% if (DB.getInstance().listContainsAction(actions, "EXPORT_CSV")) { %>
    <form action="../Report.csv" method="post">
    <input type="hidden" name="format" value="csv"/>
    <input type="hidden" name="early_end_date" value="<%=earlyEndDate%>"/>
    <input type="hidden" name="late_end_date" value="<%=lateEndDate%>"/>
    <input type="hidden" name="study_type_id" value="<%=studyTypeParam%>"/>
    <input type="hidden" name="sort_by" value="<%=sortByParam%>"/>
    <input type="hidden" name="sort_order" value="<%=sortOrderParam%>"/>
    <input type="hidden" name="free_text_search" value="<%=searchString%>"/>
    <% size = studyResponsibles != null ? studyResponsibles.length : 0;
       for ( int i = 0;i < size;i++) {
    %>
        <input type="hidden" name="study_responsibles" value="<%=studyResponsibles[i]%>"/>
    <% } %>
    <input class="input" title="Click to print the studies to CSV format" type="submit" value="Export all studies to CSV">
    </form>
    <% } // end if actions contains EXPORT_CSV %>
    <% } // end only show on first page%>
    </p>

    </td>

    <td width="5%">
    &nbsp;
    </td>

    </tr>

    </table>
<%
} // end if there where hits
%>


<!-- Include standard footer -->
<br><br><br><br><br><br>
<jsp:include page="footer.jsp" />
