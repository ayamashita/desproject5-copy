<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>
<jsp:include page="header.jsp" />

<%
String errorOccured = request.getParameter("login_error");

if (errorOccured != null && errorOccured.equalsIgnoreCase("true")) {%>
<div style="margin: 10px 10px 10px 10px;">
	Incorrect user name or password. Please try again.
</div>
<%} %>

<form action="<%=request.getContextPath()%>/login" method="POST">
<table style="margin: 10px 10px 10px 10px;">
<tr>
<td>UserId:</td>
<td><input class="input" type="text" name="login_user_id" size="16"></td>
</tr>
<tr>
<td>Password:</td>
<td><input class="input" type="password" name="login_user_pass" size="16"></td>
</tr>
<tr>
<td></td>
<td><input style="margin-top: 10px;" type="submit" class="input" value="Submit"/></td>
</tr>
</table>
</form>
<jsp:include page="footer.jsp" />
