<!-- THIS IS THE STUDY SEARCH PAGE --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>

<%@ page import="java.util.*, org.apache.commons.lang.*" %>

<!-- Include standard header -->

<%
boolean validated = true; // assume all is well
String earlyEndDate = ""; // Default
String lateEndDate = ""; // Default

// Get parameters
String dateFormat = application.getInitParameter("DateFormat");
String action = request.getParameter("action");

if (action != null && action.equals("search")){
    earlyEndDate = request.getParameter("early_end_date");
    lateEndDate = request.getParameter("late_end_date");

    // Validate dates
    if(StringUtils.isNotBlank(earlyEndDate)){
        if (StringUtils.isNumeric(earlyEndDate)){ // Check if it's just a year
            int earlyEndDateInt = Integer.parseInt(earlyEndDate);
            if (earlyEndDateInt < 1000 || earlyEndDateInt > 9999){
                validated = false;
            }
        }
        else if (!Validator.isDate(earlyEndDate, dateFormat)){
            validated = false;
        }
    }
    if(StringUtils.isNotBlank(lateEndDate)){
        if (StringUtils.isNumeric(lateEndDate)){ // Check if it's just a year
            int lateEndDateInt = Integer.parseInt(lateEndDate);
            if (lateEndDateInt < 1000 || lateEndDateInt > 9999){
                validated = false;
            }
        }
        else if (!Validator.isDate(lateEndDate, dateFormat)){
            validated = false;
        }
    }

    // If input is valid, go to results page
    if (validated == true){
        RequestDispatcher dispatcher = application.getRequestDispatcher("/jsp/list_studies.jsp");
        dispatcher.forward(request, response);
    }
}
%>

<jsp:include page="header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="45%">



<p>
<span class="path">studies > search</span>

<p>Please enter your search criteria below (either freetext or by specifying Type, End and Responsibles), then click the 'Search' button to continue.</p>

<form action="<%=request.getContextPath()%>/jsp/search.jsp" method="POST">
<table>
<%
// Print error message if input did'nt validate
if (action != null && action.equals("search") && !validated){
%>
<tr><td colspan="2">End of study fields must be blank or valid date or year.<p></td></tr>
<%
} // end if not validated
%>

<tr>
  <td><span class="label2">Type of Study:</span></td>
  <td>
        <select class="input" name="study_type_id" class="content">
                        <option value="0" selected>Any
                <%
                List studyTypeList = MasterDataDAO.getInstance().getList(StudyType.class);
                for (ListIterator i = studyTypeList.listIterator(); i.hasNext(); ){
                    StudyType st = (StudyType) i.next();
                %>
                    <option value="<%=st.getStudyTypeId()%>"><%=st.getStudyTypeName() %>
                <%
                }
                %>
        </select>
  </td>
</tr>
<tr>
  <td valign="top"><span class="label2">End of Study:</span></td>
  <td valign="top"><span class="content2">Between</span> <input class="input" type="text" size="11" maxlength="11" name="early_end_date"> <span class="content2">and</span> <input size="11" maxlength="11" class="input" type="text" name="late_end_date"> <br> <span class="path">( date or year, e.g. 02-Oct-2003 or 2003 )</span></td>
</tr> 
<tr>
  <td valign="top"><span class="label2">Study Responsibles:</span></td>
  <td>
    <select class="input" name="study_responsibles" size="4" multiple>
            <option value="0" selected>Any  <%-- Enable user to "de-select" --%>    
            <%
            List allResponsibles = DB.getInstance().getAllResponsibles();

            // Output list of all responsibles
            for (ListIterator i = allResponsibles.listIterator(); i.hasNext(); ){
                Responsible resp = (Responsible) i.next(); %>
                <option value="<%=resp.getPeopleId()%>"><%=resp.getFamilyName()%>, <%=resp.getFirstName()%>
        <%  }
            %>
    </select>
  </td>
</tr>
<tr>
    <td><span class="label2">Free text search:</span></td>
    <td><span class="content2"><input class="input" type="text" name="free_text_search" size="50"> </span><span class="path">Will override other criteria</span></td>
</tr> 
</table>
<br>
<input type="hidden" name="action" value="search">
<input class="input" type="submit" name="ok" value="Search >">
</form>


</td>

<td valign="top" width="15%">
<br><br><br>
<jsp:include page="menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>



<!-- Include standard footer -->
<br><br>
<jsp:include page="footer.jsp" />
