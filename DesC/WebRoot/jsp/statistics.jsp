<!-- THIS IS THE MAIN PAGE FOR DES ADMINS --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>

<%@page import="no.machina.simula.*, java.util.*"%>



<!-- Include standard header -->
<jsp:include page="header.jsp" />


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td valign="top" width="45%">
<p>
<span class="path">studies > statistics</span>


<p>
Here you can run statistical graphical reports based on the study data in the DES system. Click the report you wish to run from the list below.
<p>



<ul type="square">
<li>
<p class="bodytext-bold">
<a title="Click to run the Study Type by year report" href="aggregated_study_info.jsp">Aggregated Study Report (by type/year)</a>
</p>
</li>
</ul>



</td>



<td valign="top" width="15%">
<br><br><br>
<jsp:include page="menu.jsp" />
</td>


<td width="10%">
&nbsp;
</td>

</tr>

</table>






<!-- Include standard footer -->

<jsp:include page="footer.jsp" />
