<!-- THIS IS THE SINGLE STUDY VIEW PAGE --->
<%@ page import="no.machina.simula.engine.*, no.machina.simula.entityBeans.*, no.machina.simula.engine.dao.*"%>

<%@ page import="no.machina.simula.*, java.util.*, org.apache.commons.lang.*" %>
<%@page import="java.net.URLDecoder"%>



<!-- Include standard header -->
<jsp:include page="header.jsp" />


<%
String studyIdParam = request.getParameter("id");
int studyId = Integer.parseInt(studyIdParam);
// Get study from database
DB db = DB.getInstance();
Study study = db.getSingleStudy(studyId);
List publications = study.getPublications();
List responsibles = study.getResponsibles();
%>


<!-- MAIN BODY -->


<tr>

<td width="15%">
&nbsp;
</td>

<td width="70%">
<p>
<span class="path">studies > search > search results > study details</span>

<p>The study you selected is displayed in detail below.</p>


<table width="100%" cellspacing="1" cellpadding="5">
<form action="study_edit.jsp" method="POST">
<tr>
<td valign="top" class="label2">
Study name:
</td>
<td class="content">
<%=study.getName() %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Study Responsibles:
</td>
<td class="content">
<%
	for (ListIterator i = responsibles.listIterator(); i.hasNext(); ) {
	    Responsible r = (Responsible) i.next();
	    out.println("<a href=\"" + r.getUrl() + "\">" + r.getFirstName() + " " + r.getFamilyName() + "</a>");
	    // add a line break if there's more coming
	    if (i.hasNext()) out.println("<br>");
	}
%>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Type of Study:
</td>
<td class="content">
<%=db.getStudyTypeName(study.getTypeId()) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Study description: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getDesc()).replaceAll("\\n", "<br>") %>
</td>
</tr>
<tr>
<td valign="top" class="label2">
Duration of Study: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getDurationAsString())+ " " + (study.getUnitId() == -1 ? "" : study.getUnit()) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Start of Study: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getStartAsString(application.getInitParameter("DateFormat"))) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
End of Study: 
</td>
<td class="content">
<%=study.getEndAsString(application.getInitParameter("DateFormat")) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Publications: 
</td>
<td class="content">
<%
	for (ListIterator i = publications.listIterator(); i.hasNext(); ) {
	    Publication p = (Publication) i.next();
	    out.println("<a href=\"" + p.getUrl() + "\">" + URLDecoder.decode(p.getTitle()) + "</a>");
	    // add a line break if there's more coming
	    if (i.hasNext()) out.println("<br>");
	}
%>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Keywords: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getKeywords()).replaceAll("\\n", "<br>") %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Number of students: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getNoOfStudentsAsString()) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Number of professionals: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getNoOfProfessionalsAsString()) %>
</td>
</tr>

<tr>
<td valign="top" class="label2">
Study Material: 
</td>
<td class="content">
<%
    List files = db.getFilesForStudy(studyId);
    for(ListIterator i = files.listIterator(); i.hasNext(); ){
        FileInfo fi = (FileInfo) i.next();
%>
        <a href="../GetFile?file_id=<%=fi.getId()%>"><%=fi.getDesc()%></a> <br>
<%
    }
%>

<%
    List urls = db.getUrlsForStudy(studyId);
    for(ListIterator i = urls.listIterator(); i.hasNext(); ){
        Url u = (Url) i.next();
%>
        <a href="<%=u.getUrl()%>" target="_blank"><%=u.getDesc()%></a> <br>
<%
    }
%>

</td>
</tr>

<tr>
<td align="top" class="label2">
Study notes: 
</td>
<td class="content">
<%=StringUtils.defaultString(study.getStudyNotes()).replaceAll("\\n", "<br>") %>
</td>
</tr>

<tr>
<td class="content">
&nbsp;
</td>
<td class="content">
</td>
</tr>
</table>

</td>

<td width="15%">
&nbsp;
</td>

</tr>

</table>

<!-- Include standard footer -->
<jsp:include page="footer.jsp" />
