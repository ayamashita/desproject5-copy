<?xml version="1.0" encoding="UTF-8" ?>
<!--
    Document   : StudyList.xsl
    Created on : 6. oktober 2003, 19:26
    Author     : cato
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <xsl:output method="xml" 
              encoding="UTF-8"
              indent="yes"/>

    <xsl:attribute-set name="page-setings">
        <xsl:attribute name="page-width">210mm</xsl:attribute>
        <xsl:attribute name="page-height">297mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="body-margins">
        <xsl:attribute name="margin-top">3cm</xsl:attribute>
        <xsl:attribute name="margin-bottom">1.5cm</xsl:attribute>
        <xsl:attribute name="margin-left">1.0cm</xsl:attribute>
        <xsl:attribute name="margin-right">1.5cm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="data_font">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-family">verdana,sans-serif</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="bold_font">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-family">verdana,sans-serif</xsl:attribute>
    </xsl:attribute-set>

    <!-- Matches the root element and initializes the fo document
    -->
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>

                <!-- layout for the first page -->
                <fo:simple-page-master master-name="first" xsl:use-attribute-sets="page-settings">
                    <fo:region-body xsl:use-attribute-sets="body-margins"/>
                    <fo:region-before extent="3cm" />
                    <fo:region-after extent="1.5cm" />
                </fo:simple-page-master>

                <fo:page-sequence-master>
                    <fo:single-page-master-reference master-reference="first"/>
                </fo:page-sequence-master>
            </fo:layout-master-set>

            <fo:page-sequence id="contract" master-name="first">
                <!-- Create the footer with company information -->
                <fo:static-content flow-name="xsl-region-before">
                    <!--fo:block-container height="1m" width="21cm" background-color="#FE7519"/-->
                    <fo:block-container height="5cm" width="21cm" background-color="#FE7519">
                        <fo:block>
                        <fo:table>
                            <fo:table-column column-width="5cm"/>
                            <fo:table-column column-width="16cm"/>
                            <fo:table-body>
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:external-graphic src="logo-part1.gif"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block>
                                            <fo:external-graphic src="logo-part2.gif"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <!-- 
        Matches the top study-list element and creates the basic table and headers
    -->
    <xsl:template match="study-list">
        <fo:table>
            <fo:table-column column-width="30mm"/>
            <fo:table-column column-width="20mm"/>
            <fo:table-column column-width="20mm"/>
            <fo:table-column column-width="35mm"/>
            <fo:table-column column-width="40mm"/>
            <fo:table-column column-width="40mm"/>

            <fo:table-header>
                <fo:table-row xsl:use-attribute-sets="bold_font" text-align="start">
                    <fo:table-cell>
                        <fo:block>Study Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>Type of Study</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>End of Study</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>Study Responsible</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>Study Description</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>Publications</fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-header>

            <fo:table-body>
                <xsl:apply-templates/>
            </fo:table-body>

        </fo:table>
    </xsl:template>

    <!-- 
         Matches the study element and creates adds one row for each study.
         Each addition study responsible and publication is added on separate lines
    -->
    <xsl:template match="study">
        <fo:table-row xsl:use-attribute-sets="data_font">
            <fo:table-cell>
                <fo:block><xsl:value-of select="name"/></fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block><xsl:value-of select="type"/></fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block><xsl:value-of select="end"/></fo:block>
            </fo:table-cell>
            <xsl:if test="count(responsible)>0">
                <fo:table-cell>
                    <fo:block>
                        <xsl:value-of select="responsible[1]/name"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="count(responsible)=0">
                <fo:table-cell/>
            </xsl:if>
            <fo:table-cell>
                <fo:block><xsl:value-of select="desc"/></fo:block>
            </fo:table-cell>
            <xsl:if test="count(publication)>0">
                <fo:table-cell>
                    <fo:block>
                        <xsl:value-of select="publication[1]/title"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="count(publication)=0">
                <fo:table-cell/>
            </xsl:if>
        </fo:table-row>

        <xsl:for-each select="publication[position()>1]">
            <fo:table-row xsl:use-attribute-sets="data_font">
                <fo:table-cell/>
                <fo:table-cell/>
                <fo:table-cell/>
                <xsl:if test="count(responsible) > position()">
                    <fo:table-cell>
                        <fo:block><xsl:value-of select="responsible[position()]/first-name"/></fo:block>
                        <fo:block><xsl:value-of select="concat(' ',responsible[position()]/family-name)"/></fo:block>
                    </fo:table-cell>
                </xsl:if>
                <xsl:if test="not(count(responsible) > position())">
                    <fo:table-cell/>
                </xsl:if>
                <fo:table-cell/>
                <fo:table-cell>
                    <fo:block><xsl:value-of select="title"/></fo:block>
                </fo:table-cell>
            </fo:table-row>
        </xsl:for-each>
                
        <!-- Insert any remaining responibles -->
        <xsl:for-each select="responsible[position()>count(../publication) and count(../publication)>0]">
            <fo:table-row xsl:use-attribute-sets="data_font">
                <fo:table-cell/>
                <fo:table-cell/>
                <fo:table-cell/>
                <fo:table-cell>
                    <fo:block><xsl:value-of select="name"/></fo:block>
                </fo:table-cell>
                <fo:table-cell/>
                <fo:table-cell/>
            </fo:table-row>    
        </xsl:for-each>

    </xsl:template>

</xsl:stylesheet> 
