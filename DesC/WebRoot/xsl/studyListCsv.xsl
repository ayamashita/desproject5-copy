<?xml version="1.0" encoding="UTF-8" ?>

<!--
    Document   : studyListCsv.xsl
    Created on : 13. oktober 2003, 10:55
    Author     : cato
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" 
              encoding="UTF-8"
              indent="no"/>
    <xsl:template match="/study-list/study">

        <xsl:for-each select="publication[position()>0]">
            <xsl:value-of select="../name"/>, 
            <xsl:value-of select="../type"/>, 
            <xsl:value-of select="../desc"/>, 
            <xsl:value-of select="../duration"/>, 
            <xsl:value-of select="../unit"/>, 
            <xsl:value-of select="../start"/>, 
            <xsl:value-of select="../end"/>, 
            <xsl:value-of select="../no-students"/>, 
            <xsl:value-of select="../no-profs"/>, 
            <xsl:value-of select="../notes"/>, 
            <xsl:value-of select="../owner"/>, 
            <xsl:value-of select="../last-edited-by"/>, 

            <xsl:if test="count(responsible) > position()">
                <xsl:value-of select="responsible[position()]/first-name"/>, 
                <xsl:value-of select="responsible[position()]/family-name"/>, 
            </xsl:if>

            <xsl:if test="count(publication) > position()">
            <xsl:value-of select="publication[position()]/first-name"/>, 
            <xsl:value-of select="publication[position()]/family-name"/>
            </xsl:if>

        </xsl:for-each>

        <!-- Insert any remaining responsibles -->
        <xsl:for-each select="responsible[position()>count(../publication)]">
            <xsl:value-of select="../name"/>, 
            <xsl:value-of select="../type"/>, 
            <xsl:value-of select="../desc"/>, 
            <xsl:value-of select="../duration"/>, 
            <xsl:value-of select="../unit"/>, 
            <xsl:value-of select="../start"/>, 
            <xsl:value-of select="../end"/>, 
            <xsl:value-of select="../no-students"/>, 
            <xsl:value-of select="../no-profs"/>, 
            <xsl:value-of select="../notes"/>, 
            <xsl:value-of select="../owner"/>, 
            <xsl:value-of select="../last-edited-by"/>, 

            <xsl:if test="count(responsible) > position()">
                <xsl:value-of select="responsible[position()]/first-name"/>, 
                <xsl:value-of select="responsible[position()]/family-name"/>, 
            </xsl:if>

            <xsl:if test="count(publication) > position()">
                <xsl:value-of select="publication[position()]/first-name"/>, 
                <xsl:value-of select="publication[position()]/family-name"/>
            </xsl:if>

        </xsl:for-each>

    </xsl:template>

</xsl:stylesheet> 

