CREATE table tbl_report_criteria (
	report_criteria_id int not null,
	report_criteria_name varchar(4) not null,
	primary key (report_criteria_id)
);

CREATE table tbl_report_sort_order (
	report_sort_order_id int not null,
	report_sort_order_name varchar(4) not null,
	report_sort_order_desc varchar(32) not null,
	primary key (report_sort_order_id)
);

CREATE table tbl_column (
	column_id int not null auto_increment,
	column_name varchar(32) not null,
	column_display_name varchar(32) not null,
	column_type int not null default 0,
	primary key (column_id)
);

CREATE table tbl_report (
	report_id int not null auto_increment,
	report_name varchar(32) not null,
	people_id varchar(32) not null,
	report_sort_order_id int default 1 not null,
	report_criteria_id int default 1 not null,
	primary key (report_id),
	foreign key (report_sort_order_id) references tbl_report_sort_order(report_sort_order_id),
	foreign key (report_criteria_id) references tbl_report_criteria(report_criteria_id)
);

CREATE table tbl_report_column (
	report_id int not null,
	column_id int not null,
	report_column_order int default 0 not null,
	primary key (report_id, column_id),
	foreign key (report_id) references tbl_report(report_id),
	foreign key (column_id) references tbl_column(column_id)
);


CREATE table tbl_report_responsible (
	report_id int not null,
	people_id varchar(32) not null,
	primary key (report_id, people_id),
	foreign key (report_id) references tbl_report(report_id)
);

