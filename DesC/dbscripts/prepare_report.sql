insert into tbl_report_sort_order (report_sort_order_id, report_sort_order_name, report_sort_order_desc) values
(1,'-', 'unsorted'), (2,'ASC', 'ascending'), (3,'DESC', 'descending');

insert into tbl_report_criteria (report_criteria_name) values ('OR'), ('AND');

# these are columns that can be read from study database
insert into tbl_column (column_name, column_display_name)
values 
	('study_name', 'Study Name'),
	('study_desc', 'Study description'),
	('study_start_date', 'Start of Study'),
	('study_end_date', 'End of Study'),
	('keywords', 'Keywords'),
	('no_of_students', 'No of Student participants'),
	('no_of_professionals', 'No of Professional participants'),
	('study_notes', 'Study Notes');
	 
# these are special columns, that have to be handled individually	 
insert into tbl_column (column_name, column_display_name, column_type)
values  
	('study_type', 'Study type', 1),
	('study_responsible', 'Study_responsible', 1),
	('study_duration', 'Duration of Study', 1),
	('study_publications', 'Publication',1),
	('study_files', 'Study material',1),
	('study_owner', 'Study Owner',1),
	('last_edited_by', 'Last edited by',1);
