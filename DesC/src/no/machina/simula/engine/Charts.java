package no.machina.simula.engine;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import no.machina.simula.engine.dao.DB;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.DefaultCategoryDataset;

public class Charts {
    
	private static Logger log = Logger.getLogger(Charts.class);
    public static int width = 300;
    public static int height = 300;
    
    public static void setHeadLess( boolean headless ) {
        System.setProperty( "java.awt.headless", Boolean.toString( headless ) ); 
    }
    
    public static class Year {
        public String year;
        private Map valueSets = new HashMap();
        public void addValueSet( String studyName, int value ) {
            valueSets.put( studyName, new Integer( value ) );
        }
        public Year( String year ) {
            this.year = year;
        }
    }
    
    /** Creates a new instance of ChartTester */
    public Charts() {
    }
    
    
    public static byte[] getBytes( Year[] years ) throws Exception {
        BufferedImage bimage = getImage( years );
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write( bimage, "PNG", bos );
        return bos.toByteArray();
    }
    
    //    public static byte[] getBytes( File f ) {
    //        ByteArrayOutputStream bos = new ByteArrayOutputStream();
    //        InputStream is = new BufferedInputStream( new FileInputStream( f ) );
    //        int i;
    //        byte[] buf
    //        while( ( i = is.available() ) > 0 )
    //            bos.write( is.read( i ) );
    //        return bos.toByteArray();
    //    }
    
    //    public static File getFile( Year[] years ) {
    //    }
    
    
    /**
     * @param args the command line arguments
     */
    public static BufferedImage getImage(Year[] years) throws Exception {
        
        // Hardcoded info:
        String title = "No of studies pr. year by type";
        String categoryAxisLabel = "Year";
        String valueAxisLabel = "No of Studies";
        DefaultCategoryDataset data = null;
        PlotOrientation orientation = PlotOrientation.VERTICAL;
        boolean legend = true;
        boolean tooltips = true;
        boolean urls = true;
        
        
        // Data:
        data = new DefaultCategoryDataset();
        for( int i = 0; i < years.length; i++ ) {
            Year y = years[ i ];
            for( Iterator it = y.valueSets.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry me = (Map.Entry)it.next();
                String studyName = (String)me.getKey();
                Integer studyCount = (Integer)me.getValue();
                data.addValue( studyCount, studyName, y.year );
            }
        }
        
        
        // Test:
        log.debug("Cols: " + data.getColumnCount() );
        log.debug("Rows: " + data.getRowCount() );
        
        // Image stuff:
//        File toWrite = new File( "c:/temp/jfreechart.png" );
        
        // Create chart:
        JFreeChart chart = ChartFactory.createBarChart3D( title, categoryAxisLabel, valueAxisLabel, data, orientation, legend, tooltips, urls );
        
        CategoryPlot plot = chart.getCategoryPlot(); 
        log.debug("Plot: " + plot );
        
        ValueAxis axis = plot.getRangeAxis(); 
        log.debug("Axis: " + axis );
        NumberAxis na = (NumberAxis)axis; 
        na.setTickUnit( new NumberTickUnit( 1 ) ); 
        
        // Create image:
        BufferedImage bimage = chart.createBufferedImage( width, height );
        return bimage;
    }
    
    public static byte[] getBytes() throws Exception {
        
        Year[] years = getYears();
        return getBytes( years );
    }
    
    public static Connection getConnection() throws Exception {
        throw new RuntimeException("Dont use this!");
    }
    
    public static ResultSet getRS() throws Exception {
        String sql = "SELECT YEAR(tbl_study.study_end_date) AS STUDY_YEAR, tbl_study_type.study_type_name, count( tbl_study.study_type_id ) ";
        sql += " FROM tbl_study, tbl_study_type ";
        sql += " WHERE tbl_study.study_type_id = tbl_study_type.study_type_id ";
        sql += " GROUP BY study_year, tbl_study.study_type_id ";
        sql += " ORDER BY study_year";
        Connection c = null;
        try {
            c = DB.getInstance().getConnection();
            PreparedStatement ps = c.prepareStatement( sql );
            ResultSet rs = ps.executeQuery();
            return rs;
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	throw e;
        }
//        } finally {
//            try {
//                if( c != null )
//                   DB.getInstance().closeConnection(c);
//            } catch( Exception e ) {
//                e.printStackTrace();
//            }
//        }
    }
    
    public static Year[] getYears() throws Exception {
        
        ResultSet rs = getRS();
        
        List years = new ArrayList();
        // Ordered by year:
        int lastYear = -1;
        Year year = null;
        while( rs.next() ) {
            int y = rs.getInt( 1 );
            String s = rs.getString( 2 );
            int c = rs.getInt( 3 );
            
            log.debug("Adding data: year=" + y +  ", study=" + s + ", count=" + c );
            
            if( lastYear != y ) {
                year = new Year( ""+y );
                years.add( year );
                lastYear = y;
            }
            year.addValueSet( s, c );
        }
        rs.close();
        return (Year[])years.toArray( new Year[ years.size() ] );
    }
    
//    public static void main( String[] args ) throws Exception {
//        
//        //        // Create data:
//        //        String ctr = "Controlled";
//        //        String cs = "Case study";
//        //
//        //        Year[] years = new Year[] { new Year( "2001" ), new Year( "2002" ), new Year( "2003" ) };
//        //        years[ 0 ].addValueSet( ctr, 70 );
//        //        years[ 1 ].addValueSet( ctr, 80 );
//        //        years[ 2 ].addValueSet( ctr, 90 );
//        //
//        //        years[ 0 ].addValueSet( cs, 30 );
//        //        years[ 1 ].addValueSet( cs, 40 );
//        //        years[ 2 ].addValueSet( cs, 50 );
//        //
//        //        // Get the byte:
//        //        byte[] bytes = getBytes( years );
//        
//        
//        byte[] bytes = getBytes();
//                
//        // Write to file:
//        FileOutputStream fos = new FileOutputStream( "c:/temp/jfreechart.png" );
//        fos.write( bytes );
//        fos.close();
//        
//        // Done.
//        System.out.println("Done?");
//        
//    }
    
}
