package no.machina.simula.engine;

import java.util.*;
import java.text.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Misc.  methods for converting.
 * 
 */
public class Converter{
    
    /** 
     * Converts an array of strings into an array of ints.
     * @param stringArray An array of string denoting integers.
     * @throws NumberFormatException If one of the strings does not contain an integer.
     * @return An array containing the ints corresponding to the strings in the input.
     *  Null if the input array is null.
     */    
    public static int[] stringArrayToIntArray(String[] stringArray) throws NumberFormatException {
        if (stringArray == null) return null;
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++){
            intArray[i] = Integer.parseInt(stringArray[i]);
        }
        return intArray;
    }

    /** 
     * Converts an array of strings into an array of Integers.
     * @param stringArray An array of string denoting integers.
     * @throws NumberFormatException If one of the strings does not contain an integer.
     * @return An array containing the ints corresponding to the strings in the input.
     *  Null if the input array is null.
     */    
    public static Integer[] stringArrayToIntegerArray(String[] stringArray) throws NumberFormatException {
        if (stringArray == null) return null;
        Integer[] integerArray = new Integer[stringArray.length];
        for (int i = 0; i < stringArray.length; i++){
            integerArray[i] = new Integer(stringArray[i]);
        }
        return integerArray;
    }
        
    /**
     * Converts a string into a java.util.Date.
     * @param stringDate A string containing a date.
     * @param dateFormat The date format to be used. This should be a format as
     * specified in the java.text.SimpleDateFormat documentation.
     * @return A date corresponding to the input string. If the input is 
     * null, the empty string or can't be parsed as a Date using the
     * given format, null will be returned.
     */
    public static Date stringToDate(String stringDate, String dateFormat){
        Date date = null;
        DateFormat df = new SimpleDateFormat(dateFormat, new Locale("en", "GB"));

        if (stringDate != null){
            if (!stringDate.equals("")){
                try{
                    stringDate = stringDate.trim();
                    date = df.parse(stringDate);
                }
                catch(ParseException pe){
                    // do nothing, date will be null
                }
            }
        }
        return date;
    }

    /** Return the first string if the object is a string array, null if not
     */
    public static String getFirstString(Object strArr) {
        String retValue = null;
        
        if ( strArr instanceof String[] ) {
            String[] tmpStrArr = (String[])strArr;
            if ( tmpStrArr.length > 0 ) {
                retValue = tmpStrArr[0];
            }
        } else if ( strArr instanceof String ) {
            retValue = (String)strArr;
        } else if ( strArr instanceof Integer ) {
            retValue = ((Integer)strArr).toString();
        }

        return retValue;
    }
    
}
