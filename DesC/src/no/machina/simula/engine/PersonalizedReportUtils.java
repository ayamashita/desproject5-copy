package no.machina.simula.engine;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import no.machina.simula.engine.dao.DB;
import no.machina.simula.entityBeans.Column;
import no.machina.simula.entityBeans.PersonalizedReport;
import no.machina.simula.entityBeans.Publication;
import no.machina.simula.entityBeans.ReportCriteria;
import no.machina.simula.entityBeans.ReportSortOrder;
import no.machina.simula.entityBeans.Responsible;
import no.machina.simula.entityBeans.Study;
import no.machina.simula.entityBeans.StudyType;
import no.machina.simula.entityBeans.Unit;

import org.apache.log4j.Logger;

import com.sun.rsasign.n;

public class PersonalizedReportUtils {

	private static PersonalizedReportUtils instance = new PersonalizedReportUtils();

	private PersonalizedReportUtils() {
	};

	public static PersonalizedReportUtils getInstance() {
		return instance;
	}

	private static Logger log = Logger.getLogger(PersonalizedReportUtils.class);

	private List allResponsibles = null;
	private List allColumns = null;
	private List allCriteria = null;
	private List allSortOrder = null;
	private HashSet columnNames = null;
	private HashSet responsibleIds = null;

	protected HashSet getColumnNames() {
		if (columnNames == null || columnNames.isEmpty()) {
			List cols = getAvailableColumns();
			columnNames = new HashSet();
			Iterator colIter = cols.iterator();

			while (colIter.hasNext()) {
				Column col = (Column) colIter.next();
				columnNames.add(col.getName());
			}
		}
		return columnNames;
	}

	protected HashSet getResponsibleIds() {
		if (responsibleIds == null || responsibleIds.isEmpty()) {
			List responsibles = getAllResponsibles();
			responsibleIds = new HashSet();
			Iterator respIter = responsibles.iterator();

			while (respIter.hasNext()) {
				Responsible resp = (Responsible) respIter.next();
				responsibleIds.add(resp.getPeopleId());
			}
		}
		return responsibleIds;
	}

	public List getAvailableColumns() {
		if (allColumns == null || allColumns.isEmpty()) {
			try {
				allColumns = DB.getInstance().getAllColumns();
			} catch (SQLException e) {
				log.error("SQL error occured:" + e);
				log.error("SQL error message:" + e);
				e.printStackTrace();
				allColumns = new ArrayList();
			} catch (Exception e) {
				log.error("Error occured:" + e);
				log.error("Error message:" + e);
				e.printStackTrace();
				allColumns = new ArrayList();
			}
		}

		return new ArrayList(allColumns);
	}

	public List getAllResponsibles() {
		if (allResponsibles == null || allResponsibles.isEmpty()) {
			try {
				allResponsibles = DB.getInstance().getAllResponsibles();
			} catch (SQLException e) {
				log.error("SQL error occured:" + e);
				log.error("SQL error message:" + e);
				e.printStackTrace();
				allResponsibles = new ArrayList();
			} catch (Exception e) {
				log.error("Error occured:" + e);
				log.error("Error message:" + e);
				e.printStackTrace();
				allResponsibles = new ArrayList();
			}
		}

		return new ArrayList(allResponsibles);
	}

	public List getAllCriteria() {
		if (allCriteria == null || allCriteria.isEmpty()) {
			try {
				allCriteria = DB.getInstance().getAllCriteria();
			} catch (SQLException e) {
				log.error("SQL error occured:" + e);
				log.error("SQL error message:" + e);
				e.printStackTrace();
				allCriteria = new ArrayList();
			} catch (Exception e) {
				log.error("Error occured:" + e);
				log.error("Error message:" + e);
				e.printStackTrace();
				allCriteria = new ArrayList();
			}
		}

		return allCriteria;
	}

	public List getAllSortOrder() {
		if (allSortOrder == null || allSortOrder.isEmpty()) {
			try {
				allSortOrder = DB.getInstance().getAllSortOrder();
			} catch (SQLException e) {
				log.error("SQL error occured:" + e);
				log.error("SQL error message:" + e);
				e.printStackTrace();
				allSortOrder = new ArrayList();
			} catch (Exception e) {
				log.error("Error occured:" + e);
				log.error("Error message:" + e);
				e.printStackTrace();
				allSortOrder = new ArrayList();
			}
		}

		return allSortOrder;
	}

	public void refreshCache() {
		allResponsibles = null;
		getAllResponsibles();

		allColumns = null;
		getAvailableColumns();

		allCriteria = null;
		getAllCriteria();

		allSortOrder = null;
		getAllSortOrder();

		columnNames = null;
		getColumnNames();

		responsibleIds = null;
		getResponsibleIds();
	}

	public PersonalizedReport insertOrUpdateReport(int id, String reportName, String reportOwner,
			String[] reportColumnNames, String timeSorting, String[] reportResponsibleIds, String responsibleCriteria)
			throws SQLException, Exception {
		PersonalizedReport report = new PersonalizedReport();
		report.setId(id);
		report.setOwner(reportOwner);
		report.setColumns(getColumnsByNames(reportColumnNames));
		report.setResponsibles(getResponsiblesByIds(reportResponsibleIds));
		report.setName(reportName);
		report.setResponsibleCriteria(getCriteriaByName(responsibleCriteria));
		report.setTimeSortOrder(getSortOrderByName(timeSorting));

		return DB.getInstance().insertOrUpdateReport(report);
	}

	private ReportCriteria getCriteriaByName(String responsibleCriteria) {
		List criteriaList = getAllCriteria();
		Iterator criteriaIter = criteriaList.iterator();

		while (criteriaIter.hasNext()) {
			ReportCriteria criteria = (ReportCriteria) criteriaIter.next();
			if (criteria.getCriteriaName().equals(responsibleCriteria)) {
				return criteria;
			}
		}

		return null;
	}

	private ReportSortOrder getSortOrderByName(String sortOrderName) {
		List allSortOrder = getAllSortOrder();
		Iterator sortIter = allSortOrder.iterator();

		while (sortIter.hasNext()) {
			ReportSortOrder order = (ReportSortOrder) sortIter.next();
			if (order.getSortOrderName().equals(sortOrderName)) {
				return order;
			}
		}

		return null;
	}

	public List validateReportData(String reportName, String reportOwner, String[] reportColumnNames,
			String timeSorting, String[] reportResponsibleIds, String responsibleCriteria) {
		// validate report columns
		List errors = new ArrayList();

		if (reportName == null || reportName.length() == 0) {
			errors.add("Report name has to be specified!");
		}

		if (reportOwner == null || reportOwner.length() == 0) {
			errors.add("Report name has to be specified!");
		}

		if (reportColumnNames == null || reportColumnNames.length == 0) {
			errors.add("You have to select at least one column for report.");
		} else {
			validateColumnNames(reportColumnNames, errors);
		}

		if (timeSorting == null || timeSorting.length() == 0) {
			errors.add("Sort order must be specified.");
		} else {
			if (getSortOrderByName(timeSorting) == null) {
				errors.add("Sort order " + timeSorting + " is invalid!");
			}
		}

		if (reportResponsibleIds == null || reportResponsibleIds.length == 0) {
			errors.add("You have to select at least one responsible for report.");
		} else {
			validateResponsibles(reportResponsibleIds, errors);
		}

		if (responsibleCriteria == null || responsibleCriteria.length() == 0) {
			errors.add("Responsible criteria must be specified");
		}
		return errors;
	}

	private void validateResponsibles(String[] reportResponsibleIds, List errors) {
		for (int i = 0; i < reportResponsibleIds.length; i++) {
			if (!getResponsibleIds().contains(reportResponsibleIds[i])) {
				errors.add("Responsible with id " + reportResponsibleIds[i] + " is invalid!");
			}
		}

	}

	private void validateColumnNames(String[] reportColumnNames, List errors) {
		for (int i = 0; i < reportColumnNames.length; i++) {
			if (!getColumnNames().contains(reportColumnNames[i])) {
				errors.add("Column " + reportColumnNames[i] + " is invalid!");
			}
		}
	}

	private List getColumnsByNames(String[] names) {
		if (names == null || names.length == 0) {
			return new ArrayList();
		}

		HashSet namesSet = new HashSet(Arrays.asList(names));
		List cols = this.getAvailableColumns();
		Iterator colIter = cols.iterator();
		ArrayList selectedColumns = new ArrayList();

		while (colIter.hasNext()) {
			Column col = (Column) colIter.next();

			if (namesSet.contains(col.getName())) {
				selectedColumns.add(col);
			}
		}

		return selectedColumns;
	}

	private List getResponsiblesByIds(String[] ids) {
		if (ids == null || ids.length == 0) {
			return new ArrayList();
		}

		HashSet namesSet = new HashSet(Arrays.asList(ids));
		List responsibles = this.getAllResponsibles();
		Iterator respIter = responsibles.iterator();
		ArrayList selectedResponsibles = new ArrayList();

		while (respIter.hasNext()) {
			Responsible resp = (Responsible) respIter.next();

			if (namesSet.contains(resp.getPeopleId())) {
				selectedResponsibles.add(resp);
			}
		}

		return selectedResponsibles;
	}

	public PersonalizedReport getReport(String id) {
		int reportId = -1;

		try {
			reportId = Integer.parseInt(id);
		} catch (NumberFormatException nfe) {
			log.error("Report id must be numeric!");
			return null;
		}

		PersonalizedReport pr = null;

		try {
			pr = DB.getInstance().getReport(reportId);
		} catch (SQLException e) {
			log.error("SQL error occured: " + e);
			log.error("SQL error message: " + e.getMessage());
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			log.error("Error occured: " + e);
			log.error("Error message: " + e.getMessage());
			e.printStackTrace();
			return null;
		}

		return pr;
	}

	public List getReportsForUser(String userId) throws SQLException, Exception {
		return DB.getInstance().getReportsForUser(userId);
	}

	public void deleteReport(String id) throws Exception {
		int reportId = -1;

		try {
			reportId = Integer.parseInt(id);
		} catch (NumberFormatException nfe) {
			log.error("Report id must be numeric!");
			throw new Exception("Report id must be numeric!", nfe);
		}

		try {
			DB.getInstance().deleteReport(reportId);
		} catch (Exception e) {
			log.error("Error while deleting personalized report with id = " + reportId  );
			throw e;
		}

	}

	public List queryReport(String reportId) throws SQLException, Exception {
		PersonalizedReport pr = getReport(reportId);
		return DB.getInstance().queryReport(pr);
	}
}
