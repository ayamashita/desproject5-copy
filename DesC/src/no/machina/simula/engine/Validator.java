package no.machina.simula.engine;

import org.apache.commons.lang.*;
import java.util.*;
import java.text.*;
/**
 * Validation methods
 */
public class Validator {
    
    /**
     * Check if the string contains an integer larger than min.
     *
     * @param s The string to check, may be null.
     * @return true if the string only contains digits and is larger than min.
     */
    public static boolean isNumericLargerThan(String s, int min) {
        if (StringUtils.isNumeric(s)){
            int i = Integer.parseInt(s);
            if (i > min){
                return true;
            }
        }
        return false;
    }
    
    public static boolean isDate(String s, String format){
        boolean isDate = true;
        DateFormat df = new SimpleDateFormat(format, new Locale("en", "GB"));
        df.setLenient(false);

        if (StringUtils.isNotBlank(s)){
            try{
                s = s.trim();
                df.parse(s);
            }
            catch(ParseException pe){
                isDate = false;
            }
        }
        else{
            isDate = false;
        }

        return isDate;
            
    }

    public static boolean isUrl(String url) {
        if (url.startsWith("http://")){
            return true;
        }
        else if (url.startsWith("https://")){
            return true;
        }
        else if (url.startsWith("ftp://")){
            return true;
        }
        return false;
    }
    
    public static void main(String[] args){
        System.out.println(isDate(args[0], args[1]));
    }
        
}
