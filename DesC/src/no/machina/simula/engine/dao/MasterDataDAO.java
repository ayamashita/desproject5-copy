/*
 * MasterDataDAO.java
 *
 * Created on 14. oktober 2003, 18:13
 */

package no.machina.simula.engine.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.machina.simula.engine.wsclient.SimulaWSClient;
import no.machina.simula.entityBeans.Action;
import no.machina.simula.entityBeans.Audit;
import no.machina.simula.entityBeans.People;
import no.machina.simula.entityBeans.StudyType;
import no.machina.simula.entityBeans.Unit;
import no.machina.simula.entityBeans.UserType;

import org.apache.log4j.Logger;


/**
 *
 * @author  cato
 */
public class MasterDataDAO {
    
	private static Logger log = Logger.getLogger(MasterDataDAO.class);
    /** Creates a new instance of MasterDataDAO */
    private MasterDataDAO() {
    }

    private static MasterDataDAO _instance;

 // Simula WS client
    private SimulaWSClient wsClient = null;
    
    public SimulaWSClient getWSClient() {
    	if (wsClient == null) {
    		wsClient = new SimulaWSClient();
    	}
    	
    	return wsClient;
    }
    
    public static MasterDataDAO getInstance() {
        if ( _instance == null ) {
            _instance = new MasterDataDAO();
        }
        return _instance;
    }
    
    

    /** Delete an instance in the database
     */
    public void delete(int id, Class type) {
        if ( StudyType.class.equals(type)) {
            String sql = "delete from tbl_study_type where study_type_id=?";
            runUpdateSql(sql, new Object[] {new Integer(id)});
        } else if ( Unit.class.equals(type)) {
            String sql = "delete from tbl_unit where unit_id=?";
            runUpdateSql(sql, new Object[] {new Integer(id)});
        } else if ( Action.class.equals(type)) {
            String sql = "delete from tbl_action where action_id=?";
            runUpdateSql(sql, new Object[] {new Integer(id)});
        } else if ( UserType.class.equals(type)) {
            String sql = "delete from tbl_user_type where user_type_id=?";
            runUpdateSql(sql, new Object[] {new Integer(id)});
        } else {
            throw new RuntimeException("Don't know how to delete " + (type != null ? type.getName() : "null"));
        }
    }

    /** Update the instance 
     */
    public void update(Object value) {
        if ( StudyType.class.equals(value.getClass())) {
            StudyType st = (StudyType)value;
            String sql = "update tbl_study_type set study_type_name=?, study_type_desc=? where study_type_id=?";
            runUpdateSql(sql, new Object[] {st.getStudyTypeName(), st.getStudyTypeDesc(), new Integer(st.getStudyTypeId())});
        } else if ( Unit.class.equals(value.getClass())) {
            Unit st = (Unit)value;
            String sql = "update tbl_unit set unit_name=? where unit_id=?";
            runUpdateSql(sql, new Object[] {st.getUnitName(), new Integer(st.getUnitId())});
        } else if ( Action.class.equals(value.getClass())) {
            Action st = (Action)value;
            String sql = "update tbl_action set action_name=?, action_desc=? where action_id=?";
            runUpdateSql(sql, new Object[] {st.getActionName(), st.getActionDesc(), new Integer(st.getActionId())});
        } else if ( UserType.class.equals(value.getClass())) {
            UserType st = (UserType)value;
            String sql = "update tbl_user_type set user_type_name=?, user_type_desc=? where user_type_id=?";
            runUpdateSql(sql, new Object[] {st.getUserTypeName(), st.getUserTypeDesc(), new Integer(st.getUserTypeId())});
        } else if ( Audit.class.equals(value.getClass())) {
            Audit st = (Audit)value;
            String sql = "update tbl_audit set study_id=?, user_id=?, audit_date=? where audit_id=?";
            runUpdateSql(sql, new Object[] {new Integer(st.getStudyId()), st.getPeopleId(), st.getAuditDate()});
        } else {
            throw new RuntimeException("Don't know how to update " + (value != null ? value.getClass().getName() : "null"));
        }
    }
    
    /** Create a new instance and update
     */
    public void newInstance(Object value) {
        if ( StudyType.class.equals(value.getClass())) {
            StudyType st = (StudyType)value;
            String sql = "insert into tbl_study_type (study_type_name, study_type_desc) values (?,?)";
            runUpdateSql(sql, new Object[] {st.getStudyTypeName(), st.getStudyTypeDesc()});
        } else if ( Unit.class.equals(value.getClass())) {
            Unit st = (Unit)value;
            String sql = "insert into tbl_unit (unit_name) values (?)";
            runUpdateSql(sql, new Object[] {st.getUnitName()});
        } else if ( Action.class.equals(value.getClass())) {
            Action st = (Action)value;
            String sql = "insert into tbl_action (action_name, action_desc) values (?,?)";
            runUpdateSql(sql, new Object[] {st.getActionName(), st.getActionDesc()});
        } else if ( UserType.class.equals(value.getClass())) {
            UserType st = (UserType)value;
            String sql = "insert into tbl_user_type (user_type_name, user_type_desc) values (?,?)";
            runUpdateSql(sql, new Object[] {st.getUserTypeName(), st.getUserTypeDesc()});
        } else {
            throw new RuntimeException("Don't know how to create new instance of " + (value != null ? value.getClass().getName() : "null"));
        }
    }
    
    public Map getById(int id, String sql) {
        Map retValue = new HashMap();
        Connection con = null;
        try {
            con = DB.getInstance().getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setObject(1, new Integer(id));
            ResultSet rs = stmt.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int cols = rsmd.getColumnCount();
            if ( rs.next() ) {
                Map tmpMap = new HashMap();
                for ( int j = 1;j <= cols;j++) {
                    String colName = rsmd.getColumnName(j);
                    Object value = rs.getObject(j);
                    tmpMap.put(colName, value);
                }
                retValue = tmpMap;
            }
        } catch(Exception ex) {
            log.error(ex);
        } finally {
            if ( con != null ) {
                DB.getInstance().closeConnection(con);
            }
        }
        
        return retValue;
    }
    
    public List getList(Class type) {
        List retList = new ArrayList();
        if ( type == null ) {
            return retList;
        }

        if ( type.equals(StudyType.class)) {
            List stList = getValues("select * from tbl_study_type", null);
            int size = stList != null ? stList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)stList.get(i); 
                StudyType st = StudyType.createStudyType(values);
                retList.add(st);
            }
        } else if ( type.equals(Unit.class)) {
            List stList = getValues("select * from tbl_unit", null);
            int size = stList != null ? stList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)stList.get(i); 
                Unit st = Unit.createUnit(values);
                //System.out.println("Adding " + st);
                retList.add(st);
            }
        } else if ( type.equals(Action.class)) {
            List stList = getValues("select * from tbl_action", null);
            int size = stList != null ? stList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)stList.get(i); 
                Action st = Action.createAction(values);
                retList.add(st);
            }
        } else if ( type.equals(UserType.class))  {
            List stList = getValues("select * from tbl_user_type", null);
            int size = stList != null ? stList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)stList.get(i); 
                UserType st = UserType.createUserType(values);
                retList.add(st);
            }
        } else if ( type.equals(People.class)) {
            // Get the people data first
            List peopleList = null;
            
            try {
                peopleList = getWSClient().getPeopleSortedByName();
            } catch(Exception ex) {
                throw new RuntimeException(ex);
            }
            
            // Then get the usertype data and insert into people
            List userList = getValues("select tbl_user.user_type_id, tbl_user.people_id from tbl_user_type, tbl_user where tbl_user.user_type_id=tbl_user_type.user_type_id", null);
            int size = userList != null ? userList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)userList.get(i);
                Object tmpObj = values.get(People.USER_TYPE_ID);
                Object id = values.get(People.PEOPLE_ID_DB);
                Map peopleMap = findMap(peopleList, People.PEOPLE_ID_WS, id);
                if ( peopleMap != null ) {
                    peopleMap.put(People.USER_TYPE_ID, tmpObj);
                }
            }
            
            // Create the People instances
            size = peopleList != null ? peopleList.size() : 0;
            for ( int i = 0;i < size;i++) {
                Map values = (Map)peopleList.get(i); 
                People st = People.createPeople(values);
                retList.add(st);
            }

        } else {
            throw new RuntimeException("Don't know how to get list of values of type " + (type != null ? type.getName() : "null"));
        }

        return retList;
    }

    /** Find a particular map from the list of maps by looking for a value named paramName with the 
     *  input value.
     */
    private Map findMap(List maps, String paramName, Object value) {
        Map retMap = null;
        
        int size = maps != null ? maps.size() : 0;
        for ( int i = 0;i < size;i++) {
            Map tmpMap = (Map)maps.get(i);
            Object tmpValue = tmpMap.get(paramName);
            if ( tmpValue != null && tmpValue.equals(value)) {
                retMap = tmpMap;
            }
        }

        return retMap;
    }
    
    public Object get(int id, Class type) {
        Object retValue = null;
        
        if ( type.equals(StudyType.class)) {
            Map values = getById(id, "select * from tbl_study_type where study_type_id=?");
            StudyType st = StudyType.createStudyType(values);
            retValue = st;
        } else if ( type.equals(Unit.class)) {
            Map values = getById(id, "select * from tbl_unit where unit_id=?");
            Unit st = Unit.createUnit(values);
            retValue = st;
        } else if ( type.equals(Action.class)) {
            Map values = getById(id, "select * from tbl_action where action_id=?");
            Action st = Action.createAction(values);
            retValue = st;
        } else if ( type.equals(UserType.class)) {
            Map values = getById(id, "select * from tbl_user_type where user_type_id=?");
            UserType st = UserType.createUserType(values);
            retValue = st;
        } else {
            throw new RuntimeException("Don't know how to get values of type " + (type != null ? type.getName() : "null"));
        }   

        return retValue;
    }

    public void runUpdateSql(String sql, Object[] params) {
        Connection con = null;
        try {
            con = DB.getInstance().getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            int size = params != null ? params.length : 0;
            for ( int i = 0;i < size;i++) {
                stmt.setObject(i+1, params[i]);
            }
            int retValue = stmt.executeUpdate();
        } catch(Exception ex) {
            log.error(ex);
        } finally {
            if ( con != null ) {
                DB.getInstance().closeConnection(con);
            }
        }
    }

    public List getValues(String sql, Object[] params, Connection con)  {
        List retList = new ArrayList();
        try {
            if ( con == null ) {
               con = DB.getInstance().getConnection();
            }
            PreparedStatement stmt = con.prepareStatement(sql);
            int size = params != null ? params.length : 0;
            for ( int i = 0;i < size;i++) {
                stmt.setObject(i+1, params[i]);
            }
            ResultSet rs = stmt.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int cols = rsmd.getColumnCount();
            while ( rs.next() ) {
                Map tmpMap = new HashMap();
                for ( int j = 1;j <= cols;j++) {
                    String colName = rsmd.getColumnName(j);
                    Object value = rs.getObject(j);
                    tmpMap.put(colName, value);
                }
                retList.add(tmpMap);
            }
        } catch(Exception ex) {
            log.error(ex);
        } finally {
            if ( con != null ) {
                DB.getInstance().closeConnection(con);
            }
        }
        return retList;
    }
    
    public List getValues(String sql, Object[] params)  {
        return getValues(sql, params, null);
    }

}
