package no.machina.simula.engine.wsclient;

import java.net.MalformedURLException;
import java.util.HashMap;


import org.apache.log4j.Logger;

/**
 * Client used to query simula login web service
 * @author kasia
 *
 */
public class LoginWSClient extends WSClient {
	
	private static String CHECK_CREDENTIALS_METHOD = "check_credentials";
	
	private static Logger log = Logger.getLogger(LoginWSClient.class);
	private String WS_URL;

	/*
	 * (non-Javadoc)
	 * @see no.machina.simula.wsutils.WSClient#configure()
	 */
	public void configure() {
		WS_URL = System.getProperty("no.machina.simula.login_ws_url");
		try {
			configure(WS_URL);
			log.info("WS configured : " + WS_URL);
		} catch (MalformedURLException mue) {
			log.error(mue.getMessage());
			WS_URL = null;
		}
	}
	
	/**
	 * Checks whether given credentials allow to authenticate user
	 * @param userId
	 * @param passwd
	 * @return True if the user has been authenticated against simula webservice, otherwise false
	 * @throws Exception
	 */
	public Boolean checkCredentials(String userId, String passwd) throws Exception {
		HashMap params = new HashMap();
		params.put("login", userId);
		params.put("password", passwd);
		
		return queryBooleanMethod("check_credentials", params);
	}
}
