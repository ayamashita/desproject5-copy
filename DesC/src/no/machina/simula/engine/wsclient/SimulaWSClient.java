package no.machina.simula.engine.wsclient;

import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import no.machina.simula.entityBeans.People;
import no.machina.simula.entityBeans.Publication;
import no.machina.simula.entityBeans.Responsible;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 * Client for accessing simula webservice
 * @author kasia
 *
 */
public class SimulaWSClient extends WSClient {
	private static Logger log = Logger.getLogger(SimulaWSClient.class);
	private String WS_URL = null;
	

	/**
	 * Reads standard configuration and configures the client
	 */
	public void configure() {
		WS_URL = System.getProperty("no.machina.simula.simula_ws_url");
		try {
			configure(WS_URL);
			log.info("WS configured : " + WS_URL);
		} catch (MalformedURLException mue) {
			log.error(mue.getMessage());
			WS_URL = null;
		}
	}

	/**
	 * Queries people WS with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object[] queryPeople(HashMap params) throws Exception {
		return queryObjectArrayMethod("getPeople", params);
	}
	
	/**
	 * Queries publication WS with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object[] queryPublication(HashMap params) throws Exception {
		return queryObjectArrayMethod("getPublications", params);
	}

	/**
	 * Searches for people with first name or family name like the given search
	 * string
	 * 
	 * @param searchString
	 * @return The list of ids
	 */
	public List getResponsibleIDs(String searchString) throws Exception {
		// "SELECT people_id FROM people WHERE people_first_name LIKE ? OR
		// people_family_name LIKE ?";

		// prepare first search
		HashMap params1 = new HashMap();
		params1.put("firstname", searchString);

		// prepare second search
		HashMap params2 = new HashMap();
		params2.put("lastname", searchString);

		Object[] resList1 = queryPeople(params1);
		Object[] resList2 = queryPeople(params2);

		HashSet ids = new HashSet();

		// read ids from first result
		for (int i = 0; i < resList1.length; i++) {
			ids.add(((HashMap) resList1[i]).get("id"));
		}

		// read ids from second result
		for (int i = 0; i < resList2.length; i++) {
			ids.add(((HashMap) resList2[i]).get("id"));
		}

		return new ArrayList(ids);
	}

	/**
	 * Searches for people with the given id string
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public People getPeopleById(String id) throws Exception {

		HashMap params = new HashMap();
		params.put("id", id);

		Object[] resTable = queryPeople(params);

		People people = null;
		for (int i = 0; i < resTable.length; i++) {
			people = new People();

			HashMap peopleMap = (HashMap) resTable[i];
			people.setPeopleId((String) peopleMap.get("id"));
			people.setPeopleFirstName((String) peopleMap.get("firstname"));
			people.setPeopleFamilyName((String) peopleMap.get("lastname"));
			people.setPeoplePosition((String) peopleMap.get("jobtitle"));
		}

		return people;
	}

	/**
	 * Returns all possible responsibles
	 * 
	 * @return A list of Responsible objects
	 * @throws Exception
	 */
	public List getAllResponsibles() throws Exception {
		HashMap params = new HashMap();
		params.put("sort_on", "lastname");
		Object[] result = queryPeople(params);

		List responsibles = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			responsibles.add(createResponsible((HashMap) result[i]));
		}
		return responsibles;
	}

	/**
	 * Searches for and creates a list of responsibles with given ids
	 * @param respIds A list of String ids to search for
	 * @return A list of Responsible objects with ids matching the given ids
	 * @throws Exception
	 */
	public List getResponsiblesByIds(List respIds) throws Exception {
		Iterator respIter = respIds.iterator();

		List responsibles = new ArrayList();
		while (respIter.hasNext()) {
			Responsible resp = getResponsibleById((String) respIter.next());
			if (resp != null) {
				responsibles.add(resp);
			}
		}

		return responsibles;
	}

	/**
	 * Queries webservice and creates responsible with the given id string
	 * @param peopleId The string containing id
	 * @return A Responsible object with the given id
	 * @throws Exception
	 */
	public Responsible getResponsibleById(String peopleId) throws Exception {
		HashMap params = new HashMap();
		params.put("id", peopleId);

		Object[] resTable = queryPeople(params);

		if (resTable == null || resTable.length == 0) {
			return null;
		}

		return createResponsible((HashMap) resTable[0]);
	}

	/**
	 * Creates Responsible object with the values delivered by webservice
	 * @param peopleMap A HashMap delivered by webservice
	 * @return
	 */
	protected Responsible createResponsible(HashMap peopleMap) {
		Responsible resp = new Responsible();
		resp.setPeopleId((String) peopleMap.get("id"));
		resp.setFirstName((String) peopleMap.get("firstname"));
		resp.setFamilyName((String) peopleMap.get("lastname"));
		resp.setUrl((String) peopleMap.get("url"));
		return resp;
	}

	/**
	 * Queries web service for a list of people sorted by name
	 * @return A list of People object containing all people data delivered by web service
	 * @throws Exception
	 */
	public List getPeopleSortedByName() throws Exception {
		HashMap params = new HashMap();
		params.put("sort_on", "lastname");
		Object[] result = queryPeople(params);

		List people = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			people.add((HashMap) result[i]);
		}
		return people;
	}

	/**
	 * Creates People object with the values delivered by web service
	 * @param peopleMap A HashMap delivered by web service
	 * @return
	 */
	protected People createPeople(HashMap peopleMap) {
		People people = new People();
		people.setPeopleId((String) peopleMap.get("id"));
		people.setPeopleFirstName((String) peopleMap.get("firstname"));
		people.setPeopleFamilyName((String) peopleMap.get("lastname"));
		people.setPeoplePosition((String) peopleMap.get("jobtitle"));
		return people;
	}
	
	/**
	 * Creates Publication object with the values delivered by web service
	 * @param peopleMap A HashMap delivered by web service
	 * @return
	 */
	protected Publication createPublication(HashMap pubMap) {
		Publication publication = new Publication();
		publication.setId((String)pubMap.get("id"));
		publication.setTitle((String)pubMap.get("title"));
		publication.setUrl((String)pubMap.get("url"));
		return publication;
	}
	
	/**
	 * Returns all possible publications
	 * 
	 * @return A list of Publication objects
	 * @throws Exception
	 */
	public List getAllPublications() throws Exception {
		Object[] result = queryPublication(new HashMap());

		List publications = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			publications.add(createPublication((HashMap) result[i]));
		}
		return publications;
	}
	
	/**
	 * Returns all publication ids with title matching search string
	 * @param searchString
	 * @return A list of String objects
	 * @throws Exception 
	 */
	public List getPublicationIds(String searchString) throws Exception {
//		  "SELECT publication_id FROM publication WHERE publication_title LIKE ?";	
		HashMap params = new HashMap();
		params.put("title", searchString);
		Object[] result = queryPublication(params);

		List ids = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			ids.add(((HashMap) result[i]).get("id"));
		}
		return ids;
	}
	
	/**
	 * Queries web service for publication data for the given ids
	 * @param pubIds A list of id strings
	 * @return A list of publication objects
	 * @throws Exception
	 */
	public List getPublicationsByIds(List pubIds) throws Exception {
		Iterator respIter = pubIds.iterator();

		List publications = new ArrayList();
		while (respIter.hasNext()) {
			Publication publication = getPublicationById((String) respIter.next());
			if (publication != null) {
				publications.add(publication);
			}
		}

		return publications;
	}

	/**
	 * Queries for a single publication with the given id
	 * @param pubId An id string
	 * @return A single Publication object or null, when such publication does not exist
	 * @throws Exception
	 */
	public Publication getPublicationById(String pubId) throws Exception {
		HashMap params = new HashMap();
		params.put("id", pubId);

		Object[] pubTable = queryPublication(params);

		if (pubTable == null || pubTable.length == 0) {
			return null;
		}

		return createPublication((HashMap) pubTable[0]);
	}

	
}
