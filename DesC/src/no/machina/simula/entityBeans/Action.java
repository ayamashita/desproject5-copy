/*
 * Action.java
 *
 * Created on 10. oktober 2003, 11:15
 */

package no.machina.simula.entityBeans;

import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/** The class representing rows in tbl_action
 *
 * @author  Cato Ervik
 */
public class Action {

    public static String ACTION_NAME="action_name";
    public static String ACTION_DESC="action_desc";
    public static String ACTION_ID="action_id";
    
    /** Holds value of property actionId. */
    private int actionId;
    
    /** Holds value of property actionName. */
    private String actionName;
    
    /** Holds value of property actionDesc. */
    private String actionDesc;
    
    /** Creates a new instance of Action */
    public Action() {
    }
   
    public static Action createAction(Map params) {
        Action act = new Action();
        act.setActionDesc(Converter.getFirstString(params.get(ACTION_DESC)));
        act.setActionName(Converter.getFirstString(params.get(ACTION_NAME)));
        act.setActionId(NumberUtils.stringToInt(Converter.getFirstString(params.get(ACTION_ID)), -1));
        return act;
    }
    
    public String toString() {
        return getActionId() + ", " + getActionName() + ", " + getActionDesc();
    }
    
    public boolean equals(Object obj) {
        boolean retValue = false;
        
        if ( obj instanceof Action ) {
            retValue = ((Action)obj).getActionId() == this.getActionId();
        }

        return retValue;
    }
    
    public boolean equalsName(String name) {
        boolean retValue = false;
        retValue = actionName != null ? actionName.equalsIgnoreCase(name) : false;
        return retValue;
    }
    
    /** Getter for property actionId.
     * @return Value of property actionId.
     *
     */
    public int getActionId() {
        return this.actionId;
    }    
    
    /** Setter for property actionId.
     * @param actionId New value of property actionId.
     *
     */
    public void setActionId(int actionId) {
        this.actionId = actionId;
    }    
    
    /** Getter for property actionName.
     * @return Value of property actionName.
     *
     */
    public String getActionName() {
        return this.actionName;
    }
    
    /** Setter for property actionName.
     * @param actionName New value of property actionName.
     *
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
    
    /** Getter for property actionDesc.
     * @return Value of property actionDesc.
     *
     */
    public String getActionDesc() {
        return this.actionDesc;
    }
    
    /** Setter for property actionDesc.
     * @param actionDesc New value of property actionDesc.
     *
     */
    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }
    
}
