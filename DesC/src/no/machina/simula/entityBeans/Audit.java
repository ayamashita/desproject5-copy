/*
 * Audit.java
 *
 * Created on 15. oktober 2003, 18:30
 */

package no.machina.simula.entityBeans;

import java.util.Date;
import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class Audit {
    
    public static final String AUDIT_ID = "audit_id";
    public static final String ACTION_ID = "action_id";
    public static final String USER_ID = "user_id";
    public static final String AUDIT_DATE = "audit_date";
    public static final String STUDY_ID = "study_id";
    
    /** Holds value of property actionId. */
    private int actionId;
    
    /** Holds value of property studyId. */
    private int studyId;
    
    /** Holds value of property userId. */
    private int userId;

    /** Holds value of property peopleId.**/
    private String peopleId;
    
    /** Holds value of property auditDate. */
    private Date auditDate;
    
    /** Holds value of property auditId. */
    private int auditId;
    
    /** Creates a new instance of Audit */
    public Audit() {
    }
    
    private static String dateFormat = "dd.MM.yyyy";
    public static Audit createAudit(Map params) {
        Audit audit = new Audit();

        audit.setAuditId(NumberUtils.stringToInt(Converter.getFirstString(params.get(AUDIT_ID)), -1));
        //audit.setActionId(NumberUtils.stringToInt(Converter.getFirstString(params.get(ACTION_ID)), -1));
        audit.setStudyId(NumberUtils.stringToInt(Converter.getFirstString(params.get(STUDY_ID)), -1));
        audit.setAuditDate(Converter.stringToDate(Converter.getFirstString(params.get(AUDIT_DATE)), dateFormat));
        audit.setPeopleId((String)params.get(USER_ID));

        return audit;
    }

    public String toString () {
        return "auditid=" + auditId + ", actionid=" + actionId + ", userid=" + userId + ", auditDate=" + auditDate;
    }
    
    /** Getter for property actionId.
     * @return Value of property actionId.
     *
     */
    public int getActionId() {
        return this.actionId;
    }    
    
    /** Setter for property actionId.
     * @param actionId New value of property actionId.
     *
     */
    public void setActionId(int actionId) {
        this.actionId = actionId;
    }    
    
    /** Getter for property studyId.
     * @return Value of property studyId.
     *
     */
    public int getStudyId() {
        return this.studyId;
    }
    
    /** Setter for property studyId.
     * @param studyId New value of property studyId.
     *
     */
    public void setStudyId(int studyId) {
        this.studyId = studyId;
    }
    
    /** Getter for property userId.
     * @return Value of property userId.
     * @deprecated
     */
    public int getUserId() {
        return this.userId;
    }
    
    /** Setter for property userId.
     * @param userId New value of property userId.
     * @deprecated
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    /** Getter for property auditDate.
     * @return Value of property auditDate.
     *
     */
    public Date getAuditDate() {
        return this.auditDate;
    }
    
    /** Setter for property auditDate.
     * @param auditDate New value of property auditDate.
     *
     */
    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }
    
    /** Getter for property auditId.
     * @return Value of property auditId.
     *
     */
    public int getAuditId() {
        return this.auditId;
    }
    
    /** Setter for property auditId.
     * @param auditId New value of property auditId.
     *
     */
    public void setAuditId(int auditId) {
        this.auditId = auditId;
    }

	public String getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
    
}
