package no.machina.simula.entityBeans;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Column {
	
	public final static String COLUMN_TABLE = "tbl_column";
	public final static String COLUMN_ID = "column_id";
	public final static String COLUMN_NAME = "column_name";
	public final static String COLUMN_DISPLAY_NAME = "column_display_name";
	public final static String COLUMN_TYPE = "column_type";
	
	public final static String COLUMN_LAST_EDITED_BY = "last_edited_by";
	public final static String COLUMN_STUDY_PUBLICATIONS = "study_publications";
	public final static String COLUMN_STUDY_RESPONSIBLE = "study_responsible";
	public final static String COLUMN_STUDY_OWNER = "study_owner";
	
	public final static int DEFAULT_COLUMN_TYPE = 1;
	
	private int columnId;
	private String name;
	private String displayName;
	private int type;
	public int getColumnId() {
		return columnId;
	}
	public void setColumnId(int columnId) {
		this.columnId = columnId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

}
