package no.machina.simula.entityBeans;

/**
 *
 * @author  sigmund
 */
public class FileInfo {
    
    private int id;
    
    private String contentType;
    
    private String desc;
    
    private int size;
    
    private String name;
    
    /** Creates a new instance of FileInfo */
    public FileInfo() {
    }
    
    /** Getter for property id.
     * @return Value of property id.
     *
     */
    public int getId() {
        return id;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     *
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /** Getter for property contentType.
     * @return Value of property contentType.
     *
     */
    public java.lang.String getContentType() {
        return contentType;
    }
    
    /** Setter for property contentType.
     * @param contentType New value of property contentType.
     *
     */
    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }
    
    /** Getter for property desc.
     * @return Value of property desc.
     *
     */
    public java.lang.String getDesc() {
        return desc;
    }
    
    /** Setter for property desc.
     * @param desc New value of property desc.
     *
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }
    
    /** Getter for property size.
     * @return Value of property size.
     *
     */
    public int getSize() {
        return size;
    }
    
    /** Setter for property size.
     * @param size New value of property size.
     *
     */
    public void setSize(int size) {
        this.size = size;
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
}
