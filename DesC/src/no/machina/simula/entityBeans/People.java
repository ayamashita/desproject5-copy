/*
 * People.java
 *
 * Created on 14. oktober 2003, 22:10
 */

package no.machina.simula.entityBeans;

import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class People {
    
    public static final String PEOPLE_ID_WS="id";
    public static final String PEOPLE_ID_DB="people_id";
    public static final String PEOPLE_POSITION="jobtitle";
    public static final String PEOPLE_FIRST_NAME="firstname";
    public static final String PEOPLE_FAMILY_NAME="lastname";
    public static final String USER_TYPE_ID="user_type_id";
	private static final String PEOPLE_URL = "url";

    
    /** Holds value of property peopleId. */
    private String peopleId;
    
    /** Holds value of property peoplePosition. */
    // jobtitle from simula WS
    private String peoplePosition;
    
    /** Holds value of property peopleFirstName. */
    private String peopleFirstName;
    
    /** Holds value of property peopleFamilyName. */
    private String peopleFamilyName;
    
    /** Holds value of property userTypeId. */
    //not available in simula WS
    private int userTypeId;
    
    /** Homepage address **/
    private String url;
    
    /** Creates a new instance of People */
    public People() {
    }
    
    public static People createPeople(Map params) {
        People p = new People();
        p.setPeopleId((String)params.get(PEOPLE_ID_WS));
        p.setPeoplePosition(Converter.getFirstString(params.get(PEOPLE_POSITION)));
        p.setPeopleFirstName(Converter.getFirstString(params.get(PEOPLE_FIRST_NAME)));
        p.setPeopleFamilyName(Converter.getFirstString(params.get(PEOPLE_FAMILY_NAME)));
        p.setUserTypeId(NumberUtils.stringToInt(Converter.getFirstString(params.get(USER_TYPE_ID))));
        p.setUrl((String)params.get(PEOPLE_URL));
        return p;
    }
    
    public String toString() {
        return peopleId + ", " + peopleFirstName + " " + peopleFamilyName + ", " + peoplePosition;
    }
    
    public boolean equals(Object obj) {
        boolean retValue = false;

        if ( obj instanceof People ) {
            retValue = ((People)obj).getPeopleId() == getPeopleId();
        }

        return retValue;
    }
    
    /** Getter for property peopleId.
     * @return Value of property peopleId.
     *
     */
    public String getPeopleId() {
        return this.peopleId;
    }
    
    /** Setter for property peopleId.
     * @param peopleId New value of property peopleId.
     *
     */
    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }
    
    /** Getter for property peoplePosition.
     * @return Value of property peoplePosition.
     *
     */
    public String getPeoplePosition() {
        return this.peoplePosition;
    }
    
    /** Setter for property peoplePosition.
     * @param peoplePosition New value of property peoplePosition.
     *
     */
    public void setPeoplePosition(String peoplePosition) {
        this.peoplePosition = peoplePosition;
    }
    
    /** Getter for property peopleFirstName.
     * @return Value of property peopleFirstName.
     *
     */
    public String getPeopleFirstName() {
        return this.peopleFirstName;
    }    

    /** Setter for property peopleFirstName.
     * @param peopleFirstName New value of property peopleFirstName.
     *
     */
    public void setPeopleFirstName(String peopleFirstName) {
        this.peopleFirstName = peopleFirstName;
    }    
    
    /** Getter for property peopleFamilyName.
     * @return Value of property peopleFamilyName.
     *
     */
    public String getPeopleFamilyName() {
        return this.peopleFamilyName;
    }
    
    /** Setter for property peopleFamilyName.
     * @param peopleFamilyName New value of property peopleFamilyName.
     *
     */
    public void setPeopleFamilyName(String peopleFamilyName) {
        this.peopleFamilyName = peopleFamilyName;
    }
    
    /** Getter for property userTypeId.
     * @return Value of property userTypeId.
     *
     */
    public int getUserTypeId() {
        return this.userTypeId;
    }
    
    /** Setter for property userTypeId.
     * @param userTypeId New value of property userTypeId.
     *
     */
    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    /** 
     * Getter for property url
     * @return The value of property url
     */
	public String getUrl() {
		return url;
	}

	/**
	 * Setter for property url
	 * @param url New value of property url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
    
}
