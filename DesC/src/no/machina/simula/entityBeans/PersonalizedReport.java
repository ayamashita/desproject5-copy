package no.machina.simula.entityBeans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class PersonalizedReport {

	public final static String REPORT_TABLE = "tbl_report";
	public final static String REPORT_ID = "report_id";
	public final static String REPORT_NAME = "report_name";
	public final static String PEOPLE_ID = "people_id";

	public final static String REPORT_COLUMN_TABLE = "tbl_report_column";
	public final static String REPORT_RESPONSIBLE_TABLE = "tbl_report_responsible";
	
	public final static String REPORT_COLUMN_ORDER = "report_column_order";
	
	private List columns;
	private List responsibles;
	private ReportSortOrder timeSortOrder;
	private ReportCriteria responsibleCriteria;
	private int id;
	private String name;

	/* people id */
	private String owner;

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
		this.columnNames = null;
	}

	public List getResponsibles() {
		return responsibles;
	}

	public void setResponsibles(List responsibles) {
		this.responsibles = responsibles;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public ReportSortOrder getTimeSortOrder() {
		return timeSortOrder;
	}

	public void setTimeSortOrder(ReportSortOrder timeSortOrder) {
		this.timeSortOrder = timeSortOrder;
	}

	public ReportCriteria getResponsibleCriteria() {
		return responsibleCriteria;
	}

	public void setResponsibleCriteria(ReportCriteria responsibleCriteria) {
		this.responsibleCriteria = responsibleCriteria;
	}
	
	private HashMap columnNames = null;
	
	protected HashMap getColumnNamesMap() {
		if (columnNames == null) {
			columnNames = new HashMap();
			Iterator iter = this.getColumns().iterator();
			while (iter.hasNext()) {
				Column col = (Column)iter.next();
				columnNames.put(col.getName(),col);
			}
		}
		return columnNames;
	}
	
	public boolean containsColumn(String name) {
		return (getColumnNamesMap().keySet().contains(name));
	}
	
	public Column getColumnByName (String name) {
		if (getColumnNamesMap().containsKey(name)) {
			return (Column)getColumnNamesMap().get(name);
		}
		return null;
	}
}
