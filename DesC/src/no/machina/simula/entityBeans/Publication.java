package no.machina.simula.entityBeans;

/*
 * Publication.java
 *
 * Created on 8. oktober 2003, 16:38
 */

/**
 *
 * @author  sigmund
 */
public class Publication {

    public static final int MAX_SHORT_TITLE_LENGTH = 20;
    
    private String id;

    private String title;
    
    private String shortTitle;
    
    private String url;

    /** Creates a new instance of Publication */
    public Publication() {
    }

    /** Getter for property id.
     * @return Value of property id.
     *
     */
    public String getId() {
        return id;
    }

    /** Setter for property id.
     * @param id New value of property id.
     *
     */
    public void setId(String id) {
        this.id = id;
    }

    /** Getter for property title.
     * @return Value of property title.
     *
     */
    public java.lang.String getTitle() {
        return title;
    }

    /** Setter for property title.
     * @param title New value of property title.
     *
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
        setShortTitle(title);
    }
    
    /** Getter for property shortTitle.
     * @return Value of property shortTitle.
     *
     */
    public java.lang.String getShortTitle() {
        return shortTitle;
    }
    
    /** Setter for property shortTitle.
     * @param shortTitle New value of property shortTitle.
     *
     */
    public void setShortTitle(java.lang.String shortTitle) {
        if (shortTitle.length() > MAX_SHORT_TITLE_LENGTH){
            this.shortTitle = shortTitle.substring(0, MAX_SHORT_TITLE_LENGTH) + "...";
        }
        else{
            this.shortTitle = shortTitle;
        }
    }
    
    public String toString(){
		return "Publication - id: " + getId() + " - title: " + getTitle();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
