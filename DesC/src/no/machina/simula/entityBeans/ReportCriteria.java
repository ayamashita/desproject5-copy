package no.machina.simula.entityBeans;

public class ReportCriteria {

	public final static String REPORT_CRITERIA_TABLE = "tbl_report_criteria";
	public final static String REPORT_CRITERIA_ID = "report_criteria_id";
	public final static String REPORT_CRITERIA_NAME = "report_criteria_name";
	
	public final static int REPORT_CRITERIA_OR_ID = 1;
	public final static int REPORT_CRITERIA_AND_ID = 2;

	private int criteriaId;
	private String criteriaName;

	public int getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(int criteriaId) {
		this.criteriaId = criteriaId;
	}

	public String getCriteriaName() {
		return criteriaName;
	}

	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}
}
