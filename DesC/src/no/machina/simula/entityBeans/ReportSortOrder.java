package no.machina.simula.entityBeans;

public class ReportSortOrder {

	public final static String REPORT_SORT_ORDER_TABLE = "tbl_report_sort_order";
	public final static String REPORT_SORT_ORDER_ID = "report_sort_order_id";
	public final static String REPORT_SORT_ORDER_NAME = "report_sort_order_name";
	public final static String REPORT_SORT_ORDER_DESC = "report_sort_order_desc";

	private int sortOrderId;
	private String sortOrderName;
	private String sortOrderDesc;

	public int getSortOrderId() {
		return sortOrderId;
	}

	public void setSortOrderId(int sortOrderId) {
		this.sortOrderId = sortOrderId;
	}

	public String getSortOrderName() {
		return sortOrderName;
	}

	public void setSortOrderName(String sortOrderName) {
		this.sortOrderName = sortOrderName;
	}

	public String getSortOrderDesc() {
		return sortOrderDesc;
	}

	public void setSortOrderDesc(String sortOrderDesc) {
		this.sortOrderDesc = sortOrderDesc;
	}
	
	
}
