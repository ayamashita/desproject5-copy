package no.machina.simula.entityBeans;

import java.util.*;
import java.text.*;

/*
 * Study.java
 *
 */

public class Study {
    
    private int id;

    private String name;

    private String type;
    
    private int typeId = -1; // negative value means "not registered"

    private String desc;

    private int duration = -1; // negative value means "not registered"

    private String unit;
    
    private int unitId = -1; // negative value means "not registered"

    private Date start;

    private Date end;

    private String keywords;

    private int noOfStudents = -1; // negative value means "not registered"

    private int noOfProfessionals = -1; // negative value means "not registered"

    private String studyNotes;

    private List publications = new ArrayList();

    private List responsibles = new ArrayList();
    
    private String owner;
    
    private String lastEditedBy;

    /** Creates a new instance of Study */
    public Study() {
    }

    /** Getter for property id.
     * @return Value of property id.
     *
     */
    public int getId() {
        return id;
    }

    /** Setter for property id.
     * @param id New value of property id.
     *
     */
    public void setId(int id) {
        this.id = id;
    }

    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }

    /** Setter for property name.
     * @param type New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    /** Getter for property type.
     * @return Value of property type.
     *
     */
    public java.lang.String getType() {
        return type;
    }

    /** Setter for property type.
     * @param type New value of property type.
     *
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    /** Getter for property desc.
     * @return Value of property desc.
     *
     */
    public java.lang.String getDesc() {
        return desc;
    }

    /** Setter for property desc.
     * @param desc New value of property desc.
     *
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }

    /** Getter for property duration.
     * @return Value of property duration.
     *
     */
    public int getDuration() {
        return duration;
    }

    /** Setter for property duration.
     * @param duration New value of property duration.
     *
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /** Getter for property unit.
     * @return Value of property unit.
     *
     */
    public java.lang.String getUnit() {
        return unit;
    }

    /** Setter for property unit.
     * @param type New value of property unit.
     *
     */
    public void setUnit(java.lang.String unit) {
        this.unit = unit;
    }
    /** Getter for property start.
     * @return Value of property start.
     *
     */
    public java.util.Date getStart() {
        return start;
    }

    /** Setter for property start.
     * @param start New value of property start.
     *
     */
    public void setStart(java.util.Date start) {
        this.start = start;
    }

    /** Getter for property end.
     * @return Value of property end.
     *
     */
    public java.util.Date getEnd() {
        return end;
    }

    /** Setter for property end.
     * @param end New value of property end.
     *
     */
    public void setEnd(java.util.Date end) {
        this.end = end;
    }

    /** Getter for property keywords.
     * @return Value of property keywords.
     *
     */
    public java.lang.String getKeywords() {
        return keywords;
    }

    /** Setter for property keywords.
     * @param keywords New value of property keywords.
     *
     */
    public void setKeywords(java.lang.String keywords) {
        this.keywords = keywords;
    }

    /** Getter for property noOfStudents.
     * @return Value of property noOfStudents.
     *
     */
    public int getNoOfStudents() {
        return noOfStudents;
    }

    /** Setter for property noOfStudents.
     * @param noOfStudents New value of property noOfStudents.
     *
     */
    public void setNoOfStudents(int noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    /** Getter for property noOfProfessionals.
     * @return Value of property noOfProfessionals.
     *
     */
    public int getNoOfProfessionals() {
        return noOfProfessionals;
    }

    /** Setter for property noOfProfessionals.
     * @param noOfProfessionals New value of property noOfProfessionals.
     *
     */
    public void setNoOfProfessionals(int noOfProfessionals) {
        this.noOfProfessionals = noOfProfessionals;
    }

    /** Getter for property studyNotes.
     * @return Value of property studyNotes.
     *
     */
    public java.lang.String getStudyNotes() {
        return studyNotes;
    }

    /** Setter for property studyNotes.
     * @param studyNotes New value of property studyNotes.
     *
     */
    public void setStudyNotes(java.lang.String studyNotes) {
        this.studyNotes = studyNotes;
    }

    /** Getter for property publications.
     * @return Value of property publications.
     *
     */
    public List getPublications() {
        return this.publications;
    }

    /** Setter for property publications.
     * @param publications New value of property publications.
     *
     */
    public void setPublications(List publications) {
        this.publications = publications;
    }

    /** Getter for property responsibles.
     * @return Value of property responsibles.
     *
     */
    public List getResponsibles() {
        return this.responsibles;
    }

    /** Setter for property responsibles.
     * @param responsibles New value of property responsibles.
     *
     */
    public void setResponsibles(List responsibles) {
        this.responsibles = responsibles;
    }

    /** Getter for property owner.
     * @return Value of property owner.
     *
     */
    public java.lang.String getOwner() {
        return owner;
    }
    
    /** Setter for property owner.
     * @param owner New value of property owner.
     *
     */
    public void setOwner(java.lang.String owner) {
        this.owner = owner;
    }
    
    /** Getter for property lastEditedBy.
     * @return Value of property lastEditedBy.
     *
     */
    public java.lang.String getLastEditedBy() {
        return lastEditedBy;
    }
    
    /** Setter for property lastEditedBy.
     * @param lastEditedBy New value of property lastEditedBy.
     *
     */
    public void setLastEditedBy(java.lang.String lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    /**
     * Gets duration as a String value.
     * @return Duration as a String. Null if duration is negative
     */
    public String getDurationAsString() {
        if (duration < 0) return null;
        else return new Integer(duration).toString();
    }

    /**
     * Gets noOfStudents as a String value.
     * @return noOfStudents as a String. Null if noOfStudents is negative
     */
    public String getNoOfStudentsAsString() {
        if (noOfStudents < 0) return null;
        else return new Integer(noOfStudents).toString();
    }

    /**
     * Gets noOfProfessionals as a String value.
     * @return noOfStudents as a String. Null if noOfProfessionals is negative
     */
    public String getNoOfProfessionalsAsString() {
        if (noOfProfessionals < 0) return null;
        else return new Integer(noOfProfessionals).toString();
    }

    public String getStartAsString(String format){
        if (start == null) {return null;}
        DateFormat df = new SimpleDateFormat(format, new Locale("en", "GB"));
        return df.format(start);
    }
    
    public String getEndAsString(String format){
        if (end == null) {return null;}
        DateFormat df = new SimpleDateFormat(format, new Locale("en", "GB"));
        return df.format(end);
    }

    public String getTypeIdAsString() {
        if (typeId < 0) return null;
        else return new Integer(typeId).toString();
    }
    
    public String getUnitIdAsString() {
        if (unitId < 0) return null;
        else return new Integer(unitId).toString();
    }

    /** Getter for property typeId.
     * @return Value of property typeId.
     *
     */
    public int getTypeId() {
        return typeId;
    }
    
    /** Setter for property typeId.
     * @param typeId New value of property typeId.
     *
     */
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
    
    /** Getter for property unitId.
     * @return Value of property unitId.
     *
     */
    public int getUnitId() {
        return unitId;
    }
    
    /** Setter for property unitId.
     * @param unitId New value of property unitId.
     *
     */
    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }
    
    public HashSet getResponsibleIds(){
        if (responsibles == null) return null;
        HashSet h = new HashSet();
        for (ListIterator i = responsibles.listIterator(); i.hasNext(); ){
            String id = ((Responsible) i.next()).getPeopleId();
            h.add(id);
        }
        return h;
    }
    
    public HashSet getPublicationIds(){
        if (publications == null) return null;
        HashSet h = new HashSet();
        for (ListIterator i = publications.listIterator(); i.hasNext(); ){
        	String id = ((Publication) i.next()).getId();
            h.add(id);
        }
        return h;
    }
}
