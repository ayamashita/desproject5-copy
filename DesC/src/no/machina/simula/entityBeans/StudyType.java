/*
 * StudyType.java
 *
 * Created on 10. oktober 2003, 14:10
 */

package no.machina.simula.entityBeans;

import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class StudyType {
    
    public final static String STUDY_TYPE_ID="study_type_id";
    public final static String STUDY_TYPE_NAME="study_type_name";
    public final static String STUDY_TYPE_DESC="study_type_desc";
    
    /** Holds value of property studyTypeId. */
    private int studyTypeId;
    
    /** Holds value of property studyTypeName. */
    private String studyTypeName;
    
    /** Holds value of property studyTypeDesc. */
    private String studyTypeDesc;
    
    /** Creates a new instance of StudyType */
    public StudyType() {
    }

    public static StudyType createStudyType(Map values) {
        StudyType type = new StudyType();
        type.setStudyTypeId(NumberUtils.stringToInt(Converter.getFirstString(values.get(STUDY_TYPE_ID)), -1));
        type.setStudyTypeName(Converter.getFirstString(values.get(STUDY_TYPE_NAME)));
        type.setStudyTypeDesc(Converter.getFirstString(values.get(STUDY_TYPE_DESC)));
        return type;
    }

    /** Getter for property studyTypeId.
     * @return Value of property studyTypeId.
     *
     */
    public int getStudyTypeId() {
        return this.studyTypeId;
    }
    
    /** Setter for property studyTypeId.
     * @param studyTypeId New value of property studyTypeId.
     *
     */
    public void setStudyTypeId(int studyTypeId) {
        this.studyTypeId = studyTypeId;
    }
    
    /** Getter for property studyTypeName.
     * @return Value of property studyTypeName.
     *
     */
    public String getStudyTypeName() {
        return this.studyTypeName;
    }
    
    /** Setter for property studyTypeName.
     * @param studyTypeName New value of property studyTypeName.
     *
     */
    public void setStudyTypeName(String studyTypeName) {
        this.studyTypeName = studyTypeName;
    }
    
    /** Getter for property studyTypeDesc.
     * @return Value of property studyTypeDesc.
     *
     */
    public String getStudyTypeDesc() {
        return this.studyTypeDesc;
    }
    
    /** Setter for property studyTypeDesc.
     * @param studyTypeDesc New value of property studyTypeDesc.
     *
     */
    public void setStudyTypeDesc(String studyTypeDesc) {
        this.studyTypeDesc = studyTypeDesc;
    }

    public String toString() {
        return studyTypeId + ", " + studyTypeName + ", " + studyTypeDesc;
    }

}
