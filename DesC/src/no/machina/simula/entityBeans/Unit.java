/*
 * Unit.java
 *
 * Created on 10. oktober 2003, 14:15
 */

package no.machina.simula.entityBeans;

import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class Unit {
    
    public static String UNIT_ID="unit_id";
    public static String UNIT_NAME="unit_name";
    
    /** Holds value of property unitId. */
    private int unitId;
    
    /** Holds value of property unitName. */
    private String unitName;
    
    /** Creates a new instance of Unit */
    public Unit() {
    }
    
    public static Unit createUnit(Map params) {
        Unit retUnit = new Unit();
        retUnit.setUnitId(NumberUtils.stringToInt(Converter.getFirstString(params.get(UNIT_ID)), -1));
        retUnit.setUnitName(Converter.getFirstString(params.get(UNIT_NAME)));
        return retUnit;
    }
    
    public String toString() {
        return this.getUnitId() + ", " + this.getUnitName();
    }

    /** Getter for property unitId.
     * @return Value of property unitId.
     *
     */
    public int getUnitId() {
        return this.unitId;
    }
    
    /** Setter for property unitId.
     * @param unitId New value of property unitId.
     *
     */
    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }
    
    /** Getter for property unitName.
     * @return Value of property unitName.
     *
     */
    public String getUnitName() {
        return this.unitName;
    }
    
    /** Setter for property unitName.
     * @param unitName New value of property unitName.
     *
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
    
}
