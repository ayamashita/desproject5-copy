package no.machina.simula.entityBeans;

/**
 * Represents an URL.
 * @author  sigmund
 */
public class Url {
    
    private String url;
    
    private String desc;
    
    /** Creates a new instance of URL */
    public Url(String url, String desc) {
        this.url = url;
        this.desc = desc;
    }
    
    public String getUrl() {
        return url;
    }
    
    public String getDesc() {
        return desc;
    }
    
}
