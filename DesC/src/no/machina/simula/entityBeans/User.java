/*
 * User.java
 *
 * Created on 10. oktober 2003, 11:15
 */

package no.machina.simula.entityBeans;

/**
 *
 * @author  Cato Ervik
 */
public class User {
    
    public final static String STUDY_ADMIN = "STUDY_ADMINISTRATOR";
    public final static String DATABASE_ADMIN = "DATABASE_ADMINISTRATOR";
    
    /** Holds value of property userName. */
    private String userName;
    
    /** Holds value of property userId. */
    private String userId;
    
    /** Holds value of property password. */
    private String password;
    
    /** Holds value of property userType. */
    private String userType;
    
    /** Creates a new instance of User */
    public User() {
    }
    
    /** Getter for property userName.
     * @return Value of property userName.
     *
     */
    public String getUserName() {
        return this.userName;
    }    
    
    /** Setter for property userName.
     * @param userName New value of property userName.
     *
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }    
    
    /** Getter for property userId.
     * @return Value of property userId.
     *
     */
    public String getUserId() {
        return this.userId;
    }
    
    /** Setter for property userId.
     * @param userId New value of property userId.
     *
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    /** Getter for property password.
     * @return Value of property password.
     *
     */
    public String getPassword() {
        return this.password;
    }
    
    /** Setter for property password.
     * @param password New value of property password.
     *
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /** Getter for property userType.
     * @return Value of property userType.
     *
     */
    public String getUserType() {
        return this.userType;
    }
    
    /** Setter for property userType.
     * @param userType New value of property userType.
     *
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

}
