/*
 * UserType.java
 *
 * Created on 14. oktober 2003, 19:36
 */

package no.machina.simula.entityBeans;

import java.util.Map;

import no.machina.simula.engine.Converter;

import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class UserType {
    
    public static final String USER_TYPE_ID="user_type_id";
    public static final String USER_TYPE_NAME="user_type_name";
    public static final String USER_TYPE_DESC="user_type_desc";
    
    /** Holds value of property userTypeId. */
    private int userTypeId;
    
    /** Holds value of property userTypeName. */
    private String userTypeName;
    
    /** Holds value of property userTypeDesc. */
    private String userTypeDesc;
    
    /** Creates a new instance of UserType */
    public UserType() {
    }
    
    public static UserType createUserType(Map values) {
        UserType retType = new UserType();
        retType.setUserTypeId(NumberUtils.stringToInt(Converter.getFirstString(values.get(USER_TYPE_ID)), -1));
        retType.setUserTypeName(Converter.getFirstString(values.get(USER_TYPE_NAME)));
        retType.setUserTypeDesc(Converter.getFirstString(values.get(USER_TYPE_DESC)));
        return retType;
    }
    
    /** Getter for property userId.
     * @return Value of property userId.
     *
     */
    public int getUserTypeId() {
        return this.userTypeId;
    }
    
    /** Setter for property userId.
     * @param userId New value of property userId.
     *
     */
    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }
    
    /** Getter for property userName.
     * @return Value of property userName.
     *
     */
    public String getUserTypeName() {
        return this.userTypeName;
    }
    
    /** Setter for property userName.
     * @param userName New value of property userName.
     *
     */
    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }
    
    /** Getter for property userDesc.
     * @return Value of property userDesc.
     *
     */
    public String getUserTypeDesc() {
        return this.userTypeDesc;
    }
    
    /** Setter for property userDesc.
     * @param userDesc New value of property userDesc.
     *
     */
    public void setUserTypeDesc(String userTypeDesc) {
        this.userTypeDesc = userTypeDesc;
    }
    
}
