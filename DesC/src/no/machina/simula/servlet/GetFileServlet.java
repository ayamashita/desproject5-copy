package no.machina.simula.servlet;

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import no.machina.simula.*;
import no.machina.simula.engine.dao.DB;
import no.machina.simula.entityBeans.FileInfo;
/**
 *
 * @author  sigmund
 * @version
 */
public class GetFileServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
    throws ServletException, IOException {
        String bufferSizeParam = getServletConfig().getInitParameter("BufferSizeInKb");
        String idParam = request.getParameter("file_id");
        int bufferSizeInKb = 0;
        int id = 0;
        
        try{
            bufferSizeInKb = Integer.parseInt(bufferSizeParam);
            id = Integer.parseInt(idParam);
        }
        catch(NumberFormatException nfe){
            return;
        }
        
        try{
            DB db = DB.getInstance();
            FileInfo fi = db.getFileInfo(id);

            response.setContentType(fi.getContentType());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fi.getName() + "\"");

            ServletOutputStream out = response.getOutputStream();
            BufferedInputStream in = new BufferedInputStream(db.getFile(id));
            byte[] buf = new byte[bufferSizeInKb * 1024]; // buffer
            int len;
            while ((len = in.read(buf, 0, buf.length)) != 1){
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        catch(SQLException sqle){
        }
        catch(Exception e){
        }
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
