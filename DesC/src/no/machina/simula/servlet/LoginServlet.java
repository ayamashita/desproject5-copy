package no.machina.simula.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import no.machina.simula.engine.wsclient.LoginWSClient;
import no.machina.simula.engine.wsclient.SimulaWSClient;
import no.machina.simula.entityBeans.People;

import org.apache.log4j.Logger;

/**
 * Servlet providing authentication with the use of simula login web service
 * @author kasia
 *
 */
public class LoginServlet extends HttpServlet {
	
	public static boolean TEST_MODE = false;
	public static String USER_ID_ATTR = "login_user_id";
	public static String PASSWD_ATTR = "login_user_pass";

	private static final long serialVersionUID = -3195849592414701129L;
	private static Logger log = Logger.getLogger(LoginServlet.class);
	private static LoginWSClient wsClient = new LoginWSClient();
	private static SimulaWSClient simulaClient = new SimulaWSClient();

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	/**
	 * Processes request for both GET and POST methods
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		log.info("start authentication");
		Boolean authenticated = null;
		String userId = null;
		String passwd = null;

		if (TEST_MODE) {
			HttpSession session = request.getSession();
			session.setAttribute("simula_userid", "aiko");
			session.setAttribute("status", "OK");
			session.setAttribute("simula_username", "Aiko Fallas Yamashita");
			
			// send redirect to main admin page
			response.sendRedirect(request.getContextPath() + "/jsp/admin/main.jsp");
			return;
		}
		
		userId = request.getParameter(USER_ID_ATTR);
		passwd = request.getParameter(PASSWD_ATTR);
		
		if (userId == null || passwd == null || userId.length() == 0 || passwd.length() == 0) {
			log.info("Incomplete authentication data. Redirecting back to login page.");
			response.sendRedirect(request.getContextPath() + "/jsp/login.jsp?login_error=true");
			return;
		}
				
		try {
			authenticated = wsClient.checkCredentials(userId, passwd);
		} catch (Exception e) {
			log.error("Error occured: " + e.getMessage());
			log.error(e);
			e.printStackTrace();
			throw new ServletException(e);
		}
		
		if (authenticated != null && authenticated.booleanValue()) {
			
			String username = null;
			
			People user = null;
			try {
				user = simulaClient.getPeopleById(userId);
			} catch (Exception e) {
				log.error("Error occured: " + e.getMessage());
				log.error(e);
				e.printStackTrace();
				throw new ServletException(e);
			}
			
			username = user.getPeopleFirstName() + " " + user.getPeopleFamilyName();
			
			HttpSession session = request.getSession();
			session.setAttribute("simula_userid", userId);
			session.setAttribute("status", "OK");
			session.setAttribute("simula_username", username);
			
			// send redirect to main admin page
			response.sendRedirect(request.getContextPath() + "/jsp/admin/main.jsp");
		}
		else {
			// login failed, redirect back to login page
			log.info("Login failed. Redirecting back to login page.");
			response.sendRedirect(request.getContextPath() + "/jsp/login.jsp?login_error=true");
		}
		
	}

}
