package no.machina.simula.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import no.machina.simula.engine.PersonalizedReportUtils;
import no.machina.simula.engine.dao.DB;
import no.machina.simula.entityBeans.PersonalizedReport;
import no.machina.simula.entityBeans.Study;

import org.apache.log4j.Logger;

public class PersonalizedReportServlet extends HttpServlet {

	private static final String REPORT_ID = "report_id";
	private static final String SIMULA_USER_ID = "simula_userid";
	private static final String REPORT_COLUMNS = "report_columns";
	private static final String REPORT_RESPONSIBLE = "report_responsible";
	private static final String REPORT_RESP_CRITERIA = "report_resp_criteria";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String REPORT_TIME_SORT = "report_time_sort";
	private static final String REPORT_NAME = "report_name";
	private static final String REPORT_ACTION = "report_action";
	private static Logger log = Logger.getLogger(PersonalizedReportServlet.class);

	private PersonalizedReportUtils utils = PersonalizedReportUtils.getInstance();

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		

		HttpSession session = request.getSession();
		String reportAction = request.getParameter(REPORT_ACTION);
		log.info("Processing request..." + reportAction);

		String fatalError = null;
		String redirectURL = "/jsp/admin/report_edit.jsp";

		log.info(reportAction);

		if ("insert".equalsIgnoreCase(reportAction)) {
			try {
				PersonalizedReport pr = processInsert(request, response);
				request.getSession().setAttribute("report_cached_object", pr);
				if (pr != null) {
					redirectURL += "?action=edit&report_id=" + pr.getId();
				} else {
					redirectURL += "?action=insert";
				}
			} catch (SQLException e) {
				fatalError = handleSQLError(e);
			} catch (Exception e) {
				fatalError = handleError(e);
			}
		} else if ("update".equalsIgnoreCase(reportAction)) {
			try {
				PersonalizedReport pr = processUpdate(request, response);
				request.getSession().setAttribute("report_cached_object", pr);
				if (pr!= null) {
					redirectURL += "?action=edit&report_id=" + pr.getId();
				} else {
					redirectURL += "?action=edit";
				}
			} catch (SQLException e) {
				fatalError = handleSQLError(e);
			} catch (Exception e) {
				fatalError = handleError(e);
			}
		} else if ("query".equalsIgnoreCase(reportAction)) {
			try {
				processQuery(request, response);
				redirectURL = "/jsp/admin/list_studies.jsp";
			} catch (SQLException e) {
				fatalError = handleSQLError(e);
			} catch (Exception e) {
				fatalError = handleError(e);
			}
		} else if ("list".equalsIgnoreCase(reportAction)) {
			// search for reports for current user
			try {
				List reports = processList(request, response);
				request.getSession().setAttribute("report_definition_list", reports);
				redirectURL = "/jsp/admin/report_list.jsp";
			} catch (SQLException e) {
				fatalError = handleSQLError(e);
			} catch (Exception e) {
				fatalError = handleError(e);
			}
		} else if ("delete".equalsIgnoreCase(reportAction)) {
			try{
				processDelete(request, response);
			} catch (SQLException e) {
				fatalError = handleSQLError(e);
			} catch (Exception e) {
				fatalError = handleError(e);
			}
			redirectURL = "/PersonalizedReport?report_action=list";
		}

		if (fatalError != null && fatalError.length() != 0) {
			session.setAttribute("report_fatal_error", fatalError);
		}
		
		response.sendRedirect(request.getContextPath() + redirectURL);
	}

	private void processDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String reportId = request.getParameter(REPORT_ID);
		try {
			utils.deleteReport(reportId);
			request.getSession().setAttribute("report_delete_message", "Report successfully deleted!");
		}	catch (Exception e) {
			log.error("Error while deleting report");
			throw e;
		}
	}

	private String handleError(Exception e) {
		String fatalError;
		log.error("Error occured: " + e);
		log.error("Error message: " + e.getMessage());
		e.printStackTrace();
		fatalError = "ERROR: " + e + ". Message: " + e.getMessage();
		return fatalError;
	}

	private String handleSQLError(SQLException e) {
		String fatalError;
		log.error("SQL error occured: " + e);
		log.error("SQL error message: " + e.getMessage());
		e.printStackTrace();
		fatalError = "SQL ERROR: " + e + ". Message: " + e.getMessage();
		return fatalError;
	}

	private PersonalizedReport processUpdate(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		log.debug("Update report action");

		String reportIdStr = request.getParameter(REPORT_ID);
		String reportName = request.getParameter(REPORT_NAME);
		String timeSorting = request.getParameter(REPORT_TIME_SORT);
		String responsibleCriteria = request.getParameter(REPORT_RESP_CRITERIA);
		String[] reportColumnNames = request.getParameterValues(REPORT_COLUMNS);
		String[] reportResponsibleIds = request.getParameterValues(REPORT_RESPONSIBLE);
		String reportOwner = (String) request.getSession().getAttribute(SIMULA_USER_ID);

		int reportId = -1;
		PersonalizedReport pr = null;
		try {
			try {
				reportId = Integer.parseInt(reportIdStr);
			} catch (NumberFormatException nfe) {
				throw new ServletException("report_id must be a number!", nfe);
			}
			List validationErrors = utils.validateReportData(reportName, reportOwner, reportColumnNames, timeSorting,
					reportResponsibleIds, responsibleCriteria);
	
			if (validationErrors != null && validationErrors.size() > 0) {
				log.info("Report data invalid!");
				request.getSession().setAttribute("report_validation_errors", validationErrors);
				putRequestParamsIntoSession(request.getSession(), reportIdStr, reportName, reportOwner, reportColumnNames, timeSorting,
						reportResponsibleIds, responsibleCriteria);
				return null;
			}
	
			pr = utils.insertOrUpdateReport(reportId, reportName, reportOwner, reportColumnNames,
					timeSorting, reportResponsibleIds, responsibleCriteria);
		} catch (Exception e) {
			putRequestParamsIntoSession(request.getSession(), reportIdStr, reportName, reportOwner, reportColumnNames, timeSorting,
					reportResponsibleIds, responsibleCriteria);
			throw e;
		}


		return pr;
	}

	private PersonalizedReport processInsert(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, Exception {
		log.debug("Insert report action");

		String reportName = request.getParameter(REPORT_NAME);
		String timeSorting = request.getParameter(REPORT_TIME_SORT);
		String responsibleCriteria = request.getParameter(REPORT_RESP_CRITERIA);
		String[] reportColumnNames = request.getParameterValues(REPORT_COLUMNS);
		String[] reportResponsibleIds = request.getParameterValues(REPORT_RESPONSIBLE);
		String reportOwner = (String) request.getSession().getAttribute(SIMULA_USER_ID);

		PersonalizedReport pr;
		try {
			List validationErrors = utils.validateReportData(reportName, reportOwner, reportColumnNames, timeSorting,
					reportResponsibleIds, responsibleCriteria);

			if (validationErrors != null && validationErrors.size() > 0) {
				log.info("Report data invalid!");
				request.getSession().setAttribute("report_validation_errors", validationErrors);
				putRequestParamsIntoSession(request.getSession(), null, reportName, reportOwner, reportColumnNames, timeSorting,
						reportResponsibleIds, responsibleCriteria);
				return null;
			}

			pr = utils.insertOrUpdateReport(-1, reportName, reportOwner, reportColumnNames, timeSorting,
					reportResponsibleIds, responsibleCriteria);
		} catch (Exception e) {
			putRequestParamsIntoSession(request.getSession(), null, reportName, reportOwner, reportColumnNames, timeSorting,
					reportResponsibleIds, responsibleCriteria);
			throw e;
		}

		return pr;
	}
	
	private List processQuery(HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception {
		DB d = DB.getInstance();
		String reportId = request.getParameter(REPORT_ID);
	
		PersonalizedReport pr = utils.getReport(reportId);
		request.getSession().setAttribute("report_query_definition", pr);
		
		List result = d.queryReport(pr);
		request.getSession().setAttribute("report_query_result", result);
		
		return result;
	}
	
	private List processList(HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception {
		String userId = (String)request.getSession().getAttribute("simula_userid");
		
		List reports = utils.getReportsForUser(userId);
		
		return reports;
	}

	private void putRequestParamsIntoSession(HttpSession session, String reportId, String reportName, String reportOwner,
			String[] reportColumnNames, String timeSorting, String[] reportResponsibleIds, String responsibleCriteria) {
		session.setAttribute("error_" + REPORT_NAME, reportName);
		session.setAttribute("error_" + REPORT_COLUMNS, reportColumnNames);
		session.setAttribute("error_" + REPORT_RESP_CRITERIA, responsibleCriteria);
		session.setAttribute("error_" + REPORT_RESPONSIBLE, reportResponsibleIds);
		session.setAttribute("error_" + REPORT_TIME_SORT, timeSorting);
		if (reportId != null && reportId.length()>0) {
			session.setAttribute("error_" + REPORT_ID, reportId);
		}
	}
}
