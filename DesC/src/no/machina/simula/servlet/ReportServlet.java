/*
 * ReportServlet.java
 *
 * Created on 6. oktober 2003, 18:19
 */

package no.machina.simula.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import no.machina.simula.engine.Converter;
import no.machina.simula.engine.PersonalizedReportUtils;
import no.machina.simula.engine.dao.DB;
import no.machina.simula.entityBeans.Publication;
import no.machina.simula.entityBeans.Responsible;
import no.machina.simula.entityBeans.Study;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.fop.apps.Driver;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.xml.sax.InputSource;

/**
 *
 * @author  Cato Ervik
 * @version
 */
public class ReportServlet extends HttpServlet {
    
	private static final String REPORT_PDF = "report_pdf";
	private static Logger log = Logger.getLogger(ReportServlet.class);
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String path = this.getServletConfig().getServletContext().getRealPath("xsl");
        String format = request.getParameter("format");
        String dateFormat = (String)getServletContext().getInitParameter("DateFormat");
        
        // Copy all the parameters to a HashMap to avoid the locked status
        Map paramMap = request.getParameterMap();
        Map newMap = new HashMap();
        Iterator it = paramMap.keySet().iterator();
        while ( it.hasNext() ) {
            String key = (String)it.next();
            newMap.put(key, paramMap.get(key));
        }
        paramMap = newMap;
        
        // Insert the dateformat and outputformat for the report
        paramMap.put("dateformat", dateFormat);
        paramMap.put("output-format", format);
        
        //Get and insert the sorting and searching parameters
        String sortByParam = request.getParameter("sort_by");
        String sortOrderParam = request.getParameter("sort_order");
        String searchString = request.getParameter("free_text_search");
        paramMap.put("sort_by", sortByParam);
        paramMap.put("sort_order", sortOrderParam);
        paramMap.put("free_text_search", searchString);
        
              
        // Start creating the report
        String contentStr = null;
        byte[] content = null;
        try {
            // If the format is pdf, create the xml document first, then format into pdf
            if ( "pdf".equalsIgnoreCase(format)) {
                Document doc = createDoc(paramMap);
                content = createPDF(doc, path, paramMap);
            } // If the format is csv, create the csv format directly
            else if ( "csv".equalsIgnoreCase(format)) {
                contentStr = createCSV(paramMap);
            } // Redirect the user to an error page if the format is unknown
            else if (REPORT_PDF.equalsIgnoreCase(format)){
            	content = createPdfForRaport(paramMap,path);
        	}
            else {
                response.sendRedirect("jsp/error.jsp?error=1");
            }
        } catch(Exception ex) {
            log.error(ex.getClass().getName() + ": " + ex.getMessage());
            log.error(ExceptionUtils.getFullStackTrace(ex));
            Throwable root = ExceptionUtils.getRootCause(ex);
            String stack = ExceptionUtils.getFullStackTrace(root);
            if ( root != null ) {
                log.error(root.getClass().getName() + ": " + root.getMessage());
                log.error(stack);
            }
            HttpSession sess = request.getSession();
            if ( sess != null ) {
                sess.setAttribute("error.exception", ex);
            }
            response.sendRedirect("jsp/error.jsp?error=2");
            
            //request.getSession().setAttribute("error.exception", ex);
        }
        
        // Finally, return the resulting reports either as binary pdf or a csv string
        if ( "pdf".equalsIgnoreCase(format) && content != null) {
            response.setContentLength(content.length);
            response.setContentType("application/pdf");
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        }
        else if ( REPORT_PDF.equalsIgnoreCase(format) && content != null) {
            response.setContentLength(content.length);
            response.setContentType("application/pdf");
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        } 
        else if ( "csv".equalsIgnoreCase(format) && contentStr != null) {
            byte[] csvAsBytes = contentStr.getBytes();
            response.setContentType("text/csv");
            response.setContentLength(contentStr.length());
            response.getWriter().write(contentStr);
        }
    }
    
    private byte[] createPdfForRaport(Map paramMap, String path) throws Exception {
    	 Document doc = createDocFromReportResults(paramMap);
         byte[] content = createPDF(doc, path, paramMap);
         return content;
	}

	private static String[] csvHeaders;
    
    public static String createCSV(Map params) throws Exception {
        String retStr = null;
        
        // Get the dateformat that will be used to format start and end dates
        String dateFormat = (String)params.get("dateformat");
        if ( dateFormat == null || dateFormat.trim().length() == 0 ) {
            dateFormat = "yyyy-MM-dd";
        }
        
        // Get all the searching and sorting parameters and convert to a suitable format
        StringBuffer sb = new StringBuffer();
        Date earlyDate = Converter.stringToDate(Converter.getFirstString(params.get("early_end_date")), dateFormat);
        Date lateDate = Converter.stringToDate(Converter.getFirstString(params.get("late_end_date")), dateFormat);
        int studyType = NumberUtils.stringToInt(Converter.getFirstString(params.get("study_type_id")));
        String[] studyreps = (String[])params.get("study_responsibles");
       
        int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
        int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
        String sortByParam = (String)params.get("sort_by");
        String sortOrderParam = (String)params.get("sort_order");
        try{ sortBy = Integer.parseInt(sortByParam); }
        catch (NumberFormatException nfe){}
        try{ sortOrder = Integer.parseInt(sortOrderParam); }
        catch (NumberFormatException nfe){}
        String freeTextSearch = (String)params.get("free_text_search");
        
        // Get the list of studies, either using the freetext search or using studytype, dates etc.
        List studyList = null;
        if ( freeTextSearch == null || freeTextSearch.trim().length() == 0) {
            studyList = DB.getInstance().getStudies(studyType, earlyDate, lateDate, studyreps,sortBy,sortOrder);
        } else {
            studyList = DB.getInstance().getStudies(freeTextSearch,sortBy,sortOrder);
        }
        int size = -1;
        
        // // Add headers the headers for the csv report
        csvHeaders = new String[] {"Study Name", "Study type", "Desc", "Duration", "Unit", "Start",
        "End", "Keywords", "NoStudents", "NoProfs", "Notes", "Owner", "Last Edited By"};
        size = csvHeaders.length;
        for ( int i = 0;i < size;i++) {
            addToBuffer(csvHeaders[i], sb);
        }
        sb.append("\n");

        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        
        size = studyList != null ? studyList.size() : 0;
        for ( int i = 0;i < size;i++) {
            Study tmpStudy = (Study)studyList.get(i);
            StringBuffer studyBuffer = new StringBuffer();
            addToBuffer(tmpStudy.getName(), studyBuffer);
            addToBuffer(DB.getInstance().getStudyTypeName(tmpStudy.getTypeId()), studyBuffer);
            addToBuffer(tmpStudy.getDesc(), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getDuration()), studyBuffer);
            addToBuffer(tmpStudy.getUnit(), studyBuffer);
            addToBuffer(tmpStudy.getStart() != null ? df.format(tmpStudy.getStart()) :  "", studyBuffer);
            addToBuffer(tmpStudy.getEnd() != null ? df.format(tmpStudy.getEnd()) :  "", studyBuffer);
            addToBuffer(tmpStudy.getKeywords(), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getNoOfStudents()), studyBuffer);
            addToBuffer(new Integer(tmpStudy.getNoOfProfessionals()), studyBuffer);
            addToBuffer(tmpStudy.getStudyNotes(), studyBuffer);
            addToBuffer(tmpStudy.getOwner(), studyBuffer);
            addToBuffer(tmpStudy.getLastEditedBy(), studyBuffer);
            
            // Creates the string for the the basic elements for a study
            String currentStr = studyBuffer.toString();
            
            // Then get the publications and responsibles and output
//            List respsList = tmpStudy.getResponsibles();
//            int respSize = respsList!= null ? respsList.size() : 0;
//            List pubList = tmpStudy.getPublications();
//            int pubSize = pubList != null ? pubList.size() : 0;
//            for ( int j = 0;j < pubSize;j++) {
//                Publication pub = (Publication)pubList.get(j);
//                sb.append(currentStr);
//                addToBuffer(pub.getTitle(), sb);
//                addToBuffer(pub.getShortTitle(), sb);
//                if ( respSize > j ) {
//                    Responsible resp = (Responsible)respsList.get(j);
//                    addToBuffer(resp.getFirstName() + " " + resp.getFamilyName(), sb);
//                    //addToBuffer(resp.getFamilyName(), sb);
//                } else {
//                    addToBuffer("", sb);
//                    //addToBuffer("", sb);
//                }
//                sb.append("\n");
//            }
            
//            for ( int j = pubSize;j < respSize;j++) {
//                Responsible resp = (Responsible)respsList.get(j);
//                sb.append(currentStr);
//                addToBuffer("", sb);
//                addToBuffer("", sb);
//                addToBuffer(resp.getFirstName(), sb);
//                addToBuffer(resp.getFamilyName(), sb);
//                sb.append("\n");
//            }
            
            sb.append(currentStr);
            sb.append("\n");
        }
        
        retStr = sb.toString();
        return retStr;
    }
    
    private static char separator = ';';
    
    private static void addToBuffer(Object obj, StringBuffer sb) {
        int lineIndex = sb.length() - sb.lastIndexOf("\n");
        
        if ( lineIndex > 1 ) {
            sb.append(separator);
        }
        // Replace negative numbers
        if ( obj instanceof Integer && ((Integer)obj).intValue() < 0 ) {
            obj = new Integer(0);
        }
        
        // Search and replace special characters
        String candString = obj != null ? obj.toString() : "";

        StringUtils.replace(candString, "\"", "\"\"");
        //StringUtils.replace(candString, "\n", " ");
        // Cut the string to 100 chars
        if ( candString != null && candString.length() > 100 ) {
            candString = candString.substring(0,100);
        }

        if ( candString.indexOf(',') != -1 || candString.indexOf('\n') != -1 || candString.indexOf(';') != -1) {
            candString = "\"" + candString + "\"";
        }
        //log.error("Appending: " + candString);
        sb.append(candString);
    }
    
    public static Document createDocFromReportResults(Map params) throws Exception {
              
    	String reportId = ((String[])params.get("report_id"))[0];
        List studyList = PersonalizedReportUtils.getInstance().queryReport(reportId);
        Document doc = createXmlDocFromStudyList(studyList);
        
        return doc;
    }
    
    public static Document createDoc(Map params) throws Exception {
               
        String dateFormat = (String)params.get("dateformat");
        if ( dateFormat == null || dateFormat.trim().length() == 0 ) {
            dateFormat = "yyyy-MM-dd";
        }
        
        Date earlyDate = Converter.stringToDate(Converter.getFirstString(params.get("early_end_date")), dateFormat);
        Date lateDate = Converter.stringToDate(Converter.getFirstString(params.get("late_end_date")), dateFormat);
        int studyType = NumberUtils.stringToInt(Converter.getFirstString(params.get("study_type_id")));
        String[] studyreps = (String[])params.get("study_responsibles");
        
        int sortBy = DB.SORT_STUDIES_STUDY_NAME; // Set default sort column
        int sortOrder = DB.SORT_ORDER_ASCENDING; // Set default sort order
        String sortByParam = (String)params.get("sort_by");
        String sortOrderParam = (String)params.get("sort_order");
        try{ sortBy = Integer.parseInt(sortByParam); }
        catch (NumberFormatException nfe){}
        try{ sortOrder = Integer.parseInt(sortOrderParam); }
        catch (NumberFormatException nfe){}
        String freeTextSearch = (String)params.get("free_text_search");
        
        //System.err.print("studytype=" + studyType + ", earlyDate=" + earlyDate + ", lateDate=" + lateDate);
        int stsize = studyreps != null ? studyreps.length : 0;
        //        for ( int i = 0;i < stsize;i++) {
        //            System.err.print(", rep=" + studyreps[i]);
        //        }
        //        log.error("");
        
        List studyList = null;
        if ( freeTextSearch == null || freeTextSearch.trim().length() == 0) {
            studyList = DB.getInstance().getStudies(studyType, earlyDate, lateDate, studyreps,sortBy,sortOrder);
        } else {
            studyList = DB.getInstance().getStudies(freeTextSearch,sortBy,sortOrder);
        }
        
        Document doc = createXmlDocFromStudyList(studyList);
        
        return doc;
    }

	public static Document createXmlDocFromStudyList(List studyList) throws Exception, SQLException {
		Document doc = null;
        Element root = new Element("study-list");
        doc = new Document(root);
        
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en", "GB"));

        int size = studyList != null ? studyList.size() : 0;
        //log.error("Retrieved " + (studyList != null ? studyList.size() : 0) + " records");
        for ( int i = 0;i < size;i++) {
            Study tmpStudy = (Study)studyList.get(i);
            Element tmpElem = new Element("study");
            root.addContent(tmpElem);
            tmpElem.addContent(new Element("name").addContent(tmpStudy.getName()));
            tmpElem.addContent(new Element("type").addContent(DB.getInstance().getStudyTypeName(tmpStudy.getTypeId())));
            tmpElem.addContent(new Element("desc").addContent(tmpStudy.getDesc()));
            tmpElem.addContent(new Element("duration").addContent(Integer.toString(tmpStudy.getDuration())));
            tmpElem.addContent(new Element("unit").addContent(tmpStudy.getUnit()));
            tmpElem.addContent(new Element("end").addContent(tmpStudy.getEnd() != null ? df.format(tmpStudy.getEnd()) : ""));
            tmpElem.addContent(new Element("start").addContent(tmpStudy.getStart() != null ? df.format(tmpStudy.getStart()) : ""));
            tmpElem.addContent(new Element("unit").addContent(tmpStudy.getKeywords()));
            tmpElem.addContent(new Element("no-students").addContent(Integer.toString(tmpStudy.getNoOfStudents())));
            tmpElem.addContent(new Element("no-profs").addContent(Integer.toString(tmpStudy.getNoOfProfessionals())));
            tmpElem.addContent(new Element("notes").addContent(tmpStudy.getStudyNotes()));
            tmpElem.addContent(new Element("owner").addContent(tmpStudy.getOwner()));
            tmpElem.addContent(new Element("last-edited-by").addContent(tmpStudy.getLastEditedBy()));
            
            // Then create the list of publications
            List pubs = tmpStudy.getPublications();
            int noPubs = pubs != null ? pubs.size() : 0;
            for ( int j = 0;j < noPubs;j++) {
                Publication pub = (Publication)pubs.get(j);
                Element pubElem = new Element("publication");
                tmpElem.addContent(pubElem);
                pubElem.addContent(new Element("title").addContent(URLDecoder.decode(pub.getTitle())));
                //pubElem.addContent(new Element("short-title").addContent(pub.getShortTitle()));
            }
            
            // and the list of responsibles
            List resps = tmpStudy.getResponsibles();
            int noResps = resps != null ? resps.size() : 0;
            for ( int j = 0;j < noResps;j++) {
                Responsible resp = (Responsible)resps.get(j);
                Element respElem = new Element("responsible");
                tmpElem.addContent(respElem);
                respElem.addContent(new Element("first-name").addContent(resp.getFirstName()));
                respElem.addContent(new Element("family-name").addContent(resp.getFamilyName()));
                respElem.addContent(new Element("name").addContent(resp.getFirstName() + " " + resp.getFamilyName()));
            }
        }
		return doc;
	}
    
    public static byte[] createPDF(Document doc, String xslPath, Map userInput) throws Exception {
        XMLOutputter xo = new XMLOutputter();

        StringWriter xmlOut = new StringWriter();
        xo.output(doc, xmlOut);
        String xmlOutStr = xmlOut.getBuffer().toString();
        System.out.println(xmlOutStr);
    
        String stylesheet = xslPath + File.separatorChar + "StudyList2.xsl";
        //log.error("Opening: " + stylesheet);
        
        // get an instance of a transformer, or initialize a new
        Transformer t = null;
        Transformer transInstance = null;
        if ( transInstance == null ) {
            StreamSource xslSource = new StreamSource(new FileReader(stylesheet));
            TransformerFactory tf = TransformerFactory.newInstance();
            transInstance = tf.newTransformer(xslSource);
        }
        t = transInstance;
        
        // Transform the document
        Source xmlSource = new javax.xml.transform.dom.DOMSource(new org.jdom.output.DOMOutputter().output(doc));
        StringWriter sw = new StringWriter();
        Result dr = new StreamResult(sw);
        t.transform(xmlSource,dr);
        sw.flush();
        String foOutStr = sw.getBuffer().toString();
        
        // Run FOP
        InputSource foSrc = new InputSource(new StringReader(sw.toString()));
        ByteArrayOutputStream outByte = new ByteArrayOutputStream();
        Driver driverInstance = null;
        if ( driverInstance == null ) {
            org.apache.fop.configuration.Configuration.put("baseDir",xslPath);
            driverInstance = new Driver(foSrc, outByte);
            driverInstance.setRenderer(org.apache.fop.apps.Driver.RENDER_PDF);
        } else {
            driverInstance.reset();
            driverInstance.setInputSource(foSrc);
            driverInstance.setOutputStream(outByte);
        }
        
        driverInstance.run();
        byte[] content = outByte.toByteArray();
        return content;
    }
    
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
