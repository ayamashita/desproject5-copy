package no.machina.simula.servlet;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  sigmund
 * @version
 */
public class StudyEditServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        // Get button parameters. Only one button can be pushed, so only one of
        // these can be != null.
        String addFile = request.getParameter("add_file");
        String editUrl = request.getParameter("edit_url");
        String confirm = request.getParameter("confirm");
        
        ServletContext context = getServletContext();
        HttpSession session = request.getSession(true);
        
        RequestDispatcher dispatcher = null;
        
        if (confirm != null){
            //forward to study_edit.jsp
            dispatcher = context.getRequestDispatcher("/jsp/admin/study_edit.jsp");
        }
        else{ //user pressed add file or add URL
            // get all parameters
            String studyName = request.getParameter("study_name");
            String studyTypeId = request.getParameter("study_type_id");
            String studyDuration = request.getParameter("study_duration");
            String studyDurationUnitId = request.getParameter("study_duration_unit_id");
            String studyStartDate = request.getParameter("study_start_date");
            String studyEndDate = request.getParameter("study_end_date");
            String studyDesc = request.getParameter("study_desc");
            String studyKeywords = request.getParameter("study_keywords");
            String numberOfStudents = request.getParameter("number_of_students");
            String numberOfProfessionals = request.getParameter("number_of_professionals");
            String[] studyResponsibleIds = request.getParameterValues("study_responsible_ids");            
            String[] studyPublicationIds = request.getParameterValues("study_publication_ids");

            // set parameters in session
            session.setAttribute("study_name", studyName);
            session.setAttribute("study_type_id", studyTypeId);
            session.setAttribute("study_duration", studyDuration);
            session.setAttribute("study_duration_unit_id", studyDurationUnitId);
            session.setAttribute("study_start_date", studyStartDate);
            session.setAttribute("study_end_date", studyEndDate);
            session.setAttribute("study_desc", studyDesc);
            session.setAttribute("study_keywords", studyKeywords);
            session.setAttribute("number_of_students", numberOfStudents);
            session.setAttribute("number_of_professionals", numberOfProfessionals);
            session.setAttribute("study_responsible_ids", studyResponsibleIds);
            session.setAttribute("study_publication_ids", studyPublicationIds);
            
            if (addFile != null){ // user pressed add file
                dispatcher = context.getRequestDispatcher("/jsp/admin/upload_file.jsp");
            }
            else if (editUrl != null){ // user pressed add URL
                dispatcher = context.getRequestDispatcher("/jsp/admin/edit_url.jsp");
            }
        }
        
        dispatcher.forward(request, response);            
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
 
}
