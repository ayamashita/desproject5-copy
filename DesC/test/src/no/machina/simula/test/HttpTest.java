/*
 * HttpTest.java
 *
 * Created on 25. september 2003, 18:22
 */

package no.machina.simula.test;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.log4j.Logger;


/**
 *
 * @author  Cato Ervik
 */
public class HttpTest {
    
	private static Logger log = Logger.getLogger(HttpTest.class);
    /** Creates a new instance of HttpTest */
    public HttpTest() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //String strURL = "https://prefect:443/login.php";
//        HttpState initialState = new HttpState();
//        initialState.setCookiePolicy(CookiePolicy.RFC2109);
        HttpClient httpclient = new HttpClient();
        Protocol myhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
        httpclient.getHostConfiguration().setHost("prefect", 443, myhttps);
        httpclient.setConnectionTimeout(30000);
        httpclient.getState().setCookiePolicy(CookiePolicy.COMPATIBILITY);
        
        GetMethod httpget = new GetMethod("/get_sessionstatus.php");
        int result = httpclient.executeMethod(httpget);
        log.info("Response status code: " + result);
        Cookie[] cookies = httpclient.getState().getCookies();
        log.info("Present cookies (" + (cookies != null ? cookies.length : 0) + "): ");
        for (int i = 0; i < cookies.length; i++) {
            log.info(" - " + cookies[i].toExternalForm());
        }
        String res = httpget.getResponseBodyAsString();
        log.info("result=" + res);
        // Release current connection to the connection pool once you are done
        httpget.releaseConnection();
    }
}
