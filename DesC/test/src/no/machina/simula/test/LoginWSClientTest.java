package no.machina.simula.test;

import no.machina.simula.engine.wsclient.LoginWSClient;
import junit.framework.TestCase;

public class LoginWSClientTest extends TestCase {
	public void setUp() {
		System.setProperty("no.machina.simula.login_ws_url", "http://simula.no:9080/simula/xmlrpcdes");
	}
	
	public void testAuthentication() throws Exception {
		LoginWSClient wsClient = new LoginWSClient();
		
		Boolean res1 = wsClient.checkCredentials("aiko", "xxx");
		assertEquals(Boolean.FALSE, res1);
		
		Boolean res2 = wsClient.checkCredentials("aiko", "8foEY#mB");
		assertEquals(Boolean.TRUE, res2);
	}
}
