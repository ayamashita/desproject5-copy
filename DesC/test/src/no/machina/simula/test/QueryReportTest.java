package no.machina.simula.test;

import java.util.List;

import junit.framework.TestCase;
import no.machina.simula.engine.dao.DB;
import no.machina.simula.entityBeans.PersonalizedReport;
import no.machina.simula.entityBeans.Study;

public class QueryReportTest extends TestCase {
	
	public QueryReportTest() {
		System.setProperty("no.machina.simula.dbdriver", "org.gjt.mm.mysql.Driver");
		System.setProperty("no.machina.simula.dburl", "jdbc:mysql://localhost:3306/des_c");
		System.setProperty("no.machina.simula.dbuser", "admin");
		System.setProperty("no.machina.simula.dbpwd", "masterkey");
	}

	private static int REPORT_ID = 9;
	public void testQueryReport() throws Exception {
		DB d = DB.getInstance();
		
		PersonalizedReport pr = d.getReport(REPORT_ID);
		List result = d.queryReport(pr);
		
		for (int i = 0; i < result.size(); i++) {
			Study s = (Study)result.get(i);
			System.out.println(s.getName());
		}
	}
}
