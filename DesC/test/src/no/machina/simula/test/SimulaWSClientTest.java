package no.machina.simula.test;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;
import no.machina.simula.engine.dao.DB;
import no.machina.simula.engine.wsclient.SimulaWSClient;
import no.machina.simula.engine.wsclient.WSClient;
import no.machina.simula.entityBeans.People;

public class SimulaWSClientTest extends TestCase {
	
	public void setUp() {
		System.setProperty("no.machina.simula.simula_ws_url", "http://simula.no:9080/simula/xmlrpcdes");
	}
	
	public void testCreateAndConfigure() throws Exception {
		WSClient wsClient = new SimulaWSClient();
	}
	
	public void testGetResponsibles() throws Exception{
		SimulaWSClient wsClient = new SimulaWSClient();
		List result = wsClient.getResponsibleIDs("Aiko");
		for (int i = 0; i < result.size(); i++) {
			System.out.println(i + ": " + result.get(i));
		}
	}
	
	public void testGetPeopleByID() throws Exception{
		SimulaWSClient wsClient = new SimulaWSClient();
		People p = wsClient.getPeopleById("aiko");
		System.out.println(p);
	}
	
	public void testGetAllResponsibles() throws Exception {
		SimulaWSClient wsClient = new SimulaWSClient();
		List list = wsClient.getAllResponsibles();
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	public void testGetResponsiblesByIds () throws Exception {
		SimulaWSClient wsClient = new SimulaWSClient();
		
		String[] ids = {"aiko", "khk", "hake" };
		
		List list = wsClient.getResponsiblesByIds(Arrays.asList(ids));
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}

}
