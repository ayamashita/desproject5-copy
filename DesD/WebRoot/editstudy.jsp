<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="study" class="com.tec.des.dto.StudyDTO" scope="request"/> 
<jsp:useBean id="studyTypes" class="java.util.Hashtable" scope="request"/> 
<jsp:useBean id="studyDurationUnits" class="java.util.Hashtable" scope="request"/>
<jsp:useBean id="user" class="com.tec.des.dto.UserDTO" scope="session"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;list studies</A> &gt; study overview report &gt; edit study</TD>
        <TD align="right"><A href="JavaScript:help('help.html#editstudy')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="4" cellpadding="4"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="" method="post">
<TABLE cellspacing="4" cellpadding="0"> 
<TR>
<TD valign="top">
<TABLE>
    <TR><TD class="bodytext-bold" valign="top">Study Name*</TD></TR>
    <TR><TD class="bodytext" valign="top"><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_NAME%>" value="<%=study.getName()%>" size="98"></TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Description*</TD></TR>
    <TR><TD class="bodytext" width='1000' valign="top"><TEXTAREA class="bodytext" cols="100" rows="8" name="<%=WebKeys.REQUEST_PARAM_STUDY_DESCRIPTION%>"><%=study.getDescription()%></TEXTAREA></TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Notes</TD></TR>
    <TR><TD class="bodytext" valign="top"><TEXTAREA class="bodytext" cols="100" rows="4" name="<%=WebKeys.REQUEST_PARAM_STUDY_NOTES%>"><%=study.getNotes()%></TEXTAREA></TD></TR>
    <TR><TD class="bodytext-bold" valign="top"><A href="JavaScript:openPublications()" onclick="document.form.target='editpublications';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_PUBLICATIONS%>';">Publications</A></TD></TR>
    <TR><TD class="bodytext" valign="top"><util:links url="http://www.simula.no/publication_one.php?publication_id=" items="<%=study.getPublications()%>" separator="<BR><BR>" /></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-italic" valign="top">Fields marked with * are required.</TD></TR>
</TABLE>
</TD>
<TD valign="top">
<TABLE>
    <TR>
        <TD class="bodytext-bold" valign="top"><A href="JavaScript:openStudyTypes()" onclick="document.form.target='editstudytypes';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_TYPES%>';">Type of Study*</A></TD>
        <TD class="bodytext" valign="top">
        <util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_STUDY_TYPE%>" attributes="class=bodytext" displayedvalues="<%=(String[])studyTypes.values().toArray(new String[0]) %>" selectedvalue="<%=String.valueOf(study.getTypeId())%>" optionValues="<%=(String[])studyTypes.keySet().toArray(new String[0]) %>" /> 
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top"><A href="JavaScript:openStudyResponsibles()" onclick="document.form.target='editstudyresponsibles';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_RESPONSIBLES%>';">Study Responsibles*</A></TD>
        <TD class="bodytext" valign="top"><util:links url="http://www.simula.no/people_one.php?people_id=" items="<%=study.getResponsibles()%>"/></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Duration of Study</TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_DURATION%>" value="<%=study.getDuration()%>" size="1" maxlength="4">
            <util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_STUDY_DURATION_UNIT%>" attributes="class=bodytext" optional="true" selectedvalue="<%=String.valueOf(study.getDurationUnitId()) %>" displayedvalues="<%=(String[])studyDurationUnits.values().toArray(new String[0]) %>" optionValues="<%=(String[])studyDurationUnits.keySet().toArray(new String[0]) %>" /> 
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top"><A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_STUDY_START_DATE%>'">Start of Study</A></TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_START_DATE%>" value="<%=study.getStartDate()%>" size="7" maxlength='10'><FONT class="bodytext-italic">&nbsp;&nbsp;yyyy/mm/dd</FONT>
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top"><A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_STUDY_END_DATE%>'">End of Study*</A></TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_END_DATE%>" value="<%=study.getEndDate()%>" size="7" maxlength='10'><FONT class="bodytext-italic">&nbsp;&nbsp;yyyy/mm/dd</FONT>
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Keywords</TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_KEYWORDS%>" value="<%=study.getKeywords()%>" size="25">
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Student participants</TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_NO_STUNDENTS%>" value="<%=study.getNoOfStudentParticipants()%>" size="1">
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Professional participants</TD>
        <TD class="bodytext" valign="top">
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_NO_PROFESSIONALS%>" value="<%=study.getNoOfProfessionalParticipants()%>" size="1" >
        </TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top"><A href="JavaScript:openStudyMaterial()" onclick="document.form.target='editstudymaterial';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_STUDY_MATERIAL%>';">Study Material</A></TD>
        <TD class="bodytext" valign="top"><util:studymaterial items="<%=study.getMaterial()%>" fromTemp="true" /></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Study Owner</TD>
        <TD class="bodytext" valign="top"><%=study.getOwner()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Last Edited by</TD>
        <TD class="bodytext" valign="top"><%=study.getLastEditedBy()%></TD>
    </TR>
</TABLE>
</TD>
</TR>
<TR><TD>&nbsp;</TD></TR>
<TR>
    <TD>
        <INPUT type="submit" name="" value="Save Study" class="bodytext" 
            onclick="document.form.target='_self';
                     document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_UPDATE_STUDY%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>.value='<%=Constants.MAINTAIN_MODE_FINAL%>';">
        <INPUT type="submit" name="" value="Delete Study" class="bodytext" 
            onclick="document.form.target='_self';
                     document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>.value='<%=Constants.MAINTAIN_MODE_FINAL%>';
                     return confirm('Do you really want to delete this Study?');">
        <INPUT type="submit" name="" value="Cancel" class="bodytext" 
            onclick="document.form.target='_self';
                     document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY%>';
                     document.form.<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>.value='<%=Constants.MAINTAIN_MODE_TEMP_EDIT%>';
                     return confirm('Discard all changes?');">
    </TD>
</TR>
<TR><TD>&nbsp;</TD></TR>
<TR>
    <TD class="bodytext"><A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank';
            document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>'; 
            document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SINGLE_STUDY_REPORT%>';
            document.form.<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>.value='true';">Printer friendly version</A>
    </TD>
</TR>
</TABLE>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAINTAIN_MODE%>" value="<%=Constants.MAINTAIN_MODE_TEMP_EDIT%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_UPDATE_MODE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="<%=study.getId()%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID_DELETE%>" value="<%=study.getId()%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLES%>" value="<util:arrayprinter items="<%= (String[])study.getResponsibles().values().toArray(new String[0]) %>" />" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS%>" value="<util:arrayprinter items="<%= (String[])study.getResponsibles().keySet().toArray(new String[0]) %>" />" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_PUBLICATIONS%>" value="<util:arrayprinter items="<%= (String[])study.getPublications().values().toArray(new String[0]) %>" />" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_PUBLICATION_IDS%>" value="<util:arrayprinter items="<%= (String[])study.getPublications().keySet().toArray(new String[0]) %>" />" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL%>" value="<%=study.getMaterial().isEmpty()?false:true%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_OWNER%>" value="<%=study.getOwner()%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_LAST_EDITED_BY%>" value="<%=user.getSingleName()%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_EDIT_STUDY%>" >
</FORM>
</TD></TR> 
</TABLE> 
<jsp:include page="bottom.html"/>