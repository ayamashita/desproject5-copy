<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Validator" %>
<%@ taglib uri="/util" prefix="util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>[ simula . research laboratory ]</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
</HEAD>
<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<jsp:useBean id="study" class="com.tec.des.dto.StudyDTO" scope="request"/> 
<TABLE cellspacing="4" cellpadding="4" width="100%" bgcolor="#f5f5f5"><TR><TD>
<jsp:include page="messages.jsp"/>
<TABLE cellspacing="4" cellpadding="0"> 
<TR>
<TD valign="top">
<TABLE width="400">
    <TR><TD class="bodytext-bold" valign="top">Study Name</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=study.getName()%></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Description</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getDescription())%></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Study Notes</TD></TR>
    <TR><TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getNotes())%></TD></TR>
    <TR><TD class="bodytext" valign="top">&nbsp;</TD></TR>
    <TR><TD class="bodytext-bold" valign="top">Publications</TD></TR>
    <TR><TD class="bodytext" valign="top"><util:arrayprinter items="<%= (String[])study.getPublications().values().toArray(new String[0]) %>" separator="<BR><BR>" /></TD></TR>
</TABLE>
</TD>
<TD valign="top">
<TABLE>
    <TR>
        <TD class="bodytext-bold" valign="top">Type of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getType()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Study Responsibles</TD>
        <TD class="bodytext" valign="top"><util:arrayprinter items="<%= (String[])study.getResponsibles().values().toArray(new String[0]) %>" separator="<BR>" /></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Duration of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getDuration()%>&nbsp;<%=study.getDurationUnit()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Start of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getStartDate()%></TD> 
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">End of Study</TD>
        <TD class="bodytext" valign="top"><%=study.getEndDate()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Keywords</TD>
        <TD class="bodytext" valign="top"><%=study.getKeywords()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Student participants</TD>
        <TD class="bodytext" valign="top"><%=study.getNoOfStudentParticipants()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">No of Professional participants</TD>
        <TD class="bodytext" valign="top"><%=study.getNoOfProfessionalParticipants()%></TD>
    </TR>
    <TR>
        <TD class="bodytext-bold" valign="top">Study Material</TD>
        <TD class="bodytext" valign="top"><util:studymaterial items="<%=study.getMaterial()%>" printerFriendly="true" /></TD>
    </TR>
</TABLE>
</TD>
</TR>
</TABLE>
</TD></TR> 
</TABLE> 