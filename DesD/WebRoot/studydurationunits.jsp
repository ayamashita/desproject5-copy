<%@ page import="com.tec.des.http.*" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top_sml.html"/>
<jsp:useBean id="studyDurationUnits" class="java.util.Hashtable" scope="request"/> 
<!-- Start content -->
<TABLE cellspacing="0" cellpadding="0" width="100%">
    <TR>
        <TD align="right"><A href="JavaScript:help('help.html#editstudydurationunits')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="2" cellpadding="0"> 
    <TR><TD colspan="3"><H1>Edit Study Duration Units</H1></TD></TR> 
    <TR><TD><P class="bodytext-bold" >All Study Duration Units</P></TD><TD></TR>
    <TR>
        <TD class="bodytext">
        <util:multiselect name="<%=WebKeys.REQUEST_PARAM_DELETE_STUDY_DURATION_UNIT%>" boxsize="8" attributes="class=bodytext" optionvalues="<%=(String[])studyDurationUnits.keySet().toArray(new String[0]) %>" displayedvalues="<%=(String[])studyDurationUnits.values().toArray(new String[0]) %>"/> 
        </TD>
    </TR>
    <TR>
        <TD>
        <INPUT type="submit" name="" value="Delete" class="bodytext" onClick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY_DURATION_UNIT%>';return confirm('Do you really want to delete selected Study Duration Unit(s)?');">
        </TD>
    </TR>
</TABLE>
<TABLE cellspacing="2" cellpadding="0"> 
    <TR><td>&nbsp;</td></TR>
    <TR>
        <TD class="bodytext-bold">Add new Study Duration Unit</TD>
    </TR>
    <TR>
        <TD><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_ADD_STUDY_DURATION_UNIT%>" value="" size="40"></TD>
        <TD><INPUT type="submit" name="" value="Add" class="bodytext" onClick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_ADD_STUDY_DURATION_UNIT%>';"></TD>
    </TR>
    <TR><TD>&nbsp;</TD></TR>
</TABLE>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_STUDY_DURATION_UNITS%>" > 
</FORM>
<INPUT type="submit" value="Close" class="bodytext" onClick="JavaScript:window.close();">
<!-- End content -->
<jsp:include page="bottom_sml.html"/> 