<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top_sml.html"/>
<jsp:useBean id="selectedMaterial" class="java.util.Vector" scope="request"/>
<jsp:useBean id="studyMaterial" class="com.tec.des.dto.StudyMaterialDTO" scope="request"/>
<jsp:useBean id="studyId" class="java.lang.String" scope="request"/> 
<!-- Start mainframe -->
<TABLE cellspacing="0" cellpadding="0" width="100%">
    <TR>
        <TD align="right"><A href="JavaScript:help('help.html#selectstudymaterial')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<jsp:include page="messages.jsp"/>

<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="2" cellpadding="0"> 
    <TR><TD colspan="3"><H1>Select Study Material</H1></TD></TR> 

<% if (!selectedMaterial.isEmpty()) { %>
    <TR>
        <TD colspan="2" ><p class="bodytext-bold">Selected Study Material</p></TD>
        <TD align="middle"><p class="bodytext-bold">Delete</p></TD>
    </TR>
<util:iteration name="material" type="com.tec.des.dto.StudyMaterialDTO" rowAlternator="row" group="<%= (Object[])selectedMaterial.toArray(new Object[0]) %>">
    <TR>
        <TD class="bodytext" valign="top"><%=material.getDescription()%></A></TD>    
        <TD class="bodytext" valign="top">
<% if (material.isFile()) { %>
<%=material.getFileName()%>
<% } else { %>
<%=material.getUrl()%>
<% } %>
        </TD>
        <TD valign="top" align="center"><INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_DELETE%><%=material.getId()%>" value="<%=material.getId()%>" class="bodytext"></TD>
    </TR>
</util:iteration>
    <TR>
        <TD>&nbsp;</TD>
        <TD>&nbsp;</TD>
        <TD align="middle"><INPUT type="submit" name="" value="Delete" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_DELETE_STUDY_MATERIAL%>';" class="bodytext"></TD>
   </TR>
<% } else {%>
    <TR>
        <TD><P class="bodytext">No Study Material selected yet</P></TD></TR>
<% } %>
    </TR>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="<%=WebConstants.USECASE_ADD_STUDY_MATERIAL%>" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_STUDY_MATERIAL%>" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="<%=studyId%>" >
</FORM>

<FORM name="form2" enctype="multipart/form-data" action="/DesD/AddStudyMaterialHandler" method="post">
    <TR><TD colspan="3">&nbsp;</TD></TR>
    <TR><TD colspan="3">&nbsp;</TD></TR>
    <TR>
        <TD colspan="2" class="bodytext-bold">Add Study Material</TD>
    </TR>
    <TR>
        <TD class="bodytext" colspan="3">Description&nbsp;&nbsp;
            <INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_DESCRIPTION%>" value="<%=studyMaterial.getDescription()%>" size="86">
        </TD>
    </TR>
<%
String checkedForFile;
String checkedForUrl; 
if (studyMaterial.isFile()) { 
    checkedForFile = "checked";
    checkedForUrl = ""; 
} else {
    checkedForFile = "";
    checkedForUrl = "checked"; 
}
%>
    <TR>
        <TD class="bodytext" colspan="3">
            <INPUT type="radio" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_TYPE%>" value="<%=WebConstants.STUDY_MATERIAL_ISURL%>" <%=checkedForUrl%>>Url
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_URL%>" value="<%=studyMaterial.getUrl()%>" size="50">
            <I>e.g. http://www.simula.no</I>
        </TD>
    </TR>
    <TR>
        <TD class="bodytext" colspan="3">
            <INPUT type="radio" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_TYPE%>" value="<%=WebConstants.STUDY_MATERIAL_ISFILE%>" <%=checkedForFile%>>File
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <INPUT type="file" size="71" name="<%=WebKeys.REQUEST_PARAM_STUDY_MATERIAL_FILE%>" value="" class="bodytext">
        </TD>
    </TR>
    <TR><TD>&nbsp;</TD></TR>
    <TR><TD><INPUT type="submit" name="" value="Add" class="bodytext"></TD></TR>
    <TR><TD>&nbsp;</TD></TR>
    <TR><TD>&nbsp;</TD></TR>
 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_STUDY_ID%>" value="<%=studyId%>" >
</FORM>

    <TR>
        <TD>
            <INPUT type="submit" name="" value=" Ok " class="bodytext" onClick="JavaScript:closeStudyMaterial('<%=WebConstants.USECASE_UPDATE_STUDY%>', '<%=Constants.MODE_OK%>');">
            <INPUT type="submit" name="" value="Cancel" class="bodytext" onClick="JavaScript:closeStudyMaterial('<%=WebConstants.USECASE_UPDATE_STUDY%>', '<%=Constants.MODE_CANCEL%>');">
        </TD>
    </TR>
</TABLE>
<!-- End content -->
<jsp:include page="bottom_sml.html"/>