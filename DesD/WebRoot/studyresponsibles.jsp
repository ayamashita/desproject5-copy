<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top_sml.html"/>
<jsp:useBean id="allPeople" class="java.util.Vector" scope="request"/> 
<jsp:useBean id="selectedPeople" class="java.util.Vector" scope="request"/> 
<!-- Start mainframe -->
<TABLE cellspacing="0" cellpadding="0" width="100%">
    <TR>
        <TD align="right"><A href="JavaScript:help('help.html#selectstudyresponsibles')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<jsp:include page="messages.jsp"/>
<FORM name="selectPeople" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="2" cellpadding="0"> 
    <TR>
        <TD colspan="3"><H1>Select Study Responsibles</H1></TD>
    </TR> 
    <TR>
        <TD><p class="bodytext-bold" >All people</p></TD><td>&nbsp;</td>
        <TD><p class="bodytext-bold" >Study Responsibles</p></TD> 
    </TR>
    <TR>
        <TD class="bodytext">
        <util:studyresponsibleselect name="<%=WebKeys.REQUEST_PARAM_ALL_PEOPLE%>" boxsize="15" attributes="class=bodytext" items="<%=allPeople%>"/> 
        </TD>
        <TD>
            <TABLE>
                <TR>
                    <TD><INPUT type="submit" name="<%=WebKeys.REQUEST_PARAM_BUTTON_ADD%>" value=" >> "  class="bodytext" onclick=""></TD>
                </TR>
                <TR><TD>&nbsp;</TD></TR>
                <TR>
                    <TD><INPUT type="submit" name="<%=WebKeys.REQUEST_PARAM_BUTTON_REMOVE%>" value=" << "  class="bodytext" onclick=""></TD>
                </TR>
            </TABLE>
        </TD>
        <TD class="bodytext">
        <util:studyresponsibleselect name="<%=WebKeys.REQUEST_PARAM_SELECTED_PEOPLE%>" boxsize="15" attributes="class=bodytext" items="<%=selectedPeople%>"/> 
        </TD>
    </TR>
</TABLE>
<TABLE cellspacing="2" cellpadding="0"> 
    <TR><TD>&nbsp;</TD></TR>
    <TR><TD>&nbsp;</TD></TR>
    <TR>
        <TD>
            <INPUT type="submit" value=" Ok " class="bodytext" 
                onClick="JavaScript:updateOpenerWindow('<%=WebConstants.USECASE_UPDATE_STUDY%>', '<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLE_IDS%>', '<util:studyresponsibleprinter items="<%=selectedPeople%>" />', '<%=WebKeys.REQUEST_PARAM_STUDY_RESPONSIBLES%>', '<util:studyresponsibleprinter items="<%=selectedPeople%>" printName="true" />');">
            <INPUT type="submit" value="Cancel" class="bodytext" onClick="JavaScript:closeEditWindow('<%=WebConstants.USECASE_UPDATE_STUDY%>');">
        </TD>
    </TR>
</TABLE>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="<%=WebConstants.USECASE_SELECT_STUDY_RESPONSIBLES%>" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_STUDY_RESPONSIBLES%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_BEAN_SELECTED_PEOPLE%>" value="<util:studyresponsibleprinter items="<%=selectedPeople%>" />" >
</FORM>
<!-- End content -->
<jsp:include page="bottom_sml.html"/>