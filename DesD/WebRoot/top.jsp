<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>[ simula . research laboratory ]</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
<SCRIPT type="text/javascript" src="javascript.js"></SCRIPT>
</HEAD>

<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c">
<TABLE border="0" width="100%" cellspacing="0" cellpadding="0"> 
<!-- Start header -->
<%@ page import="com.tec.des.http.WebConstants" %>
    <TR>
        <TD>
        <TABLE width="100%">
            <TR height="26">
            <FORM name="formMenuSearch" action="http://www.simula.no/search.php?search_advance=n" method="post">
                <TD>&nbsp;</TD>
                <TD valign="bottom" align="right">
                    <INPUT  class="bodytext" type="text" name="search_text" value="" size="12">&nbsp;&nbsp;&nbsp;&nbsp;</TD>
            </TR>
            <TR>
                <TD>&nbsp;</TD>
                <TD align="right">
                    <A href="JavaScript:searchSimulaweb();" onmouseover="document.search.src='images/topright-search-mouseover.png'" onmouseout="document.search.src='images/topright-search-normal.png'">
                    <IMG border="0" src="images/topright-search-normal.png" name="search"></A>
                </TD>
            </FORM>    
            </TR>
            <TR>
                <TD align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <IMG src="images/logo-part1.png">
                    <IMG src="images/logo-part2.png">
                </TD>
                <TD align="right">
                    <A href="http://www.simula.no/search.php" onmouseover="document.advsearch.src='images/topright-advsearch-mouseover.png'" onmouseout="document.advsearch.src='images/topright-advsearch-normal.png'">
                    <IMG  border="0" src="images/topright-advsearch-normal.png" name="advsearch" alt="Advanced Search"></A>
                </TD>
            </TR>
        </TABLE>
        </TD>
    </TR>
    <TR bgcolor="aeaeae"><!-- Start grey menu -->
        <TD> 
        <TABLE cellspacing="0" cellpadding="0">
            <TR>
                <TD>
                <TABLE cellspacing="0" cellpadding="0" width='138'>
                    <TR><TD>&nbsp;</TD></TR>
                </TABLE> 
                </TD>
                <TD>
                <TABLE cellspacing="0" cellpadding="0">
                    <TR>
                        <TD>
                            <A href="http://www.simula.no/index.php" onmouseover="document.home.src='images/menu-home-selected.png'" onmouseout="document.home.src='images/menu-home-normal.png'"><IMG border="0" src="images/menu-home-normal.png" name="home" alt="Home Page"></A><A href="http://www.simula.no/news.php" onmouseover="document.news.src='images/menu-news-selected.png'" onmouseout="document.news.src='images/menu-news-normal.png'"><IMG border="0" src="images/menu-news-normal.png" name="news" alt="News and press releases at Simula Research Laboratory"></A><A href="http://www.simula.no/publication.php" onmouseover="document.publications.src='images/menu-publications-selected.png'" onmouseout="document.publications.src='images/menu-publications-normal.png'"><IMG border="0" src="images/menu-publications-normal.png" name="publications" alt="Publications by Simula Research Laboratory"></A><A href="http://www.simula.no/project.php" onmouseover="document.projects.src='images/menu-projects-selected.png'" onmouseout="document.projects.src='images/menu-projects-normal.png'"><IMG border="0" src="images/menu-projects-normal.png" name="projects" alt="Scientific projects at Simula Research Laboratory"></A><A href="http://www.simula.no/department.php" onmouseover="document.research.src='images/menu-research-selected.png'" onmouseout="document.research.src='images/menu-research-normal.png'"><IMG border="0" src="images/menu-research-normal.png" name="research" alt="Research departments at Simula Research Laboratory"></A><A href="http://www.simula.no/innovation.php" onmouseover="document.innovation.src='images/menu-innovation-selected.png'" onmouseout="document.innovation.src='images/menu-innovation-normal.png'"><IMG border="0" src="images/menu-innovation-normal.png" name="innovation" alt="Innovation at Simula Research Laboratory"></A><A href="http://www.simula.no/people.php" onmouseover="document.people.src='images/menu-people-selected.png'" onmouseout="document.people.src='images/menu-people-normal.png'"><IMG border="0" src="images/menu-people-normal.png" name="people" alt="People that work at Simula Research Laboratory"></A><A href="http://www.simula.no/opportunity.php" onmouseover="document.opportunities.src='images/menu-opportunities-selected.png'" onmouseout="document.opportunities.src='images/menu-opportunities-normal.png'"><IMG border="0" src="images/menu-opportunities-normal.png" name="opportunities" alt="Opportunities for students and proffesionals at Simula Research Laboratory"></A><A href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=com.tec.des.http.handler.OpenListStudiesHandler"><IMG border="0" src="images/menu-des-selected.png" name="des" alt="Database of Empirical Studies"></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<IMG src="images/menu-all-grey.png" width="80" height="22"><A href="https://www.simula.no/intranet/" onmouseover="document.intranet.src='images/menu-intranet-selected.png'" onmouseout="document.intranet.src='images/menu-intranet-normal.png'"><IMG border="0" src="images/menu-intranet-normal.png" name="intranet"></A>
                        </TD>
                    </TR>
                </TABLE>
                </TD>
            </TR>
        </TABLE>
        </TD>
    </TR><!-- End grey menu -->
<!-- End header -->
<!-- Start mainframe -->
    <TR bgcolor="#f5f5f5">
        <TD>
        <TABLE cellspacing="0" cellpadding="0">
            <TR>
            <jsp:include page="adminmenu.jsp"/>
<!-- Start content -->
            <TD valign="top">
