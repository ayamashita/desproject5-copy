<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.dto.UserDTO" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="peopleRole" class="java.util.Vector" scope="request"/>
<jsp:useBean id="roles" class="java.util.Hashtable" scope="request"/>  
<% UserDTO user = (UserDTO)session.getAttribute(WebKeys.SESSION_PARAM_USER); %>
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
<TR><TD class="path" valign="top">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;user administration</TD>
<TD align="right"><A href="JavaScript:help('help.html#useradministration')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
</TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="6" cellpadding="6"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">

<TABLE cellspacing="2" cellpadding="0"> 
<TR><TD colspan="3"><H1>User Administration</H1></TD></TR>  
<TR>
<TD><INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_TEXT%>" value=""  class="bodytext" ></TD>
<TD><INPUT type="submit" name="" value="Search" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_FIND_PEOPLE_ROLE%>';" class="bodytext"></TD>
<TD>&nbsp;</TD>
<TD align="center"><INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_ONLY_ROLE%>" value="" class="bodytext" checked></TD>
<TD class="bodytext">Display only people having a role</TD>
</TR>
<TR><TD class="bodytext-italic" colspan="2">Empty search displays all people</TD></TR>
</TABLE>
<BR><BR>

<TABLE cellspacing="4" cellpadding="0"> 
<TR><TD colspan="2"><H2>Search results</H2></TD></TR>
<TR>
<TD class="bodytext-bold" valign="top">Name</TD>
<TD class="bodytext-bold" valign="top">Role</TD>
</TR>
<util:multidropdown name="<%=WebKeys.REQUEST_PARAM_ROLES%>" attributes="class=bodytext" peopleRole="<%=peopleRole %>" optional="true" roles="<%=roles %>" /> 
<TR><TD>&nbsp;</TD></TR>
<TR><TD><INPUT type="submit" name="" value="Save roles" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_SAVE_ROLES%>';" class="bodytext">
<INPUT type="submit" name="" value="Cancel" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_ADMIN_PAGE%>';return confirm('Discard all changes?');" class="bodytext"></TD></TR>

<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" >
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_USER_ADMIN%>" >

</TABLE>
</FORM>
</TD></TR> 
</TABLE> 
</TD>
<!-- End content -->
</TR> 
</TABLE> 
</TD></TR> 
<!-- End mainframe -->
</TABLE>
<jsp:include page="bottom.html"/>