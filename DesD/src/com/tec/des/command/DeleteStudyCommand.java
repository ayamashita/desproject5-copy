package com.tec.des.command;
import com.tec.des.dao.StudyDAO;
import com.tec.shared.util.Nuller;

/**
 * Command for deleting a study based on the id.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class DeleteStudyCommand extends Command {

    private int id = Nuller.getIntNull();
    private int mode = Nuller.getIntNull();
    
/**
 * Constructs a new Command object.
 */
public DeleteStudyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute()  
    && !Nuller.isNull(id) && !Nuller.isNull(mode);
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    StudyDAO dao = new StudyDAO();
    try {
        dao.deleteStudy(id, mode);
    } finally {
        dao.close();
    }
}

public void setId(int id) {
    this.id = id;
}

public void setMode(int mode) {
    this.mode = mode;
}

}