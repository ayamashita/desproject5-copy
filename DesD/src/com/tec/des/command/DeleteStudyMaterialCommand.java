package com.tec.des.command;
import java.util.Vector;
import com.tec.des.dao.StudyDAO;

/**
 * Command for deleting Study Material from a Study.
 * The result of the command is a Vector containing selected Study Material, after deletion.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class DeleteStudyMaterialCommand extends Command  {
    
    private Vector studyMaterial;
    private String selectedMaterial;
    private int studyId;

/**
 * Constructs a new Command object.
 */
    public DeleteStudyMaterialCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyDAO dao = new StudyDAO();
        try {
            dao.deleteStudyMaterial(selectedMaterial);
            studyMaterial = dao.getStudyMaterial(studyId); 
        } finally {
            dao.close();
        }
    }

    public void setSelectedMaterial (String selectedMaterial) {
        this.selectedMaterial = selectedMaterial;
    }
    
    public void setStudyId (int studyId) {
        this.studyId = studyId;
    }
    
    public Vector getResult() {
        return studyMaterial;
    }

}