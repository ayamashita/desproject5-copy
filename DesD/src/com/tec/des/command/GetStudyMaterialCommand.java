package com.tec.des.command;
import java.util.Vector;
import com.tec.des.dao.StudyDAO;
import com.tec.shared.util.Nuller;

/**
 * Command for fetching Study Material either by study material id or by study id.
 * If studyId is set all study material connected to this study is fetched.
 * If studyMaterialId is set the study material with this id is fetched. 
 * The result of the command is a Vector containing the Study Material.
 *
 * @author :  Per Kristian Foss
 * 
 */
public class GetStudyMaterialCommand extends Command  {
    
    private Vector studyMaterial;
    private int studyId = Nuller.getIntNull();
    private int studyMaterialId = Nuller.getIntNull();
    private boolean fromTemp;
    private boolean forStudyMaterialWindow;
    private boolean deleteNotConfirmed = true;

/**
 * Constructs a new Command object.
 */
    public GetStudyMaterialCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute() && (!Nuller.isNull(studyId) || !Nuller.isNull(studyMaterialId));
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyDAO dao = new StudyDAO();
        try {
            if (!Nuller.isNull(studyId)) {
                if (forStudyMaterialWindow)
                    studyMaterial = dao.getStudyMaterial(studyId); 
                else 
                    studyMaterial = dao.getStudyMaterial(studyId, true, deleteNotConfirmed); 
            } else {
                studyMaterial = new Vector();
                studyMaterial.add(dao.getStudyMaterialById(studyMaterialId, fromTemp));
            }
        } finally {
            dao.close();
        }
    }
    
    public void setStudyId (int studyId) {
        this.studyId = studyId;
    }

    public void setStudyMaterialId (int studyMaterialId) {
        this.studyMaterialId = studyMaterialId;
    }

    public void setFromTemp(boolean fromTemp) {
        this.fromTemp = fromTemp;
    }
    
    public void setForStudyMaterialWindow(boolean forStudyMaterialWindow) {
        this.forStudyMaterialWindow = forStudyMaterialWindow;
    }

    public void setDeleteNotConfirmed(boolean deleteNotConfirmed) {
        this.deleteNotConfirmed = deleteNotConfirmed;
    }
    
    
    public Vector getResult() {
        return studyMaterial;
    }

}