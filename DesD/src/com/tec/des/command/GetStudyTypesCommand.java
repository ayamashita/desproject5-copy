package com.tec.des.command;
import java.util.Hashtable;
import com.tec.des.dao.StudyTypeDAO;

/**
 * Command for fetching all study types.
 * The result of the command is a Hashtable containing all study types.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetStudyTypesCommand extends Command  {
    
    private Hashtable studyTypes;

/**
 * Constructs a new Command object.
 */
    public GetStudyTypesCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    StudyTypeDAO dao = new StudyTypeDAO();
        try {
            studyTypes = dao.getStudyTypes();
        } finally {
            dao.close();
        }
    }

    public Hashtable getResult() {
        return studyTypes;
    }

}
