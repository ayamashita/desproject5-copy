package com.tec.des.command;
import com.tec.des.dao.AdminPageDAO;

/**
 * Command for fetching the text on the admin page.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class GetTextOnAdminPageCommand extends Command  {
    
    private String text;

/**
 * Constructs a new Command object.
 */
    public GetTextOnAdminPageCommand() {
    }

/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
    public boolean isReadyToCallExecute() {
        return super.isReadyToCallExecute();
    }

   /**
    * Executes the command. 
    * @throws Exception 
    */
    protected void performExecute() throws Exception {
    AdminPageDAO dao = new AdminPageDAO();
        try {
            text = dao.getTextOnAdminPage();
        } finally {
            dao.close();
        }
    }

    public String getResult() {
        return text;
    }

}
