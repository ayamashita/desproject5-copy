package com.tec.des.command;
import com.tec.des.dao.StudyDAO;

/**
 * Command for initializing a new study.
 * The result of the command is a id refering to the study template
 * inserted in the temp_study table.
 *
 * @author :  Norunn Haug Christensen
 * 
 */
public class NewStudyCommand extends Command {

    private int id;
    
/**
 * Constructs a new Command object.
 */
    public NewStudyCommand() {
	super();
}


/**
 * Checks if all input parameters has been set, and returns true if the command 
 * is ready for execution.
 */ 
public boolean isReadyToCallExecute() {
  return super.isReadyToCallExecute();
}

/**
 * Executes the command. 
 * @throws Exception 
 */
protected void performExecute() throws Exception {

    StudyDAO dao = new StudyDAO();
    try {
        id = dao.newStudy();
    } finally {
        dao.close();
    }
}

public int getResult() {
    return id;
}

}