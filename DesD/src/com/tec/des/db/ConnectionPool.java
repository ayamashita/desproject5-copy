package com.tec.des.db;
import java.util.ResourceBundle;
import java.sql.*;
import java.util.Hashtable;
import com.tec.des.util.ExceptionMessages;
import com.tec.des.util.Constants;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.SystemException;

/**
 * Singleton pool for obtaining database connections.
 *
 * @author: Norunn Haug Christensen
 */

public class ConnectionPool {

    private static ConnectionPool theConnectionPool;
    private static ResourceBundle resource = ResourceBundle.getBundle(Constants.DES_PROPERTIES_FILE);
    public static final String DRIVER = resource.getString("driver");
    public static final String DB_URL = resource.getString("dbUrl");
    public static final String USERNAME = resource.getString("username");
    public static final String PASSWORD = resource.getString("password");
    public static final String AUTO_RECONNECT = resource.getString("autoReconnect");
    private static final int POOL_SIZE = new Integer(resource.getString("poolSize")).intValue();
    
    private Connection[] connections;
    private Hashtable usedConnections;
   
    private ConnectionPool() throws SQLException {
        connections = new Connection[POOL_SIZE];
        for (int i=0; i<POOL_SIZE; i++) {
            connections[i] = getNewConnection();
        }
        usedConnections = new Hashtable();
    }
    
    /**
     * Closes all connections.
     */
    protected void finalize() throws Throwable {
        for (int i=0; i<POOL_SIZE; i++) {
            connections[i].close();
        }
    }
    
    
    /**
     * Fetches a new connection from the database.
     */
    private Connection getNewConnection() throws SQLException {
        try {
            Class.forName(DRIVER);
            LoggerFactory.getLogger(ConnectionPool.class).debug("ConnectionPool:constructor");
            return DriverManager.getConnection(DB_URL + "?autoReconnect=" + AUTO_RECONNECT, USERNAME, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new SQLException(ExceptionMessages.CONNECTION_NOT_FOUND + e.getMessage()); 
        }
    }    
    
    /**
     * Returns true if there is an available connection in the pool
     */
    private boolean connectionIsAvailable() {
        return usedConnections.size() < POOL_SIZE;
    }
    
    /**
     * Fetches a new connection from the connection pool
     */
    public Connection getConnection() throws SQLException, SystemException {
        synchronized (theConnectionPool) {
            while (!theConnectionPool.connectionIsAvailable()) {
                try {
                    theConnectionPool.wait(new Long(resource.getString("timeout")).longValue());
                } catch (InterruptedException ignore) {
                    throw new SystemException(ExceptionMessages.CONNECTION_NOT_AVAILABLE);
                }
                
            }
            // Returns the first available connection from the pool.
            Connection connection = null;
            for (int i=0; i<POOL_SIZE; i++) {
                connection = theConnectionPool.connections[i];
                if (!theConnectionPool.usedConnections.containsKey(connection)) {
                    break;
                }
            }
            
            theConnectionPool.usedConnections.put(connection, connection);
            
            return connection;
        }
    }
    
    /**
     * Returnerer a connection after use.
     */
    public void releaseConnection(Connection conn) throws SQLException {
        synchronized(theConnectionPool) { 
             theConnectionPool.usedConnections.remove(conn);
             theConnectionPool.notify();
        }  
    }

/**
  * Gets the unique instance of this singleton, constructing it if
  * necessary.
  *
  * @throws SQLException
  */
public static ConnectionPool Instance() throws SQLException {
    if (theConnectionPool == null) 
          theConnectionPool = new ConnectionPool();

    return theConnectionPool;
}

}