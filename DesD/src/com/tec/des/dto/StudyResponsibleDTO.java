package com.tec.des.dto;
import com.tec.shared.util.Nuller;
import com.tec.des.util.Constants;

/**
 * Data Transfer Object for a study responsible.
 *
 * @author : Norunn Haug Christensen
 */
public class StudyResponsibleDTO implements DTO{

  private String id = Nuller.getStringNull();
  // ListName is used when displaying people's name in a list: Lastname, Firstname
  private String listName = Nuller.getStringNull();
  // SingleName is used when displaying a single person's name: Firstname, Lastname 
  private String singleName = Nuller.getStringNull();  

  public StudyResponsibleDTO() {
    super();
  }
  
  public void setListName(String listName) {
    this.listName = listName;
  }

  public String getListName() {
    return listName;
  }
  
  public void setSingleName(String singleName) {
    this.singleName = singleName;
  }

  public String getSingleName() {
    return singleName;
  }

  
  public void setId(String id) {
    this.id = id;
  }
    
  public String getId() {
    return id;
  }
  
}