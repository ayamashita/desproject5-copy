package com.tec.des.dto;
import com.tec.shared.util.Nuller;
import com.tec.des.util.Constants;

/**
 * Data Transfer Object for a user.
 *
 * @author : Norunn Haug Christensen
 */
public class UserDTO implements DTO{

  private int id = Nuller.getIntNull();
  // ListName is used when displaying people's name in a list: Lastname, Firstname
  private String listName = Nuller.getStringNull();
  // SingleName is used when displaying a single person's name: Firstname, Lastname 
  private String singleName = Nuller.getStringNull();  
  private int roleId = Nuller.getIntNull();
  private String roleName = Nuller.getStringNull();

  public UserDTO() {
    super();
  }
  
  public void setListName(String listName) {
    this.listName = listName;
  }

  public String getListName() {
    return listName;
  }
  
  public void setSingleName(String singleName) {
    this.singleName = singleName;
  }

  public String getSingleName() {
    return singleName;
  }

  public void setRoleId(int roleId) {
    this.roleId = roleId;
  }

  public int getRoleId() {
    return roleId;
  }
  
  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleName() {
    return roleName;
  }
  
  public void setId(int id) {
    this.id = id;
  }
    
  public int getId() {
    return id;
  }
  
  public boolean hasRole() {
    return !Nuller.isNull(roleId);
  }

  public boolean isDbAdmin() {
    return roleId == Constants.ROLE_DATABASE_ADMIN;
  }
  
}