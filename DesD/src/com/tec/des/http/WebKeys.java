package com.tec.des.http;
import com.tec.des.util.Constants;

/**
 * This class contains keys that are used to 
 * store data in the different scopes of web-tier. 
 *
 * @author: Norunn Haug Christensen
 */
public class WebKeys  {
 
   protected WebKeys() {} // prevent instantiation

/*
 * Keys used for user authentication
 */
public final static String REQUEST_PARAM_USER_NAME = "username"; 
public final static String REQUEST_PARAM_PASSWORD = "password";
public final static String SESSION_PARAM_USER = "user"; 

/*
 * Request input parameters; these are sent by JSP pages.
 */
public final static String REQUEST_PARAM_USECASE = "usecase";
public final static String REQUEST_PARAM_SENDER_VIEW = "sender_view";

public final static String REQUEST_PARAM_SEARCH_TEXT = "search_free_text";
public final static String REQUEST_PARAM_SEARCH_TYPE_OF_STUDY = "search_type_of_study";
public final static String REQUEST_PARAM_SEARCH_END_DATE_FROM = "search_end_date_from";
public final static String REQUEST_PARAM_SEARCH_END_DATE_TO = "search_end_date_to";
public final static String REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES = "search_study_responsibles";
public final static String REQUEST_PARAM_SEARCH_SORT_BY = "search_sort_by";
public final static String REQUEST_PARAM_SEARCH_SORT_DESCENDING = "search_sort_descending";

public final static String REQUEST_PARAM_MAX_HIT_LIST_LENGTH = "max_list_length";
public final static String REQUEST_PARAM_START_POSITION = "start_position";

public final static String REQUEST_PARAM_STUDY_TYPES = "study_types";
public final static String REQUEST_PARAM_DELETE_STUDY_TYPE = "delete_study_type";
public final static String REQUEST_PARAM_ADD_STUDY_TYPE = "add_study_type";

public final static String REQUEST_PARAM_DELETE_STUDY_DURATION_UNIT = "delete_study_duration_unit";
public final static String REQUEST_PARAM_ADD_STUDY_DURATION_UNIT = "add_study_duration_unit";

public final static String REQUEST_PARAM_ALL_PEOPLE = "all_people";
public final static String REQUEST_PARAM_SELECTED_PEOPLE = "selected_people";

public final static String REQUEST_PARAM_STUDY_ID = "study_id";
public final static String REQUEST_PARAM_STUDY_NAME = "study_name";
public final static String REQUEST_PARAM_STUDY_TYPE = "study_type";
public final static String REQUEST_PARAM_STUDY_START_DATE = "study_start_date";
public final static String REQUEST_PARAM_STUDY_END_DATE = "study_end_date";
public final static String REQUEST_PARAM_STUDY_RESPONSIBLES = "study_responsibles";
public final static String REQUEST_PARAM_STUDY_RESPONSIBLE_IDS = "study_responsible_ids";
public final static String REQUEST_PARAM_STUDY_DESCRIPTION = "study_description";
public final static String REQUEST_PARAM_STUDY_PUBLICATIONS = "study_publications";
public final static String REQUEST_PARAM_STUDY_PUBLICATION_IDS = "study_publication_ids";
public final static String REQUEST_PARAM_STUDY_DURATION = "study_duration";
public final static String REQUEST_PARAM_STUDY_DURATION_UNIT = "study_duration_unit";
public final static String REQUEST_PARAM_STUDY_KEYWORDS = "study_keywords";
public final static String REQUEST_PARAM_STUDY_NO_STUNDENTS = "study_no_students";
public final static String REQUEST_PARAM_STUDY_NO_PROFESSIONALS = "study_no_professionals";
public final static String REQUEST_PARAM_STUDY_MATERIAL = "study_material";
public final static String REQUEST_PARAM_STUDY_MATERIAL_IDS = "study_material_ids";
public final static String REQUEST_PARAM_STUDY_NOTES = "study_notes";
public final static String REQUEST_PARAM_STUDY_OWNER = "study_owner";
public final static String REQUEST_PARAM_STUDY_LAST_EDITED_BY = "study_last_edited_by";

public final static String REQUEST_PARAM_ROLES = "roles";
public final static String REQUEST_PARAM_ONLY_ROLE = "only_role";

public final static String REQUEST_PARAM_STUDY_ID_DELETE = "study_id_delete";

public final static String REQUEST_PARAM_PUBLICATION_REMOVE = "publication_remove";
public final static String REQUEST_PARAM_PUBLICATION_ADD = "publication_add";

public final static String REQUEST_PARAM_STUDY_MATERIAL_DELETE = "material_delete";
public final static String REQUEST_PARAM_STUDY_MATERIAL_DESCRIPTION = "material_desc";
public final static String REQUEST_PARAM_STUDY_MATERIAL_URL = "material_url";
public final static String REQUEST_PARAM_STUDY_MATERIAL_TYPE = "material_type";
public final static String REQUEST_PARAM_STUDY_MATERIAL_FILE = "material_file";
public final static String REQUEST_PARAM_STUDY_MATERIAL_ID = "material_id";
public final static String REQUEST_PARAM_STUDY_MATERIAL_UPDATE_MODE = "study_material_update_mode";

public final static String REQUEST_PARAM_ADMIN_PAGE_TEXT = "admin_page_text";

public final static String REQUEST_PARAM_FROM_TEMP = "from_temp";
public final static String REQUEST_PARAM_OPENER_IS_MENU = "opener_is_menu";

/* 
 * Buttons
 */
public final static String REQUEST_PARAM_BUTTON_ADD = "button_add";
public final static String REQUEST_PARAM_BUTTON_REMOVE = "button_remove";

/*
 * Miscellaneous 
 */
public final static String REQUEST_PARAM_DATE_FIELD = "date_field";
public final static String REQUEST_PARAM_MAINTAIN_MODE = "maintain_mode";
public final static String REQUEST_PARAM_PRINTER_FRIENDLY = "printer_friendly";

/*
 * Request output parameters; these JavaBean components are used by 
 * JSP pages to present results.
 */
public final static String REQUEST_ERROR_MESSAGE = "error_message";
public final static String REQUEST_BEAN_SEARCH_CRITERIA = "searchCriteria";
public final static String REQUEST_BEAN_SEARCH_RESULT = "searchResult";
public final static String REQUEST_BEAN_STUDY = "study";
public final static String REQUEST_BEAN_STUDY_ID = "studyId";
public final static String REQUEST_BEAN_STUDY_TYPES = "studyTypes";
public final static String REQUEST_BEAN_STUDY_DURATION_UNITS = "studyDurationUnits";
public final static String REQUEST_BEAN_ALL_PEOPLE = "allPeople";
public final static String REQUEST_BEAN_SELECTED_PEOPLE = "selectedPeople";
public final static String REQUEST_BEAN_SELECTED_PUBLICATIONS = "selectedPublications";
public final static String REQUEST_BEAN_SELECTED_PUBLICATION_TITLES = "selectedPublicationTitles";
public final static String REQUEST_BEAN_SELECTED_PUBLICATION_IDS = "selectedPublicationIds";
public final static String REQUEST_BEAN_PEOPLE_ROLE = "peopleRole";
public final static String REQUEST_BEAN_ROLES = "roles";
public final static String REQUEST_BEAN_SELECTED_STUDY_MATERIAL = "selectedMaterial";
public final static String REQUEST_BEAN_STUDY_MATERIAL = "studyMaterial";
public final static String REQUEST_BEAN_ADMIN_TEXT = "adminText";
public final static String REQUEST_BEAN_GRAPHICAL_REPORT_ITEMS = "graphicalReportItems";

/*
 * Values
 */
public final static String VALUE_TRUE = "true";
public final static String VALUE_FALSE = "false";

}