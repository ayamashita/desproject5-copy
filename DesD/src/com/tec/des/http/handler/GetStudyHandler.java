package com.tec.des.http.handler;
import javax.servlet.http.*;
import com.tec.shared.util.Parser;
import com.tec.shared.util.Nuller;
import com.tec.des.http.*;
import com.tec.des.dto.UserDTO;
import com.tec.des.command.GetStudyCommand;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.command.GetStudyDurationUnitsCommand;

/**
 * 
 * Handles the get study use case. 
 *
 * @author Norunn Haug Christensen
 */
public class GetStudyHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   * If the user has a role, the fetched study is shown in edit mode, 
   * otherwise the study is shown read-only in the singel study report.
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    String printerFriendlyStr = request.getParameter(WebKeys.REQUEST_PARAM_PRINTER_FRIENDLY);
    boolean printerFriendly = !Nuller.isReallyNull(printerFriendlyStr) && Parser.parseBoolean(printerFriendlyStr);
    boolean showStudyAsReadOnly = true;
    String view = WebConstants.VIEW_SINGLE_STUDY_REPORT;

    if (printerFriendly) {
        view = WebConstants.VIEW_SINGLE_STUDY_REPORT_PF;
    } else {
        HttpSession session = request.getSession(false);
        if (session != null) {
            UserDTO user = (UserDTO) session.getAttribute(WebKeys.SESSION_PARAM_USER);
            if (user != null && user.hasRole()) { //User are allowed to open the study in edit mode
                showStudyAsReadOnly = false;
                view = WebConstants.VIEW_EDIT_STUDY;
            }
        }
    }
    
    GetStudyCommand command = new GetStudyCommand();
    command.setId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_ID)));
    command.setIsReadOnly(showStudyAsReadOnly);
    command.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY, command.getResult());

    if (view.equals(WebConstants.VIEW_EDIT_STUDY)) { 
        GetStudyTypesCommand studyTypeCommand = new GetStudyTypesCommand();
        studyTypeCommand.execute();
        request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, studyTypeCommand.getResult());
        
        GetStudyDurationUnitsCommand unitsCommand = new GetStudyDurationUnitsCommand();
        unitsCommand.execute();
        request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, unitsCommand.getResult());
    }

    return view;
    
  }
  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Nuller.getIntNull();
  }
  
}