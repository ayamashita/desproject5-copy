package com.tec.des.http.handler;
import javax.servlet.http.*;
import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.http.*;
import com.tec.des.dto.StudyDTO;
import com.tec.des.util.Constants;
import com.tec.des.command.GetStudyTypesCommand;
import com.tec.des.command.GetStudyDurationUnitsCommand;
import com.tec.des.command.NewStudyCommand;

/**
 * 
 * Initialize the new study view.
 *
 * @author Norunn Haug Christensen
 */
public class OpenNewStudyHandler extends UsecaseHandler { 

  /**
   * Initialize the view by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception { 

    GetStudyTypesCommand studyTypesCommand = new GetStudyTypesCommand();
    studyTypesCommand.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_TYPES, studyTypesCommand.getResult());

    GetStudyDurationUnitsCommand unitsCommand = new GetStudyDurationUnitsCommand();
    unitsCommand.execute();
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY_DURATION_UNITS, unitsCommand.getResult());
    
    StudyDTO study = new StudyDTO();
    NewStudyCommand newStudyCommand = new NewStudyCommand();
    newStudyCommand.execute();
    study.setId(newStudyCommand.getResult());
    request.setAttribute(WebKeys.REQUEST_BEAN_STUDY, study);

    return WebConstants.VIEW_NEW_STUDY;
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}
