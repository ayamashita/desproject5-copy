package com.tec.des.http.handler;
import javax.servlet.http.*;
import java.util.Vector;

import com.tec.shared.util.*;
import com.tec.shared.exceptions.UserException;
import com.tec.des.command.GetAllPeopleCommand;
import com.tec.des.command.GetSelectedPeopleCommand;
import com.tec.des.util.Constants;
import com.tec.des.dto.StudyResponsibleDTO;
import com.tec.des.http.*;

/**
 * 
 * Handles the Select Study Responsibles use case.
 *
 * @author Per Kristian Foss
 */
public class SelectStudyResponsiblesHandler extends UsecaseHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   */
  public String doHandle(HttpServletRequest request) throws Exception {

    String[] selectedPeople;
    if (request.getParameter(WebKeys.REQUEST_PARAM_BUTTON_ADD) != null) {
        selectedPeople = getSelectedPeopleAdd(request);
    } else {
        selectedPeople = getSelectedPeopleRemove(request);
    }
    
    GetAllPeopleCommand command_all = new GetAllPeopleCommand();   
    command_all.execute();     
    request.setAttribute(WebKeys.REQUEST_BEAN_ALL_PEOPLE, command_all.getResult());
    
    GetSelectedPeopleCommand command_select = new GetSelectedPeopleCommand();   
    command_select.setSelectedPeople(selectedPeople);
    command_select.execute();
    Vector selected = command_select.getResult();
    if (selected.isEmpty()) {
        StudyResponsibleDTO emptyResponsible = new StudyResponsibleDTO();
        emptyResponsible.setId(Constants.EMPTY_ID_STUDY_RESPONSIBLES);
        emptyResponsible.setListName(Constants.EMPTY_VALUE_STUDY_RESPONSIBLES);
        selected.add(emptyResponsible);
     } 
    
    request.setAttribute(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE, selected); 
    
    return WebConstants.VIEW_STUDY_RESPONSIBLES;
  }
   
   /**
   * Returns a String[] containing people selected to be study responsibles after the >> button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleAdd(HttpServletRequest request) throws UserException {
     
    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR);
    String[] newSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_ALL_PEOPLE);
    String[] totalSelected;
    
    if (newSelected == null) {
        totalSelected = new String[oldSelected.length];
        
        for (int i = 0; i < oldSelected.length; i++) {
            totalSelected[i] = oldSelected[i];
        }
    } else {
        totalSelected = new String[oldSelected.length + newSelected.length];     
            
        for (int i = 0; i < oldSelected.length; i++) {
            totalSelected[i] = oldSelected[i];
        }
        for (int i = oldSelected.length; i < (oldSelected.length + newSelected.length); i++) {
            totalSelected[i] = newSelected[i - oldSelected.length];
        }
    }
    
    return totalSelected;
  }

   /**
   * Returns a String[] containing people selected to be study responsibles after the << button is pressed.
   * @throws UserException
   */
  private String[] getSelectedPeopleRemove(HttpServletRequest request) throws UserException {

    String[] oldSelected = request.getParameter(WebKeys.REQUEST_BEAN_SELECTED_PEOPLE).split(Constants.ID_SEPARATOR);
    String[] removeSelected = request.getParameterValues(WebKeys.REQUEST_PARAM_SELECTED_PEOPLE);
    String[] newSelected = new String[oldSelected.length];
    StringBuffer current = new StringBuffer(" ");
    
    if (removeSelected == null) {
        for (int i = 0; i < oldSelected.length; i++) {
            newSelected[i] = oldSelected[i];
        }
    } else {
        for (int i = 0; i < oldSelected.length; i++) {
            current.replace(1, current.length(), oldSelected[i]);
            for (int y = 0; y < removeSelected.length; y++) {          
                if (current.toString().trim().equals(removeSelected[y].trim()))        
                    newSelected[i] = Constants.EMPTY_ID_STUDY_RESPONSIBLES;
            }
            if (newSelected[i] == null)
                newSelected[i] = current.toString();
        }
    }   
  
    return newSelected;
  }

  
  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}