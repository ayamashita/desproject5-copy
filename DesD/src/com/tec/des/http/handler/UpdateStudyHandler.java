package com.tec.des.http.handler;
import javax.servlet.http.HttpServletRequest;
import com.tec.shared.util.Parser;
import com.tec.shared.util.Nuller;
import com.tec.des.http.*;
import com.tec.des.util.Validator;
import com.tec.des.util.Constants;
import com.tec.des.dto.StudyDTO;
import com.tec.des.command.UpdateStudyCommand;

/**
 * 
 * Handles the update study use case. The update can be a temporary update
 * of the study caused by the user adding study responsibles, publications etc 
 * (changes which open a new window) or it can be a final update caused by the
 * user pressing the 'Save' button. If the update is final, after the update
 * all studies are fetched and presented to the user in the study overview report.
 * Otherwise the user are returned to the page he/she came from.
 *
 * @author Norunn Haug Christensen
 */
public class UpdateStudyHandler extends MaintainStudyHandler {

  /**
   * Handles the use case by executing the command.
   *
   * @return The name of the resulting view. 
   * @throws Exception
   */
  public String doHandle(HttpServletRequest request) throws Exception {
    
    StudyDTO study = getStudyFromRequest(request);      
    putParametersOnRequest(study, request);
    
    //Fetch the update mode for the study
    int updateMode = Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_MAINTAIN_MODE));  
    if (updateMode == Constants.MAINTAIN_MODE_FINAL)
        Validator.validateStudy(study);
    
    //Fetch the update mode for the study material
    int studyMaterialUpdateMode = Nuller.getIntNull();
    String studyMaterialUpdateModeStr = request.getParameter(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_UPDATE_MODE);
    
    if (!Nuller.isReallyNull(studyMaterialUpdateModeStr)) {
        studyMaterialUpdateMode = Parser.parseInt(studyMaterialUpdateModeStr);
    } 
    
    UpdateStudyCommand command = new UpdateStudyCommand(); 
    command.setStudy(study);
    command.setMode(updateMode);
    command.setStudyMaterialUpdateMode(studyMaterialUpdateMode);
    command.execute();
    
    if (updateMode == Constants.MAINTAIN_MODE_FINAL) {
        request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_CRITERIA, getSearchCriteria());  
        request.setAttribute(WebKeys.REQUEST_BEAN_SEARCH_RESULT, getAllStudies());
    } else {
        request.setAttribute(WebKeys.REQUEST_BEAN_STUDY, command.getResult());
        view = request.getParameter(WebKeys.REQUEST_PARAM_SENDER_VIEW);
    }
    
    return view;
    
  }

  /**
   * Return the required role for the usecase.
   *
   * @return The role required to execute the usecase. 
   */
  public int getRequiredRole() {
      return Constants.ROLE_STUDY_ADMIN;
  }
  
}