package com.tec.des.http.handler;

import javax.servlet.http.HttpServletRequest;

/**
 * Base class for all classes handling a web based use case. All use cases should
 * be handled by a class extending this class.
 *
 * @author Norunn Haug Christensen
 */
public abstract class UsecaseHandler {

  /**
   * Empty constructor is necessary because this object is instanitated
   * using the class loader.
   */
  public UsecaseHandler() {
  }

  /**
   * Handle the use case and returns the name of the resulting view.
   *
   * @return The name of the resulting view. 
   */
  public abstract String doHandle(HttpServletRequest request) throws Exception;

  /**
   * Return the required role for the usecase.
   * If no role is required, Nuller.getIntNull is returned.
   *
   * @return The role required to execute the usecase. 
   */
  public abstract int getRequiredRole();
  
}