package com.tec.des.util;
import java.util.Hashtable;

/**
 * Class generating colours.
 * The class has a string array of different colours which can be accessed by 
 * the method getNextColour(). 
 *
 * @author  Norunn Haug Christensen
 */
public class ColourGenerator {
    
    /*
     * Red #FF0000
     * Lime  #00FF00 
     * Blue  #0000FF 
     * Yellow  #FFFF00 
     * Cyan  #00FFFF 
     * Fuchsia  #FF00FF 
     * LightGrey  #D3D3D3 
     * SaddleBrown  #8B4513 
     * Darkorange  #FF8C00 
     * Purple  #800080 
     * Green  #008000 
     * CornflowerBlue  #6495ED 
     * Orchid  #DA70D6 
     * PowderBlue  #B0E0E6 
     * YellowGreen  #9ACD32 
     *
     */
    
    private String[] colours = {"#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#00FFFF", "#FF00FF", "#D3D3D3", "#8B4513", "#FF8C00", "#800080", "#008000", "#6495ED", "#DA70D6", "#B0E0E6", "#9ACD32"};
    private Hashtable usedColours = new Hashtable();
    
    /** Creates a new instance of ColourGenerator */
    public ColourGenerator() {
    }
    
   /**
    * Returns the next available colour from the string array of defined colours.
    */
    public String getNextColour() {

        String colour = null;
        for (int i=0; i<colours.length; i++) {
            colour = colours[i];
            if (!usedColours.containsKey(colour)) {
                break;
            }
        }
            
        usedColours.put(colour, colour);
            
        return colour;
        
    }
}
