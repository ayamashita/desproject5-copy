package com.tec.des.util;
/**
 * Class containing exception messages as String constants.
 *
 * @author :  Norunn Haug Christensen 
 */
public class ExceptionMessages {

 /**
  * The class can not be instantiated
  */
    private ExceptionMessages() {
  }

  public static final String UNKNOWN_EXCEPTION = "Unknown error.";
  public static final String SYSTEM_EXCEPTION = "System error.";
  public static final String CONNECTION_NOT_FOUND = "Connection to the database not found.";
  public static final String CONNECTION_NOT_AVAILABLE = "No available connection to the database.";

  // Handeling usecases
  public static final String NO_USECASE = "No action specified.";
  public static final String UNKNOWN_USECASE_VIEW = "The view of the usecase is unknown.";
  
  // Command
  public static final String COMMAND_NOT_READY = "The command is missing input.";
  public static final String MISSING_PARAMETERS = "Missing parameters.";
  
  // Search
  public static final String INVALID_SEARCH = "If you use the free text field the other fields must be empty."; 
  public static final String INVALID_DATE_PART1 = "The format of the date field with value "; 
  public static final String INVALID_DATE_PART2 = " is incorrect. The format must be yyyy/mm/dd."; 
  
  // User rights 
  public static final String INSUFFICIENT_RIGHTS = "Your do not have permission to perform this action.";
  public static final String NO_RIGHTS = "Your do not have permission to log on.";
  public static final String INVALID_USER_NAME_OR_PASSWORD = "Invalid user name and/or password.";
  public static final String EMPTY_USER_NAME = "Please type a user name.";
  public static final String EMPTY_PASSWORD = "Please type a password.";
  
  // Study type
  public static final String EMPTY_STUDY_TYPE = "New Study Type cannot be blank.";
  public static final String STUDY_TYPE_EXISTS = "Study Type already exists: ";  
  public static final String NO_STUDY_TYPE_SELECTED = "No Study Types were deleted.";
  public static final String STUDY_TYPES_IN_USE_PART1 = "The following Study Type(s) are in use: ";
  public static final String STUDY_TYPES_IN_USE_PART2 = ". No Study Types were deleted.";
  
  //Study duration unit
  public static final String EMPTY_DURATION_UNIT = "New Duration unit cannot be blank.";
  public static final String STUDY_DURATION_UNIT_EXISTS = "Duration unit already exists: ";  
  public static final String NO_DURATION_UNIT_SELECTED = "No Duration units were deleted.";
  public static final String STUDY_DURATION_UNITS_IN_USE_PART1 = "The following Duration unit(s) are in use: ";
  public static final String STUDY_DURATION_UNITS_IN_USE_PART2 = ". No Duration units were deleted.";
  
  public static final String NO_PEOPLE_SELECTED = "Please select at least one person.";
 
  
  //Study
  public static final String NO_STUDY = "The Study does no longer exist.";
  public static final String STUDY_NAME_EXISTS_PART1 = "A Study with name '";
  public static final String STUDY_NAME_EXISTS_PART2 = "' already exists.";
  public static final String NO_SUCH_STUDY_TYPE = "The Study Type you chose does no longer exist.";
  public static final String REQUIRED_STUDY_NAME = "Study Name is a required field.";
  public static final String REQUIRED_TYPE_OF_STUDY = "Type of Study is a required field.";
  public static final String REQUIRED_STUDY_END_DATE = "End of Study is a required field.";
  public static final String REQUIRED_STUDY_DESCRIPTION = "Study Description is a required field.";
  public static final String REQUIRED_STUDY_RESPONSIBLES = "Study Responsibles is a required field.";
  public static final String EMPTY_STUDY_DURATION = "You have chosen a duration unit, but not a value. Please type a number denoting the duration.";
  public static final String EMPTY_STUDY_DURATION_UNIT = "You have typed a duration without specifying the duration unit. Please denote the duration unit.";
  public static final String END_DATE_BEFORE_START_DATE = "End of Study cannot be before Start of Study.";
  
  //Study Material
  public static final String NO_STUDY_MATERIAL = "The Study Material does not longer exist.";
  public static final String REQUIRED_MATERIAL_DESCRIPTION = "Description is a required field.";
  public static final String REQUIRED_URL = "Url field cannot be empty if type 'Url' is selected.";
  public static final String REQUIRED_FILE = "File field cannot be empty if type 'File' is selected.";
  public static final String FILE_NOT_FOUND = "Selected file not found.";
  public static final String FILE_TOO_BIG = "The size of the selected file is too big, the maximum size is 16 MB.";
  public static final String NO_MATERIAL_SELECTED = "No Study Material is selected for deletion.";
}