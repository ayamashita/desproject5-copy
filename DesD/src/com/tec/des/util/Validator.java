package com.tec.des.util;
import java.util.Date;
import java.io.File;
import com.tec.des.dto.SearchDTO;
import com.tec.des.dto.StudyDTO;
import com.tec.des.dto.StudyMaterialDTO;
import com.tec.des.http.WebConstants;
import com.tec.shared.exceptions.UserException;
import com.tec.shared.util.Nuller;
import com.tec.shared.util.Parser;
import com.tec.server.log4j.*;


/**
 * Class with static methods for user input validation.
 *
 * @author :  Norunn Haug Christensen
 */
public class Validator  {
  private static final int DATE_LENGTH = 10;  
 /**
  * The class can not be instantiated
  */
  private Validator() { 
  }

 /**
  * Validates search criteria. Error messages are accumulated and thrown as one exception.
  * @throws UserException
  */ 
  public static void validateSearchCriteria(SearchDTO searchCriteria) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      
      if (!Nuller.isReallyNull(searchCriteria.getFreeText()) && 
            (!Nuller.isReallyNull(searchCriteria.getEndOfStudyFrom()) ||
             !Nuller.isReallyNull(searchCriteria.getEndOfStudyTo()) ||
             !Nuller.isReallyNull(searchCriteria.getStudyResponsibles()) ||
             !Nuller.isReallyNull(searchCriteria.getTypeOfStudy()))) {
        errorMessage.append(ExceptionMessages.INVALID_SEARCH);
      }
      
      if (!Nuller.isReallyNull(searchCriteria.getEndOfStudyFrom())) {
          if (searchCriteria.getEndOfStudyFrom().length() != DATE_LENGTH || searchCriteria.getEndOfStudyFrom().indexOf("/") != 4) {
              if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
              errorMessage.append(ExceptionMessages.INVALID_DATE_PART1 + 
                searchCriteria.getEndOfStudyFrom() + 
                ExceptionMessages.INVALID_DATE_PART2);
          } else {
            try {
                Parser.parseDate(searchCriteria.getEndOfStudyFrom(), Constants.DATE_FORMAT_EDIT);
            } catch (UserException u) {
                if (!Nuller.isNull(errorMessage.toString()))
                    errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                errorMessage.append(u.getMessage()); 
            }
          }
      }
      
      if (!Nuller.isReallyNull(searchCriteria.getEndOfStudyTo())) {
          if (searchCriteria.getEndOfStudyTo().length() != DATE_LENGTH || searchCriteria.getEndOfStudyTo().indexOf("/") != 4) {
              if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
              errorMessage.append(ExceptionMessages.INVALID_DATE_PART1 + 
                searchCriteria.getEndOfStudyTo() + 
                ExceptionMessages.INVALID_DATE_PART2);
          } else {
            try {
                Parser.parseDate(searchCriteria.getEndOfStudyTo(), Constants.DATE_FORMAT_EDIT); 
            } catch (UserException u) {
                if (!Nuller.isNull(errorMessage.toString()))
                    errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                errorMessage.append(u.getMessage());
            }
          }
      }
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }

 /**
  * Validates a study to be added or updated. Error messages are accumulated and thrown as one exception.
  * Required fields are:
  * - Study name
  * - Study description
  * - Study type
  * - Study responible
  * - End of study
  * @throws UserException
  */ 
  public static void validateStudy(StudyDTO study) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      
      if (Nuller.isReallyNull(study.getName())) 
        errorMessage.append(ExceptionMessages.REQUIRED_STUDY_NAME);
      
      if (Nuller.isReallyNull(study.getDescription())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_STUDY_DESCRIPTION); 
      }

      if (Nuller.isNull(study.getTypeId())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_TYPE_OF_STUDY); 
      }

      if (study.getResponsibles().isEmpty()) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_STUDY_RESPONSIBLES); 
      }
      
      if (Nuller.isReallyNull(study.getEndDate())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_STUDY_END_DATE); 
      }
      
      if (!Nuller.isReallyNull(study.getDuration())) {
        try { //Checks that the duration is a number
            Parser.parseInt(study.getDuration());
        } catch (UserException u) {
            if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
            errorMessage.append(u.getMessage());
        }
      }
      
      //If duration si denoted, duration unit can not be empty and vice versa
      if (!Nuller.isReallyNull(study.getDuration()) && Nuller.isNull(study.getDurationUnitId())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.EMPTY_STUDY_DURATION_UNIT); 
      }
          
      if (Nuller.isReallyNull(study.getDuration()) && !Nuller.isNull(study.getDurationUnitId())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.EMPTY_STUDY_DURATION); 
      }
      
      Date endDate = null;
      //Checks date format (correct format is yyyy/mm/dd)
      if (!Nuller.isReallyNull(study.getEndDate())) {
          if (study.getEndDate().length() != DATE_LENGTH || study.getEndDate().indexOf("/") != 4) {
              if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
              errorMessage.append(ExceptionMessages.INVALID_DATE_PART1 + 
                study.getEndDate() + ExceptionMessages.INVALID_DATE_PART2);
          } else {
            try {
                endDate = Parser.parseDate(study.getEndDate(), Constants.DATE_FORMAT_EDIT);
            } catch (UserException u) {
                if (!Nuller.isNull(errorMessage.toString()))
                    errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                errorMessage.append(u.getMessage());
            }
          }
      }
      
      //Checks date format (correct format is yyyy/mm/dd)
      if (!Nuller.isReallyNull(study.getStartDate())) {
          if (study.getStartDate().length() != DATE_LENGTH || study.getStartDate().indexOf("/") != 4) {
              if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
              errorMessage.append(ExceptionMessages.INVALID_DATE_PART1 + 
                study.getStartDate() + ExceptionMessages.INVALID_DATE_PART2);
          } else {
            try {
                Date startDate = Parser.parseDate(study.getStartDate(), Constants.DATE_FORMAT_EDIT);
                if (endDate != null && startDate != null && endDate.before(startDate)) {
                    if (!Nuller.isNull(errorMessage.toString())) 
                        errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                    errorMessage.append(ExceptionMessages.END_DATE_BEFORE_START_DATE);
                }
            } catch (UserException u) {
                if (!Nuller.isNull(errorMessage.toString()))
                    errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                errorMessage.append(u.getMessage());
            }
          }
      }
      
      if (!Nuller.isReallyNull(study.getNoOfStudentParticipants())) {
        try { //Checks that the attribute is a number
            Parser.parseInt(study.getNoOfStudentParticipants());
        } catch (UserException u) {
            if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
            errorMessage.append(u.getMessage());
        }
      }
      
      if (!Nuller.isReallyNull(study.getNoOfProfessionalParticipants())) {
        try { //Checks that the attribute is a number
            Parser.parseInt(study.getNoOfProfessionalParticipants());
        } catch (UserException u) {
            if (!Nuller.isNull(errorMessage.toString()))
                errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
            errorMessage.append(u.getMessage());
        }
      }
      
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
      
  }
    
  
  /**
  * Validates new study type. Error messages are accumulated and thrown as one exception.
  * @throws UserException
  */ 
  public static void validateAddStudyType(String studyType) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      if (studyType.trim().length() == 0) 
        errorMessage.append(ExceptionMessages.EMPTY_STUDY_TYPE);      
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
      
  }
  
  /**
  * Validates deletion of study types. Error messages are accumulated and thrown as one exception.
  * @throws UserException
  */ 
  public static void validateDeleteStudyTypes(String[] studyTypes) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      if (studyTypes == null) 
        errorMessage.append(ExceptionMessages.NO_STUDY_TYPE_SELECTED);      
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }

 /**
  * Validates new study duration unit. Error messages are accumulated and thrown as one exception.
  * @throws UserException
  */ 
  public static void validateAddStudyDurationUnit(String studyDurationUnit) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      if (studyDurationUnit.trim().length() == 0) 
        errorMessage.append(ExceptionMessages.EMPTY_DURATION_UNIT);      
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
      
  }
  
  /**
  * Validates deletion of study duration unit. Error messages are accumulated and thrown as one exception.
  * @throws UserException
  */ 
  public static void validateDeleteStudyDurationUnit(String[] studyDurationUnit) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      if (studyDurationUnit == null) 
        errorMessage.append(ExceptionMessages.NO_DURATION_UNIT_SELECTED);      
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }
  
  /**
  * Validates new Study Material to be added. Error messages are accumulated and thrown as one exception.
  * Required fields are:
  * - Description
  * - Url OR File.
  * @throws UserException
  */ 
  public static void validateStudyMaterial(StudyMaterialDTO material) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      
      if (Nuller.isReallyNull(material.getDescription())) 
        errorMessage.append(ExceptionMessages.REQUIRED_MATERIAL_DESCRIPTION);
      
      if (material.isFile()) {
        if (Nuller.isNull(material.getFileName())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_FILE); 
        } else {
            if (material.getFile() == null) {
                if (!Nuller.isNull(errorMessage.toString()))
                    errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                errorMessage.append(ExceptionMessages.FILE_NOT_FOUND); 
            } else {
                File file = (File)material.getFile();
                if (file.length() > Constants.MAX_FILE_SIZE) {
                    if (!Nuller.isNull(errorMessage.toString()))
                        errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
                    errorMessage.append(ExceptionMessages.FILE_TOO_BIG); 

                }
            }
        }
      } else {
        if (Nuller.isNull(material.getUrl())) {
          if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
          errorMessage.append(ExceptionMessages.REQUIRED_URL); 
        }
      }
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }
  
 /**
  * Validates deletion of Study Material.
  * Required is:
  * - At least one Study Material selected
  * @throws UserException
  */ 
  public static void validateDeleteStudyMaterial(String toBeDeleted) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      
      if (Nuller.isReallyNull(toBeDeleted)) 
        errorMessage.append(ExceptionMessages.NO_MATERIAL_SELECTED);
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }

 /**
  * Checks that the username and password are not empty.
  * @throws UserException
  */ 
  public static void validateUserNameAndPassword(String userName, String password) throws UserException {
      
      StringBuffer errorMessage = new StringBuffer();
      
      if (Nuller.isReallyNull(userName)) 
        errorMessage.append(ExceptionMessages.EMPTY_USER_NAME);

      if (Nuller.isReallyNull(password)) {
        if (!Nuller.isNull(errorMessage.toString()))
            errorMessage.append(Constants.ERROR_MESSAGE_SEPARATOR);
        errorMessage.append(ExceptionMessages.EMPTY_PASSWORD);
      }
      
      if (!Nuller.isNull(errorMessage.toString()))
        throw new UserException(errorMessage.toString());
  }  
  
  /**
  * Handles the use of special characters in strings
  */   
  public static String checkEscapes(String text) {
      StringBuffer sb = new StringBuffer();
      if (text!=null) {
          for (int i = 0; i < text.length(); i++) {
              char ch = text.charAt(i);
              if (ch == ';' || ch == '\'' || ch == '\\')
                sb.append('\\');
              sb.append(ch);
          }
      }
      return sb.toString();
  }
 
 /**
  * Replaces characters that harms JavaScript
  */   
  public static String replaceChar(String text) {
      StringBuffer sb = new StringBuffer(text);
      if (text!=null) {
          for (int i = 0; i < sb.length(); i++) {
              char ch = sb.charAt(i);
              if (ch == '"' || ch == '\'')
                sb.deleteCharAt(i);
          }
      }
      return sb.toString();
  }

 /**
  * Replaces enter (/n) with break <BR>.
  */   
  public static String replaceEnterWithBreak(String text) {
      StringBuffer sb = new StringBuffer();
      if (text!=null) {
          for (int i = 0; i < text.length(); i++) {
              char ch = text.charAt(i);
              if (ch == '\n')
                sb.append("<BR>");
              else 
                sb.append(ch);
          }
      }
      return sb.toString();
  }
  
}
