package com.tec.shared.exceptions;
/**
 * General exception class, the main node of the system's exception hierarchy.
 *
 * @author :  Norunn Haug Christensen
 */
public class GeneralException extends Exception  {

  /**
   * Creates a GeneralException with the specified message.
   */
  public GeneralException(String message) {
    super(message);
  }
}