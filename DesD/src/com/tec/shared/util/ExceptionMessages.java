package com.tec.shared.util;
/**
 * Class containing global exception messages as String constants.
 *
 * @author :  Norunn Haug Christensen
 */
public class ExceptionMessages {

 /**
  * The class can not be instantiated
  */
  private ExceptionMessages() {
  }

  public static final String NO_HITS = "No hits! Please try again.";
  public static final String TOO_MANY_HITS = "Too many hits!";
  
}