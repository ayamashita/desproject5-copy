package com.tec.shared.util;

/**
 * Class with static methods for handling null values for primitive datatypes
 * and String.
 *
 * @author :  Norunn Haug Christensen
 */
public class Nuller  {
        private static final int INTNULL = -999999999;
	private static final long LONGNULL = -999999999;
	private static final double DOUBLENULL = -999999999;
	private static final String STRINGNULL = "";

   /**
    * Nuller can not be instantiated.  
    */
  private  Nuller() {
    super();
  }

   /**
    * Returns true if the String equals the null value.
    *
    * @param s the String
    * @return 
    */
  public static boolean isNull(String s){
    return (STRINGNULL.equals(s));
  }

   /**
    * Returns true if the String equals the null value or null.
    *
    * @param s the String
    * @return 
    */
  public static boolean isReallyNull(String s){
    return ((s == null) || (STRINGNULL.equals(s)));
  }

  /**
    * Returns true if the int equals the null value.
    *
    * @param l the int value
    * @return 
    */
  public static boolean isNull(int l){
    return INTNULL == l;
  }
  
  /**
    * Returns true if the long equals the null value.
    *
    * @param l the long value
    * @return 
    */
  public static boolean isNull(long l){
    return LONGNULL == l;
  }

  /**
    * Returns true if the double equals the null value.
    *
    * @param d the double value
    * @return 
    */
  public static boolean isNull(double d){
    return DOUBLENULL == d;
  }

   /**
    * Returns the String null value
    *
    * @return 
    */
  public static String getStringNull(){
    return STRINGNULL;
  }

   /**
    * Returns the int null value
    *
    * @return 
    */

  public static int getIntNull(){
    return INTNULL;
  }  
  
  
  /**
    * Returns the long null value
    *
    * @return 
    */

  public static long getLongNull(){
    return LONGNULL;
  }  

   /**
    * Returns the double null value
    *
    * @return 
    */
  public static double getDoubleNull(){
    return DOUBLENULL;
  }  

  /**
   * Returns a String that is STRINGNULL if the original String is null.
   */
  public static String noNull(String orig) {
    if (orig == null) {
      return STRINGNULL;
    } else {
      return orig;
    }
  }
}