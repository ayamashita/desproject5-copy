import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TaskA {

	/**
	 * Get study and publication data for given person id
	 * @param id
	 */
	public void retrieveData(int id) {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");

			Statement s1 = conn.createStatement();

			System.out.println("Getting studies for person with id : " + id);
			s1
					.executeQuery("select s.* from study s join res_study rs on s.study_id = rs.study_id where rs.people_id =  "
							+ id + ";");

			ResultSet rs = s1.getResultSet();

			while (rs.next()) {
				int studyId = rs.getInt("study_id");
				String name = rs.getString("study_name");
				Statement s2 = conn.createStatement();
				System.out.println("Publications for study: " + name);
				s2.executeQuery("select p.* from "
						+ "publication p join pub_study ps on p.publication_id = ps.publication_id "
						+ "where ps.study_id = " + studyId + ";");
				
				ResultSet rs2 = s2.getResultSet();
				int count = 0;
				while (rs2.next()) {
					int pubId = rs2.getInt("publication_id");
					String pubName = rs2.getString("publication_title");
					System.out.println(++count+ ": " + "id = " + pubId + ", title = " +pubName) ;
				}
				
				rs2.close();
				s2.close();
			}
			rs.close();
			s1.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Cannot connect to database server");
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
		
	}
	
	public static void main (String[] args) {
		TaskA taskA = new TaskA();
		taskA.retrieveData(36);
	}
}
