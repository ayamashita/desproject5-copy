import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TaskB {

	public void retrieveData() {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");

			Statement s = conn.createStatement();

			s.executeQuery("SELECT st.study_type as type, COUNT(study_id) as count "
					+ "FROM study s join study_type st on s.study_type = st.study_type_id  GROUP BY st.study_type;");

			ResultSet rs = s.getResultSet();
			int count = 0;
			
			while (rs.next()) {
				System.out.print("" + rs.getObject("type") );
				System.out.print(" -> " + rs.getObject("count"));
				System.out.println();
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Cannot connect to database server");
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
	}

	public static void main(String[] args) {
		TaskB task = new TaskB();
		task.retrieveData();
	}
}
