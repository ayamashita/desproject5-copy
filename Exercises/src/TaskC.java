import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class TaskC {

	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;

	public TaskC() {
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
	}

	public void configure(String webServiceUrl) throws MalformedURLException {
		this.rpcConfig.setServerURL(new URL(webServiceUrl));
		this.rcpClient.setConfig(rpcConfig);
	}

	private Object[] getPeopleByEmail(String email) {
		try {
			HashMap hash = new HashMap();
			hash.put("email", email);
			Object[] methodParams = new Object[] { hash };

			Object resultSet = this.rcpClient.execute("getPeople", methodParams);

			return (Object[]) resultSet;
		} catch (XmlRpcException xmlException) {
			return new Object[] {};
		}

	}

	public void retrieveDataFromWS(List emails) {
		if (emails == null || emails.isEmpty()) {
			return;
		}

		Iterator email = emails.iterator();
		int count = 0;
		while (email.hasNext()) {
			Object[] peopleData = getPeopleByEmail((String) email.next());
			if (peopleData != null && peopleData.length > 0) {
				for (int i = 0; i < peopleData.length; i++) {
					HashMap peopleDataMap = (HashMap)peopleData[i];
					System.out.println(++count + ": " + peopleDataMap.get("email") + " -> "
							+ peopleDataMap.get("firstname") + " " + peopleDataMap.get("lastname"));
				}
			}
			
		}
	}

	public void retrieveDataFromDB(List emails) {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/des_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");

			Statement s = conn.createStatement();
			StringBuffer sbuf = new StringBuffer();
			if (emails != null) {
				Iterator iter = emails.iterator();
				while (iter.hasNext()) {
					sbuf.append("'" + iter.next() + "' , ");
				}

				if (sbuf.toString().endsWith(", ")) {
					sbuf.delete(sbuf.length() - 2, sbuf.length());
				}

				System.out.println(sbuf.toString());
			}

			s.executeQuery("SELECT * FROM people WHERE people_email in (" + sbuf.toString() + ");");

			ResultSet rs = s.getResultSet();
			int count = 0;

			while (rs.next()) {
				System.out.println(count++ + ": " + rs.getString("people_email") + " -> "
						+ rs.getString("people_first_name") + " " + rs.getString("people_family_name"));
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Cannot connect to database server");
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Database connection terminated");
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
	}

	public static void main(String[] args) {
		TaskC task = new TaskC();
		try {
			task.configure("http://simula.no:9080/simula/xmlrpcdes");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		}
		ArrayList list = new ArrayList();
		list.add("aiko@simula.no");
		list.add("carelius@simula.no");
//		task.retrieveDataFromDB(list);
		task.retrieveDataFromWS(list);
	}

}
