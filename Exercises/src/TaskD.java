import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class TaskD {
	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;
	
	public TaskD() {
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
	}
	
	public void configure(String webServiceUrl) throws MalformedURLException 
	{
		this.rpcConfig.setServerURL(new URL(webServiceUrl));
		this.rcpClient.setConfig(rpcConfig);
	}
	
	/**
	 * Gets people data from Simula and sorts them by jobtitle
	 * @return
	 */
	public Object[] retrieveData() {
		try
		{
			HashMap hash = new HashMap();
			hash.put("sort_on","jobtitle");
			Object[] methodParams = new Object[]{ hash };
	    	Object resultSet = this.rcpClient.execute("getPeople", methodParams);
	    	return (Object[])resultSet;
		}
		catch (XmlRpcException xmlException)
		{
			return new Object[]{};	
		}
	}
	
	public static void main(String[] args) {
		TaskD task = new TaskD();
		try {
			task.configure("http://simula.no:9080/simula/xmlrpcdes");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		}
		
		Object[] data = task.retrieveData();
		
		for (int i=0; i < data.length; i++) {
			HashMap peopleDataMap = (HashMap)data[i];
			System.out.println(i + ": " + peopleDataMap.get("jobtitle") + ",  "
					+ peopleDataMap.get("firstname") + " " + peopleDataMap.get("lastname"));
		}
		
	}
}
