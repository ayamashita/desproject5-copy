import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class TestConnection {

	public void testDBConnection()
	{
	       Connection conn = null;
	       try
	       {
	           String userName = "admin";
	           String password = "masterkey";
	           String url = "jdbc:mysql://localhost:3306/des_a";
	           Class.forName ("com.mysql.jdbc.Driver").newInstance ();
	           conn = DriverManager.getConnection (url, userName, password);
	           System.out.println ("Database connection established");
	                      Statement s = conn.createStatement ();
	           s.executeQuery ("SELECT * FROM people");
	           ResultSet rs = s.getResultSet ();
	           int count = 0;
	           while (rs.next ())
	           {
//	               int idVal = rs.getInt ("study_id");
	               int idPeople = rs.getInt ("people_id");
	               System.out.println (
//	                       "id = " + idVal
	                       //+ 
	                       ", name = " + idPeople);
	               ++count;
	           }
	           rs.close ();
	           s.close ();
	       }
	       catch (Exception e)
	       {
	           System.err.println ("Cannot connect to database server");
	       }
	       finally
	       {
	           if (conn != null)
	           {
	               try
	               {
	                   conn.close ();
	                   System.out.println ("Database connection terminated");
	               }
	               catch (Exception e) {  /*ignore close errors*/  }
	           }
	       }
	}

	
	public static void main(String[] args) {
		TestConnection conn = new TestConnection();
		conn.testDBConnection();
	}
}
