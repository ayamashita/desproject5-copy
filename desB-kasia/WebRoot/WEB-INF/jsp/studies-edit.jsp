<%@include file="include/include-top.jsp"%>

  <h1>
    <logic:present name="<%= Constants.FORM_STUDY %>" property="id">Edit study</logic:present>
    <logic:notPresent name="<%= Constants.FORM_STUDY %>" property="id">Register new study</logic:notPresent>
  </h1>

  <html:form action="/adm/studies/edit" method="post">

  <logic:messagesPresent>
    <table class="error">
      <tr>
        <td>
          <ul>
            <html:messages id="error">
              <li><bean:write name="error"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

  <%-- The main table that divides the page into two columns --%>
  <table class="layout">
    <tr>

      <%-- Column One --%>
      <td>
        study information
        <table class="container">
          <tr>
            <td>name*</td>
            <td><html:text property="name" style="width: 100%"/></td>
          </tr>
          <tr>
            <td>description*</td>
            <td><html:textarea property="description" style="width: 100%" rows="8"/></td>
          </tr>
          <tr>
            <td>type of study*</td>
            <td>
              <html:select property="typeId">
                <html:optionsCollection name="<%= Constants.BEAN_STUDY_TYPES %>" value="id" label="name"/>
              </html:select>
            </td>
          </tr>
          <tr>
            <td>responsible*</td>
            <td>
              <table class="container">
                <tr>
                  <th>available persons</th>
                  <th/>
                  <th>responsibles</th>
                </tr>
                <tr>
                  <td>
                    <logic:empty name="<%= Constants.FORM_STUDY %>" property="persons">
                      <html:select property="addResponsible" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_STUDY %>" property="persons">
                      <html:select property="addResponsible" multiple="true" size="5">
                        <html:optionsCollection property="persons" value="id" label="fullName"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                  <td style="text-align: center">
                    <html:submit property="add" value=">"/><br/>
                    <html:submit property="remove" value="<"/>
                  </td>
                  <td>
                    <logic:empty name="<%= Constants.FORM_STUDY %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_STUDY %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5">
                        <html:optionsCollection property="responsibles" value="id" label="fullName"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>number of student participants</td>
            <td><html:text property="noOfStudents" size="3"/></td>
          </tr>
          <tr>
            <td>number of professional participants</td>
            <td><html:text property="noOfProfessionals" size="3"/></td>
          </tr>
          <tr>
            <td>duration</td>
            <td>
              <html:text property="duration" size="3"/>
              <html:select property="durationUnitId">
                <html:optionsCollection name="<%= Constants.BEAN_DURATION_UNITS %>" value="id" label="name"/>
              </html:select>
            </td>
          </tr>
          <tr>
            <td>start (dd/mm-yyyy)</td>
            <td>
              <html:text property="startDay" size="2" maxlength="2"/> /
              <html:text property="startMonth" size="2" maxlength="2"/> -
              <html:text property="startYear" size="4" maxlength="4"/>
              <a href="javascript:openWindow('start');">
                <html:img page="/images/calendar.gif" border="0"/>
              </a>
            </td>
          </tr>
          <tr>
            <td>end (dd/mm-yyyy)*</td>
            <td>
              <html:text property="endDay" size="2" maxlength="2"/> /
              <html:text property="endMonth" size="2" maxlength="2"/> -
              <html:text property="endYear" size="4" maxlength="4"/>
              <a href="javascript:openWindow('end');">
                <html:img page="/images/calendar.gif" border="0"/>
              </a>
            </td>
          </tr>
          <tr>
            <td>keywords</td>
            <td><html:text property="keywords" style="width: 100%"/></td>
          </tr>
          <tr>
            <td>notes</td>
            <td><html:textarea property="notes" style="width: 100%" rows="8"/></td>
          </tr>
        </table>
      </td>
      <%-- End of Column One --%>

      <%-- Column Two --%>
      <td class="justified">
        attached publications
        <table class="container">
          <logic:iterate name="<%= Constants.FORM_STUDY %>" property="publications" id="publication" type="no.simula.Publication">
            <tr>
              <td>
                <html:link
                  href="<%= publication.getUrl()%>" >
                  <%
                  if(publication.getTitle().length() > 30) {
                    out.write("<span title=\"" + publication.getTitle() + "\">");
                    out.write(publication.getTitle().substring(0, 30) + "...");
                    out.write("</span>");
                  }
                  else {
                    out.write(publication.getTitle());
                  } %>
                </html:link>
              </td>
            </tr>
          </logic:iterate>
          <logic:empty name="<%= Constants.FORM_STUDY %>" property="publications">
            <tr>
              <td><i>--- empty ---</i></td>
            </tr>
          </logic:empty>
          <tr>
            <td class="button">
              <html:submit property="relations-publications" value="attach / detach"/>
            </td>
          </tr>
        </table>

        attached study material
        <table class="container">
          <logic:iterate name="<%= Constants.FORM_STUDY %>" property="studyMaterial" id="material" type="no.simula.des.StudyMaterial">
            <tr>
              <td>
                <logic:equal name="material" property="type" value="link">
                  <html:link href="<%= ((no.simula.des.Link)material).getUrl() %>">
                    <%
                    if(material.getDescription().length() > 30) {
                      out.write("<span title=\"" + material.getDescription() + "\">");
                      out.write(material.getDescription().substring(0, 30) + "...");
                      out.write("</span>");
                    }
                    else {
                      out.write(material.getDescription());
                    } %>
                  </html:link>
                </logic:equal>

                <logic:equal name="material" property="type" value="file">
                  <html:link page="<%= "/file/" + material.getId() + "/" + ((no.simula.des.File)material).getName() %>">
                    <%
                    if(material.getDescription().length() > 30) {
                      out.write("<span title=\"" + material.getDescription() + "\">");
                      out.write(material.getDescription().substring(0, 30) + "...");
                      out.write("</span>");
                    }
                    else {
                      out.write(material.getDescription());
                    } %>
                  </html:link>
                </logic:equal>
              </td>
            </tr>
          </logic:iterate>
          <logic:empty name="<%= Constants.FORM_STUDY %>" property="studyMaterial">
            <tr>
              <td><i>--- empty ---</i></td>
            </tr>
          </logic:empty>
          <tr>
            <td class="button">
              <html:submit property="relations-studymaterial" value="attach / detach"/>
            </td>
          </tr>
        </table>

        study administrators
        <table class="container">
          <logic:present name="<%= Constants.FORM_STUDY %>" property="lastEditedBy">
            <tr>
              <td>last edited by</td>
              <td><b><bean:write name="<%= Constants.FORM_STUDY %>" property="lastEditedBy.fullName"/></b></td>
            </tr>
          </logic:present>
          <logic:present name="<%= Constants.FORM_STUDY %>" property="ownedBy">
            <tr>
              <td>owned by</td>
              <td><b><bean:write name="<%= Constants.FORM_STUDY %>" property="ownedBy.fullName"/></b></td>
            </tr>
          </logic:present>
        </table>

      </td>
      <%-- End of Column Two --%>
      
    </tr>
  </table>
  <%-- End of Main Table --%>

  <html:submit property="save" value="save changes"/>
  <html:cancel value="cancel"/>

 </html:form>

<%@include file="include/include-bottom.jsp"%>