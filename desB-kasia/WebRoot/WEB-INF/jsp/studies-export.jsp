<%@page import="no.simula.SimulaFactory,no.simula.des.AdminModule"%>
<%@include file="include/include-top.jsp"%>

  <h1>Export study information</h1>

  <p>
    Click the link below to save study information as a 
    comma-separated file on your local computer.
  </p>

  <p>
    <html:link page="/export/studies.csv">
      <html:img page="/images/csv.gif" border="0" align="absmiddle"/>
      studies.csv
    </html:link>
  </p>

<%@include file="include/include-bottom.jsp"%>