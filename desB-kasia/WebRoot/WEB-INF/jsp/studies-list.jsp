<%@include file="include/include-top.jsp"%>

  <h1>Manage studies</h1>

  <logic:messagesPresent>
    <table class="error">
      <tr>
        <td>
          <ul>
            <html:messages id="error">
              <li><bean:write name="error"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

  <html:form action="/adm/studies/manage">
    <halogen:table name="<%= Constants.TABLE_MANAGE_STUDIES %>" selectableRows="true" id="id"/>
    <html:submit property="delete" value="delete selected"/>
  </html:form>

<%@include file="include/include-bottom.jsp"%>
