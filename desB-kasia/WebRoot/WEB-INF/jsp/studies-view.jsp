<%@page import="java.util.List"%>
<%@include file="include/include-top.jsp"%>
<%
  List nav = (List)request.getAttribute(no.halogen.presentation.web.Constants.NAVIGATION_PATH);
  String action = (String)nav.get(nav.size() - 1);
%>

<h1>
	Study details
</h1>

<%-- The main table that divides the page into two columns --%>
<table class="layout">
	<tr>

		<%-- Column One --%>
		<td>
			study information
			<table class="container">
				<tr>
					<td>
						name
					</td>
					<td>
						<bean:write name="study" property="name" />
					</td>
				</tr>
				<tr class="odd">
					<td>
						description
					</td>
					<td>
						<bean:write name="study" property="description" />
					</td>
				</tr>
				<tr>
					<td>
						type of study
					</td>
					<td>
						<bean:write name="study" property="type.name" />
					</td>
				</tr>
				<tr class="odd">
					<td>
						responsible
					</td>
					<td>
						<ul>
							<logic:iterate name="study" property="responsibles"
								id="responsible" type="no.simula.Person">
								<li>
									<html:link href="<%responsible.getUrl() %>">
										<bean:write name="responsible" property="fullName" />
									</html:link>
								</li>
							</logic:iterate>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						number of student participants
					</td>
					<td>
						<bean:write name="study" property="noOfStudents" />
					</td>
				</tr>
				<tr class="odd">
					<td>
						number of professional participants
					</td>
					<td>
						<bean:write name="study" property="noOfProfessionals" />
					</td>
				</tr>
				<tr>
					<td>
						duration
					</td>
					<td>
						<bean:write name="study" property="duration" />
						<bean:write name="study" property="durationUnit.name" />
					</td>
				</tr>
				<tr class="odd">
					<td>
						start
					</td>
					<td>
						<bean:write name="study" property="start" format="dd MMM yyyy" />
					</td>
				</tr>
				<tr>
					<td>
						end
					</td>
					<td>
						<bean:write name="study" property="end" format="dd MMM yyyy" />
					</td>
				</tr>
				<tr class="odd">
					<td>
						keywords
					</td>
					<td>
						<bean:write name="study" property="keywords" />
					</td>
				</tr>
				<tr>
					<td>
						notes
					</td>
					<td>
						<bean:write name="study" property="notes" />
					</td>
				</tr>
			</table>
		</td>
		<%-- End of Column One --%>

		<%-- Column Two --%>
		<td>
			<logic:notEmpty name="study" property="publications">
          attached publications
          <table class="container">
					<logic:iterate name="study" property="publications"
						id="publication" type="no.simula.Publication">
						<tr>
							<td>
								<html:link href="<%=publication.getUrl()%>">
									<%
                    if(publication.getTitle().length() > 30) {
                      out.write("<span title=\"" + publication.getTitle() + "\">");
                      out.write(publication.getTitle().substring(0, 30) + "...");
                      out.write("</span>");
                    }
                    else {
                      out.write(publication.getTitle());
                    } %>
								</html:link>
							</td>
						</tr>
					</logic:iterate>
				</table>
			</logic:notEmpty>

			<logic:notEmpty name="study" property="studyMaterial">
          attached study material
          <table class="container">
					<logic:iterate name="study" property="studyMaterial" id="material"
						type="no.simula.des.StudyMaterial">
						<tr>
							<td>
								<logic:equal name="material" property="type" value="link">
									<html:link
										href="<%= ((no.simula.des.Link)material).getUrl() %>">
										<%
                      if(material.getDescription().length() > 30) {
                        out.write("<span title=\"" + material.getDescription() + "\">");
                        out.write(material.getDescription().substring(0, 30) + "...");
                        out.write("</span>");
                      }
                      else {
                        out.write(material.getDescription());
                      } %>
									</html:link>
								</logic:equal>

								<logic:equal name="material" property="type" value="file">
									<html:link
										page="<%= "/file/" + material.getId() + "/" + ((no.simula.des.File)material).getName() %>">
										<%
                      if(material.getDescription().length() > 30) {
                        out.write("<span title=\"" + material.getDescription() + "\">");
                        out.write(material.getDescription().substring(0, 30) + "...");
                        out.write("</span>");
                      }
                      else {
                        out.write(material.getDescription());
                      } %>
									</html:link>
								</logic:equal>

							</td>
						</tr>
					</logic:iterate>
				</table>
			</logic:notEmpty>
			<html:form action="<%= action %>">
				<html:cancel value="back to list" />
			</html:form>
		</td>
		<%-- End of Column Two --%>

	</tr>
</table>
<%-- End of Main Table --%>

<html:link target="_blank" page="/pdf/study.pdf" paramId="id"
	paramName="study" paramProperty="id">
    printer friendly version
  </html:link>

<%@include file="include/include-bottom.jsp"%>