<%@include file="include/include-top.jsp"%>

    <h1>Confirm delete study material</h1>

    <html:form action="/adm/studymaterial/delete">

      <table class="container">
        <tr>
          <td>

            <logic:present name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" scope="request">
              <html:hidden name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" property="id"/>
              Are you sure you want to delete study material
              <b><bean:write name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" property="description"/></b>?
            </logic:present>

            <logic:notPresent name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" scope="request">
              study material
              <b><bean:write name="<%= Constants.DONT_DELETE_THIS_STUDYMATERIAL %>" property="description"/></b>
              could not be deleted. It is linked to the following studies:
              <ul>
                <logic:iterate name="<%= Constants.BEAN_STUDIES %>" id="study" type="no.simula.des.Study">
                  <li><bean:write name="study" property="name"/></li>
                </logic:iterate>
              </ul>
            </logic:notPresent>
            
          </td>
        </tr>
      </table>

      <logic:present name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" scope="request">
        <html:submit property="confirm" value="delete"/>
      </logic:present>
      <logic:notPresent name="<%= Constants.DELETE_THIS_STUDYMATERIAL %>" scope="request">
        <html:submit property="confirm" value="delete" disabled="true"/>
      </logic:notPresent>
      <html:cancel property="skip" value="skip"/>
      <html:cancel value="cancel"/>
    </html:form>

<%@include file="include/include-bottom.jsp"%>