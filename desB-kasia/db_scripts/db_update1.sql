alter table person_privilege_rel modify people_id varchar(32) not null;

alter table stu_study modify stu_owner varchar(32) not null;
alter table stu_study modify stu_lasteditedby varchar(32)  not null;
update stu_study set stu_owner = 'aiko', stu_lasteditedby = 'aiko';

alter table study_responsible_rel modify study_responsible_rel.people_id varchar(32) not null;

alter table publication_author modify people_id varchar(32) not null;
update publication_author set people_id = 'aiko';

alter table study_publication_rel modify publication_id varchar(64) not null;
