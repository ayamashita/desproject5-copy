package no.halogen.presentation.web;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableSource;
import no.simula.Simula;

/** This <code>Table</code> implements <CODE>HttpSessionBindingListener</CODE>
 * and <CODE>Observer</CODE> to allow it to register as an <code>Observer</code>
 * on the underlying data when the table is stored in the users
 * <code>HttpSession</code>
 * @author Stian Eide
 */
public class HttpSessionTable extends Table implements HttpSessionBindingListener, Observer {
  
  /** Creates a new instance of HttpSessionTable.
   * @param columns a <CODE>List</CODE> of columns that this <CODE>Table</CODE> will contain
   * @param source a class that implements <CODE>TableSource</CODE> and represents the content
   * source of this <CODE>Table</CODE>
   * @param rowsPerPage the number of rows to be displayed on each page
   * @param sortedBy the <CODE>SortableTableColumn</CODE> that this <CODE>Table</CODE> is initially
   * sorted by
   * @param observable the <CODE>Observable</CODE> object that will tell this <CODE>Table</CODE> when
   * it is manipulated
   */
  public HttpSessionTable(List columns, TableSource source, int rowsPerPage, SortableTableColumn sortedBy, Observable observable) {
    super(columns, source, rowsPerPage, sortedBy);
    this.observable = observable;
    observable.addObserver(this);
  }
  
  private Observable observable;
  
  /** This method is called by the <CODE>Observable</CODE> object when it is updated.
   * @param o the observed object that is the source of this update-call
   * @param arg an <CODE>Object</CODE> that represents arguments to this method-call
   */  
  public void update(Observable o, Object arg) {
    getSource().update();
  }
  
  /** This method is called when this object is bound to a <CODE>HttpSession</CODE>
   * @param httpSessionBindingEvent the event that identifies the session
   */  
  public void valueBound(HttpSessionBindingEvent httpSessionBindingEvent) {}
  
  /** This method is called when this object is unbound from a <CODE>HttpSession</CODE>
   * @param httpSessionBindingEvent the event that identifies the session
   */  
  public void valueUnbound(HttpSessionBindingEvent httpSessionBindingEvent) {
    observable.deleteObserver(this);
  } 
  
  /** Getter for property observable.
   * @return Value of property observable.
   *
   */
  public Observable getObservable() {
    return(observable);
  }
  
  /** Setter for property observable.
   * @param observable New value of property observable.
   *
   */
  public void setObservable(Observable observable) {
    this.observable = observable;
  }
}