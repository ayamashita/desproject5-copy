/**
 * @(#) CriteriumFactory.java
 */

package no.halogen.search;

import java.io.Serializable;


/**
 * Creates a new Criterium
 * 
 * @author Frode Langseth
 */
public interface CriteriumFactory extends Serializable
{
	/**
	 * Method create
	 * @param criterium String
	 * @return Criterium
	 */
	public Criterium create(String criterium);
	
}
