/**
 * @(#) Search.java
 */

package no.halogen.search;

import java.util.List;

/**
 * A search contains one or more criteria, and a method to execute the search.
 * 
 * @author Frode Langseth
 */
public interface Search
{
	/**
	 * Method addCriterium
	 * Adds <code>Criterium</code> that will form the search conditions
	 * @param attributes Criterium the <code>criteria</code> to search for 
	 */
	void addCriterium( Criterium attributes );
	
	/**
	 * Method getCriteria
	 * Returns the search <code>criteria</code>
	 * @return List the <code>criteria</code>
	 */
	List getCriteria( );
	
	/**
	 * Executes the search based on the criteria added to the search.
	 * 
	 * @return a list of objects. The type of the objects is determined by the dataBean
	 */
	List doSearch();	
	
	void addSortOrder(SortOrder order);
	
	List getSortOrders();
}
