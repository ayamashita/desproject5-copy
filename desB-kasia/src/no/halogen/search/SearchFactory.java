/**
 * @(#) CriteriumFactory.java
 */

package no.halogen.search;

import java.io.Serializable;
import java.util.List;

/**
 * Injterface of a factory that can be used to create a <code>Search</code>
 */
public interface SearchFactory extends Serializable
{
	/**
	 * Method create
	 * Creates a <code>Search</code>
	 * @param search String name of the <code>Search</code> to create
	 * @return Search the <code>Search</code>
	 */
	public Search create(String search);
	/**
	 * Method create
	 * Creates a <code>Search</code> and adds it's <code>criteria</code>
	 * @param search String name of the <code>Search</code> to create
	 * @param criteria List The <code>Search</code> <code>criteria</code> 
	 * @return Search the <code>Search</code>
	 */
	public Search create(String search, List criteria);
	public Search create(String search, List criteria, List sorting);
}
