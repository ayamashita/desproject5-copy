package no.halogen.search;

public interface SortOrder {
	public static final String SORT_ASC = "ASC";
	public static final String SORT_DESC = "sDESC";
	String getSortColumn();
	void setSortColumn(String column);
	String getSortOrder();
	void setSortOrder(String order);
}
