package no.halogen.search;

public class SortOrderImpl implements SortOrder {

	private String sortColumn;
	private String sortOrder;
	public SortOrderImpl() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SortOrderImpl(String sortColumn, String sortOrder) {
		super();
		this.sortColumn = sortColumn;
		this.sortOrder = sortOrder;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}


}
