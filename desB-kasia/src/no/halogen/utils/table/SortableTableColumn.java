package no.halogen.utils.table;

/**
 * A column with a clickable column headings that may be used to
 * sort the table by this column, either ascending or descending. This column 
 * will be sorted ascending initially, if not specified in the constructor.
 *
 * The target of the link in this column is set by specifying one of
 * <CODE>forward</CODE>, <CODE>href</CODE>, <CODE>page</CODE> or
 * <CODE>action</CODE>.
 *
 * @author  Stian Eide
 */
public class SortableTableColumn extends TableColumn {
  
  public SortableTableColumn(String name, String labelKey, String subProperty, boolean notNeeded) {
		super(name, labelKey, subProperty, notNeeded);
		// TODO Auto-generated constructor stub
	}

/** Creates a new <code>SortableTableColumn</code> sorted ascending.
   */
  public SortableTableColumn(String name, String labelKey, Integer maxSize) {
    super(name, labelKey, maxSize);
    ascending = true;
  }
  
  /** Creates a new <CODE>SortableTableColumn</CODE>. 
   * @param name the name of this colum
   * @param forward logical forward name for which to look up the context-relative URI (if specified)
   * @param href URL to be utilized unmodified (if specified)
   * @param page module-relative page for which a URL should be created (if specified)
   * @param labelKey the key to the label heading in application resources
   * @param action logical action name for which to look up the context-relative URI (if specified)
   * @param paramName the name of the parameter, which is appended to links in this colunm
   * @param property specify the property of the content of this column, which value will be appended
   * to links in this column
   * @param maxSize the maximum length of values. Longer values are clipped
   */
  public SortableTableColumn(
    String name, 
    String labelKey, 
    String forward,
    String href,
    String page,
    String action, 
    String paramName, 
    String property, 
    Integer maxSize
  ) {
    super(name, labelKey, forward, href, page, action, paramName, property, maxSize);
    ascending = true;
  }

  /** Creates a new <CODE>SortableTableColumn</CODE>. This column 
   * will be sorted as specified by <code>ascending</code>.
   *
   * @param name the name of this colum
   * @param forward logical forward name for which to look up the context-relative URI (if specified)
   * @param href URL to be utilized unmodified (if specified)
   * @param page module-relative page for which a URL should be created (if specified)
   * @param labelKey the key to the label heading in application resources
   * @param action logical action name for which to look up the context-relative URI (if specified)
   * @param paramName the name of the parameter, which is appended to links in this colunm
   * @param property specify the property of the content of this column, which value will be appended
   * to links in this column
   * @param maxSize the maximum length of values. Longer values are clipped
   */
  public SortableTableColumn(
    String name, 
    String labelKey, 
    String forward,
    String href,
    String page,
    String action, 
    String paramName, 
    String property, 
    Integer maxSize,
    boolean ascending
  ) {
    super(name, labelKey, forward, href, page, action, paramName, property, maxSize);
    this.ascending = ascending;
  }

  private boolean ascending; 
  
  /** Getter for property ascending.
   * @return Value of property ascending.
   *
   */
  public boolean isAscending() {
    return(ascending);
  }
  
  /** Setter for property ascending.
   * @param ascending New value of property ascending.
   *
   */
  public void setAscending(boolean ascending) {
    this.ascending = ascending;
  }

  public void toggleSorting() {
    ascending = (ascending) ? false : true;
  }
}