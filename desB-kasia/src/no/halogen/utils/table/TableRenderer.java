package no.halogen.utils.table;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.servlet.jsp.PageContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.RequestUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class renders <CODE>Table</CODE>s in different formats. Currently
 * supports HTML and XML. Should consider refactoring this into an interface
 * with two different implementations, one for HTML and one for XML.
 * 
 * @author Stian Eide
 */
public class TableRenderer {

	/**
	 * Creates a new <CODE>TableRenderer</CODE>.
	 * 
	 * @throws IOException
	 *             if the property file as specified by <CODE>RESOURCE</CODE>
	 *             cannot be found
	 */
	public TableRenderer() throws IOException {
		ClassLoader cl = TableRenderer.class.getClassLoader();
		resource = new Properties();
		resource.load(cl.getResourceAsStream(RESOURCE));
	}

	/** The package and name of the Struts massage resource file */
	public static final String RESOURCE = "resources/application.properties";
	private static Log log = LogFactory.getLog(TableRenderer.class);

	/** XML-element name for a table */
	public static final String TABLE = "table";
	/** XML-element name for a collection of columns */
	public static final String COLUMNS = "columns";
	/** XML-element name for a column */
	public static final String COLUMN = "column";
	/** XML-attribute name for a column name */
	public static final String COLUMN_NAME = "name";
	/** XML-element name for a collection of rows */
	public static final String ROWS = "rows";
	/** XML-element name for a row */
	public static final String ROW = "row";

	/** XML-element name for a list of items */
	public static final String LIST = "ul";
	/** XML-element name for a list item */
	public static final String LIST_ITEM = "li";

	private Properties resource;

	/** The maximum lenght of the values in the tooltip shown on pagenumbers */
	private static final Integer TITLE_LENGTH = new Integer(20);

	/**
	 * Returns this table and its content as an XML Dom Document.
	 * 
	 * @param table
	 *            the <CODE>Table</CODE> to be rendered
	 * @throws TableRenderException
	 *             if the table cannot be rendered
	 * @return a Dom Document representing the table
	 */
	public Document getXML(Table table) throws TableRenderException {

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xml = builder.newDocument();
			Element tableElement = xml.createElement(TABLE);

			Element columns = xml.createElement(COLUMNS);
			for (Iterator i = table.getColumns().iterator(); i.hasNext();) {
				TableColumn tableColumn = (TableColumn) i.next();
				Element columnElement = xml.createElement(COLUMN);
				String displayName = resource.getProperty(tableColumn.getLabelKey());
				columnElement.setAttribute(COLUMN_NAME, tableColumn.getName());
				columnElement.appendChild(xml.createTextNode(displayName));
				columns.appendChild(columnElement);
			}
			tableElement.appendChild(columns);

			Element rows = xml.createElement(ROWS);
			for (Iterator i = table.getContent().iterator(); i.hasNext();) {
				Object rowContent = i.next();

				Element rowElement = xml.createElement(ROW);
				for (Iterator j = table.getColumns().iterator(); j.hasNext();) {
					TableColumn tableColumn = (TableColumn) j.next();
					Object columnValue = tableColumn.getRowValue(rowContent);
					Element columnElement = xml.createElement(COLUMN);
					columnElement.setAttribute(COLUMN_NAME, tableColumn.getName());

					if (tableColumn instanceof ActionTableColumn) {
						// Do nothing
					} else if (columnValue instanceof List) {
						Element list = xml.createElement(LIST);
						for (Iterator k = ((List) columnValue).iterator(); k.hasNext();) {
							Element item = xml.createElement(LIST_ITEM);
							item.appendChild(xml.createTextNode(k.next().toString()));
							list.appendChild(item);
						}
						columnElement.appendChild(list);
					} else {
						if (tableColumn.getDecorator() == null) {
							if (columnValue != null) {
								columnElement.appendChild(xml.createTextNode(columnValue.toString()));
							}
						} else {
							columnElement.appendChild(xml
									.createTextNode(tableColumn.getDecorator().render(columnValue)));
						}
					}
					rowElement.appendChild(columnElement);
				}
				rows.appendChild(rowElement);
			}
			tableElement.appendChild(rows);
			xml.appendChild(tableElement);
			return (xml);
		}

		catch (Exception e) {
			log.error("Failed to render table as XML.", e);
			throw new TableRenderException("Failed to render table as XML.");
		}
	}

	/**
	 * Returns the specified <CODE>Table</CODE> as a HTML-fragment using a
	 * html table and creating navigation between several pages if the number of
	 * rows exceeds the <CODE>rowsPerPage</CODE> as specified in the
	 * <CODE>Table</CODE>.
	 * 
	 * This method is coupled to the jsp-api through the use of
	 * <CODE>pageContext</CODE>.
	 * 
	 * @param pageContext
	 *            the jsp context that this table is rendered in
	 * @param table
	 *            the table to be rendered
	 * @param id
	 *            the name of the property in a row that contains the value to
	 *            be used to identify checkboxes if the table has
	 *            <CODE>selectableRows</CODE>
	 * @param selectableRows
	 *            renders a column with checkboxes if <CODE>true</CODE>
	 * @throws TableRenderException
	 *             if this table cannot be rendered
	 * @return a <CODE>String</CODE> with a HTML-fragment representing the
	 *         table
	 * @see no.halogen.presentation.web.TableTag
	 */
	public String getHTML(PageContext pageContext, Table table, String id, boolean selectableRows)
			throws TableRenderException {
		try {
			StringBuffer html = new StringBuffer();
			html.append("<table class=\"tabletag\">");
			html.append("<tr>");
			for (Iterator i = table.getColumns().iterator(); i.hasNext();) {
				TableColumn column = (TableColumn) i.next();
				String label = "";
				if (column.getLabelKey() != null) {
					label = resource.getProperty(column.getLabelKey());
				}

				if (column instanceof SortableTableColumn) {
					if (table.getSortedBy().equals(column)) {
						if (table.getSortedBy().isAscending()) {
							html.append("<th class=\"sorted\" nowrap=\"nowrap\"><a href=\"?sortBy=" + column.getName()
									+ "\">" + label + " &#x25b2;</a></th>");
						} else {
							html.append("<th class=\"sorted\" nowrap=\"nowrap\"><a href=\"?sortBy=" + column.getName()
									+ "\">" + label + " &#x25bc;</a></th>");
						}
					} else {
						html.append("<th nowrap=\"nowrap\"><a href=\"?sortBy=" + column.getName() + "\">" + label
								+ "</a></th>");
					}
				} else if (column.getName() == null){
					html.append("<th nowrap=\"nowrap\"/>");
				} else{
					html.append("<th nowrap=\"nowrap\">" + label + "</th>");
				}
			}
			if (selectableRows) {
				html.append("<th></th>");
			}
			html.append("</tr>");

			boolean oddRow = true;
			for (Iterator i = table.getVisibleRows().iterator(); i.hasNext(); oddRow = !oddRow) {
				Object row = i.next();
				String cssClass = (oddRow) ? " class=\"odd\"" : "";
				html.append("<tr" + cssClass + ">");
				for (Iterator j = table.getColumns().iterator(); j.hasNext();) {

					String linkPre = "";
					String linkPost = "";
					TableColumn column = (TableColumn) j.next();

					Object columnValue = (column.getName() == null)? "" :column.getRowValue(row);

					// Allows special design of the sorted column.
					String sorted = "";
					if (table.getSortedBy().equals(column)) {
						sorted = " class=\"sorted\"";
					}

					if (column instanceof ActionTableColumn) {
						String key = ((ActionTableColumn) column).getButtonKey();
						String button = "[ " + resource.getProperty(key) + " ]";
						html.append("<td class=\"button\">" + getLink(pageContext, column, row, button) + "</td>");
					} else if (columnValue instanceof List) {
						html.append("<td" + sorted + "><ul>");
						for (Iterator k = ((List) columnValue).iterator(); k.hasNext();) {
							Object item = k.next();
							html.append("<li>" + getLink(pageContext, column, item, item) + "</li>");
						}
						html.append("</ul></td>");
					} else if (column.getName() == null && column.getLabelKey() != null) {
						String label = resource.getProperty(column.getLabelKey());
						html.append("<td" + sorted + ">" + getLink(pageContext, column, row, label) + "</td>");
					} else {
						if (column.getDecorator() == null) {
							html
									.append("<td" + sorted + ">" + getLink(pageContext, column, row, columnValue)
											+ "</td>");
						} else {
							html.append("<td" + sorted + ">"
									+ getLink(pageContext, column, row, column.getDecorator().render(columnValue))
									+ "</td>");
						}
					}
				}
				if (selectableRows) {
					Object rowId = getProperty(row, id);
					html.append("<td><input type=\"checkbox\" name=\"id[" + rowId + "].id\" value=\"true\"/></td>");
				}
				html.append("</tr>");
			}

			if (table.getNumberOfPages() > 1) {
				int noOfColumns = table.getColumns().size();
				if (selectableRows) {
					noOfColumns++;
				}
				html.append("<tr><td class=\"pagenumbers\" colspan=\"" + noOfColumns + "\">");
				if (table.getCurrentPage() > 1) {
					int prev = table.getCurrentPage() - 1;
					html.append("<a href=\"?page=" + (prev) + "\" ");
					html.append(getTitle(table, prev)).append(">");
					html.append("[ prev ]");
					html.append("</a>");
				} else {
					html.append("[ prev ]");
				}
				html.append("&nbsp;");
				for (int i = 1; i <= table.getNumberOfPages(); i++) {
					if (i == table.getCurrentPage()) {
						html.append("<span class=\"active\">" + i + "</span>");
					} else {
						html.append("<a href=\"?page=" + i + "\" ").append(getTitle(table, i)).append(">");
						html.append(i);
						html.append("</a>");
					}
					html.append("&nbsp;");
				}
				if (table.getCurrentPage() < table.getNumberOfPages()) {
					int next = table.getCurrentPage() + 1;
					html.append("<a href=\"?page=" + (next) + "\" ");
					html.append(getTitle(table, next)).append(">");
					html.append("[ next ]");
					html.append("</a>");
				} else {
					html.append("[ next ]");
				}
				html.append("</td></tr>");
			}
			html.append("</table>");

			return (html.toString());
		}

		catch (Exception e) {
			log.error("Failed to render table as HTML.", e);
			throw new TableRenderException("Failed to render table as HTML.");
		}
	}

	/**
	 * Calls the get-method on the property with name <code>name</name>
	 * on the object <code>obj</code>
	 * @return The return value of the invoked method
	 * @param obj The object to be invoked
	 * @param name The name of the property to get
	 * @throws TableRenderException if get-method could not be invoked
	 */
	private Object getProperty(Object obj, String name) throws TableRenderException {
		String method = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
		try {
			return (obj.getClass().getMethod(method, null).invoke(obj, null));
		} catch (Exception e) {
			throw new TableRenderException("Failed to call getter-method '" + method + "' on bean.");
		}
	}

	/**
	 * Creates a tooltip that may be used for the pagenumbers of a table.
	 * 
	 * @param table
	 *            the table that is rendered
	 * @param index
	 *            the index of the first visible row
	 * @throws Exception
	 *             if the tooltip cannot be created
	 * @return a <CODE>String</CODE> with the tooltip
	 */
	private String getTitle(Table table, int index) throws Exception {
		StringBuffer title = new StringBuffer("title=\"");
		int rowIndex = (index - 1) * table.getRowsPerPage();
		Object row = table.getSortedBy().getRowValue(table.getSource().getContent().get(rowIndex));
		title.append("'" + getMaxLengthString(row, TITLE_LENGTH) + "' =&gt; ");
		rowIndex = ((rowIndex + table.getRowsPerPage()) > table.getSource().getContent().size()) ? table.getSource()
				.getContent().size() - 1 : rowIndex + table.getRowsPerPage() - 1;
		row = table.getSortedBy().getRowValue(table.getSource().getContent().get(rowIndex));
		title.append("'" + getMaxLengthString(row, TITLE_LENGTH) + "'\"");
		return (title.toString());
	}

	/**
	 * Returns the clipped version of the string <code>string</code> if
	 * <code>chars</code> is not <code>null</code>. Will wrap the clipped
	 * version in a span-element with a title containing the complete string.
	 * This will display as a tooltip in most browsers.
	 * 
	 * @param string
	 *            the string that should be clipped if too short
	 * @param chars
	 *            the number of maximum characters that the string may contain
	 *            without being clipped
	 * @return the string, or a clipped version with three trailing dots ("...")
	 *         if it's longer than <CODE>chars</CODE>
	 */
	private String getClippedString(Object string, Integer chars) {

		if (chars == null || string.toString().length() <= chars.intValue()) {
			return (string.toString());
		} else {
			return ("<span title=\"" + string + "\">" + string.toString().substring(0, chars.intValue()) + "...</span>");
		}
	}

	/**
	 * Returns the clipped version of the string <code>string</code> if
	 * <code>chars</code> is not <code>null</code>.
	 * 
	 * @param string
	 *            the string that should be clipped if too short
	 * @param chars
	 *            the number of maximum characters that the string may contain
	 *            without being clipped
	 * @return the string, or a clipped version with three trailing dots ("...")
	 *         if it's longer than <CODE>chars</CODE>
	 */
	private String getMaxLengthString(Object string, Integer chars) {
		if (chars == null || string.toString().length() <= chars.intValue()) {
			return (string.toString());
		} else {
			return (string.toString().substring(0, chars.intValue()) + "...");
		}
	}

	/**
	 * This method returns a hyperlink if either a Struts action or a URL is
	 * specified in the column. The display value will be generated by the
	 * <CODE>toString()</CODE>-method of <CODE>string</CODE>. This value
	 * will be clipped as described in <CODE>getClippedString()</CODE>
	 * 
	 * @param pageContext
	 *            the jsp page context
	 * @param column
	 *            the column to check for a hyperlink definition
	 * @param obj
	 *            an object whos property with the name
	 *            <CODE>TableColumn.getProperty()</CODE> returns the value to
	 *            be used with the <CODE>TableColumn.getParamName()</CODE>-parameter
	 *            that is appended to this url
	 * @param string
	 *            the display value of this hyperlink
	 * @throws TableRenderException
	 *             if an error occurs during the inspection of this hyperlink
	 * @return either <CODE>string</CODE>, or <CODE>string</CODE> wrapped
	 *         in a hyperlink if an action or a url is specified for this column
	 * @see #getClippedString()
	 * @see TableColumn#getProperty()
	 * @see TableColumn#getParamName()
	 */
	private String getLink(PageContext pageContext, TableColumn column, Object obj, Object string)
			throws TableRenderException {
		String linkPre = "";
		String linkPost = "";
		String text = (string == null) ? "" : getClippedString(string.toString(), column.getMaxSize());

		String url = "";

		if (column instanceof TableColumn && column.getSubProperty() != null) {
			Object propValue = getProperty(obj, column.getSubProperty());
			url = propValue.toString();
			linkPre = "<a href=\"" + url + "\">";
			linkPost = "</a>";
		} else if (column.getHref() == null && column.getAction() == null) {
			if (column.getSubProperty() != null) {
				Object propValue = getProperty(obj, column.getSubProperty());
				url = propValue.toString();
				linkPre = "<a href=\"" + url + "\">";
				linkPost = "</a>";
			}
		} else {
			try {
				url = RequestUtils.computeURL(pageContext, column.getForward(), column.getHref(), column.getPage(),
						column.getAction(), null, null, false);
			} catch (MalformedURLException mfurle) {
				// Do nothing. Just don't make this a link
				// log.error("Could not create link for Struts action '" +
				// column.getAction() + "'.", mfurle);
			}

			String param = "";
			if (column.getParamName() != null && column.getProperty() != null) {
				Object propValue = getProperty(obj, column.getProperty());
				String separator = "?";
				if (url.indexOf("?") != -1) {
					separator = "&amp;";
				}
				param = separator + column.getParamName() + "=" + propValue;
			}

			if (url.length() > 0 || param.length() > 0) {
				linkPre = "<a href=\"" + url + param + "\">";
				linkPost = "</a>";
			}
		}
		return (linkPre + text + linkPost);
	}

	/**
	 * Getter for property resource.
	 * 
	 * @return Value of property resource.
	 * 
	 */
	public Properties getResource() {
		return (resource);
	}

	/**
	 * Setter for property resource.
	 * 
	 * @param resource
	 *            New value of property resource.
	 * 
	 */
	public void setResource(Properties resource) {
		this.resource = resource;
	}
}