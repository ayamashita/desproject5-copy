package no.simula;

import java.io.*;
import java.net.*;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.*;
import no.halogen.utils.table.TableRenderer;
import no.simula.MemoryCachingSimula;
import no.simula.des.presentation.web.actions.Constants;
import no.simula.wsclient.SimulaWSClient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This servlet pre-loads persistent data into memory cache, stores the table
 * renderer into the serlvet context and starts an update thread that updates
 * data from the SimulaWeb-database at intevals specified by
 * <CODE>simula.properties</CODE>.
 * <p>
 * This servlet should be started before all other servlets when this
 * webapplication is started by the web container (e.g. Tomcat).
 * <p>
 * When this servlet is destroyed it will stop the update thread.
 * 
 * @author Stian Eide
 */
public class ConfigServlet extends HttpServlet {

	private static Log log = LogFactory.getLog(ConfigServlet.class);
	private Simula simula;
	private static final String CONFIG = "no/simula/simula.properties";
	private static final String UPDATE_INTERVAL = "simulaweb.update.interval";
	private UpdateThread updateThread;

	/**
	 * Initializes the servlet.
	 * 
	 * @param config
	 *            the servlet config
	 * @throws ServletException
	 *             never
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			// configure webservices
			String simulaWSUrl = config.getServletContext().getInitParameter("simula_ws_url");
			String loginWSUrl = config.getServletContext().getInitParameter("login_ws_url");
			System.setProperty("no.machina.simula.simula_ws_url", simulaWSUrl);
			System.setProperty("no.machina.simula.login_ws_url", loginWSUrl);

			// Pre-load simula to avoid heavy processing for first user
			simula = SimulaFactory.getSimula();

			// Store TableRenderer in application scope
			TableRenderer renderer = new TableRenderer();
			config.getServletContext().setAttribute(Constants.TABLE_RENDERER, renderer);

			if (simula instanceof MemoryCachingSimula) {
				MemoryCachingSimula cache = (MemoryCachingSimula) simula;
				ClassLoader cl = ConfigServlet.class.getClassLoader();
				Properties pr = new Properties();
				pr.load(cl.getResourceAsStream(CONFIG));
				int interval = Integer.parseInt(pr.getProperty(UPDATE_INTERVAL));
				updateThread = new UpdateThread(cache, interval);
				updateThread.start();
			}

		}

		catch (Exception e) {
			log.error("Could not initialize ConfigServlet.", e);
		}
	}

	/**
	 * Destroys the servlet.
	 */
	public void destroy() {
		// Stop the update thread so Tomcat will stop gracefully
		updateThread = null;
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             never
	 * @throws IOException
	 *             if a print writer cannot be retrieved
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.close();
	}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             never
	 * @throws IOException
	 *             if a print writer cannot be retrieved
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             never
	 * @throws IOException
	 *             if a print writer cannot be retrieved
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return the description
	 */
	public String getServletInfo() {
		return "Configures the DES Web Application";
	}

	/**
	 * This thread updates external data in the memory cache at the specified
	 * interval.
	 */
	class UpdateThread extends Thread {

		private int updateInterval;
		private MemoryCachingSimula cache;

		/**
		 * Creates a new update thread for the given cache at the specified
		 * interval.
		 * 
		 * @param cache
		 *            the memory cache to update
		 * @param interval
		 *            the number of minutes between every update
		 */
		UpdateThread(MemoryCachingSimula cache, int interval) {
			this.cache = cache;
			updateInterval = interval;
		}

		/** Starts the thread. */
		public void run() {
			long lastUpdate = System.currentTimeMillis();
			Thread myThread = Thread.currentThread();
			log.info("Started UpdateThread with an update interval of " + updateInterval);
			try {
				while (myThread == updateThread) {
					log.debug("New iteration of UpdateThread.");
					this.sleep(5000); // Wait for five seconds
					// Minutes in config, millisecs here
					if (System.currentTimeMillis() - lastUpdate > updateInterval * 1000 * 60) {
						updateSimulaweb();
						lastUpdate = System.currentTimeMillis();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void updateSimulaweb() {
			cache.updateExternalData();
		}
	}
}