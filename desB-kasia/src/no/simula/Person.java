package no.simula;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import no.halogen.persistence.Persistable;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.simula.wsclient.StringIdObject;

import java.io.Serializable;


/** A person is someone that has a relation to the DES-application. This
 * includes roles as a privileged user, a study responsible or as an author
 * of publications.
 */
public class Person implements StringIdObject, Comparable, Serializable {

  /** Creates an empty person. */  
  public Person() {}
  
  /** Creates a person with the specified id.
   * @param id the id
   */  
  public Person(String id) {
    this.id = id;
  }
  
  /** Creates a person with the specified id, first name, family name, email and list
   * of privileges.
   * @param id the id
   * @param firstName the first name
   * @param familyName the family name
   * @param email the e-mail
   * @param privileges a list of privileges
   */  
  public Person(String id, String firstName, String familyName, String email, List privileges) {
    this.id = id;
    this.firstName = firstName;
    this.familyName = familyName;
    this.email = email;
    this.privileges = privileges;
  }
  
  private String id;
  private String firstName;  
  private String familyName;
  private String email;
  private List privileges;
  private String url;
    
  /** Returns the list of privileges that this person have.
   * @return the list of privileges for this person
   */  
  public List getPrivileges( ) {
    return(privileges);
  }
  
  /** Grants, i.e.&nbsp;adds, a privilege to this person
   * @param privilege the privilege to add
   */  
  public void grantPrivilege(Privilege privilege) {
    if(!hasPrivilege(privilege)) {
      privileges.add(privilege);
    }
  }
  
  /** Returns whether this person has a certain privilege.
   * @param privilege the privilege to check
   * @return <CODE>true</CODE> if this person has the given privilege
   */  
  public boolean hasPrivilege(Privilege privilege) {
    return(privileges.contains(privileges));
  }
  
  /** Revokes, i.e.&nbsp;removes, a privilege from this person
   * @param privilege the privilege to remove
   */  
  public void revokePrivilege(Privilege privilege) {
    privileges.remove(privilege);
  }
  
  /** Getter for property familyName.
   * @return Value of property familyName.
   *
   */
  public String getFamilyName() {
    return(familyName);
  }
  
  /** Setter for property familyName.
   * @param familyName New value of property familyName.
   *
   */
  public void setFamilyName(String familyName) {
    this.familyName = familyName;
  }
  
  /** Getter for property firstName.
   * @return Value of property firstName.
   *
   */
  public String getFirstName() {
    return(firstName);
  }
  
  /** Setter for property firstName.
   * @param firstName New value of property firstName.
   *
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * Returns the persons name in the form "FirstName FamilyName"
   * @return The full name of this person
   */
  public String getFirstAndFamilyName() {
    return(firstName + " " + familyName);
  }

  /**
   * Returns the persons name in the form "FamilyName, FirstName"
   * @return The full name of this person
   */
  public String getFamilyAndFirstName() {
    return(familyName + ", " + firstName);
  }
  
  /**
   * Returns the persons name in the form "FamilyName, FirstName"
   * @return The full name of this person
   */
  public String getFullName() {
    return(getFamilyAndFirstName());
  }
  
  /** Returns the full name of this person as specified by the
   * <CODE>getFullName</CODE>-method.
   * @return the full name of this person
   */  
  public String toString() {
    return(getFullName());
  }

  public String getId() {
    return(id);
  }
  
  public void setId(String id) {
    this.id = id;
  }

  /** Returns whether this person is equal to another person. Two persons are equal if
   * their ids are equal.
   * @param o another person
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(((Person)o).id.equals(this.id));
  }  
  
  /** Getter for property email.
   * @return Value of property email.
   *
   */
  public String getEmail() {
    return(email);
  }
  
  /** Setter for property email.
   * @param email New value of property email.
   *
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /** Sets the privileges of this person
   * @param privileges list of priviliges to be set for this person
   */
  public void setPrivileges(List privileges) {
    this.privileges = privileges;
  } 
  
  /** Compares two persons by their ids.
   * @param o another person
   * @return the value <code>0</code> if this id is equal to the other id; a value less than
   * <code>0</code> if this id is numerically less than the other id; and a value
   * greater than <code>0</code> if this id is numerically greater than the other id
   * (signed comparison).
   */  
  public int compareTo(Object o) {
    return(id.compareTo(((Person)o).id));
  }

public void setId(Integer id) {
	throw new RuntimeException("Method not allowed!");
	
}

public String getUrl() {
	return url;
}

public void setUrl(String url) {
	this.url = url;
}
}