package no.simula;

import java.io.Serializable;
import no.halogen.persistence.Persistable;
import no.simula.wsclient.StringIdObject;

/** A publication is written by persons and may be related to studies. Publications
 * are external to DES in that they are managed by another application and DES has
 * read-only access to this data.
 */
public class Publication implements StringIdObject, Serializable {
  
  /** Creates an empty publication. */  
  public Publication() {}
  
  /** Creates a publication with the specified id, title and list of authors (as a
   * comma-separated {@link java.lang.String}).
   * @param id the id
   * @param title the title
   * @param authors a comma-separated string with authors
   */  
  public Publication(String id, String title, String authors, String url) {
    this.id = id;
    this.title = title;
    this.authors = authors;
    this.url = url;
  }

  private String id;
  private String title;
  private String authors;
  private String url;
  
  /** Getter for property title.
   * @return Value of property title.
   *
   */
  public String getTitle() {
    return(title);
  }  
  
  /** Setter for property title.
   * @param title New value of property title.
   *
   */
  public void setTitle(String title) {
    this.title = title;
  }
  
  /** Returns a comma-separated list of authors.
   * @return a comma-separated list of authors
   */
  public String getAuthors() {
    return(authors);
  }
  
  /** Sets a comma-separated list of authors.
   * @param authors a comma-separated list of authors
   */
  public void setAuthors(String authors) {
    this.authors = authors;
  }
  
  /** Returns the title of this publication.
   * @return the title of this publication
   */  
  public String toString() {
    return(getTitle());
  }
  
  public String getId() {
    return(id);
  }

  public void setId(String id) {
    this.id = id;
  }
  
  /** Returns whether this publication is equal to another publication. Two
   * publications are equal if their ids are equal.
   * @param o another publication
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(this.id.equals(((Publication)o).id));
  }

public String getUrl() {
	return url;
}

public void setUrl(String url) {
	this.url = url;
}
}