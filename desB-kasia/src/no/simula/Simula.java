package no.simula;

import java.util.List;
import no.halogen.search.Criterium;
import no.simula.des.AdminModule;
import no.simula.des.AggregatedStudyReport;
import no.simula.des.Column;
import no.simula.des.DurationUnit;
import no.simula.des.Link;
import no.simula.des.PersonalizedReport;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportSortOrder;
import no.simula.des.Study;
import no.simula.des.StudyMaterial;
import no.halogen.search.Search;
import no.halogen.utils.ObservableList;
import no.simula.des.File;
import no.simula.des.StudyType;

/**
 * This interface represents Simula as an organization and is the single point
 * of entry for finding, updating and deleting information about studies.
 * 
 * How the information is stored is transparant to the application that uses
 * <CODE>Simula</CODE> and it is used as if <CODE>Simula</CODE> itself holds
 * all objects in memory.
 * 
 * The application will use {@link no.simula.SimulaFactory} to get an
 * implementation of <code>Simula</code>.
 */
public interface Simula {

	/**
	 * The name that {@link no.halogen.search.SearchFactory} use to create a
	 * study search
	 */
	public static final String STUDY_SEARCH = "study";

	// ---------------------------------------------------------------------------
	// //////////////// STUDY ////////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Adds a new study to <CODE>Simula</CODE>. The id will be set
	 * automatically by <CODE>Simula</CODE> and should not be set by the
	 * application. The value will be ignored if it is set.
	 * 
	 * @return the study with <CODE>id</CODE> set
	 * @param study
	 *            the <CODE>Study</CODE> to add
	 * @throws SimulaException
	 *             if the study cannot be added
	 */
	public Study addStudy(Study study) throws SimulaException;

	/**
	 * Deletes a study from <CODE>Simula</CODE>.
	 * 
	 * @param id
	 *            the id of the study to be deleted
	 * @throws SimulaException
	 *             if an error occurs while deleting the study
	 * @return <CODE>true</CODE> if the study is deleted
	 */
	public boolean deleteStudy(Integer id) throws SimulaException;

	/**
	 * Returns the study with the specified id.
	 * 
	 * @param id
	 *            the id of the study
	 * @throws SimulaException
	 *             if the study cannot be retrieved
	 * @return the study
	 */
	public Study getStudy(Integer id) throws SimulaException;

	/**
	 * Returns a list of studies that satisfy the list of criteria
	 * 
	 * @param criteria
	 *            the criteria that the studies must satisfy
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of studies that satisfy the list of criteria
	 */
	public List getStudiesByCriteria(List criteria) throws SimulaException;
	public List getStudiesByCriteriaAndSorting(List criteria, List sorting) throws SimulaException;
	/**
	 * Returns a list of studies with one study for each id in the specified
	 * list of ids.
	 * 
	 * @param ids
	 *            a list of study ids
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of studies with the specified ids
	 */
	public List getStudiesByIds(List ids) throws SimulaException;

	/**
	 * Returns a list of all the studies that contains the study material with
	 * the specified id.
	 * 
	 * @param id
	 *            the study material id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of studies that contain the specified study material
	 */
	public List getStudiesByStudyMaterial(Integer id) throws SimulaException;

	/**
	 * Returns a list of all studies that are of the study type as specified by
	 * id.
	 * 
	 * @param id
	 *            the study type id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all studies of the specified study type
	 */
	public List getStudiesByStudyType(Integer id) throws SimulaException;

	/**
	 * Returns a list of all studies that have durations with the unit specified
	 * by id.
	 * 
	 * @param id
	 *            the duration unit id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all studies with a duration unit as specified
	 */
	public List getStudiesByDurationUnit(Integer id) throws SimulaException;

	/**
	 * Updates a study in <CODE>Simula</CODE>. This requires that the study
	 * already exists and that the id is set in <CODE>study</CODE>.
	 * 
	 * @param study
	 *            the study to store permanently
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the study was updated in the permanent
	 *         store
	 */
	public boolean storeStudy(Study study) throws SimulaException;

	/**
	 * Returns all studies in a CSV-format using <CODE>fieldSeparator</CODE>
	 * as a field delimiter.
	 * 
	 * @param fieldSeparator
	 *            the field delimiter to use
	 * @throws SimulaException
	 *             if an error occurs
	 * @return all studies as CSV
	 */
	public String getStudiesAsCSV(char fieldSeparator) throws SimulaException;

	// Criterium Methods

	/**
	 * Returns a "study by end date"-criterion.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a "study by end date"-criterion
	 */
	public Criterium getStudyByEndDateCriterium() throws SimulaException;

	/**
	 * Returns a "study by responsibles"-criterion.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a "study by responsibles"-criterion.
	 */
	public Criterium getStudyByResponsiblesCriterium() throws SimulaException;

	/**
	 * Returns a "study by study type"-criterion.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a "study by study type"-criterion
	 */
	public Criterium getStudyByStudyTypeCriterium() throws SimulaException;

	/**
	 * Returns a "study by freetext"-criterion.
	 * 
	 * @throws SimulaException
	 *             an error occurs
	 * @return a "study by freetext"-criterion
	 */
	public Criterium getStudyByFreetextCriterium() throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// STUDY MATERIAL ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Adds a new study material to <CODE>Simula</CODE>. The id will be set
	 * automatically by <CODE>Simula</CODE> and should not be set by the
	 * application. The value will be ignored if it is set.
	 * 
	 * @param material
	 *            the study material to add
	 * @throws SimulaException
	 *             if the study material cannot be added
	 * @return the study material with <CODE>id</CODE> set
	 */
	public StudyMaterial addStudyMaterial(StudyMaterial material) throws SimulaException;

	/**
	 * Deletes a study material from <CODE>Simula</CODE>.
	 * 
	 * @param id
	 *            the id of the study material to delete
	 * @throws SimulaException
	 *             if an error occurs while deleting the study material
	 * @return <CODE>true</CODE> if the study material was deleted
	 */
	public boolean deleteStudyMaterial(Integer id) throws SimulaException;

	/**
	 * Returns the study material with the specified id
	 * 
	 * @param id
	 *            the study material id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the study material
	 */
	public StudyMaterial getStudyMaterial(Integer id) throws SimulaException;

	/**
	 * Returns a list of study material that match the specified list of study
	 * material ids.
	 * 
	 * @param ids
	 *            the ids of the study material to retrive
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of study material with the specified ids
	 */
	public List getStudyMaterialByIds(List ids) throws SimulaException;

	/**
	 * Returns a study material by a file name. This returns a <CODE>File</CODE>
	 * because only study material of type <CODE>File</CODE> have a file name.
	 * 
	 * @param name
	 *            the file name
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a file with the specified filename
	 * @deprecated Due to late-minute changes in the application logic, this
	 *             method should not be used. Files are now addressed by their
	 *             ids and several files with the same file name may exist in
	 *             the database. Hence this method will yield unknown results.
	 */
	public File getStudyMaterialByFilename(String name) throws SimulaException;

	/**
	 * Returns a list of all study material from <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of study material
	 */
	public List getStudyMaterials() throws SimulaException;

	/**
	 * Updates a study material in <CODE>Simula</CODE>. This requires that
	 * the study material already exists and that the id is set in
	 * <CODE>material</CODE>.
	 * 
	 * @param material
	 *            the study material to update
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the study material was updated
	 */
	public boolean storeStudyMaterial(StudyMaterial material) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// ADMIN MODULE //////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Returns the admin module from <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the admin module
	 */
	public AdminModule getAdminModule() throws SimulaException;

	/**
	 * Updates the admin module in <CODE>Simula</CODE>. This requires that
	 * the id is set correctly in <CODE>adminModule</CODE>. The best way to
	 * do this is to retrieve the admin module with the
	 * <CODE>getAdminModule</CODE>-method first and not change the
	 * id-property before calling this method.
	 * 
	 * @param adminModule
	 *            the admin module to update
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the admin module was updated
	 */
	public boolean storeAdminModule(AdminModule adminModule) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// AGGREGATED STUDY REPORT ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Returns the latest aggregated study report from <CODE>Simula</CODE>.
	 * 
	 * @deprecated This method is not depricated per se, but not implemented as
	 *             the aggregated study report is generated on-the-fly instead
	 *             of being generated at given interval.
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the aggregated study report
	 */
	public AggregatedStudyReport getAggregatedStudyReport() throws SimulaException;

	/**
	 * Stores a new aggregated study report in <CODE>Simula</CODE>.
	 * 
	 * @param report
	 *            the aggregated study report to store
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the study report was stored
	 * @deprecated This method is not depricated per se, but not implemented as
	 *             the aggregated study report is generated on-the-fly instead
	 *             of being generated at given interval.
	 */
	public boolean storeAggregatedStudyReport(AggregatedStudyReport report) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// PERSONS ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Returns a person with the specified id
	 * 
	 * @param id
	 *            the id of the person
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the person with the specified id
	 */
	public Person getPerson(String id) throws SimulaException;

	/**
	 * Returns a person with the specified e-mail address.
	 * 
	 * @param email
	 *            the e-mail address of the person
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the person with the specified e-mail address
	 */
	public Person getPersonByEmail(String email) throws SimulaException;

	/**
	 * Returns a list of all persons stored in <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all persons
	 */
	public List getPersons() throws SimulaException;

	/**
	 * Returns the privilege with the specified id.
	 * 
	 * @param id
	 *            the id of the privilege
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the privilege with the specified id
	 */
	public Privilege getPrivilege(Integer id) throws SimulaException;

	/**
	 * Returns a list of all privileges that are stored in <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all privileges
	 */
	public List getPrivileges() throws SimulaException;

	/**
	 * Returns a list of all persons that have the specified privilege.
	 * 
	 * @param priv
	 *            the privilge of the persons
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all persons with the specified privilege
	 */
	public List getPersonsByPrivilege(Privilege priv) throws SimulaException;

	/**
	 * Updates all the specified persons in <CODE>Simula</CODE>. In this
	 * case, this is limited to updating the privileges of the persons in the
	 * DES-application. All other data on the persons are read-only.
	 * 
	 * @param persons
	 *            a list of persons to be updated
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the number of updated persons
	 */
	public Integer updatePersons(List persons) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// STUDY TYPES ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Adds a new study type to <CODE>Simula</CODE>. The id will be set
	 * automatically by <CODE>Simula</CODE> and should not be set by the
	 * application. The value will be ignored if it is set.
	 * 
	 * @param type
	 *            the study type to add
	 * @throws SimulaException
	 *             if the study type cannot be added
	 * @return the study type with <CODE>id</CODE> set
	 */
	public StudyType addStudyType(StudyType type) throws SimulaException;

	/**
	 * Deletes a study type from <CODE>Simula</CODE>.
	 * 
	 * @param id
	 *            the id of the study type to delete
	 * @throws SimulaException
	 *             if an error occurs while deleting the study type
	 * @return <CODE>true</CODE> if the study type was deleted
	 */
	public boolean deleteStudyType(Integer id) throws SimulaException;

	/**
	 * Returns the study type with the specified id
	 * 
	 * @param id
	 *            the study type id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the study type
	 */
	public StudyType getStudyType(Integer id) throws SimulaException;

	/**
	 * Returns a list of study type that match the specified list of study type
	 * ids.
	 * 
	 * @param ids
	 *            the ids of the study type to retrive
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of study type with the specified ids
	 */
	public List getStudyTypesByIds(List ids) throws SimulaException;

	/**
	 * Returns a list of all study type from <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of study type
	 */
	public List getStudyTypes() throws SimulaException;

	/**
	 * Updates a study type in <CODE>Simula</CODE>. This requires that the
	 * study material already exists and that the id is set in
	 * <CODE>material</CODE>.
	 * 
	 * @param type
	 *            the study type to update
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the study type was updated
	 */
	public boolean storeStudyType(StudyType type) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// DURATION UNITS ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Adds a new duration unit to <CODE>Simula</CODE>. The id will be set
	 * automatically by <CODE>Simula</CODE> and should not be set by the
	 * application. The value will be ignored if it is set.
	 * 
	 * @param unit
	 *            the duration unit to add
	 * @throws SimulaException
	 *             if the duration unit cannot be added
	 * @return the duration unit with <CODE>id</CODE> set
	 */
	public DurationUnit addDurationUnit(DurationUnit unit) throws SimulaException;

	/**
	 * Deletes a duration unit from <CODE>Simula</CODE>.
	 * 
	 * @param id
	 *            the id of the duration unit to delete
	 * @throws SimulaException
	 *             if an error occurs while deleting the duration unit
	 * @return <CODE>true</CODE> if the duration unit was deleted
	 */
	public boolean deleteDurationUnit(Integer id) throws SimulaException;

	/**
	 * Returns the duration unit with the specified id
	 * 
	 * @param id
	 *            the duration unit id
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the duration unit
	 */
	public DurationUnit getDurationUnit(Integer id) throws SimulaException;

	/**
	 * Returns a list of duration unit that match the specified list of duration
	 * unit ids.
	 * 
	 * @param ids
	 *            the ids of the duration unit to retrive
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of duration unit with the specified ids
	 */
	public List getDurationUnitsByIds(List ids) throws SimulaException;

	/**
	 * Returns a list of all duration unit from <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of duration unit
	 */
	public List getDurationUnits() throws SimulaException;

	/**
	 * Updates a duration unit in <CODE>Simula</CODE>. This requires that the
	 * study material already exists and that the id is set in
	 * <CODE>material</CODE>.
	 * 
	 * @param unit
	 *            the duration unit to update
	 * @throws SimulaException
	 *             if an error occurs
	 * @return <CODE>true</CODE> if the duration unit was updated
	 */
	public boolean storeDurationUnit(DurationUnit unit) throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// PUBLICATIONS ///////////////////////
	// ---------------------------------------------------------------------------

	/**
	 * Returns the publication with the specified id.
	 * 
	 * @param id
	 *            the id of the publication
	 * @throws SimulaException
	 *             if an error occurs
	 * @return the publication with the specified id
	 */
	public Publication getPublication(String id) throws SimulaException;

	/**
	 * Returns a list of all publications stored in <CODE>Simula</CODE>.
	 * 
	 * @throws SimulaException
	 *             if an error occurs
	 * @return a list of all publications
	 */
	public List getPublications() throws SimulaException;

	// ---------------------------------------------------------------------------
	// ///////////////// REPORTS ///////////////////////
	// ---------------------------------------------------------------------------

	public List getReportSortOptions() throws SimulaException;

	public List getReportCriteria() throws SimulaException;

	public PersonalizedReport addReport(PersonalizedReport report) throws SimulaException;

	public boolean storeReport(PersonalizedReport report) throws SimulaException;

	public PersonalizedReport getReport(Integer reportId) throws SimulaException;

	public List getReportColumns() throws SimulaException;

	public Column getReportColumn(Integer integer) throws SimulaException;

	public List getReports() throws SimulaException;
	
	public ReportSortOrder getSortOrderById(Integer id) throws SimulaException;
	
	public ReportCriterium getCriteriumById(Integer id) throws SimulaException;

	public List getReportsByIds(List deletableReportIds);

	public List getReportsByOwner(String remoteUser);

	public void deleteReport(Integer id) throws SimulaException;

}