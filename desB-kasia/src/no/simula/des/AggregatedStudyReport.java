package no.simula.des;

import java.util.Date;

/** This is a aggregated study report rendered as an image. It represents the data
 * at a specific time.
 */
public class AggregatedStudyReport {
  
  /** Creates an empty aggregates study report */  
  public AggregatedStudyReport() {}

  /** Creates an aggregated study report representing the data at the given <CODE>date</CODE>
   * represented by the image given by <CODE>bitmap</CODE>.
   * @param date the time and date this report was created
   * @param bitmap the generated report
   */  
  public AggregatedStudyReport(Date date, byte[] bitmap) {
    this.date = date;
    this.bitmap = bitmap;
  }
  
  private Integer id;
  private Date date;
  private byte[] bitmap;
  
  public Integer getId() {
    return(id);
  }

  public void setId(Integer id) {
    this.id = id;
  }
    
  /** Returns the time this report was generated
   * @return the time this report was generated
   */  
  public Date getDate() {
    return(date);
  }

  /** Sets the time this report was generated
   * @param date the time this report was generated
   */  
  public void setDate(Date date) {
    this.date = date;
  }

  /** Returns the image of the generated report
   * @return the image of the generated report
   */  
  public byte[] getBitmap() {
    return(bitmap);
  }
  
  /** Sets the image of the generated report
   * @param bitmap the image of the generated report
   */  
  public void setBitmap(byte[] bitmap) {
    this.bitmap = bitmap;
  }
}