package no.simula.des;

import java.io.Serializable;

import no.halogen.persistence.Persistable;

public class Column implements Persistable, Serializable {

	public final static String COLUMN_TABLE = "tbl_column";
	public final static String COLUMN_ID = "column_id";
	public final static String COLUMN_NAME = "column_name";
	public final static String COLUMN_DISPLAY_NAME = "column_display_name";
	public final static String COLUMN_TYPE = "column_type";
	public final static String COLUMN_PROP_NAME = "column_prop_name";

	public final static String COLUMN_LAST_EDITED_BY = "last_edited_by";
	public final static String COLUMN_STUDY_PUBLICATIONS = "study_publications";
	public final static String COLUMN_STUDY_RESPONSIBLE = "study_responsible";
	public final static String COLUMN_STUDY_OWNER = "study_owner";

	public final static int DEFAULT_COLUMN_TYPE = 1;

	private Integer id;
	private String name;
	private String displayName;
	private Integer type;
	private String property;

	public Column(Integer id) {
		super();
		this.id = id;
	}

	public Column() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Column && ((Column) obj).id.equals(this.id)) {
			return (true);
		} else {
			return (false);
		}
	}
}
