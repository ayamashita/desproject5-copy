package no.simula.des;

import java.io.Serializable;

import no.halogen.persistence.Persistable;

public class ReportCriterium implements Persistable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String REPORT_CRITERIA_TABLE = "report_criteria";
	public final static String REPORT_CRITERIA_ID = "report_criteria_id";
	public final static String REPORT_CRITERIA_NAME = "report_criteria_name";

	public final static int REPORT_CRITERIA_OR_ID = 1;
	public final static int REPORT_CRITERIA_AND_ID = 2;

	private Integer id;
	private String criteriaName;

	public ReportCriterium(Integer criteriaId) {
		super();
		this.id = criteriaId;
	}

	public ReportCriterium() {
		super();
	}

	public ReportCriterium(Integer criteriaId, String criteriaName) {
		super();
		this.id = criteriaId;
		this.criteriaName = criteriaName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer criteriaId) {
		this.id = criteriaId;
	}

	public String getCriteriaName() {
		return criteriaName;
	}

	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}
	
	public boolean equals(Object obj) {
		if (obj instanceof ReportCriterium && ((ReportCriterium) obj).id.equals(this.id)) {
			return (true);
		} else {
			return (false);
		}
	}
}
