package no.simula.des;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import no.halogen.utils.table.TableSource;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** This is a table source that has the results from a study search as content. The
 * <CODE>update</CODE>-method will execute the search.
 * @author Stian Eide
 */
public class ReportQueryTableSource implements TableSource {
  
  /** Creates a new instance of <CODE>StudySearchTableSource</CODE> with the list of
   * criteria.
   * @param criteria the criteria that the search result must satisfy
   */
  public ReportQueryTableSource(List criteria, List sorting) {
    this.criteria = criteria;
    this.sorting = sorting;
    update();
  }

  public ReportQueryTableSource(List criteria2, ArrayList sorting2, String[] responsibleIds) {
	this(criteria2, sorting2);
	Iterator iter = content.iterator();
	
	while (iter.hasNext()) {
		Study study = (Study)iter.next();
		List responsibles = study.getResponsibles();
		Iterator ii = responsibles.iterator();
		HashSet ids = new HashSet();
		while (ii.hasNext()) {
			Person person = (Person)ii.next();
			ids.add(person.getId());
		}
		
		if (!ids.containsAll(Arrays.asList(responsibleIds))) {
			iter.remove();
		}
	}
}

private static Log log = LogFactory.getLog(ReportQueryTableSource.class);

  private List criteria;
  private List content;
  private List sorting;
  
  /** Returns a list of studies that satisfy the list of criteria.
   * @return a list of studies that satisfy the list of criteria
   */  
  public List getContent() {
    return(content);
  }

  /** Execurtes the study search with the list of criteria. */  
  public void update() {
    try {
      Simula simula = SimulaFactory.getSimula();
      content = simula.getStudiesByCriteriaAndSorting(criteria, sorting);
    }
    catch(Exception e) {
      log.error("Could not update Table Source.", e);
    }
  }
}