package no.simula.des;

import java.io.Serializable;

import no.halogen.persistence.Persistable;

public class ReportSortOrder implements Persistable, Serializable {

	public final static String REPORT_SORT_ORDER_TABLE = "report_sort_order";
	public final static String REPORT_SORT_ORDER_ID = "report_sort_order_id";
	public final static String REPORT_SORT_ORDER_NAME = "report_sort_order_name";
	public final static String REPORT_SORT_ORDER_DESC = "report_sort_order_desc";

	private Integer id;
	private String sortOrderName;
	private String sortOrderDesc;

	public ReportSortOrder(Integer id, String sortOrderName, String sortOrderDesc) {
		this.id = id;
		this.sortOrderName = sortOrderName;
		this.sortOrderDesc = sortOrderDesc;
	}

	public ReportSortOrder() {

	}

	public ReportSortOrder(Integer integer) {
		id = integer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer sortOrderId) {
		this.id = sortOrderId;
	}

	public String getSortOrderName() {
		return sortOrderName;
	}

	public void setSortOrderName(String sortOrderName) {
		this.sortOrderName = sortOrderName;
	}

	public String getSortOrderDesc() {
		return sortOrderDesc;
	}

	public void setSortOrderDesc(String sortOrderDesc) {
		this.sortOrderDesc = sortOrderDesc;
	}

	public boolean equals(Object obj) {
		if (obj instanceof ReportSortOrder && ((ReportSortOrder) obj).id.equals(this.id)) {
			return (true);
		} else {
			return (false);
		}
	}

}
