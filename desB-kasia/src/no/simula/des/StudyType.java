package no.simula.des;

import java.io.Serializable;
import no.halogen.persistence.Persistable;

/** The study type categorizes a study as one of several available study types. */
public class StudyType implements Persistable, Comparable, Serializable {
  
  /** Creates an empty study type. */  
  public StudyType() {}
  
  /** Creates a study type with the specified id.
   * @param id the id
   */  
  public StudyType(Integer id) {
    this.id = id;
  }
  
  /** Creates a study type with the specified id and name.
   * @param id the id
   * @param name the name of this type of study
   */  
  public StudyType(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  private Integer id;
  private String name;
  
  public Integer getId() {
    return(id);
  }

  public void setId(Integer id) {
    this.id = id;
  }
  
  /** Returns the name of this studytype.
   * @return the name of this studytype
   */  
  public String getName() {
    return(name);
  }  
  
  /** Sets the name of this studytype.
   * @param name the name of this studytype
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /** Returns whether this type of study is equal to another type of study. Two types
   * of study are equal if their ids are equal.
   * @param o anther type of study
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(((StudyType)o).id.equals(this.id));
  }
  
  /** Compares two <code>StudyType</code>s by comparing their name attributes.
   * @param o the <code>StudyType</code> that should be compared to <code>this</code>
   * @return the value <code>0</code> if the argument is a string
   * 		lexicographically equal to this string; a value less than
   * 		<code>0</code> if the argument is a string lexicographically
   * 		greater than this string; and a value greater than
   * 		<code>0</code> if the argument is a string lexicographically
   * 		less than this string.
   */
  public int compareTo(Object o) {
    return(this.getName().compareTo(((StudyType)o).getName()));
  }
  
  /** Returns the name of this type of study.
   * @return he name of this type of study
   */  
  public String toString() {
    return(getName());
  }
}