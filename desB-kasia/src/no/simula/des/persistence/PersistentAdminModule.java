/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.AdminModule;
import no.simula.des.statements.AdminModuleStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentAdminModule implements PersistentObject, Serializable {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentAdminModule.class);

  /**
   * Creates a new admin opening page text
   * 
   * @param instance - The admin text that will be saved
   * @return the identificator of the new admin page (the identificator is automaticly generated and unique)
   * 
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
    Integer adminModuleId = new Integer(0);

    AdminModule adminModule = (AdminModule) instance;

    AdminModuleStatement statement = new AdminModuleStatement();

    /* Tells the statement what study object to save */
    statement.setDataBean(adminModule);

    /* Save the study */
    try {
      adminModuleId = statement.executeInsert();
    } catch (StatementException se) {
      log.error("Couldn't create study");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }

    return (adminModuleId);
  }

  /**
   * Deletes a existing admin module text
   * 
   * @param id - The id of the admin module to delete
   * 
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Integer id) throws DeletePersistentObjectException {
    boolean result = false;

    AdminModuleStatement statement = new AdminModuleStatement();

    /* Delete the admin module text */
    try {
      result = statement.executeDelete(id);
    } catch (StatementException se) {
      log.error("Couldn't delete admin text");

      se.printStackTrace();

      throw new DeletePersistentObjectException();
    }

    return (result);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKey(java.lang.Integer)
   */
  /**
   * Method findByKey
   * @param id Integer
   * @return Object
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Integer id) {
    Object result = new Object();
    AdminModule adminModule = new AdminModule();
    AdminModuleStatement statement = new AdminModuleStatement();

    statement.setDataBean(adminModule);

    try {
      result = statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findBykey() - Fetching of admin module text " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof AdminModule) {
      adminModule = (AdminModule) result;
    }
    return (adminModule);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKeys(java.util.List)
   */
  /**
   * Method findByKeys
   * @param entityIds List
   * @return List
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    List results = new ArrayList();

    if (!entityIds.isEmpty()) {
      AdminModuleStatement statement = new AdminModuleStatement();

      StringBuffer stmtWhere = new StringBuffer();

      AdminModule adminModule = new AdminModule();

      statement.setDataBean(adminModule);

      if (entityIds != null) {
        stmtWhere.append("WHERE at_id IN (");

        Iterator i = entityIds.iterator();
        boolean firstIteration = true;

        while (i.hasNext()) {
          if (!firstIteration) {
            stmtWhere.append(", ");
          }

          stmtWhere.append("?");

          statement.getConditions().add(i.next());

          firstIteration = true;
        }
      }

      statement.setWhereClause(stmtWhere.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of admin module text failed!!", e);
        return (null);
      }
    }

    return (results);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Object)
   */
  /**
   * Method update
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
    AdminModule adminModule = (AdminModule) instance;

    return (update(adminModule.getId(), instance));
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Integer, java.lang.Object)
   */
  /**
   * Method update
   * @param id Integer
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
    boolean result = false;

    AdminModule adminModule = (AdminModule) instance;

    AdminModuleStatement statement = new AdminModuleStatement();

    /* Tells the statement what study object to update */
    statement.setDataBean(adminModule);

    /* Update the study */
    try {
      result = statement.executeUpdate(id);
    } catch (StatementException se) {
      log.error("Couldn't update study");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    }

    return (result);
  }

  /**
   * Finding all AdminModules
   * @return - All admin modules in the database (there should be only one)
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {
    List results = new ArrayList();

    AdminModuleStatement statement = new AdminModuleStatement();

    AdminModule adminModule = new AdminModule();

    statement.setDataBean(adminModule);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("find() - Fetching of admin module text failed!!", e);
      return (null);
    }

    return (results);
  }

}
