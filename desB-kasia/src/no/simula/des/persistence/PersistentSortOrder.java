package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.ReportSortOrder;
import no.simula.des.statements.ReportSortOrderStatement;

public class PersistentSortOrder implements PersistentObject{

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(PersistentSortOrder.class);

	public List find() {
		List results = new ArrayList(); // will contain the results

		ReportSortOrder sortOrder = new ReportSortOrder();

		ReportSortOrderStatement statement = new ReportSortOrderStatement();

		statement.setDataBean(sortOrder);

		try {
			results = (ArrayList) statement.executeSelect();
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of all sort orders failed", e);
			return (null);
		}

		return (results);
	}

	public Object findByKey(Integer id) {
		Object result = new Object();
		ReportSortOrder sortOrder = new ReportSortOrder();

		ReportSortOrderStatement statement = new ReportSortOrderStatement();

		statement.setDataBean(sortOrder);

		try {
			result = (ReportSortOrder) statement.executeSelect(id);
		} catch (StatementException e) {
			log.error("findBykey() - Fetching of criterium " + id + " failed!!", e);
			return (null);
		}

		if (result instanceof ReportSortOrder) {
			sortOrder = (ReportSortOrder) result;

			if (sortOrder.getId() == null || !sortOrder.getId().equals(id)) {
				sortOrder.setId(id);
			}

		}
		return (sortOrder);
	}

	public List findByKeys(List entityIds) {
		// TODO Auto-generated method stub
		return null;
	}

	// *** These methods should be never called since this is dictionary table
	public Integer create(Object instance) throws CreatePersistentObjectException {
		return null;
	}

	public boolean delete(Integer id) throws DeletePersistentObjectException {
		return false;
	}

	public boolean update(Object instance) throws UpdatePersistentObjectException {
		return false;
	}

	public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		return false;
	}
}
