/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.AggregatedStudyReport;
import no.simula.des.statements.StudyReportStatement;

/**
 * @author Frdoe Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyReport implements PersistentObject, Serializable {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentStudyReport.class);

  /**
   * Creates a new admin opening page text
   * 
   * @param instance - The admin text that will be saved
   * @return the identificator of the new admin page (the identificator is automaticly generated and unique)
   * 
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
    Integer studyReportId = new Integer(0);

    AggregatedStudyReport studyReport = (AggregatedStudyReport) instance;

    StudyReportStatement statement = new StudyReportStatement();

    /* Tells the statement what study object to save */
    statement.setDataBean(studyReport);

    /* Save the study */
    try {
      studyReportId = statement.executeInsert();
    } catch (StatementException se) {
      log.error("Couldn't create study report");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }

    return (studyReportId);
  }

  /**
   * Deletes a existing study report
   * 
   * @param id - The id of the study report to delete
   * 
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Integer id) throws DeletePersistentObjectException {
    boolean result = false;

    StudyReportStatement statement = new StudyReportStatement();

    /* Delete the admin module text */
    try {
      result = statement.executeDelete(id);
    } catch (StatementException se) {
      log.error("Couldn't delete study report");

      se.printStackTrace();

      throw new DeletePersistentObjectException();
    }

    return (result);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKey(java.lang.Integer)
   */
  /**
   * Method findByKey
   * @param id Integer
   * @return Object
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Integer id) {
    Object result = new Object();
    AggregatedStudyReport studyReport = new AggregatedStudyReport();

    StudyReportStatement statement = new StudyReportStatement();

    statement.setDataBean(studyReport);

    try {
      result = (ArrayList) statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findBykey() - Fetching of study report " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof AggregatedStudyReport) {
      studyReport = (AggregatedStudyReport) result;
    }

    return (studyReport);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKeys(java.util.List)
   */
  /**
   * Method findByKeys
   * @param entityIds List
   * @return List
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    List results = new ArrayList();

    if (!entityIds.isEmpty()) {
      StudyReportStatement statement = new StudyReportStatement();

      StringBuffer stmtWhere = new StringBuffer();

      AggregatedStudyReport studyReport = new AggregatedStudyReport();

      statement.setDataBean(studyReport);

      if (entityIds != null) {
        stmtWhere.append("WHERE sr_id IN (");

        Iterator i = entityIds.iterator();
        boolean firstIteration = true;

        while (i.hasNext()) {
          if (!firstIteration) {
            stmtWhere.append(", ");
          }

          stmtWhere.append("?");

          statement.getConditions().add(i.next());

          firstIteration = true;
        }
      }

      statement.setWhereClause(stmtWhere.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of study reportfailed!!", e);
        return (null);
      }
    }

    return (results);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Object)
   */
  /**
   * Method update
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
    AggregatedStudyReport studyReport = (AggregatedStudyReport) instance;

    return (update(studyReport.getId(), instance));
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#update(java.lang.Integer, java.lang.Object)
   */
  /**
   * Method update
   * @param id Integer
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
    boolean result = false;

    AggregatedStudyReport studyReport = (AggregatedStudyReport) instance;

    StudyReportStatement statement = new StudyReportStatement();

    /* Tells the statement what study object to update */
    statement.setDataBean(studyReport);

    /* Update the study */
    try {
      result = statement.executeUpdate(id);
    } catch (StatementException se) {
      log.error("Couldn't update study report");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    }

    return (result);
  }

  /**
   * Finding all Studiy reports
   * 
   * @return - All study reports in the database
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {
    List results = new ArrayList();

    StudyReportStatement statement = new StudyReportStatement();

    StringBuffer stmtWhere = new StringBuffer();

    AggregatedStudyReport studyReport = new AggregatedStudyReport();

    statement.setDataBean(studyReport);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("findByKeys() - Fetching of all study reports failed!!", e);
      return (null);
    }

    return (results);
  }

}
