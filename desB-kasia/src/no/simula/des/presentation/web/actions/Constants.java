package no.simula.des.presentation.web.actions;

/** Contains constants, which is used by the various actions.
 * @author Stian Eide
 */
public class Constants {
  
  /** The key in session where the table renderer is stored. */  
  public static final String TABLE_RENDERER = "table-renderer";
  
  /** The key in session where the <CODE>StudyForm</CODE> is stored. */  
  public static final String FORM_STUDY = "study";
  /** The key in session where the <CODE>SimpleForm</CODE> is stored. */  
  public static final String FORM_SIMPLE = "simple";
  /** The key in session where the <CODE>StudyPublicationRelationForm</CODE> is stored. */  
  public static final String FORM_STUDY_PUBLICATION_RELATION = "study-publication-relation";
  /** The key in session where the <CODE>StudyMaterialForm</CODE> is stored. */  
  public static final String FORM_STUDYMATERIAL = "studymaterial";
  /** The key in session where the <CODE>StudyStudyMaterialRelationForm</CODE> is stored. */  
  public static final String FORM_STUDY_STUDYMATERIAL_RELATION = "study-studymaterial-relation";
  /** The key in session where the <CODE>PrivilegesForm</CODE> is stored. */  
  public static final String FORM_PRIVILEGES = "privileges";
  
  public static final String FORM_REPORT = "report";

  /** The key where the list of <CODE>StudyType</CODE>s is stored. */  
  public static final String BEAN_STUDY_TYPES = "study-types";
  /** The key where the the list of <CODE>DurationUnit</CODE>s is stored. */  
  public static final String BEAN_DURATION_UNITS = "duration-units";
  /** The key where the list of <CODE>Person</CODE>s is stored. */  
  public static final String BEAN_PERSONS = "persons";
  /** The key where the list of <CODE>Study</CODE>s [sic] is stored. */  
  public static final String BEAN_STUDIES = "studies";
  
  public static final String BEAN_REPORTS = "personalized-reports";
  public static final String BEAN_SORT_OPTIONS = "report-sort-options";
  public static final String BEAN_REPORT_CRITERIA = "report-criteria";
  
  /** The key where the list of <CODE>Study</CODE>s to be deleted is stored. */  
  public static final String DELETE_STUDIES = "deletable-studies";

  /** The key where the <CODE>Study</CODE> to delete is stored. */  
  public static final String DELETE_THIS_STUDY = "deletable-study";
  /** The key where the list of <CODE>StudyMaterial</CODE>s to be deleted is stored. */  
  public static final String DELETE_STUDYMATERIAL = "deletable-studymaterial";
  /** The key where the <CODE>StudyMaterial</CODE> to delete is stored. */  
  public static final String DELETE_THIS_STUDYMATERIAL = "deletable-studymaterial";
  /** The key where the <CODE>StudyMaterial</CODE> NOT to delete is stored. */  
  public static final String DONT_DELETE_THIS_STUDYMATERIAL = "non-deletable-studymaterial";

  /** The key where the list of <CODE>StudyType</CODE>s to be deleted is stored. */  
  public static final String DELETE_STUDYTYPES = "deletable-studytypes";
  /** The key where the <CODE>StudyType</CODE> to delete is stored. */  
  public static final String DELETE_THIS_STUDYTYPE = "deletable-studytype";
  /** The key where the <CODE>StudyType</CODE> NOT to delete is stored. */  
  public static final String DONT_DELETE_THIS_STUDYTYPE = "non-deletable-studytype";

  /** The key where the list of <CODE>DurationUnit</CODE>s to be deleted is stored. */  
  public static final String DELETE_DURATION_UNITS = "deletable-duration-units";
  /** The key where the <CODE>DurationUnit</CODE> to delete is stored. */  
  public static final String DELETE_THIS_DURATION_UNIT = "deletable-duration-unit";
  /** The key where the <CODE>DurationUnit</CODE> NOT to delete is stored. */  
  public static final String DONT_DELETE_THIS_DURATION_UNIT = "non-deletable-duration-unit";
  
  /** The key in session where the table of <CODE>Study</CODE>s returned from a
   * search is stored.
   */  
  public static final String TABLE_SEARCH_STUDIES = "table-search-studies";
  /** The key in session where the table of <CODE>Study</CODE>s used when
   * managing studies is stored.
   */  
  public static final String TABLE_MANAGE_STUDIES = "table-manage-studies";
  
  public static final String TABLE_QUERY_REPORT = "table-query-report";
  
  
  public static final String TABLE_MANAGE_REPORTS = "table-manage-reports";
  /** The key in session where the table of <CODE>StudyMaterial</CODE>s is stored. */  
  public static final String TABLE_STUDYMATERIAL = "table-studymaterial";
  /** The key in session where the table of detached <CODE>Publication</CODE>s is stored. */  
  public static final String TABLE_STUDY_RELATIONS_PUBLICATIONS = "table-study-rel-publications";
  /** The key in session where the table of detached <CODE>StudyMaterial</CODE>s is stored. */  
  public static final String TABLE_STUDY_RELATIONS_STUDYMATERIAL = "table-study-rel-studymaterial";
  /** The key in session where the table of <CODE>StudyType</CODE>s is stored. */  
  public static final String TABLE_STUDYTYPES = "table-studytypes";
  /** The key in session where the table of <CODE>DurationUnit</CODE>s is stored. */  
  public static final String TABLE_DURATION_UNITS = "table-duration-units";
  
  /** The parameter used to indicate publications or study material to attach to a
   * study.
   */  
  public static final String PARAMETER_ATTACH = "attach";
  /** The parameter used to indicate publications or study material to detach to a
   * study.
   */  
  public static final String PARAMETER_DETACH = "detach";

  /** The name of the Database Administrator privilege. */  
  public static final String PRIVILEGE_DB_ADMIN = "db-admin";
}
