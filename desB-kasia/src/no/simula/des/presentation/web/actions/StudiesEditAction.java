package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import no.halogen.presentation.web.CheckNewAction;
import no.halogen.struts.ActionForwardUtil;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.presentation.web.forms.StudyForm;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;

/** Handles creation of new studies, editing existing studies and
 * saving changes in both cases.
 *
 * The study will not be saved if
 *
 * <ul>
 *  <li>no name is specified or if the name is equal to another study's name.</li>
 *  <li>no description is provided</li>
 *  <li>no responsible is defined</li>
 *  <li>
 *    no end date is specified, start date is larger than end date or provided
 *    dates are invalid
 *  </li>
 *  <li>
 *    non-integer values are provided for duration noOfProfessionals or
 *    noOfStudents
 *  </li>
 * </ul>
 *
 * Returns <CODE>edit</CODE> when the study form should be displayed. Returns
 * <CODE>relations-publications</CODE> to attach/detach publications to the study
 * and <CODE>relations-studymaterial</CODE> to attach/detach study material.
 * Returns <CODE>return</CODE> when cancel is hit or the changes have been successfully
 * saved.
 */
public class StudiesEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    // Navigate via this action to save changes in StudyForm
    // and set return forward before the user is redirected 
    // to the relations action
    if(managePublicationRelations(request)) {
      return(mapping.findForward("relations-publications"));
    }

    if(manageStudyMaterialRelations(request)) {
      return(mapping.findForward("relations-studymaterial"));
    }
    
    if(editStudyMaterial(request)) {
      ActionForward forward = mapping.findForward("studymaterial-edit");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }
    
    StudyForm form = (StudyForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();

    if(saveChanges(request)) {

      errors = validate(simula, form);
      if(errors == null) {

        Study study;
      
        // Get existing study from cache to allow tables in session to be 
        // updated automatically (they point to the same object in memory)
        if(isNewStudy(form)) {
          study = new Study();
        }
        else {
          study = simula.getStudy(form.getId());
        }
        Person admin = null;
        Principal user = request.getUserPrincipal();
        if(user != null) {
          String userId = user.getName();
          admin = simula.getPerson(userId);
        }
        form.setLastEditedBy(admin);
        BeanUtils.copyProperties(study, form);

        if(isNewStudy(form)) {
          study.setOwnedBy(admin);
          simula.addStudy(study);
        }
        else {
          simula.storeStudy(study);
        }
        
        return(mapping.findForward("return"));
      }
    }
    
    ///////////////////////////////////////////////////////
    // Mutually exclusive actions that directs the user
    // to the main view of this action
    
    if(isInitialRequest(request)) {
    
      if(isNew(request)) {
        form.setId(null);
        form.setName(null);
        form.setDescription(null);
        form.setDuration(null);
        form.setDurationUnit(null);
        form.setKeywords(null);
        form.setLastEditedBy(null);
        form.setOwnedBy(null);
        form.setNoOfProfessionals(null);
        form.setNoOfStudents(null);
        form.setNotes(null);
        form.setType(null);
        form.setStartDay(null);
        form.setStartMonth(null);
        form.setStartYear(null);
        form.setEndDay(null);
        form.setEndMonth(null);
        form.setEndYear(null);
        form.setStudyMaterial(new ArrayList(0));
        form.setPublications(new ArrayList(0));
        form.setResponsibles(new ArrayList(0));
      }
      else {
        Integer id = Integer.valueOf(request.getParameter("id"));
        BeanUtils.copyProperties(form, simula.getStudy(id));
      }
      
      form.setPersons(new ArrayList(simula.getPersons()));
      form.getPersons().removeAll(form.getResponsibles());
      
    }
    
    else if(addResponsibles(request)) {
      String[] add = form.getAddResponsible();
      for(int i = 0; i < add.length; i++) {
        Person person = simula.getPerson(add[i]);
        if(!form.getResponsibles().contains(person)) {
          form.getResponsibles().add(person);
        }
        form.getPersons().remove(person);
      }
      form.sortResponsibles();
    }

    else if(removeResponsibles(request)) {
      String[] remove = form.getRemoveResponsible();
      for(int i = 0; i < remove.length; i++) {
        Person person = simula.getPerson(remove[i]);
        if(!form.getPersons().contains(person)) {
          form.getPersons().add(person);
        }
        form.getResponsibles().remove(person);
      }
      form.sortPersons();
    }
    
    List types = simula.getStudyTypes();
    request.setAttribute(Constants.BEAN_STUDY_TYPES, types);
    List units = simula.getDurationUnits();
    request.setAttribute(Constants.BEAN_DURATION_UNITS, units);
    
    if(errors != null) {
      saveErrors(request, errors);
    }
    return(mapping.findForward("edit"));
  }
  
  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private ActionErrors validate(Simula simula, StudyForm form) throws SimulaException {
    ActionErrors errors = new ActionErrors();

    Calendar validator = Calendar.getInstance();
    validator.setLenient(false);

    if(!empty(form.getStartYear()) || !empty(form.getStartMonth()) || !empty(form.getStartDay())) {
      try {
        validator.set(
          Integer.parseInt(form.getStartYear()) - 1900, 
          Integer.parseInt(form.getStartMonth()) - 1,
          Integer.parseInt(form.getStartDay())
        );
        validator.getTime();      
      } catch(Exception e) {
        ActionError error = new ActionError("errors.date", "start");
        errors.add("start", error);
      }
    }

    if(empty(form.getEndYear()) || empty(form.getEndMonth()) || empty(form.getEndDay())) {
      ActionError error = new ActionError("errors.required", "end date");
      errors.add("end", error);      
    }
    else {
      try {
        validator.set(
          Integer.parseInt(form.getEndYear()) - 1900,
          Integer.parseInt(form.getEndMonth()) - 1, 
          Integer.parseInt(form.getEndDay())
        );
        validator.getTime();
      } catch(Exception e) {
        ActionError error = new ActionError("errors.date", "end");
        errors.add("end", error);
      }
    }
    
    try {
      if(form.getStart().after(form.getEnd())) {
        ActionError error = new ActionError("errors.date.startafterend");
        errors.add("end", error);
      }
    } catch(Exception e) {
      // Do nada. Illegal dates are handled by the two preceeding tests
    }

    if(form.getName() == null || form.getName().length() == 0) {
      ActionError error = new ActionError("errors.required", "name");
      errors.add("name", error);
    }

    for(Iterator i = simula.getStudiesByCriteria(null).iterator(); i.hasNext();) {
      Study study = (Study)i.next();
      if(
        (
          form.getId() == null ||
          (form.getId() != null && !form.getId().equals(study.getId()))
        ) &&
        study.getName().trim().equals(form.getName().trim())
      ) {
        ActionError error = new ActionError("errors.name.equal", "study", form.getName());
        errors.add("name", error);
        break;
      }
    }

    if(form.getDescription() == null || form.getDescription().length() == 0) {
      ActionError error = new ActionError("errors.required", "description");
      errors.add("name", error);
    }
    
    if(form.getResponsibles().size() == 0) {
      ActionError error = new ActionError("errors.minoccur", "1 responsible");
      errors.add("responsibles", error);
    }

    if(!empty(form.getDuration())) {
      try {
        Integer.valueOf(form.getDuration());
      } catch(Exception e) {
        ActionError error = new ActionError("errors.integer", "duration");
        errors.add("duration", error);
      }
    }

    if(!empty(form.getNoOfProfessionals())) {
      try {
        Integer.valueOf(form.getNoOfProfessionals());
      } catch(Exception e) {
        ActionError error = new ActionError("errors.integer", "number of professional participants");
        errors.add("noOfProfessionals", error);
      }
    }

    if(!empty(form.getNoOfStudents())) {
      try {
        Integer.valueOf(form.getNoOfStudents());
      } catch(Exception e) {
        ActionError error = new ActionError("errors.integer", "number of student participants");
        errors.add("noOfStudents", error);
      }
    }
    
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }

  private boolean empty(String string) {
    return(string == null || string.length() == 0);
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("new") != null || request.getParameter("id") != null);
  }
  
  private boolean isNew(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean addResponsibles(HttpServletRequest request) {
    return(request.getParameter("add") != null);
  }

  private boolean removeResponsibles(HttpServletRequest request) {
    return(request.getParameter("remove") != null);
  }
  
  private boolean managePublicationRelations(HttpServletRequest request) {
    return(request.getParameter("relations-publications") != null);
  }

  private boolean manageStudyMaterialRelations(HttpServletRequest request) {
    return(request.getParameter("relations-studymaterial") != null);
  }

  private boolean editStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("studymaterial-edit") != null);
  }
  
  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }

  private boolean isNewStudy(StudyForm form) {
    return(form.getId() == null);
  }  
}