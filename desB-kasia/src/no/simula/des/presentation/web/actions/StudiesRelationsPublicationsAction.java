package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import no.halogen.utils.table.ActionTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.TableColumn;
import no.halogen.utils.table.TableSourceList;
import no.simula.Person;
import no.simula.Publication;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.presentation.web.forms.StudyForm;
import no.simula.des.presentation.web.forms.StudyPublicationRelationForm;
import org.apache.commons.beanutils.BeanUtils;

/**
 * Populate a table with all publications that are not attached to the current
 * study and a list of attached publications.
 * <p>
 * Attach-requests move the given publication from the table to the list and
 * detach-requests move publications the other way.
 * <p>
 * If the user selects ok, the changes are updated in <CODE>StudyForm</CODE>,
 * so that the changes will be stored permanently when the study is saved.
 * <p>
 * Returns <CODE>manage</CODE> action forward to display the attach/detach
 * view, and <CODE>return</CODE> when ok or cancel is chosen.
 */
public class StudiesRelationsPublicationsAction extends Action {

	/**
	 * Execute this action
	 * 
	 * @param mapping
	 *            the action mapping that match the current request
	 * @param actionForm
	 *            the action form linked to the current action mapping
	 * @param request
	 *            the current servlet request object
	 * @param response
	 *            the current servlet response object
	 * @throws Exception
	 *             if the action could not be successfully completed
	 * @return an action forward from the action mapping depending on the
	 *         outcome of this action
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (isCancelled(request)) {
			return (mapping.findForward("return"));
		}

		StudyPublicationRelationForm form = (StudyPublicationRelationForm) actionForm;
		Simula simula = SimulaFactory.getSimula();
		Table table = (Table) request.getSession().getAttribute(Constants.TABLE_STUDY_RELATIONS_PUBLICATIONS);

		if (isOk(request)) {
			StudyForm studyForm = (StudyForm) request.getSession().getAttribute(Constants.FORM_STUDY);
			studyForm.setPublications(new ArrayList(form.getAttachedPublications()));
			return (mapping.findForward("return"));
		}

		if (table == null) {
			List columns = new ArrayList(2);
			SortableTableColumn authors = new SortableTableColumn("authors", "publications.table.heading.authors",
					new Integer(20));
			//"publications", "studies.table.heading.publications", "url", false
			SortableTableColumn title = new SortableTableColumn("title", "publications.table.heading.title","url", false);
			columns.add(authors);
			columns.add(title);
			columns.add(new ActionTableColumn(null, "publications.table.button.attach", null, null, null,
					"/adm/studies/relations/publications", Constants.PARAMETER_ATTACH, "id", new Integer(20)));
			table = new Table(columns, new TableSourceList(form.getDetachedPublications()), 20, title);
			request.getSession().setAttribute(Constants.TABLE_STUDY_RELATIONS_PUBLICATIONS, table);
		}

		if (isInitialRequest(request)) {
			StudyForm studyForm = (StudyForm) request.getSession().getAttribute(Constants.FORM_STUDY);
			form.setAttachedPublications(new ArrayList(studyForm.getPublications()));
			form.setDetachedPublications(new ArrayList(simula.getPublications()));
			form.getDetachedPublications().removeAll(form.getAttachedPublications());
			if (table != null) {
				table.setSource(new TableSourceList(form.getDetachedPublications()));
				table.sortBy("title", true);
			}
		}

		else {
			if (attachPublication(request)) {
				String id = request.getParameter(Constants.PARAMETER_ATTACH);
				Publication publication = simula.getPublication(id);
				if (!form.getAttachedPublications().contains(publication)) {
					form.getAttachedPublications().add(publication);
					form.getDetachedPublications().remove(publication);
				}
			}

			if (detachPublication(request)) {
				String id = request.getParameter(Constants.PARAMETER_DETACH);
				Publication publication = simula.getPublication(id);
				if (!form.getDetachedPublications().contains(publication)) {
					form.getDetachedPublications().add(publication);
					form.getAttachedPublications().remove(publication);
					table.doSort();
				}
			}
		}

		String sortBy = request.getParameter("sortBy");
		if (sortBy != null && sortBy.length() > 0) {
			table.sortBy(sortBy);
		}

		String page = request.getParameter("page");
		if (page != null && page.length() > 0) {
			table.setCurrentPage(Integer.parseInt(page));
		}

		return (mapping.findForward("manage"));
	}

	private boolean isInitialRequest(HttpServletRequest request) {
		return (request.getParameter("initial") != null);
	}

	private boolean attachPublication(HttpServletRequest request) {
		return (request.getParameter(Constants.PARAMETER_ATTACH) != null);
	}

	private boolean detachPublication(HttpServletRequest request) {
		return (request.getParameter(Constants.PARAMETER_DETACH) != null);
	}

	private boolean isOk(HttpServletRequest request) {
		return (request.getParameter("ok") != null);
	}
}