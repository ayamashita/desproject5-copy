package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import no.simula.Privilege;
import no.simula.utils.PersonFamilyNameComparator;
import org.apache.struts.action.ActionForm;

/** Holds data about all privileges. Is used when managning privileges. This bean
 * have one {@link PrivilegesSubForm} for each privilege that is registered in
 * the system.
 * @author Stian Eide
 */
public class PrivilegesForm extends ActionForm {
  
  /** Holds value of property selected. */
  private PrivilegeSubForm selected;
  
  /** Holds value of property all. */
  private List all;
  
  /** Holds value of property privilegeId. */
  private Integer privilegeId;
  
  /** Holds value of property addPersons. */
  private String[] addPersons;
  
  /** Holds value of property removePersons. */
  private String[] removePersons;
  
  /** Holds value of property persons. */
  private List persons;
  
  /** Creates a new instance of PrivilegesForm */
  public PrivilegesForm() {}
  
  /** Getter for property selected.
   * @return Value of property selected.
   *
   */
  public PrivilegeSubForm getSelected() {
    return this.selected;
  }
  
  /** Setter for property selected.
   * @param selected New value of property selected.
   *
   */
  public void setSelected(PrivilegeSubForm selected) {
    this.selected = selected;
  }
  
  /** Getter for property all.
   * @return Value of property all.
   *
   */
  public List getAll() {
    return this.all;
  }
  
  /** Setter for property all.
   * @param all New value of property all.
   *
   */
  public void setAll(List all) {
    this.all = all;
  }
  
  /** Getter for property privilegeId.
   * @return Value of property privilegeId.
   *
   */
  public Integer getPrivilegeId() {
    return this.privilegeId;
  }
  
  /** Setter for property privilegeId.
   * @param privilegeId New value of property privilegeId.
   *
   */
  public void setPrivilegeId(Integer privilegeId) {
    this.privilegeId = privilegeId;
  }
  
  /** Getter for property addPersons.
   * @return Value of property addPersons.
   *
   */
  public String[] getAddPersons() {
    return this.addPersons;
  }
  
  /** Setter for property addPersons.
   * @param addPersons New value of property addPersons.
   *
   */
  public void setAddPersons(String[] addPersons) {
    this.addPersons = addPersons;
  }
  
  /** Getter for property removePersons.
   * @return Value of property removePersons.
   *
   */
  public String[] getRemovePersons() {
    return this.removePersons;
  }
  
  /** Setter for property removePersons.
   * @param removePersons New value of property removePersons.
   *
   */
  public void setRemovePersons(String[] removePersons) {
    this.removePersons = removePersons;
  }
  
  /** Getter for property persons.
   * @return Value of property persons.
   *
   */
  public List getPersons() {
    return this.persons;
  }
  
  /** Setter for property persons. This list is always sorted.
   * @param persons New value of property persons.
   *
   */
  public void setPersons(List persons) {
    if(persons == null) {
      this.persons = null;
    }
    else {
      this.persons = new ArrayList(persons);
      sortPersons();
    }
  }
  
  /** Returns the privilege with the specified id.
   * @param id the id of the privilege
   * @return a privilege
   */  
  public PrivilegeSubForm getPrivilege(Integer id) {
    for(Iterator i = all.iterator(); i.hasNext();) {
      PrivilegeSubForm priv = (PrivilegeSubForm)i.next();
      if(priv.getId().equals(id)) {
        return(priv);
      }
    }
    return(null);
  }
  
  /** Sorts the persons who have no privileges by their family name. */  
  public void sortPersons() {
    Collections.sort(this.persons, new PersonFamilyNameComparator());
  }
}
