package no.simula.des.presentation.web.forms;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import no.halogen.presentation.web.CheckNewActionForm;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.utils.PersonFamilyNameComparator;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class ReportForm extends CheckNewActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReportForm() throws Exception {
		simula = SimulaFactory.getSimula();
	}

	private Simula simula;

	private Integer sortOrderId;
	private Integer criteriumId;
	private String name;
	private String ownerId;

	private List persons;
	private List responsibles;
	private String[] addResponsible;
	private String[] removeResponsible;

	private List allColumns;
	private List columns;
	private Integer[] addColumn;
	private Integer[] removeColumn;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		return super.validate(mapping, request);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List getPersons() {
		return persons;
	}

	public void setPersons(List persons) {
		this.persons = persons;
	}

	public List getResponsibles() {
		return responsibles;
	}

	public void setResponsibles(List responsibles) {
		this.responsibles = responsibles;
	}

	public String[] getAddResponsible() {
		return addResponsible;
	}

	public void setAddResponsible(String[] addResponsible) {
		this.addResponsible = addResponsible;
	}

	public String[] getRemoveResponsible() {
		return removeResponsible;
	}

	public void setRemoveResponsible(String[] removeResponsible) {
		this.removeResponsible = removeResponsible;
	}

	public List getAllColumns() {
		return allColumns;
	}

	public void setAllColumns(List allColumns) {
		this.allColumns = allColumns;
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List reportColumns) {
		this.columns = reportColumns;
	}

	public Integer[] getAddColumn() {
		return addColumn;
	}

	public void setAddColumn(Integer[] addColumn) {
		this.addColumn = addColumn;
	}

	public Integer[] getRemoveColumn() {
		return removeColumn;
	}

	public void setRemoveColumn(Integer[] removeColumn) {
		this.removeColumn = removeColumn;
	}

	public Integer getSortOrderId() {
		return sortOrderId;
	}

	public void setSortOrderId(Integer sortOrderId) {
		this.sortOrderId = sortOrderId;
	}

	public Integer getCriteriumId() {
		return criteriumId;
	}

	public void setCriteriumId(Integer criteriumId) {
		this.criteriumId = criteriumId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	private boolean isInitialRequest(HttpServletRequest request) {
		return (request.getParameter("new") != null || request.getParameter("id") != null);
	}
	
	  /** Sorts the list of non-responsible persons by family name. */  
	  public void sortPersons() {
	    Collections.sort(this.persons, new PersonFamilyNameComparator());
	  }
	  
	  /** Sort the list of responsible persons by family name. */  
	  public void sortResponsibles() {
	    Collections.sort(this.responsibles, new PersonFamilyNameComparator());
	  }

}
