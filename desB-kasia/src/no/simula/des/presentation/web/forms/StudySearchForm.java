package no.simula.des.presentation.web.forms;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.StudyType;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/** Holds data that are used when searching for studies. */
public class StudySearchForm extends ActionForm {

  /** Creates a new <CODE>StudySearchForm</CODE>
   * @throws Exception if unable to get an instance of <CODE>Simula</CODE>
   */  
  public StudySearchForm() throws Exception {
    simula = SimulaFactory.getSimula();
  }
  
  private Simula simula;
  
  private String searchText;
  private Integer[] types;
  private String[] responsibles;

  private String startDay;
  private String startMonth;
  private String startYear;
  
  private String endDay;
  private String endMonth;
  private String endYear;
    
  /** Set <CODE>types</CODE> and <CODE>responsibles</CODE> to an empty array if they
   * are null to avoid <CODE>NullPointerException</CODE>.
   * @param mapping the current action mapping
   * @param request the current http request
   */  
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    if(types == null) {
      types = new Integer[0];
    }
    if(responsibles == null) {
      responsibles = new String[0];
    }
  }
  
  /** Getter for property types.
   * @return Value of property types.
   *
   */
  public Integer[] getTypes() {
    return(types);
  }
  
  /** Setter for property types.
   * @param types New value of property types.
   */
  public void setTypes(Integer[] types) {
    this.types = types;
  }
  
  /** Getter for property responsibles.
   * @return Value of property responsibles.
   *
   */
  public String[] getResponsibles() {
    return(responsibles);
  }
  
  /** Setter for property responsibles.
   * @param responsibles New value of property responsibles.
   *
   */
  public void setResponsibles(String[] responsibles) {
    this.responsibles = responsibles;
  }
  
  /** Getter for property searchText.
   * @return Value of property searchText.
   *
   */
  public java.lang.String getSearchText() {
    return searchText;
  }
  
  /** Setter for property searchText.
   * @param searchText New value of property searchText.
   *
   */
  public void setSearchText(java.lang.String searchText) {
    this.searchText = searchText;
  }
  
  /** Getter for property endDay.
   * @return Value of property endDay.
   *
   */
  public String getEndDay() {
    return endDay;
  }
  
  /** Setter for property endDay.
   * @param endDay New value of property endDay.
   *
   */
  public void setEndDay(String endDay) {
    this.endDay = endDay;
  }
  
  /** Getter for property endMonth.
   * @return Value of property endMonth.
   *
   */
  public String getEndMonth() {
    return endMonth;
  }
  
  /** Setter for property endMonth.
   * @param endMonth New value of property endMonth.
   *
   */
  public void setEndMonth(String endMonth) {
    this.endMonth = endMonth;
  }
  
  /** Getter for property endYear.
   * @return Value of property endYear.
   *
   */
  public String getEndYear() {
    return endYear;
  }
  
  /** Setter for property endYear.
   * @param endYear New value of property endYear.
   *
   */
  public void setEndYear(String endYear) {
    this.endYear = endYear;
  }
  
  /** Getter for property startDay.
   * @return Value of property startDay.
   *
   */
  public String getStartDay() {
    return startDay;
  }
  
  /** Setter for property startDay.
   * @param startDay New value of property startDay.
   *
   */
  public void setStartDay(String startDay) {
    this.startDay = startDay;
  }
  
  /** Getter for property startMonth.
   * @return Value of property startMonth.
   *
   */
  public String getStartMonth() {
    return startMonth;
  }
  
  /** Setter for property startMonth.
   * @param startMonth New value of property startMonth.
   *
   */
  public void setStartMonth(String startMonth) {
    this.startMonth = startMonth;
  }
  
  /** Getter for property startYear.
   * @return Value of property startYear.
   *
   */
  public String getStartYear() {
    return startYear;
  }
  
  /** Setter for property startYear.
   * @param startYear New value of property startYear.
   *
   */
  public void setStartYear(String startYear) {
    this.startYear = startYear;
  }
 
  /** Returns the start date
   * @return the start date
   */  
  public Date getStart() {
    try {
      int y = Integer.parseInt(startYear);
      int m = Integer.parseInt(startMonth);
      int d = Integer.parseInt(startDay);
      return(new Date(y - 1900, m - 1, d));
    }
    catch(Exception e) {
      return(null);
    }
  }
  
  /** returns the end date
   * @return the end date
   */  
  public Date getEnd() {
    try {
      int y = Integer.parseInt(endYear);
      int m = Integer.parseInt(endMonth);
      int d = Integer.parseInt(endDay);
      return(new Date(y - 1900, m - 1, d));
    }
    catch(Exception e) {
      return(null);
    }
  }
}