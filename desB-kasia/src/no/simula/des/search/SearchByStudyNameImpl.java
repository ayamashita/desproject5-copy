/*
 * Created on 01.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.search;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.des.statements.StudyStatement;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class SearchByStudyNameImpl extends CriteriumImpl implements SearchByStudyName {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByStudyName.class);
  /**
   * Field DATABASE_COLUMN
   */
  private final String DATABASE_COLUMN = "stu_name";
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" LIKE ");

  /** 
   * Generates the criteria and "?" character as value representators as a part of a SQL where clause
   * Due to use of prepared statements, the value attributes must be fetched when a prepared statement is instatiated and will use the criterium
   * The method does not generate the start of the clause (WHERE), nor the operator (AND, OR ..)
   * @return String
   * @throws CriteriumException
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() throws CriteriumException {
    StringBuffer statement = new StringBuffer();

    if (getAttributes() == null) {
      log.error("generateWhereClause - No attributes in SearchByStudyDescription criterium");
      throw new CriteriumException("Error! No attributes ...");
    }

    statement.append("("); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    Iterator i = getAttributes().iterator();
    boolean firstIteration = true;
    for (int j = 0; i.hasNext(); j++) {
      String attribute = (String) i.next(); //the String to search for

      if (!firstIteration) { // if more than one internal pair
        statement.append(" OR ");
      }

      statement.append(" " + getCriteriumColumn() + " LIKE ? ");

      StringBuffer modifyAttribute = new StringBuffer();

      modifyAttribute.append("%");
      modifyAttribute.append(attribute);
      modifyAttribute.append("%");

      attribute = modifyAttribute.toString();

      getAttributes().set(j, attribute);

      firstIteration = false;
    }

    statement.append(")"); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    return (statement.toString());
  }

  /* (non-Javadoc)
   * @see no.halogen.search.Criterium#getStatements()
   */
  /**
   * Method getStatement
   * @return ObjectStatement
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return (new StudyStatement());
  }

  /**
   * @return Database columns
   */
  private String getCriteriumColumn() {
    return DATABASE_COLUMN;
  }
}
