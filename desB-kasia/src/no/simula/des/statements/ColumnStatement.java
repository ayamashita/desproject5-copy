package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.simula.des.Column;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportSortOrder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ColumnStatement extends ObjectStatementImpl {

	private static Log log = LogFactory.getLog(ColumnStatement.class);

	private final String BEAN_NAME = Column.class.getName();

	private final String INSERT_COLUMNS = "column_name, column_display_name, column_type, column_prop_name";

	private final String INSERT_VALUES = "(?,?,?,?)";

	private final String KEY = "column_id";

	private final String SELECT_COLUMNS = "column_id, column_name, column_display_name, column_type, column_prop_name";

	private final String TABLE_NAME = "rep_column";

	private final String UPDATE_VALUES = "column_name = ?, column_display_name= ?, column_type= ?, column_prop_name = ?";

	public Object fetchResults(ResultSet rs) throws SQLException {

		Column column = null;

		try {
			if (getDataBean() != null) {
				column = (Column) getDataBean().getClass().newInstance();
			} else {
				column = new Column();
			}
		} catch (InstantiationException e) {
			log.error("fetchResults() - Could not create new report criterium data object");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			log.error("fetchResults() - Could not create new report criterium data object");
			e.printStackTrace();
		}

		column.setId(new Integer(rs.getInt(Column.COLUMN_ID)));
		column.setName(rs.getString(Column.COLUMN_NAME));
		column.setProperty(rs.getString(Column.COLUMN_PROP_NAME));
		column.setDisplayName(rs.getString(Column.COLUMN_DISPLAY_NAME));
		column.setType(new Integer(rs.getInt(Column.COLUMN_TYPE)));

		return (column);
	}

	public String getInsertColumnNames() {
		return INSERT_COLUMNS;
	}

	public String getInsertValues() {
		return INSERT_VALUES;
	}

	public String getKey() {
		return KEY;
	}

	public String getSelectColumnNames() {
		return SELECT_COLUMNS;
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getUpdateValuesString() {
		return UPDATE_VALUES;
	}

	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		} else {
			Column entity = (Column) getDataBean();

			pstmt.setString(1, entity.getName());
			pstmt.setString(2, entity.getDisplayName());
			pstmt.setInt(3, entity.getType().intValue());
			pstmt.setString(4, entity.getProperty());
		}
	}

	// ******************** these methods should never be called
	// ***************************************
	public boolean executeDelete() throws StatementException {
		return false;
	}

	public boolean executeDelete(Integer entityId) throws StatementException {
		return false;
	}

	public boolean executeDelete(List entityIds) throws StatementException {
		return false;
	}

	public boolean executeDelete(String entityId) throws StatementException {
		return false;
	}

	public Integer executeInsert() throws StatementException {
		return null;
	}

	public boolean executeUpdate() throws StatementException {
		return false;
	}

	public boolean executeUpdate(Integer entityId) throws StatementException {
		return false;
	}

}
