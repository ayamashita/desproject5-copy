/**
 * @(#) PersonStudyRelStatement.java
 */

package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 */
public class PersonStudyRelStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersonStudyRelStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "java.util.List";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "stu_resp_id, people_id";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?)";

  /**
   * Field KEY
   */
  private final String KEY = "stu_resp_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "stu_resp_id, people_id";

  private final String TABLE_NAME = "study_responsible_rel";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "stu_resp_id = ?, people_id = ?";

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Object entity = null;

    try {
      if (getDataBean() instanceof Integer) {
        entity = getDataBean();
      } else if (getDataBean() == null) {
        entity = new Integer(0);
      } else {
        entity = getDataBean().getClass().newInstance();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new study person rel data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new study person rel data object");
      e.printStackTrace();
    }

    if (entity instanceof String) {
      entity = rs.getString("people_id");
    }

    return entity;
  }

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      List entity = (List) getDataBean();
      Integer studyId = (Integer) entity.get(0);
      String personId = (String) entity.get(1);

      pstmt.setInt(1, studyId.intValue());
      pstmt.setString(2, personId);
    }
  }

}
