package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.Person;
import no.simula.des.PersonalizedReport;
import no.simula.des.ReportCriterium;
import no.simula.des.ReportSortOrder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PersonalizedReportStatement extends ObjectStatementImpl {

	private static Log log = LogFactory.getLog(PersonalizedReportStatement.class);

	private final String BEAN_NAME = PersonalizedReport.class.getName();

	private final String INSERT_COLUMNS = "report_name, people_id, report_sort_order_id, report_criteria_id";

	private final String INSERT_VALUES = "(?, ?, ?, ?)";

	private final String KEY = "report_id";
	private final String SELECT_COLUMNS = "report_id, report_name, people_id, report_sort_order_id, report_criteria_id";

	private final String TABLE_NAME = "report";

	private final String UPDATE_VALUES = "report_name = ?, people_id = ?, report_sort_order_id = ?, report_criteria_id = ?";

	public Object fetchResults(ResultSet rs) throws SQLException {
		PersonalizedReport report = null;

		try {
			if (getDataBean() != null) {
				report = (PersonalizedReport) getDataBean().getClass().newInstance();
			} else {
				report = new PersonalizedReport();
			}
		} catch (InstantiationException e) {
			log.error("fetchResults() - Could not create new report data object");
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			log.error("fetchResults() - Could not create new report data object");
			e.printStackTrace();
			return null;
		}
		
		report.setId(new Integer(rs.getInt(PersonalizedReport.REPORT_ID)));
		report.setName(rs.getString(PersonalizedReport.REPORT_NAME));
		
		int criteriumId = rs.getInt(ReportCriterium.REPORT_CRITERIA_ID);
		if (criteriumId > 0 ){
			report.setResponsibleCriterium(new ReportCriterium(new Integer(criteriumId)));
		}
		
		int sortOrderId = rs.getInt(ReportSortOrder.REPORT_SORT_ORDER_ID);
		if (sortOrderId > 0) {
			report.setTimeSortOrder(new ReportSortOrder(new Integer(sortOrderId)));
		}
		
		String ownerId = rs.getString(PersonalizedReport.PEOPLE_ID);
		if (ownerId != null && ownerId.length() > 0) {
			Person owner = new Person(ownerId);
			report.setOwner(owner);
		}

		return report;
	}

	public String getInsertColumnNames() {
		return INSERT_COLUMNS;
	}

	public String getInsertValues() {
		return INSERT_VALUES;
	}

	public String getKey() {
		return KEY;
	}

	public String getSelectColumnNames() {
		return SELECT_COLUMNS;
	}

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getUpdateValuesString() {
		return UPDATE_VALUES;
	}

	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		} else {
			PersonalizedReport pr = (PersonalizedReport) getDataBean();

			pstmt.setString(1, pr.getName());
			pstmt.setString(2, pr.getOwner().getId());
			pstmt.setInt(3, pr.getTimeSortOrder().getId().intValue());
			pstmt.setInt(4, pr.getResponsibleCriterium().getId().intValue());
		}
	}

}
