package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import no.halogen.statements.ObjectStatementImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportResponsibleRelStatement extends ObjectStatementImpl {
	private static Log log = LogFactory.getLog(StudyPersonRelStatement.class);

	private final String BEAN_NAME = "java.util.List";

	private final String INSERT_COLUMNS = "report_id, people_id";

	private final String INSERT_VALUES = "(?, ?)";

	private final String KEY = "report_id";

	private final String SELECT_COLUMNS = "report_id, people_id";

	private final String TABLE_NAME = "report_responsible_rel";

	private final String UPDATE_VALUES = "report_id = ?, people_id = ?";

	public Object fetchResults(ResultSet rs) throws SQLException {
		Object entity = null;

		try {
			if (getDataBean() instanceof Integer) {
				entity = getDataBean();
			} else if (getDataBean() == null) {
				entity = new Integer(0);
			} else {
				entity = getDataBean().getClass().newInstance();
			}
		} catch (InstantiationException e) {
			log.error("fetchListResults() - Could not create new report column rel data object");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			log.error("fetchListResults() - Could not create new report column rel data object");
			e.printStackTrace();
		}

		// if it Integer, then we deliver people ids for the given report
		if (entity instanceof String) {
			entity = rs.getString("people_id");
		}

		return entity;
	}

	public String getInsertColumnNames() {
		return (INSERT_COLUMNS);
	}

	public String getKey() {
		return (KEY);
	}

	public String getSelectColumnNames() {
		return (SELECT_COLUMNS);
	}

	public String getTableName() {
		return (TABLE_NAME);
	}

	public String getUpdateValuesString() {
		return (UPDATE_VALUES);
	}

	public String getInsertValues() {
		return (INSERT_VALUES);
	}

	public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		} else {
			List entity = (List) getDataBean();
			Integer reportId = (Integer) entity.get(0);
			String personId = (String) entity.get(1);

			pstmt.setInt(1, reportId.intValue());
			pstmt.setString(2, personId);
		}
	}

}
