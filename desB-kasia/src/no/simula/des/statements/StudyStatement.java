/**
 * @(#) StudyStatement.java
 */

package no.simula.des.statements;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.Person;
import no.simula.des.Study;

/**
 * @author Frode Langseth
 */
public class StudyStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(StudyStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.Study";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "stu_name, stu_description, stu_sty_id, stu_dur, stu_du_id, stu_start, stu_end, stu_notes, stu_numb_stud, stu_numb_prof, stu_owner, stu_lasteditedby";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  private final String KEY = "stu_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS =
    "stu_id, stu_name, stu_description, stu_sty_id, stu_dur, stu_du_id, stu_start, stu_end, stu_notes, stu_numb_stud, stu_numb_prof, stu_owner, stu_lasteditedby";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "stu_study";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES =
    "stu_name = ?, stu_description = ?, stu_sty_id = ?, stu_dur = ?, stu_du_id = ?, stu_start = ?, stu_end = ?, stu_notes = ?, stu_numb_stud = ?, stu_numb_prof = ?, stu_owner = ?, stu_lasteditedby = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns a string with "?" to represent the values and the order they must appear in in the insert statement
   * 
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Study study = null;

    try {
      if (getDataBean() != null) {
        study = (Study) getDataBean().getClass().newInstance();
      } else {
        study = new Study();
      }
    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new study data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new study data object");
      e.printStackTrace();
    }
    
    String ownerId = null;
    String lastEditedById = null;
    Person ownedBy = null;
    Person lastEditedBy = null;

    study.setId(new Integer(rs.getInt("stu_id")));
    study.setName(rs.getString("stu_name"));
    study.setDescription(rs.getString("stu_description"));
    study.setDuration(new Integer(rs.getInt("stu_dur")));
    study.setStart(rs.getDate("stu_start"));
    study.setEnd(rs.getDate("stu_end"));
    study.setNotes(rs.getString("stu_notes"));
    study.setNoOfStudents(new Integer(rs.getInt("stu_numb_stud")));
    study.setNoOfProfessionals(new Integer(rs.getInt("stu_numb_prof")));

    /*
     * Checks to see if the query returns an id for a person that owns the study
     * If there's an id, a Person is instansiated, and added to the study.
     * To get the complete person information, execute a query in the People table in the Simulaweb database
     */
    ownerId = rs.getString("stu_owner");

    if (ownerId != null) {
      ownedBy = new Person(ownerId);

      study.setOwnedBy(ownedBy);
    }

    /*
     * Checks to see if the query returns an id for a person that last edited the study
     * If there's an id, a Person is instansiated, and added to the study.
     * To get the complete person information, execute a query in the People table in the Simulaweb database
     */
    lastEditedById = rs.getString("stu_lasteditedby");

    if (lastEditedById != null) {
      lastEditedBy = new Person(lastEditedById);

      study.setLastEditedBy(lastEditedBy);
    }

    return (study);
  }

  /**
   * Populates the prepared statement string for an update pr insert statement with values from the data bean
   * 
   * Used e.g. by INSERT statements
   * The string does not contain the id 
   * 
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      Study study = (Study) getDataBean();

      pstmt.setString(1, study.getName());
      pstmt.setString(2, study.getDescription());

      if (study.getType() != null) {
        pstmt.setInt(3, study.getType().getId().intValue());
      } else {
        pstmt.setNull(3, Types.INTEGER);
      }

      if (study.getDuration() != null && study.getDurationUnit() != null) {
        pstmt.setInt(4, study.getDuration().intValue());
        pstmt.setInt(5, study.getDurationUnit().getId().intValue());
      } else {
        pstmt.setNull(4, Types.INTEGER);
        pstmt.setNull(5, Types.INTEGER);
      }

      if (study.getStart() != null) {
        pstmt.setTimestamp(6, new Timestamp(study.getStart().getTime()));
      } else {
        pstmt.setNull(6, Types.DATE);
      }

      pstmt.setTimestamp(7, new Timestamp(study.getEnd().getTime()));

      if (study.getNotes() != null) {
        pstmt.setString(8, study.getNotes());
      } else {
        pstmt.setNull(8, Types.VARCHAR);
      }

      if (study.getNoOfStudents() != null) {
        pstmt.setInt(9, study.getNoOfStudents().intValue());
      } else {
        pstmt.setNull(9, Types.INTEGER);
      }

      if (study.getNoOfProfessionals() != null) {
        pstmt.setInt(10, study.getNoOfProfessionals().intValue());
      } else {
        pstmt.setNull(10, Types.INTEGER);
      }

      if (study.getOwnedBy() != null) {
        pstmt.setString(11, study.getOwnedBy().getId());
      } else {
        pstmt.setNull(11, Types.VARCHAR);
      }

      if (study.getLastEditedBy() != null) {
        pstmt.setString(12, study.getLastEditedBy().getId());
      } else {
        pstmt.setNull(12, Types.VARCHAR);
      }
    }
  }
}
