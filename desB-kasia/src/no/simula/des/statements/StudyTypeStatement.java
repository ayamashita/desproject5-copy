/*
 * Created on 27.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.simula.des.StudyType;
import no.halogen.statements.ObjectStatementImpl;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyTypeStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(StudyTypeStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.StudyType";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "sty_name";

  /**
   * Field KEY
   */
  private final String KEY = "sty_id";
  
	/**
	 * Field INSERT_VALUES
	 */
	private final String INSERT_VALUES = "(?)";

  private final String SELECT_COLUMNS = "sty_id, sty_name";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "sty_studytype";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "sty_name = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (INSERT_COLUMNS);
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {

    StudyType studyType = null;

    try {
      if (getDataBean() != null) {
        studyType = (StudyType) getDataBean().getClass().newInstance();
      } else {
        studyType = new StudyType();
      }
    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new study type data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new study type data object");
      e.printStackTrace();
    }

    studyType.setId(new Integer(rs.getInt("sty_id")));
    studyType.setName(rs.getString("sty_name"));

    return (studyType);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
		return (INSERT_VALUES);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      StudyType entity = (StudyType) getDataBean();

      //pstmt.setInt(1, entity.getId().intValue());
      pstmt.setString(1, entity.getName());
    }

  }

}
