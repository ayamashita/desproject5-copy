package no.simula.realm;

import java.net.MalformedURLException;
import java.util.HashMap;

import no.simula.wsclient.WSClient;

/**
 * Client used to query simula login web service
 * 
 * @author kasia
 * 
 */
public class LoginWSClient extends WSClient {
	public static LoginWSClient getInstance() {
		if (instance == null) {
			instance = new LoginWSClient();
		}
		return instance;
	}

	private static LoginWSClient instance = null;

	private static String CHECK_CREDENTIALS_METHOD = "check_credentials";

	private String WS_URL;

	/*
	 * (non-Javadoc)
	 * 
	 * @see no.machina.simula.wsutils.WSClient#configure()
	 */
	public void configure() {
		WS_URL = System.getProperty("no.machina.simula.login_ws_url");
		try {
			configure(WS_URL);
			System.err.println("WS configured : " + WS_URL);
		} catch (MalformedURLException mue) {
			System.err.println(mue.getMessage());
			WS_URL = null;
		}
	}

	/**
	 * Checks whether given credentials allow to authenticate user
	 * 
	 * @param userId
	 * @param passwd
	 * @return True if the user has been authenticated against simula
	 *         webservice, otherwise false
	 * @throws Exception
	 */
	public Boolean checkCredentials(String userId, String passwd) throws Exception {
		HashMap params = new HashMap();
		params.put("login", userId);
		params.put("password", passwd);

		return queryBooleanMethod(CHECK_CREDENTIALS_METHOD, params);
	}
}
