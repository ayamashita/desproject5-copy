package no.simula.realm;

import java.security.Principal;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import org.apache.catalina.LifecycleException;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

/**
 * A Tomcat realm-implementation that use SQL to lookup a username in a
 * specified database (the <CODE>Simula</CODE>-database) and a http-request
 * to check the provided credentials. Roles are retrieved from the
 * <CODE>DES</CODE>-database.
 * 
 * @author Stian Eide
 */
public class SimulaRealm extends RealmBase {

	/** Creates a new instance of SimulaRealm */
	public SimulaRealm() {
	}

	private Connection desCon;

	/**
	 * The connection username to use when trying to connect to the database.
	 */
	protected String connectionName = null;

	/**
	 * The connection URL to use when trying to connect to the database.
	 */
	protected String connectionPassword = null;

	/**
	 * The connection URL to use when trying to connect to the database.
	 */
	protected String simulaConnectionURL = null;

	/**
	 * The connection URL to use when trying to connect to the database.
	 */
	protected String desConnectionURL = null;

	/**
	 * Instance of the JDBC Driver class we use as a connection factory.
	 */
	protected Driver driver = null;

	/**
	 * The JDBC driver to use.
	 */
	protected String driverName = null;

	/**
	 * Descriptive information about this Realm implementation.
	 */
	protected static final String info = "no.simula.realm.SimulaRealm";

	/**
	 * Descriptive information about this Realm implementation.
	 */
	protected static final String name = "SimulaRealm";

	/**
	 * The PreparedStatement to use for finding the user id of a user.
	 */
	protected PreparedStatement preparedUserId = null;

	/**
	 * The PreparedStatement to use for identifying the roles for a specified
	 * user.
	 */
	protected PreparedStatement preparedRoles = null;

	/** Holds value of property loginHttpURL. */
	private String loginHttpURL;

	/** Holds value of property loginHttpNameParam. */
	private String loginHttpNameParam;

	/** Holds value of property loginHttpPasswordParam. */
	private String loginHttpPasswordParam;

	protected String getName() {
		return (this.getClass().getName());
	}

	/**
	 * Return the Principal associated with the specified username and
	 * credentials, if there is one; otherwise return <code>null</code>.
	 * 
	 * If there are any errors with the JDBC connection, executing the query or
	 * anything we return null (don't authenticate). This event is also logged,
	 * and the connection will be closed so that a subsequent request will
	 * automatically re-open it.
	 * 
	 * @param username
	 *            Username of the Principal to look up
	 * @param credentials
	 *            Password or other credentials to use in authenticating this
	 *            username
	 * @return the <CODE>Principal</CODE> if the provided username and
	 *         credentials are accepted.
	 */
	public Principal authenticate(String username, String credentials) {

		Connection dbConnection = null;

		try {
			// Ensure that we have an open database connection
			open();

			log("password:" + credentials);
			// Acquire a Principal object for this user
			Principal principal = doAuthenticate(username, credentials);

			// Release the database connection we just used
			release();

			// Return the Principal (if any)
			return (principal);
		}

		catch (Exception e) {
			log("Error while authenticating occured", e);
			return null;
		}
	}

	public synchronized Principal doAuthenticate(String username, String credentials) throws Exception {
		
		if (username == null || credentials == null) {
			return null;
		}

		Boolean result = LoginWSClient.getInstance().checkCredentials(username, credentials);

		if (Boolean.TRUE.equals(result)){
			List roles = new ArrayList();
			PreparedStatement pstmt = roles(username);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				roles.add(rs.getString(1).trim());
			}
			rs.close();
			desCon.commit();

			return (new GenericPrincipal(this, username, credentials, roles));
		}
		
		return null;
	}
	
	/**
	 * Return a PreparedStatement configured to perform the SELECT required to
	 * retrieve user roles for the specified username.
	 * 
	 * @param userId
	 *            UserId for which roles should be retrieved
	 * 
	 * @exception SQLException
	 *                if a database error occurs
	 */
	protected PreparedStatement roles(String userId) throws SQLException {
		if (preparedRoles == null) {
			String sql = "SELECT pr_name FROM pr_privilege, person_privilege_rel "
					+ "WHERE pr_privilege.pr_id = person_privilege_rel.pr_id "
					+ "AND person_privilege_rel.people_id  = ?";
			preparedRoles = desCon.prepareStatement(sql);
		}
		preparedRoles.setObject(1, userId);
		return (preparedRoles);
	}

	protected String getPassword(String username) {
		return (null);
	}

	protected Principal getPrincipal(String username) {
		return (null);
	}

	private Principal getPrincipal(Connection dbCon, String username, String credentials) {
		return (null);
		// List roles = new ArrayList();
		// Simula simula = SimulaFactory.getSimula();
		// Person person = simula.getPersonByEmail(username);
		// return(new GenericPrincipal(this, username, credentials,
		// getRoles(person)));
	}

	// private List getRoles(Person person) {
	// List roles = new ArrayList(person.getPrivileges().size());
	// for(Iterator i = person.getPrivileges().iterator(); i.hasNext();) {
	// roles.add(((Privilege)i.next()).getName());
	// }
	// return(roles);
	// }
	//  

	/**
	 * Open (if necessary) and return a database connection for use by this
	 * Realm.
	 * 
	 * @exception SQLException
	 *                if a database error occurs
	 */
	protected void open() throws SQLException {

		// Do nothing if there is a database connection already open
		if (desCon != null) {
			return;
		}

		// Instantiate our database driver if necessary
		if (driver == null) {
			try {
				Class clazz = Class.forName(driverName);
				driver = (Driver) clazz.newInstance();
			} catch (Throwable e) {
				throw new SQLException(e.getMessage());
			}
		}

		// Open a new connection
		Properties props = new Properties();
		if (connectionName != null) {
			props.put("user", connectionName);
		}
		if (connectionPassword != null) {
			props.put("password", connectionPassword);
		}

		if (desCon == null) {
			desCon = driver.connect(desConnectionURL, props);
			desCon.setAutoCommit(false);
		}

		return;
	}

	/**
	 * Release our use of this connection so that it can be recycled.
	 */
	protected void release() {
		; // NO-OP since we are not pooling anything
	}

	/**
	 * Close the specified database connection.
	 */
	protected void close() {

		// Do nothing if the database connection is already closed
		if (desCon == null) {
			return;
		}

		// Close our prepared statements (if any)
		try {
			preparedUserId.close();
		} catch (Throwable f) {
			;
		}
		try {
			preparedRoles.close();
		} catch (Throwable f) {
			;
		}

		// Close this database connection, and log any errors
		try {
			if (desCon != null) {
				desCon.close();
			}
		} catch (SQLException e) {
			log(sm.getString("jdbcRealm.close"), e); // Just log it here
		}

		// Release resources associated with the closed connection
		desCon = null;
		preparedUserId = null;
		preparedRoles = null;
	}

	/**
	 * 
	 * Prepare for active use of the public methods of this Component.
	 * 
	 * @exception LifecycleException
	 *                if this component detects a fatal error that prevents it
	 *                from being started
	 */
	public void start() throws LifecycleException {
		// Validate that we can open our connection
		try {
			open();
		} catch (SQLException e) {
			throw new LifecycleException("Could not get database connections", e);
		}
		// Perform normal superclass initialization
		super.start();
	}

	/**
	 * Gracefully shut down active use of the public methods of this Component.
	 * 
	 * @exception LifecycleException
	 *                if this component detects a fatal error that needs to be
	 *                reported
	 */
	public void stop() throws LifecycleException {
		// Perform normal superclass finalization
		super.stop();
		// Close any open DB connection
		close();
	}

	/**
	 * Getter for property desConnectionURL.
	 * 
	 * @return Value of property desConnectionURL.
	 * 
	 */
	public java.lang.String getDesConnectionURL() {
		return desConnectionURL;
	}

	/**
	 * Setter for property desConnectionURL.
	 * 
	 * @param desConnectionURL
	 *            New value of property desConnectionURL.
	 * 
	 */
	public void setDesConnectionURL(java.lang.String desConnectionURL) {
		this.desConnectionURL = desConnectionURL;
	}

	/**
	 * Getter for property connectionPassword.
	 * 
	 * @return Value of property connectionPassword.
	 * 
	 */
	public java.lang.String getConnectionPassword() {
		return connectionPassword;
	}

	/**
	 * Setter for property connectionPassword.
	 * 
	 * @param connectionPassword
	 *            New value of property connectionPassword.
	 * 
	 */
	public void setConnectionPassword(java.lang.String connectionPassword) {
		this.connectionPassword = connectionPassword;
	}

	/**
	 * Getter for property simulaConnectionURL.
	 * 
	 * @return Value of property simulaConnectionURL.
	 * 
	 */
	public java.lang.String getSimulaConnectionURL() {
		return simulaConnectionURL;
	}

	/**
	 * Setter for property simulaConnectionURL.
	 * 
	 * @param simulaConnectionURL
	 *            New value of property simulaConnectionURL.
	 * 
	 */
	public void setSimulaConnectionURL(java.lang.String simulaConnectionURL) {
		this.simulaConnectionURL = simulaConnectionURL;
	}

	/**
	 * Getter for property connectionName.
	 * 
	 * @return Value of property connectionName.
	 * 
	 */
	public java.lang.String getConnectionName() {
		return connectionName;
	}

	/**
	 * Setter for property connectionName.
	 * 
	 * @param connectionName
	 *            New value of property connectionName.
	 * 
	 */
	public void setConnectionName(java.lang.String connectionName) {
		this.connectionName = connectionName;
	}

	/**
	 * Getter for property driverName.
	 * 
	 * @return Value of property driverName.
	 * 
	 */
	public java.lang.String getDriverName() {
		return driverName;
	}

	/**
	 * Setter for property driverName.
	 * 
	 * @param driverName
	 *            New value of property driverName.
	 * 
	 */
	public void setDriverName(java.lang.String driverName) {
		this.driverName = driverName;
	}

	/**
	 * Getter for property loginHttpURL.
	 * 
	 * @return Value of property loginHttpURL.
	 * 
	 */
	public String getLoginHttpURL() {
		return this.loginHttpURL;
	}

	/**
	 * Setter for property loginHttpURL.
	 * 
	 * @param loginHttpURL
	 *            New value of property loginHttpURL.
	 * 
	 */
	public void setLoginHttpURL(String loginHttpURL) {
		this.loginHttpURL = loginHttpURL;
	}

	/**
	 * Getter for property loginHttpNameParam.
	 * 
	 * @return Value of property loginHttpNameParam.
	 * 
	 */
	public String getLoginHttpNameParam() {
		return this.loginHttpNameParam;
	}

	/**
	 * Setter for property loginHttpNameParam.
	 * 
	 * @param loginHttpNameParam
	 *            New value of property loginHttpNameParam.
	 * 
	 */
	public void setLoginHttpNameParam(String loginHttpNameParam) {
		this.loginHttpNameParam = loginHttpNameParam;
	}

	/**
	 * Getter for property loginHttpPasswordParam.
	 * 
	 * @return Value of property loginHttpPasswordParam.
	 * 
	 */
	public String getLoginHttpPasswordParam() {
		return this.loginHttpPasswordParam;
	}

	/**
	 * Setter for property loginHttpPasswordParam.
	 * 
	 * @param loginHttpPasswordParam
	 *            New value of property loginHttpPasswordParam.
	 * 
	 */
	public void setLoginHttpPasswordParam(String loginHttpPasswordParam) {
		this.loginHttpPasswordParam = loginHttpPasswordParam;
	}

}