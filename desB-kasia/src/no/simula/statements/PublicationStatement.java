/*
 * Created on 27.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.simula.Publication;
import no.halogen.statements.ObjectStatementImpl;
import no.halogen.statements.StatementException;
import no.halogen.utils.sql.SqlHelper;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PublicationStatement extends ObjectStatementImpl {
  //private String DATASOURCE = "jdbc:mysql://localhost:3306/simula?user=root&autoReconnect=true";
  /**
   * Field DATASOURCE
   */
  private String DATASOURCE = SqlHelper.getSimulaJNDIName();

  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PublicationStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.Publication";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = null;

  /**
   * Field KEY
   */
  private final String KEY = "publication_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "publication_id, publication_title";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "publication";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = null;

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return (null);
  }

  /**
   * Returns name of the id column
   *
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return (UPDATE_VALUES);
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Publication publication = null;

    try {
      if (getDataBean() != null) {
        publication = (Publication) getDataBean().getClass().newInstance();
      } else {
        publication = new Publication();
      }
    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new publication data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new publication data object");
      e.printStackTrace();
    }

    publication.setId(rs.getString("publication_id"));
    publication.setTitle(rs.getString("publication_title"));

    return (publication);
  } /**
       * Gets connection to the Simulaweb database, then executes super.executeSelect()
       * 
       * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
       */
  public Object executeSelect(Integer entityId) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect(entityId);
  } /**
      	* DES shall not manage any content in Simulaweb, so this method is empy
      	* 
      	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
      	*/
  public boolean executeDelete() throws StatementException {
    return (false);
  } /**
      	* DES shall not manage any content in Simulaweb, so this method is empy
      	* 
      	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
      	*/
  public boolean executeDelete(Integer entityId) throws StatementException {
    return (false);
  } /**
      	* DES shall not manage any content in Simulaweb, so this method is empy
      	* 
      	* @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
      	*/
  public Integer executeInsert() throws StatementException {
    return (null);
  } /**
      	* Gets connection to the Simulaweb database, then executes super.executeSelect()
      	* 
      	* @see no.halogen.statements.ObjectStatement#executeSelect()
      	*/
  public List executeSelect() throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect();
  } /**
      * Gets connection to the Simulaweb database, then executes super.executeSelect()
      * 
      * @see no.halogen.statements.ObjectStatement#executeSelect(java.util.List)
      */
  public List executeSelect(List entityIds) throws StatementException {
    try {
      setConn(SqlHelper.getConnection(DATASOURCE));
    } catch (NamingException e) {
      log.error("executeSelect() - NamingException looking up context...", e);
      return null;
    } catch (SQLException e) {
      log.error("executeSelect() - SQLException looking up context...", e);
      return null;
    }

    return super.executeSelect(entityIds);
  } /**
      * DES shall not manage any content in Simulaweb, so this method is empy
      * 
      * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
      */
  public boolean executeUpdate() throws StatementException {
    return (false);
  } /**
      * DES shall not manage any content in Simulaweb, so this method is empy
      * 
      * @see no.halogen.statements.ObjectStatement#executeSelect(java.lang.Integer)
      */
  public boolean executeUpdate(Integer entityId) throws StatementException {
    return (false);
  } /* (non-Javadoc)
       * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
       */
  /**
  * Method getInsertValues
  * @return String
  */
 public String getInsertValues() {
    // TODO Auto-generated method stub
    return null;
  } /* (non-Javadoc)
     * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
     */
  /**
  * Method generateValues
  * @param pstmt PreparedStatement
  * @throws SQLException
  */
 public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    }
  }

}
