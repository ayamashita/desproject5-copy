package no.simula.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.des.DurationUnit;
import no.simula.des.Study;
import no.simula.des.StudyMaterial;
import no.simula.des.StudyType;

/** Utility class to find several of the domain objects by different criteria when
 * they are stored in a {@link java.util.List}.
 * @author Stian Eide
 */
public class FindUtils {
  
  /** Creates a new instance of FindUtils */
  public FindUtils() {}
  
  /** Returns the person with the specified e-mail address.
   * @param persons a list of persons
   * @param email an e-mail address
   * @return the person with the specified e-mail address, or <CODE>null</CODE> if no person
   * match
   */  
  public static Person getPersonByEmail(List persons, String email) {
    for(Iterator i = persons.iterator(); i.hasNext();) {
      Person person = (Person)i.next();
      if(person.getEmail().equals(email)) {
        return(person);
      }
    }
    return(null);
  }

  /** Returns all persons with the specified privilege.
   * @param persons a list of persons
   * @param priv a privilege
   * @return a list of persons that have the specified privilege
   */  
  public static List getPersonsByPrivilege(List persons, Privilege priv) {
    List privilegedPersons = new ArrayList(persons.size());
    for(Iterator i = persons.iterator(); i.hasNext();) {
      Person person = (Person)i.next();
      if(person.getPrivileges().contains(priv)) {
        privilegedPersons.add(person);
      }
    }
    return(privilegedPersons);
  }

  /** Returns a list of studies that contain the specified study material.
   * @param studies a list of studies
   * @param material a study material
   * @return a list of studies that contain the specified study material
   */  
  public static List getStudiesByStudyMaterial(List studies, StudyMaterial material) {
    if(studies == null || material == null) {
      return(null);
    }
    List containingStudies = new ArrayList(studies.size());
    for(Iterator i = studies.iterator(); i.hasNext();) {
      Study study = (Study)i.next();
      if(study.getStudyMaterial().contains(material)) {
        containingStudies.add(study);
      }
    }
    if(containingStudies.isEmpty()) {
      return(null);
    }
    else {
      return(containingStudies);
    }
  }
  
  /** Returns a list of studies that contain the specified study type.
   * @param studies a list of studies
   * @param type a study type
   * @return a list of studies that contain the specified study type
   */  
  public static List getStudiesByStudyType(List studies, StudyType type) {
    if(studies == null || type == null) {
      return(null);
    }
    List containingStudies = new ArrayList(studies.size());
    for(Iterator i = studies.iterator(); i.hasNext();) {
      Study study = (Study)i.next();
      if(study.getType().equals(type)) {
        containingStudies.add(study);
      }
    }
    if(containingStudies.isEmpty()) {
      return(null);
    }
    else {
      return(containingStudies);
    }
  }

  /** Returns a list of studies that contain the specified duration unit.
   * @param studies a list of studies
   * @param unit a duration unit
   * @return a list of studies that contain the specified duration unit
   */  
  public static List getStudiesByDurationUnit(List studies, DurationUnit unit) {
    if(studies == null || unit == null) {
      return(null);
    }
    List containingStudies = new ArrayList(studies.size());
    for(Iterator i = studies.iterator(); i.hasNext();) {
      Study study = (Study)i.next();
      if(study.getDurationUnit().equals(unit)) {
        containingStudies.add(study);
      }
    }
    if(containingStudies.isEmpty()) {
      return(null);
    }
    else {
      return(containingStudies);
    }
  }
}