package no.simula.wsclient;

import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import no.simula.Person;
import no.simula.Publication;

import org.apache.log4j.Logger;
import org.apache.struts.tiles.UrlController;

/**
 * Client for accessing simula webservice
 * 
 * @author kasia
 * 
 */
public class SimulaWSClient extends WSClient {

	public static SimulaWSClient getInstance() {
		if (instance == null) {
			instance = new SimulaWSClient();
		}
		return instance;
	}

	private static SimulaWSClient instance = null;

	private static Logger log = Logger.getLogger(SimulaWSClient.class);
	private String WS_URL = null;

	/**
	 * Reads standard configuration and configures the client
	 */
	public void configure() {
		WS_URL = System.getProperty("no.machina.simula.simula_ws_url");
		try {
			configure(WS_URL);
			log.info("WS configured : " + WS_URL);
		} catch (MalformedURLException mue) {
			log.error(mue.getMessage());
			WS_URL = null;
		}
	}

	/**
	 * Queries people WS with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object[] queryPeople(HashMap params) throws Exception {
		return queryObjectArrayMethod("getPeople", params);
	}

	/**
	 * Queries publication WS with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object[] queryPublication(HashMap params) throws Exception {
		return queryObjectArrayMethod("getPublications", params);
	}

	/**
	 * Searches for people with first name or family name like the given search
	 * string
	 * 
	 * @param searchString
	 * @return The list of ids
	 */
	public List getPeopleIdsByName(String searchString) throws Exception {

		// prepare first search
		HashMap params1 = new HashMap();
		params1.put("firstname", searchString);

		// prepare second search
		HashMap params2 = new HashMap();
		params2.put("lastname", searchString);

		Object[] resList1 = queryPeople(params1);
		Object[] resList2 = queryPeople(params2);

		HashSet ids = new HashSet();

		// read ids from first result
		for (int i = 0; i < resList1.length; i++) {
			ids.add(((HashMap) resList1[i]).get("id"));
		}

		// read ids from second result
		for (int i = 0; i < resList2.length; i++) {
			ids.add(((HashMap) resList2[i]).get("id"));
		}

		return new ArrayList(ids);
	}

	/**
	 * Searches for people with the given id string
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Person getPeopleById(String id) throws Exception {
		// TODO introduce WebServiceException
		HashMap params = new HashMap();
		params.put("id", id);

		Object[] resTable = queryPeople(params);

		Person people = null;
		for (int i = 0; i < resTable.length; i++) {
			people = createPeople((HashMap) resTable[i]);
		}

		return people;
	}

	/**
	 * Queries web service for a list of people sorted by name
	 * 
	 * @return A list of hashmaps representing p object containing all people
	 *         data delivered by web service
	 * @throws Exception
	 */
	public List getPeopleSortedByName() throws Exception {
		HashMap params = new HashMap();
		params.put("sort_on", "lastname");
		Object[] result = queryPeople(params);

		List people = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			people.add((HashMap) result[i]);
		}
		return people;
	}

	/**
	 * Creates People object with the values delivered by web service
	 * 
	 * @param peopleMap
	 *            A HashMap delivered by web service
	 * @return
	 */
	protected Person createPeople(HashMap peopleMap) {
		Person people = new Person();

		people.setId((String) peopleMap.get("id"));
		people.setFirstName((String) peopleMap.get("firstname"));
		people.setFamilyName((String) peopleMap.get("lastname"));
		people.setEmail((String) peopleMap.get("email"));
		people.setUrl((String) peopleMap.get("url"));

		return people;
	}

	/**
	 * Creates Publication object with the values delivered by web service
	 * 
	 * @param peopleMap
	 *            A HashMap delivered by web service
	 * @return
	 */
	protected Publication createPublication(HashMap pubMap) {
		Publication publication = new Publication();
		publication.setId((String) pubMap.get("id"));
		String title = ((String) pubMap.get("title"));
		publication.setTitle( title != null? URLDecoder.decode(title):null);
		publication.setUrl((String) pubMap.get("url"));

		Object[] o = (Object[]) pubMap.get("authors");

		if (o != null && o.length > 0) {
			HashMap map = (HashMap) o[0];

			String full = getFullName(map);
			StringBuffer authors = new StringBuffer(full);
			
			for (int i = 1; i < o.length; i++) {
				map = (HashMap) o[i];
				full = getFullName(map);
				authors.append(", ").append(full);
			}
			
			publication.setAuthors(authors.toString());
		} else {
			publication.setAuthors("");
		}

		return publication;
	}

	private String getFullName(HashMap map) {
		String first = (String) map.get("firstname");
		String middle = (String) map.get("middlename");
		String last = (String) map.get("lastname");

		StringBuffer full = new StringBuffer();
		if (first != null && first.length() > 0) {
			full.append(URLDecoder.decode(first)).append(" ");	
		}
		if (middle != null && middle.length() > 0) {
			full.append(URLDecoder.decode(middle)).append(" ");
		}
		if (last != null && last.length() > 0) {
			full.append(URLDecoder.decode(last));
		}
		
		return full.toString();
	}

	/**
	 * Returns all possible publications
	 * 
	 * @return A list of Publication objects
	 * @throws Exception
	 */
	public List getAllPublications() throws Exception {
		Object[] result = queryPublication(new HashMap());

		List publications = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			publications.add(createPublication((HashMap) result[i]));
		}
		return publications;
	}
	
	/**
	 * Queries web service for publication data for the given ids
	 * 
	 * @param pubIds
	 *            A list of id strings
	 * @return A list of publication objects
	 * @throws Exception
	 */
	public List getPublicationsByIds(List pubIds) throws Exception {
		Iterator respIter = pubIds.iterator();

		List publications = new ArrayList();
		while (respIter.hasNext()) {
			Publication publication = getPublicationById((String) respIter.next());
			if (publication != null) {
				publications.add(publication);
			}
		}

		return publications;
	}

	/**
	 * Queries for a single publication with the given id
	 * 
	 * @param pubId
	 *            An id string
	 * @return A single Publication object or null, when such publication does
	 *         not exist
	 * @throws Exception
	 */
	public Publication getPublicationById(String pubId) throws Exception {
		HashMap params = new HashMap();
		params.put("id", pubId);

		Object[] pubTable = queryPublication(params);

		if (pubTable == null || pubTable.length == 0) {
			return null;
		}

		return createPublication((HashMap) pubTable[0]);
	}

	public List getAllPeople() throws Exception {
		HashMap params = new HashMap();
		params.put("sort_on", "lastname");
		Object[] result = queryPeople(params);

		List people = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			people.add(createPeople((HashMap) result[i]));
		}
		return people;

	}

	public Person getPeopleByEmail(String email) throws Exception {
		// TODO introduce WebServiceException
		HashMap params = new HashMap();
		params.put("email", email);

		Object[] resTable = queryPeople(params);

		Person people = null;
		for (int i = 0; i < resTable.length; i++) {
			people = createPeople((HashMap) resTable[i]);
		}

		return people;
	}

	public List getPeopleByIds(List entityIds) throws Exception {
		List people = new ArrayList();
		for (int i = 0; i < entityIds.size(); i++) {
			Person p = getPeopleById((String) entityIds.get(i));
			if (p != null && p.getId() != null && p.getId().length() != 0) {
				people.add(p);
			}
		}
		return people;
	}
	
	public List getPublicationIdsByTitle(String searchText) throws Exception {
		HashMap params = new HashMap();
		params.put("title", searchText);
		Object[] result = queryPublication(params);

		List ids = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			ids.add(((HashMap) result[i]).get("id"));
		}
		return ids;
	}

	public List getPublicationIdsByAuthor(String searchText) throws Exception {
		HashMap params = new HashMap();
		params.put("authors", searchText);
		Object[] result = queryPublication(params);

		List ids = new ArrayList();
		for (int i = 0; i < result.length; i++) {
			ids.add(((HashMap) result[i]).get("id"));
		}
		return ids;
	}

}
