package no.simula.wsclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 * Base class for all clients accessing web service methods
 * @author kasia
 *
 */
public abstract class WSClient {

	protected XmlRpcClientConfigImpl rpcConfig;
	protected XmlRpcClient rcpClient;

	protected WSClient() {
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
		configure();
	}

	/**
	 * Configures the client to work with web service with the given url.
	 * @param webServiceUrl
	 * @throws MalformedURLException
	 */
	public void configure(String webServiceUrl) throws MalformedURLException {
		this.rpcConfig.setServerURL(new URL(webServiceUrl));
		this.rcpClient.setConfig(rpcConfig);
	}
	
	/**
	 * Standard configuration method used in default constructor. Client should be able
	 * to query web service after running this method.
	 */
	public abstract void configure();

	/**
	 * Queries WS method delivering an array of Objects with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object[] queryObjectArrayMethod(String method, HashMap params) throws Exception {
		Object[] methodParams = new Object[] { params };

		try {
			Object[] res = (Object[]) this.rcpClient.execute(method, methodParams);
			return res;
		} catch (XmlRpcException e) {
			System.err.println("Error occured when accessing check credentials");
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * Queries WS method delivering Boolean objects with given params
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Boolean queryBooleanMethod(String method, HashMap params) throws Exception {
		Object[] methodParams = new Object[] { params };

		try {
			Boolean res = (Boolean) this.rcpClient.execute(method, methodParams);
			return res;
		} catch (XmlRpcException e) {
			System.err.println("Error occured when accessing check credentials");
			e.printStackTrace();
			throw e;
		}
	}
}