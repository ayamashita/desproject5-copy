<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
>

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>

        <fo:simple-page-master
          master-name="page" 
          page-height="29.7cm"
          page-width="21cm"
          margin-top="1.5cm"
          margin-bottom="2cm"
          margin-left="1.5cm"
          margin-right="1.5cm"
        >
          <fo:region-before extent="1cm"/>
          <fo:region-after extent="1cm"/>
          <fo:region-body margin-top="1.1cm" margin-bottom="1.1cm"/>
        </fo:simple-page-master>

      </fo:layout-master-set>

      <fo:page-sequence master-reference="page">

        <fo:static-content flow-name="xsl-region-before">
          <fo:block
            font-family="Arial, Helvetica"
            font-size="6pt"
            text-align="center"
          >
            <xsl:text>[ simula.research laboratory ]  study details report</xsl:text>
          </fo:block>
        </fo:static-content>

        <fo:static-content flow-name="xsl-region-after">
          <fo:block
            font-family="Arial, Helvetica"
            font-size="6pt"
            text-align="center"
          >
            Page <fo:page-number />
          </fo:block>
	</fo:static-content>
        
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates/>
        </fo:flow>
       </fo:page-sequence>
      
  </fo:root>

  </xsl:template>
  
  <xsl:template match="study">
    <fo:block font-size="24" font-weight="bold"><xsl:value-of select="name"/></fo:block>
    <fo:table table-layout="fixed">
      <fo:table-column column-width="8cm"/>
      <fo:table-column column-width="9cm"/>
      <fo:table-body>
          <xsl:apply-templates select="*[not(name() = 'name')]"/>
      </fo:table-body>
    </fo:table>  
  </xsl:template>
  
  <xsl:template match="study/*">
    <fo:table-row>
      <fo:table-cell font-weight="bold">
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
        <fo:block margin="2pt" padding="2pt">
          <xsl:value-of select="@display-name"/>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
        <fo:block margin="2pt" padding="2pt">
          <xsl:apply-templates/>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </xsl:template>

  <xsl:template match="study/duration">
    <fo:table-row>
      <fo:table-cell font-weight="bold">
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
        <fo:block margin="2pt" padding="2pt">
          <xsl:value-of select="@display-name"/>
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
        <fo:block margin="2pt" padding="2pt">
          <xsl:value-of select="."/>
          <xsl:value-of select="@unit"/>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </xsl:template>
  
  <xsl:template match="study/responsibles/person">
    <xsl:value-of select="first-name"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="family-name"/>
  </xsl:template>
  
  <xsl:template match="study/*/*">
    <xsl:for-each select="*">
      <fo:block><xsl:apply-templates/></fo:block>
    </xsl:for-each>
  </xsl:template>
    
</xsl:stylesheet> 
